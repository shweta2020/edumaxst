package com.nusys.studentedumax;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.android.material.navigation.NavigationView;
/*import com.nusys.studentedumax.activity.ChangePassword;
import com.nusys.studentedumax.activity.Doubts;
import com.nusys.studentedumax.activity.Followers;
import com.nusys.studentedumax.activity.Following;
import com.nusys.studentedumax.activity.HomeActivity;
import com.nusys.studentedumax.activity.LoginActivity;
import com.nusys.studentedumax.activity.ProfileActivity;*/
import com.nusys.studentedumax.Group.GroupActivity;
import com.nusys.studentedumax.commonModules.SharedPreference_main;
import com.nusys.studentedumax.model.ProfilePostModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.nusys.studentedumax.Constants.BASE_URL;
import static com.nusys.studentedumax.Constants.PROFILE;


public class BaseActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    DrawerLayout drawerLayout;
    Toolbar toolbar;

    public FrameLayout frameLayout;
    NavigationView navigationView;
    SharedPreference_main sharedPreference_main;
    ImageView userLoginImg;
    TextView userName, userEmail;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_navigation);
        init();
    }

    private void init() {
        toolbar = findViewById(R.id.toolbar);

        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        setSupportActionBar(toolbar);

        sharedPreference_main = SharedPreference_main.getInstance(getApplicationContext());
        frameLayout = (FrameLayout) findViewById(R.id.content_frame);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.setDrawerListener(toggle);
        toggle.syncState();
        navigationView = (NavigationView) findViewById(R.id.nav_view);


        View header = navigationView.getHeaderView(0);
        userLoginImg = header.findViewById(R.id.userLoginImg);
        userName = header.findViewById(R.id.userName);
        userEmail = header.findViewById(R.id.userEmail);
        navigationView.setNavigationItemSelectedListener(this);

        loadProfileData();

        userName.setText(sharedPreference_main.getUsername());
        userEmail.setText(sharedPreference_main.getUserEmail());
    }
    @Override
    protected void onStart() {
        super.onStart();
        loadProfileData();
    }
    private void loadProfileData() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                final JSONObject jsonBody = new JSONObject();
                try {
                    jsonBody.put("id", sharedPreference_main.getUserId());
                    jsonBody.put("page", "1");
                    jsonBody.put("user_type", sharedPreference_main.getUserType());

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //  String URL = "http://13.233.162.24/nusys(UAT)/api/student/profile.php";
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL+PROFILE, jsonBody,
                        new com.android.volley.Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                Log.e("response", response.toString());
                                try {

                                    Boolean status = response.getBoolean("status");

                                    if (status) {
                                        final List<ProfilePostModel> ab = new ArrayList<>();
                                        JSONObject jsonObj = response.getJSONObject("data");

                                        Glide.with(getApplicationContext())
                                                .load(jsonObj.getString("image"))
                                                .placeholder(R.drawable.user_icon)
                                                .into(userLoginImg);


                                    } else {
                                        //Toast.makeText(Home.this, "Something is wrong try again later", Toast.LENGTH_SHORT).show();

                                    }


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                // Toast.makeText(SignUpStepTwo.this, "" + response.toString(), Toast.LENGTH_SHORT).show();

                            }
                        }, new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("Content-Type", "application/json; charset=UTF-8");
                        params.put("Authorization", "Bearer " + sharedPreference_main.getToken());
                        return params;
                    }
                };
                requestQueue.add(jsonObjectRequest);

            }
        }, 2000);

    }
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        if (item.isChecked()) {
            drawerLayout.closeDrawer(GravityCompat.START);
            return true;
        }
        switch (item.getItemId()) {
            case R.id.nav_Home: {

                startActivity(new Intent(getApplicationContext(), Home.class));
                finish();
                break;
            }
            case R.id.nav_profile: {

                Intent intent = new Intent(getApplicationContext(), Profile.class);
                startActivity(intent);
                break;
            }
            case R.id.nav_Following: {

                startActivity(new Intent(getApplicationContext(), Following.class));
                break;
            }

            case R.id.nav_Add_Group: {

                startActivity(new Intent(getApplicationContext(), GroupActivity.class));
                break;
            }
            case R.id.nav_Batch: {

                startActivity(new Intent(getApplicationContext(), Batch.class));
                break;
            }
            case R.id.nav_ChangePassword: {

                Intent intent = new Intent(getApplicationContext(), ChangePassword.class);
                startActivity(intent);

                break;
            }

            case R.id.nav_LogOut: {
                sharedPreference_main.removePreference();
                sharedPreference_main.setIs_LoggedIn(false);
                startActivity(new Intent(getApplicationContext(), Login.class));
                finish();

                break;
            }
            default:
                drawerLayout.closeDrawer(GravityCompat.START);
        }
        return true;
    }

    @Override
    public void onBackPressed() {

        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
}
