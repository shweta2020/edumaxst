package com.nusys.studentedumax;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.nusys.studentedumax.adapter.AdapterTeacher;
import com.nusys.studentedumax.commonModules.SharedPreference_main;
import com.nusys.studentedumax.model.TeacherModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.nusys.studentedumax.Constants.BASE_URL;
import static com.nusys.studentedumax.Constants.BASE_URL_TEACHER;
import static com.nusys.studentedumax.Constants.CAMERA_REQUEST_CODE;
import static com.nusys.studentedumax.Constants.GALLERY_REQUEST_CODE;
import static com.nusys.studentedumax.Constants.STUDENT_DOUBT_ADD;
import static com.nusys.studentedumax.Constants.STUDENT_EXAM;
import static com.nusys.studentedumax.Constants.STUDENT_SUB;
import static com.nusys.studentedumax.Constants.STUDENT_UNIT;

public class DoubtsAsk extends AppCompatActivity {
    LinearLayout back_acti, llBaselayout;
    Spinner sp_sub, sp_unit, spin, sp_exam;
    EditText et_doubt, et_remark;
    ImageView choose_file;
    Button btn_choose_file, btn_send;
    private String userImageBase64;
    String[] priority_array = {"High", "Medium", "Low"};
    SharedPreference_main sharedPreference_main;
    private ArrayList<String> spin_exam_list = new ArrayList<String>();
    private ArrayList<String> spin_exam_list_id = new ArrayList<String>();

    private ArrayList<String> spin_sub_list = new ArrayList<String>();
    private ArrayList<String> spin_sub_list_id = new ArrayList<String>();

    private ArrayList<String> spin_unit_list = new ArrayList<String>();
    private ArrayList<String> spin_unit_list_id = new ArrayList<String>();

    String id_exam;
    String id_sub1;
    String id_unit;
    Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doubts_ask);

        llBaselayout = (LinearLayout) findViewById(R.id.ll_baselayout);

        initialization();
        actions();

    }

    private void initialization() {
        sharedPreference_main = SharedPreference_main.getInstance(this);
        back_acti = findViewById(R.id.back_activity);
        sp_sub = findViewById(R.id.sp_sub_doubt);
        sp_exam = findViewById(R.id.sp_exam_doubt);
        sp_unit = findViewById(R.id.sp_unit_doubt);
        spin = findViewById(R.id.sp_priority);
        et_doubt = findViewById(R.id.et_doubt_detail);
        et_remark = findViewById(R.id.et_remark_doubt);
        choose_file = findViewById(R.id.choose_file_doubt);
        btn_choose_file = findViewById(R.id.btn_choose_file_doubt);
        btn_send = findViewById(R.id.btn_send_doubt);
        context = DoubtsAsk.this;

        spin_exam_list.add("Select Exam");
        spin_exam_list_id.add("-1");

    }

    private void actions() {


        sp_exam.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                id_exam = spin_exam_list_id.get(position);

                hitSubjectSpinApi(spin_exam_list_id.get(position));

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        sp_sub.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(context, ""+parent.getSelectedItemId(), Toast.LENGTH_SHORT).show();
                 id_sub1 = spin_sub_list_id.get(position);
                //   Toast.makeText(context, "Subjectclick"+id_sub1, Toast.LENGTH_SHORT).show();
                hitUnitSpinApi(id_exam, id_sub1);


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sp_unit.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, spin_unit_list));

        sp_unit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(context, ""+parent.getSelectedItemId(), Toast.LENGTH_SHORT).show();

                 id_unit = spin_unit_list_id.get(position);
                //  Toast.makeText(context, "unit"+id_unit, Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(et_doubt.getText().toString())) {
                    et_doubt.setError("field can't be empty");
                } else if (TextUtils.isEmpty(et_remark.getText().toString())) {
                    et_remark.setError("field can't be empty");
                } else {
                    hitAddDoubtApi();
                }
            }
        });

        hitExamSpinApi();
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_dropdown_item_1line, priority_array);
        spin.setAdapter(arrayAdapter);
        spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String value = spin.getItemAtPosition(position).toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        back_acti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(DoubtsAsk.this, Home.class);
                startActivity(i);
                finish();

            }
        });

        btn_choose_file.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                openChooser(CAMERA_REQUEST_CODE, GALLERY_REQUEST_CODE);

            }
        });
    }

    private void hitAddDoubtApi() {


        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        JSONObject jsonBody = new JSONObject();
        try {
            //input your API parameters
            jsonBody.put("student_id", sharedPreference_main.getUserId());
          //  jsonBody.put("exam_id", sp_exam.getSelectedItem());
            jsonBody.put("exam_id", id_exam);
           // jsonBody.put("subject_id", sp_sub.getSelectedItem());
            jsonBody.put("subject_id", id_sub1);
           // jsonBody.put("unit_id", sp_unit.getSelectedItem());
            jsonBody.put("unit_id",id_unit);
            jsonBody.put("upload_type", "image");
            jsonBody.put("description", et_doubt.getText().toString());
            jsonBody.put("priority", spin.getSelectedItem());
            jsonBody.put("remark", et_remark.getText().toString());
            jsonBody.put("filename", userImageBase64);

            //Log.e("rrrr",""+sharedPreference_main.getUserId()+ sp_exam.getSelectedItem()+sp_sub.getSelectedItem()+sp_unit.getSelectedItem()+et_doubt.getText().toString()+spin.getSelectedItem()+et_remark.getText().toString()+userImageBase64);


        } catch (JSONException e) {
            e.printStackTrace();
        }
        // Enter the correct url for your api service site
        // String url = getResources().getString(R.string.url);
      //  String URL = "http://13.233.162.24/nusys(UAT)/api/student/add_doubt.php";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL+STUDENT_DOUBT_ADD, jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            // Toast.makeText(ChangePassword.this, "true", Toast.LENGTH_SHORT).show();

                            Boolean status = response.getBoolean("status");
                            if (status) {
                                Toast.makeText(context, "" + response.getString("message"), Toast.LENGTH_SHORT).show();
                                finish();
                            } else {
                                Toast.makeText(context, "" + response.getString("message"), Toast.LENGTH_SHORT).show();
                                finish();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        // Toast.makeText(ChangePassword.this, ""+response.toString(), Toast.LENGTH_SHORT).show();
                        //resultTextView.setText("String Response : "+ response.toString());
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //  resultTextView.setText("Error getting response");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json; charset=UTF-8");
                params.put("Authorization", "Bearer " + sharedPreference_main.getToken());
                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);


    }


    private void hitExamSpinApi() {

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        JSONObject jsonBody = new JSONObject();
        try {
            //input your API parameters
            jsonBody.put("student_id", sharedPreference_main.getUserId());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        // Enter the correct url for your api service site
        // String url = getResources().getString(R.string.url);
      //  String URL = "http://13.233.162.24/nusys(UAT)/api/student/student_exam.php";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL+STUDENT_EXAM, jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            // Toast.makeText(ChangePassword.this, "true", Toast.LENGTH_SHORT).show();

                            Boolean status = response.getBoolean("status");
                            if (status) {
                                JSONArray jsonArray = response.getJSONArray("data");
                                // Toast.makeText(context, "ccccc", Toast.LENGTH_SHORT).show();
                                for (int i = 0; i < jsonArray.length(); i++) {

                                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                                    String exam_name = jsonObject.getString("exam_name");
                                    String exam_id = jsonObject.getString("exam_id");
                                    //  Toast.makeText(context, "" + jsonObject.getString("exam_name"), Toast.LENGTH_SHORT).show();
                                    spin_exam_list.add(exam_name);
                                    spin_exam_list_id.add(exam_id);

                                }
                                sp_exam.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, spin_exam_list));

                            } else {
                                Toast.makeText(context, "" + response.getString("message"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {

                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });


        requestQueue.add(jsonObjectRequest);

    }


    private void hitSubjectSpinApi(final String id_exam) {

        spin_sub_list.clear();
        spin_sub_list_id.clear();
        spin_sub_list.add("Select Subject");
        spin_sub_list_id.add("-1");
        // Toast.makeText(context, "hdjhdhfj", Toast.LENGTH_SHORT).show();
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        JSONObject jsonBody = new JSONObject();
        try {
            //input your API parameters
            jsonBody.put("category_id", id_exam);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        // Enter the correct url for your api service site
        // String url = getResources().getString(R.string.url);
        //String URL = "http://13.233.162.24/nusys(UAT)/api/teacher/select_subject.php";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL_TEACHER+STUDENT_SUB, jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            //Toast.makeText(context, "true", Toast.LENGTH_SHORT).show();

                            Boolean status = response.getBoolean("status");
                            if (status) {
                                JSONArray jsonArray = response.getJSONArray("data");
                                //  Toast.makeText(context, "ccccc", Toast.LENGTH_SHORT).show();
                                for (int i = 0; i < jsonArray.length(); i++) {

                                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                                    String sub_id = jsonObject.getString("sub_id");
                                    String name = jsonObject.getString("name");
                                    // Toast.makeText(context, "fjhgjhg" + jsonObject.getString("name"), Toast.LENGTH_SHORT).show();
                                    spin_sub_list.add(name);
                                    spin_sub_list_id.add(sub_id);

                                }
                                sp_sub.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, spin_sub_list));

                            } else {


                                sp_sub.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, spin_sub_list));

//                                Toast.makeText(context, "" + response.getString("message"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                           /* spin_sub_list.add("Select Subject");
                            spin_sub_list_id.add("-1");*/
                            e.printStackTrace();
                        }
                        // Toast.makeText(ChangePassword.this, ""+response.toString(), Toast.LENGTH_SHORT).show();
                        //resultTextView.setText("String Response : "+ response.toString());
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                sp_sub.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, spin_sub_list));

                //  resultTextView.setText("Error getting response");
            }
        });


        requestQueue.add(jsonObjectRequest);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void hitUnitSpinApi(String id_exam, String id_sub) {
        spin_unit_list.clear();
        spin_unit_list_id.clear();
        spin_unit_list.add("Select Unit");
        spin_unit_list_id.add("-1");

        // Toast.makeText(context, "hdjhdhfj", Toast.LENGTH_SHORT).show();
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        JSONObject jsonBody = new JSONObject();
        try {
            //input your API parameters
            jsonBody.put("exam_id", id_exam);
            jsonBody.put("subject_id", id_sub);

            //Toast.makeText(context, "exam" + id_exam, Toast.LENGTH_SHORT).show();
            // Toast.makeText(context, "subject" + id_sub, Toast.LENGTH_SHORT).show();

        } catch (JSONException e) {
            e.printStackTrace();
        }
        // Enter the correct url for your api service site
        // String url = getResources().getString(R.string.url);
      //  String URL = "http://13.233.162.24/nusys(UAT)/api/student/select_unit.php";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL+STUDENT_UNIT, jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            //Toast.makeText(context, "true", Toast.LENGTH_SHORT).show();

                            Boolean status = response.getBoolean("status");
                            if (status) {
                                JSONArray jsonArray = response.getJSONArray("data");
                                //  Toast.makeText(context, "ccccc", Toast.LENGTH_SHORT).show();
                                for (int i = 0; i < jsonArray.length(); i++) {

                                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                                    String unit_id = jsonObject.getString("unit_id");
                                    String unit_name = jsonObject.getString("unit_name");
                                    // Toast.makeText(context, "fjhgjhg" + jsonObject.getString("name"), Toast.LENGTH_SHORT).show();
                                    spin_unit_list.add(unit_name);
                                    spin_unit_list_id.add(unit_id);

                                }
                                sp_unit.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, spin_unit_list));
                            } else {


                                sp_unit.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, spin_unit_list));
                                //Toast.makeText(context, "" + response.getString("message"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        // Toast.makeText(ChangePassword.this, ""+response.toString(), Toast.LENGTH_SHORT).show();
                        //resultTextView.setText("String Response : "+ response.toString());
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                sp_unit.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, spin_unit_list));
                //  resultTextView.setText("Error getting response");
            }
        });


        requestQueue.add(jsonObjectRequest);

    }


    // for open camera and gallery
    private void chooseImageFromGallery(final int code) {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(pickPhoto, code);
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();

    }

    private void takeCameraImage(final int code) {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
//                            fileName = System.currentTimeMillis() + ".jpg";

                            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(cameraIntent, code);
//                            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, getCacheImagePath(fileName));
//                            if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
//                                startActivityForResult(takePictureIntent, code);
//                            }
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {

            case CAMERA_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    Bitmap photo = (Bitmap) data.getExtras().get("data");
                    choose_file.setImageBitmap(photo);
                    getEncoded64ImageStringFromBitmap(photo);
                    userImageBase64 = getEncoded64ImageStringFromBitmap(photo);
                    Log.e("excam", userImageBase64);
//                    tv_userImage.setText(getCacheImagePath(fileName).toString());

//                  getCacheImagePath(fileName);
                }
                break;
            case GALLERY_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    Uri imageUri = data.getData();
                    choose_file.setImageURI(imageUri);
                    Bitmap bitmap;
                    try {
                        bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(imageUri));
                        Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, 500, 500, false);
                        userImageBase64 = getEncoded64ImageStringFromBitmap(resizedBitmap);
                        Log.e("exp", userImageBase64);


                    } catch (FileNotFoundException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
//                    tv_userImage.setText(imageUri.getPath());

                break;
        }
    }

    public String getEncoded64ImageStringFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteFormat = stream.toByteArray();
        String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);
        Log.e("", imgString);
        return imgString;
    }

    private void openChooser(final int code1, final int code2) {

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.chooser);
        TextView select_camera = dialog.findViewById(R.id.camera);
        TextView select_gallery = dialog.findViewById(R.id.gallery);
        dialog.show();
        select_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takeCameraImage(code1);
                dialog.dismiss();
            }
        });
        select_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseImageFromGallery(code2);
                dialog.dismiss();


            }
        });
    }
}
