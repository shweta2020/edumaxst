package com.nusys.studentedumax;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.nusys.studentedumax.model.ResponeForgotPassword;
import com.nusys.studentedumax.retrofit.ApiClient;
import com.nusys.studentedumax.retrofit.ServiceInterface;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPassword extends AppCompatActivity {
    Context context;
    EditText et_mobileNumber;
    Button bt_generateOtp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        setTitle("Forgot Password");

        initial();
        listener();
    }

    private void initial() {
        context =ForgotPassword.this;
        et_mobileNumber = findViewById(R.id.et_mobile_num);
        bt_generateOtp = findViewById(R.id.bt_send_otp);
    }

    private void listener() {
        bt_generateOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(et_mobileNumber.getText().toString())) {

                    et_mobileNumber.setError("Please enter mobile number");
                } else {
                    forgot_password();
                   /* startActivity(new Intent(ForgotPassword.this, Otp_activity.class));
                    finish();*/
                }
            }
        });
    }

    /* method for forgot password
     *created date: 03-03-2020
     */
    private void forgot_password() {

        HashMap<String, String> map = new HashMap<>();

        map.put("mobile", et_mobileNumber.getText().toString());


        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<ResponeForgotPassword> call = serviceInterface.forgot_pwd(map);
        call.enqueue(new Callback<ResponeForgotPassword>() {
            @Override
            public void onResponse(Call<ResponeForgotPassword> call, Response<ResponeForgotPassword> response) {
                if (response.isSuccessful()) {
                    ResponeForgotPassword bean = response.body();
                    Boolean status= bean.getStatus();
                    if (status) {


                        Toast.makeText(context, "" + bean.getMessage(), Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(context, Login.class));
                        finish();

                    } else {
                        Toast.makeText(context, "" + bean.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(context, "Something is wrong", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponeForgotPassword> call, Throwable t) {
                Toast.makeText(context, "Something is wrongs", Toast.LENGTH_SHORT).show();

            }
        });

    }


}
