package com.nusys.studentedumax;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.nusys.studentedumax.adapter.AdapterShowComment;
import com.nusys.studentedumax.commonModules.ExpandableTextView;
import com.nusys.studentedumax.commonModules.SharedPreference_main;
import com.nusys.studentedumax.model.ShowCommentModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static com.nusys.studentedumax.Constants.BASE_URL_TEACHER;
import static com.nusys.studentedumax.Constants.MAKE_LIKE;
import static com.nusys.studentedumax.Constants.SINGLE_POST;

public class ViewIndividualPostDetails extends AppCompatActivity {
    TextView user_name, post_time, tv_heading, tv_likes, tv_comment, tv_viewmore, tvToolbarHead, tvPostType;
    ImageView user_profile, feed_image, img_like, img_unlike;
    ExpandableTextView expandableText;
    LinearLayout linear_options, llBackActivity;
    SharedPreference_main sharedPreference_main;
    Context context;
    String i_id, post_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_individual_post_details);

        sharedPreference_main = SharedPreference_main.getInstance(context);
        user_profile = findViewById(R.id.uploader_img);
        user_name = findViewById(R.id.user_name_idPost);
        post_time = findViewById(R.id.post_time_idpost);
        tv_heading = findViewById(R.id.tv_heading_idpost);
        feed_image = findViewById(R.id.feed_image_idpost);
        expandableText = findViewById(R.id.tv_content_idpost);
        tv_viewmore = findViewById(R.id.tv_viewmore_idpost);
        tv_likes = findViewById(R.id.tv_likes_idpost);
        tv_comment = findViewById(R.id.tv_comment_idpost);
        img_like = findViewById(R.id.img_like_idpost);
        img_unlike = findViewById(R.id.img_unlike_idpost);
        tvToolbarHead = findViewById(R.id.tv_toolbar_head);
        linear_options = findViewById(R.id.linear_options_idpost);
        llBackActivity = findViewById(R.id.ll_back_activity);
        tvPostType = findViewById(R.id.tv_post_type);
        context = ViewIndividualPostDetails.this;

        llBackActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        feed_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in= new Intent(context, ViewImage.class);
                in.putExtra("postIds",getIntent().getStringExtra("postId"));
                startActivity(in);
            }
        });

        Intent intent = getIntent();
        i_id = intent.getStringExtra("postId");
        // user_name.setText(i_name);
        viewCommentApi();




    }

    @Override
    protected void onStart() {
        super.onStart();
        viewCommentApi();
    }

    private void viewCommentApi() {

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        final JSONObject jsonBody = new JSONObject();
        try {

            jsonBody.put("id", i_id);
            jsonBody.put("user_id", sharedPreference_main.getUserId());
            jsonBody.put("user_type", sharedPreference_main.getUserType());
           // Toast.makeText(context, ""+i_id, Toast.LENGTH_SHORT).show();

        } catch (JSONException e) {
            e.printStackTrace();
        }
        // Enter the correct url for your api service site
        // String url = getResources().getString(R.string.url);
      //  String URL = "http://13.233.162.24/nusys(UAT)/api/teacher/single_post.php";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL_TEACHER+SINGLE_POST, jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Boolean status = response.getBoolean("status");
                            //Toast.makeText(Profile.this, "", Toast.LENGTH_SHORT).show();
                            // Toast.makeText(Profile.this, "abcd"+status, Toast.LENGTH_SHORT).show();
                            if (status) {
                                JSONArray jsonArray = response.getJSONArray("data");
                                for (int i = 0; i < jsonArray.length(); i++) {

                                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                                    user_name.setText(jsonObject.getString("user_name"));
                                    tv_heading.setText(jsonObject.getString("heading"));
                                    expandableText.setText(jsonObject.getString("content"));
                                    post_time.setText(jsonObject.getString("time"));
                                    tv_likes.setText(jsonObject.getString("likes"));
                                    //  tv_likes.setText(jsonObject.getString("like_status"));
                                    tv_comment.setText(jsonObject.getString("total_comments") + " COMMENT");
                                    tvToolbarHead.setText(jsonObject.getString("user_name"));
                                    Glide.with(getApplicationContext())
                                            .load(jsonObject.getString("user_profile"))
                                            .placeholder(R.drawable.user_icon)
                                            .into(user_profile);
                                    Glide.with(getApplicationContext())
                                            .load(jsonObject.getString("filename"))
                                            .into(feed_image);
                                    post_id = jsonObject.getString("post_id");
                                    if (Objects.equals(jsonObject.getString("like_status"), "false")) {
                                        //Toast.makeText(context, "", Toast.LENGTH_SHORT).show();
                                        img_unlike.setVisibility(View.VISIBLE);
                                        img_like.setVisibility(View.GONE);
                                    } else {
                                        img_unlike.setVisibility(View.GONE);
                                        img_like.setVisibility(View.VISIBLE);
                                    }
                                    if (Objects.equals(jsonObject.getString("post_type"),"quiz")){
                                        tvPostType.setVisibility(View.VISIBLE);
                                    }
                                    else {
                                        tvPostType.setVisibility(View.GONE);
                                    }


                                    img_unlike.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            doLike(post_id);
                                        }
                                    });


                                    img_like.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            doLike(post_id);
                                        }
                                    });

                                    tv_comment.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Intent in = new Intent(context, CommentHomePost.class);
                                            in.putExtra("postId", post_id);
                                            context.startActivity(in);
                                            // Intent i= new Intent(context, CommentHomePost.class);

                                        }
                                    });

                                    expandableText.setAnimationDuration(750L);
                                    // set interpolators for both expanding and collapsing animations
                                    expandableText.setInterpolator(new OvershootInterpolator());
                                    // or set them separately
                                    expandableText.setExpandInterpolator(new OvershootInterpolator());
                                    expandableText.setCollapseInterpolator(new OvershootInterpolator());
                                    tv_viewmore.setOnClickListener(new View.OnClickListener() {
                                        @SuppressWarnings("ConstantConditions")
                                        @Override
                                        public void onClick(final View v) {
                                            tv_viewmore.setText(expandableText.isExpanded() ? R.string.expand : R.string.collapse);
                                            expandableText.toggle();
                                        }
                                    });

                                    // but, you can also do the checks yourself
                                    tv_viewmore.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(final View v) {
                                            if (expandableText.isExpanded()) {
                                                expandableText.collapse();
                                                tv_viewmore.setText(R.string.expand);
                                            } else {
                                                expandableText.expand();
                                                tv_viewmore.setText(R.string.collapse);
                                            }
                                        }
                                    });


                                    expandableText.addOnExpandListener(new ExpandableTextView.OnExpandListener() {
                                        @Override
                                        public void onExpand(@NonNull final ExpandableTextView view) {
                                            Log.d("texzt", "ExpandableTextView expanded");
                                        }

                                        @Override
                                        public void onCollapse(@NonNull final ExpandableTextView view) {
                                            Log.d("texzt", "ExpandableTextView collapsed");
                                        }
                                    });
                                }


                            } else {

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        // Toast.makeText(SignUpStepTwo.this, "" + response.toString(), Toast.LENGTH_SHORT).show();
                        //resultTextView.setText("String Response : "+ response.toString());
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //  resultTextView.setText("Error getting response");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json; charset=UTF-8");
                params.put("Authorization", "Bearer " + sharedPreference_main.getToken());
                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);


    }

    private void doLike(String post_id) {


        RequestQueue requestQueue = Volley.newRequestQueue(context.getApplicationContext());
        JSONObject jsonBody = new JSONObject();
        try {
            //input your API parameters
            jsonBody.put("post_id", post_id);
            jsonBody.put("liked_user_id", sharedPreference_main.getUserId());
            jsonBody.put("liker_type", sharedPreference_main.getUserType());


        } catch (JSONException e) {
            e.printStackTrace();
        }
        // Enter the correct url for your api service site
        // String url = getResources().getString(R.string.url);
       // String URL = "http://13.233.162.24/nusys(UAT)/api/teacher/make_like.php";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL_TEACHER+MAKE_LIKE, jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            // Toast.makeText(ChangePassword.this, "true", Toast.LENGTH_SHORT).show();

                            Boolean status = response.getBoolean("status");
                            String key = response.getString("key");
                            String count = response.getString("count");
                            if (status) {

                                if (key.equals("UnLike")) {

                                    img_like.setVisibility(View.GONE);
                                    img_unlike.setVisibility(View.VISIBLE);
                                    tv_likes.setText(count);
                                } else {
                                    img_like.setVisibility(View.VISIBLE);
                                    img_unlike.setVisibility(View.GONE);
                                    tv_likes.setText(count);
                                }
                            } else {
                                Toast.makeText(context, "" + response.getString("message"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        // Toast.makeText(ChangePassword.this, ""+response.toString(), Toast.LENGTH_SHORT).show();
                        //resultTextView.setText("String Response : "+ response.toString());
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //  resultTextView.setText("Error getting response");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json; charset=UTF-8");
                params.put("Authorization", "Bearer " + sharedPreference_main.getToken());
                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);


    }
}
