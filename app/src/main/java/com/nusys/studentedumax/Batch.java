package com.nusys.studentedumax;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.nusys.studentedumax.Group.batch_Activity;


import com.nusys.studentedumax.adapter.AdapterBatch;
import com.nusys.studentedumax.commonModules.DialogProgress;
import com.nusys.studentedumax.commonModules.NetworkUtil;
import com.nusys.studentedumax.commonModules.SharedPreference_main;
import com.nusys.studentedumax.model.FollowingModel;
import com.nusys.studentedumax.model.ResponeStudentBatch;
import com.nusys.studentedumax.retrofit.ApiClient;
import com.nusys.studentedumax.retrofit.ServiceInterface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

import static com.nusys.studentedumax.Constants.Content_Type;
import static com.nusys.studentedumax.commonModules.Extension.showErrorDialog;

public class Batch extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {
    SwipeRefreshLayout refreshLayout;

    ProgressBar mainProgress;
    LinearLayout errorLayout;
    TextView errorTxtCause;
    Button errorBtnRetry;
    Context context;

    SharedPreference_main sharedPreference_main;
    RecyclerView recyclerView_group_view;
    AdapterBatch Adapter;
    private ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_batch, frameLayout);
        setTitle("Batch");

        context = Batch.this;
        sharedPreference_main = SharedPreference_main.getInstance(this);
        pd = new DialogProgress(context, "");
        pd.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        pd.setCancelable(false);
        refreshLayout = findViewById(R.id.refresh_layout);
        mainProgress = findViewById(R.id.main_progress);
        errorLayout = findViewById(R.id.error_layout);
        errorTxtCause = findViewById(R.id.error_txt_cause);
        errorBtnRetry = findViewById(R.id.error_btn_retry);
        refreshLayout.setOnRefreshListener(this);
        refreshLayout.setColorSchemeColors(Color.RED, Color.YELLOW, Color.BLUE);


        recyclerView_group_view = findViewById(R.id.rv_group_view);
        student_batch();

    }

    private void student_batch() {
        if (NetworkUtil.isConnected(context)) {

            pd.show();
        HashMap<String, String> map = new HashMap<>();

        map.put("student_id", sharedPreference_main.getUserId());

        //   if (NetworkUtils.isConnected(getActivity())) {
        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<ResponeStudentBatch> call = serviceInterface.student_batch("Bearer " + sharedPreference_main.getToken(), Content_Type, map);
        call.enqueue(new Callback<ResponeStudentBatch>() {


            @Override
            public void onResponse(Call<ResponeStudentBatch> call, retrofit2.Response<ResponeStudentBatch> response) {

                if (response.isSuccessful()) {

                    ResponeStudentBatch bean = response.body();

                    //Toast.makeText(batch_Activity.this, "sucess", Toast.LENGTH_SHORT).show();
                    if (bean.getStatus()) {
                        ResponeStudentBatch view = response.body();
                        errorLayout.setVisibility(View.GONE);
                        recyclerView_group_view.setVisibility(View.VISIBLE);

                        Adapter = new AdapterBatch(context, bean.getData());
                        recyclerView_group_view.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
                        recyclerView_group_view.setItemAnimator(new DefaultItemAnimator());
                        recyclerView_group_view.setAdapter(Adapter);
                        Log.e("Group_response", view.toString());
                        pd.dismiss();
                    } else {
                        pd.dismiss();
                        errorLayout.setVisibility(View.VISIBLE);
                        recyclerView_group_view.setVisibility(View.GONE);
                        Toast.makeText(context, bean.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    pd.dismiss();
                    errorLayout.setVisibility(View.VISIBLE);
                    recyclerView_group_view.setVisibility(View.GONE);
                    Toast.makeText(context, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponeStudentBatch> call, Throwable t) {
                pd.dismiss();
                errorLayout.setVisibility(View.VISIBLE);
                recyclerView_group_view.setVisibility(View.GONE);
                Log.e("error", t.getMessage());
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        } else {
            showErrorDialog(context, new Dialog(context));
        }
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                student_batch();
                refreshLayout.setRefreshing(false);
            }
        }, 2000);

    }

}
