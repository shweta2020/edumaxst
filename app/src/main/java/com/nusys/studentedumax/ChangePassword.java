package com.nusys.studentedumax;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.nusys.studentedumax.commonModules.SharedPreference_main;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.nusys.studentedumax.Constants.BASE_URL;
import static com.nusys.studentedumax.Constants.CHANGE_PWD;

public class ChangePassword extends BaseActivity {
    LinearLayout back;
    Button btn_submit;
    EditText st_old_password, st_confirm_password, st_new_password;
    SharedPreference_main sharedPreference_main;
    String st_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_change_password, frameLayout);
        setTitle("Change Password");

      //  setContentView(R.layout.activity_change_password);
        init();
        listener();

    }

    private void init() {

        back = findViewById(R.id.back_activity);
        btn_submit = findViewById(R.id.btSubmit);
        st_confirm_password = findViewById(R.id.stConfirmPassword);
        st_old_password = findViewById(R.id.etOldPassword);
        st_new_password = findViewById(R.id.stNewPassword);
        sharedPreference_main = SharedPreference_main.getInstance(this);

    }

    private void listener() {

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(st_old_password.getText().toString())) {
                    st_old_password.setError("field can't be empty");
                } else if (TextUtils.isEmpty(st_new_password.getText().toString())) {
                    st_new_password.setError("field can't be empty");
                } else if (TextUtils.isEmpty(st_confirm_password.getText().toString())) {
                    st_confirm_password.setError("field can't be empty");
                } else if (!st_new_password.getText().toString().equals(st_confirm_password.getText().toString())) {
                    st_confirm_password.setError("confirm password not matched");

                } else {

                    post_Json_Request();
                }

            }
        });

    }

    private void post_Json_Request() {


        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        JSONObject jsonBody = new JSONObject();
        try {
            //input your API parameters

            jsonBody.put("id", sharedPreference_main.getUserId());
            jsonBody.put("old_password", st_old_password.getText().toString());
            jsonBody.put("new_password", st_new_password.getText().toString());


        } catch (JSONException e) {
            e.printStackTrace();
        }

       // String URL = "http://13.233.162.24/nusys(UAT)/api/student/change_password.php";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL+CHANGE_PWD, jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {


                            Boolean status = response.getBoolean("status");
                            if (status) {
                                startActivity(new Intent(ChangePassword.this, Login.class));
                                finish();
                                Toast.makeText(ChangePassword.this, "" + response.getString("message"), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(ChangePassword.this, "" + response.getString("message"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        // Toast.makeText(ChangePassword.this, ""+response.toString(), Toast.LENGTH_SHORT).show();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //  resultTextView.setText("Error getting response");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json; charset=UTF-8");
                params.put("Authorization", "Bearer " + sharedPreference_main.getToken());
                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);


    }
}
