package com.nusys.studentedumax;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.nusys.studentedumax.adapter.AdapterHomePost;
import com.nusys.studentedumax.commonModules.BottomNavigationBehavior;
import com.nusys.studentedumax.commonModules.DialogProgress;
import com.nusys.studentedumax.commonModules.NetworkUtil;
import com.nusys.studentedumax.commonModules.PaginationAdapterCallback;
import com.nusys.studentedumax.commonModules.PaginationScrollListener;
import com.nusys.studentedumax.commonModules.SharedPreference_main;
import com.nusys.studentedumax.model.HomePostModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeoutException;

import static com.nusys.studentedumax.Constants.BASE_URL_TEACHER;
import static com.nusys.studentedumax.Constants.VIEW_POST;
import static com.nusys.studentedumax.commonModules.Extension.showErrorDialog;

public class Home extends BaseActivity implements PaginationAdapterCallback, SwipeRefreshLayout.OnRefreshListener {
    RecyclerView rv_post_list;
    AdapterHomePost myAdapter;
    SharedPreference_main sharedPreference_main;
    LinearLayoutManager linearLayoutManager;
    private static int TOTAL_PAGES;
    ProgressBar progressBar;
    LinearLayout errorLayout;
    Button btnRetry;
    SwipeRefreshLayout swipeRefreshLayout;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int currentPage = 1;

    //  private List<HomePostModel> ab;
    FloatingActionButton fabAdd;
    FloatingActionButton fab_post;
    FloatingActionButton fab_quiz;
    Boolean isRotate = false;
    FrameLayout mainFrame, frame_container;

    LinearLayout floating_layout;

    TextView textCartItemCount;
    int mCartItemCount = 10;
    BottomNavigationView navigation;

    private ProgressDialog pd;
    Context context;
    boolean doubleBackToExitPressedOnce = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_home, frameLayout);
        setTitle("Home");

        context=Home.this;
        //ab=new ArrayList<>();

        navigation  = (BottomNavigationView) findViewById(R.id.navigation_home);
        removePaddingFromNavigationItem();
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        sharedPreference_main = SharedPreference_main.getInstance(this);
        // Toast.makeText(this, "token"+sharedPreference_main.getToken(), Toast.LENGTH_SHORT).show();

        CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) navigation.getLayoutParams();
        layoutParams.setBehavior(new BottomNavigationBehavior());

        progressBar = findViewById(R.id.main_progress);
        errorLayout = findViewById(R.id.error_layout);
        btnRetry = findViewById(R.id.error_btn_retry);
        rv_post_list = findViewById(R.id.rv_post_list_home);
        mainFrame = findViewById(R.id.mainFrameHome);

        pd = new DialogProgress(context, "");
        pd.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        pd.setCancelable(false);
        // txtError = findViewById(R.id.error_txt_cause);
        swipeRefreshLayout = findViewById(R.id.main_swiperefresh);
        frame_container = findViewById(R.id.frame_container_home);
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rv_post_list.setLayoutManager(linearLayoutManager);
        rv_post_list.setItemAnimator(new DefaultItemAnimator());

        floating_layout = findViewById(R.id.floating_layout_home);

        //LinearLayoutManager layoutManager = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
        // rv_post_list.setLayoutManager(new LinearLayoutManager(this));
        frame_container.setVisibility(View.GONE);
        mainFrame.setVisibility(View.VISIBLE);

        myAdapter = new AdapterHomePost(Home.this);
        rv_post_list.setAdapter(myAdapter);

        rv_post_list.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;

                loadNextPage();
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        //loadFirstPage();
        btnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadFirstPage();
            }
        });

        swipeRefreshLayout.setOnRefreshListener(this);

        fabAdd = findViewById(R.id.fabAddHome);
        fab_post = findViewById(R.id.fab_postHome);
        // fab_quiz = findViewById(R.id.fab_quizHome);
        ViewAnimation.init(fab_post);
        //ViewAnimation.init(fab_quiz);

        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isRotate = ViewAnimation.rotateFab(v, !isRotate);
                if (isRotate) {
                    ViewAnimation.showIn(fab_post);
                    // ViewAnimation.showIn(fab_quiz);
                } else {
                    ViewAnimation.showOut(fab_post);
                    //  ViewAnimation.showOut(fab_quiz);
                }
            }
        });
        fab_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Home.this, CreatePost.class));
            }
        });
    }

    public void onRefresh() {
        // TODO: Check if data is stale.
        //  Execute network request if cache is expired; otherwise do not update data.

        progressBar.setVisibility(View.VISIBLE);
        isLastPage = false;

        swipeRefreshLayout.setRefreshing(false);
        loadFirstPage();
    }

    private void loadFirstPage() {
        //changed
        currentPage = 1;
        //relative.setVisibility(View.VISIBLE);
        errorLayout.setVisibility(View.VISIBLE);
        //   frame_container.setVisibility(View.GONE);
        // To ensure list is visible when retry button in error view is clicked
        if (NetworkUtil.isConnected(context)) {
            hideErrorView();
            pd.show();

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        final JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("user_id", sharedPreference_main.getUserId());
            jsonBody.put("page", currentPage);
            jsonBody.put("user_type", "student");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        // Enter the correct url for your api service site
        // String url = getResources().getString(R.string.url);
        //String URL = "http://13.233.162.24/nusys(UAT)/api/teacher/view_post.php";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL_TEACHER+VIEW_POST, jsonBody,
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("response", response.toString());
                        try {

                            Boolean status = response.getBoolean("status");
                            //Toast.makeText(Profile.this, "", Toast.LENGTH_SHORT).show();
                            // Toast.makeText(Profile.this, "abcd"+status, Toast.LENGTH_SHORT).show();
                            if (status) {
                                myAdapter.clear();
                                myAdapter.notifyDataSetChanged();
                                final List<HomePostModel> ab = new ArrayList<>();
                                //JSONObject jsonObj = response.getJSONObject("data");
                                JSONArray jsonArray = response.getJSONArray("data");
                                for (int i = 0; i < jsonArray.length(); i++) {

                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    HomePostModel entity = new HomePostModel();
                                    //  entity.setData(response.getString("namezz"));
                                    entity.setUser_name(jsonObject.getString("user_name"));
                                    entity.setHeading(jsonObject.getString("heading"));
                                    entity.setContent(jsonObject.getString("content"));
                                    entity.setFilename(jsonObject.getString("filename"));
                                    entity.setTime(jsonObject.getString("time"));
                                    entity.setLikes(jsonObject.getString("likes"));
                                    entity.setLike_status(jsonObject.getString("like_status"));
                                    entity.setTotal_comments(jsonObject.getString("total_comments"));
                                    entity.setUser_profile(jsonObject.getString("user_profile"));
                                    entity.setUploader_id(jsonObject.getString("uploader_id"));
                                    entity.setUser_type(jsonObject.getString("user_type"));
                                    entity.setPost_id(jsonObject.getString("post_id"));
                                    entity.setPost_type(jsonObject.getString("post_type"));
                                    ab.add(entity);
                                }
                                //reprog.setVisibility(View.GONE);
                                //myAdapter = new PaginationAdapter(MainActivity.this, ab);
                                //rv_post_list.setAdapter(myAdapter);
                                if (ab != null) {

                                    TOTAL_PAGES = ab.size();
                                    Log.e("totalPage", String.valueOf(ab.size()));
                                    myAdapter.addAll(ab);
//                                    rv_post_list.setAdapter(myAdapter);

                                } else {

                                    Toast.makeText(Home.this, "Something is wrong try again later", Toast.LENGTH_SHORT).show();
                                }
                                progressBar.setVisibility(View.GONE);
                                if (currentPage <= TOTAL_PAGES) myAdapter.addLoadingFooter();
                                else isLastPage = true;
                                pd.dismiss();
                            } else {
                                pd.dismiss();
                                ShowErrorView();
                                Toast.makeText(Home.this, "Something is wrong try again later", Toast.LENGTH_SHORT).show();

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        // Toast.makeText(SignUpStepTwo.this, "" + response.toString(), Toast.LENGTH_SHORT).show();
                        //resultTextView.setText("String Response : "+ response.toString());
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                ShowErrorView();
                //  resultTextView.setText("Error getting response");
            }
        })


        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json; charset=UTF-8");
                params.put("Authorization", "Bearer " + sharedPreference_main.getToken());
                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);
        } else {
            showErrorDialog(context, new Dialog(context));
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        loadFirstPage();
//        onRefresh();
    }

    private void loadNextPage() {
        Log.d("page", "loadNextPage: " + currentPage);
        Log.e("total", String.valueOf(TOTAL_PAGES));
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                final JSONObject jsonBody = new JSONObject();
                try {
                    jsonBody.put("user_id", sharedPreference_main.getUserId());
                    jsonBody.put("page", String.valueOf(currentPage));
                    jsonBody.put("user_type", "student");


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                // Enter the correct url for your api service site
                // String url = getResources().getString(R.string.url);
                // String URL = "http://13.233.162.24/nusys(UAT)/api/teacher/view_post.php";
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL_TEACHER+VIEW_POST, jsonBody,
                        new com.android.volley.Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                Log.e("response", response.toString());
                                try {


                                    myAdapter.removeLoadingFooter();
                                    isLoading = false;

                                    Boolean status = response.getBoolean("status");
                                    //Toast.makeText(Profile.this, "", Toast.LENGTH_SHORT).show();
                                    // Toast.makeText(Profile.this, "abcd"+status, Toast.LENGTH_SHORT).show();
                                    if (status) {

                                        final List<HomePostModel> ab = new ArrayList<>();
                                        //JSONObject jsonObj = response.getJSONObject("data");
                                        JSONArray jsonArray = response.getJSONArray("data");
                                        for (int i = 0; i < jsonArray.length(); i++) {

                                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                                            HomePostModel entity = new HomePostModel();
                                            //  entity.setData(response.getString("namezz"));
                                            entity.setUser_name(jsonObject.getString("user_name"));

                                            entity.setHeading(jsonObject.getString("heading"));
                                            entity.setContent(jsonObject.getString("content"));
                                            entity.setFilename(jsonObject.getString("filename"));
                                            entity.setTime(jsonObject.getString("time"));
                                            entity.setLikes(jsonObject.getString("likes"));
                                            entity.setLike_status(jsonObject.getString("like_status"));
                                            entity.setTotal_comments(jsonObject.getString("total_comments"));
                                            entity.setPost_id(jsonObject.getString("post_id"));
                                            entity.setUploader_id(jsonObject.getString("uploader_id"));
                                            entity.setUser_profile(jsonObject.getString("user_profile"));
                                            entity.setUser_type(jsonObject.getString("user_type"));
                                            ab.add(entity);
                                        }
                                        //reprog.setVisibility(View.GONE);
                                        //myAdapter = new PaginationAdapter(MainActivity.this, ab);
                                        // rv_post_list.setAdapter(myAdapter);

                                        if (ab != null) {
                                            //TOTAL_PAGES = ab.size();
                                            myAdapter.addAll(ab);
                                            // myAdapter.removeLoadingFooter();
                                            if (currentPage != TOTAL_PAGES)
                                                myAdapter.addLoadingFooter();

                                            else isLastPage = true;

                                        }
                                        else {
                                            isLastPage = true;
                                            //Toast.makeText(Home.this, "Something is wrong try again later", Toast.LENGTH_SHORT).show();

                                        }

                                    }
                                    else {
                                        isLastPage = true;
                                        //Toast.makeText(Home.this, "Something is wrong try again later", Toast.LENGTH_SHORT).show();

                                    }


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    isLastPage = true;
                                }

                                // Toast.makeText(SignUpStepTwo.this, "" + response.toString(), Toast.LENGTH_SHORT).show();
                                //resultTextView.setText("String Response : "+ response.toString());
                            }
                        }, new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        isLastPage = true;
                        Log.e("error", error.getMessage());
                        //  resultTextView.setText("Error getting response");
                    }
                }) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("Content-Type", "application/json; charset=UTF-8");
                        params.put("Authorization", "Bearer " + sharedPreference_main.getToken());
                        return params;
                    }
                };
                requestQueue.add(jsonObjectRequest);

            }
        }, 2000);

    }


    @Override
    public void retryPageLoad() {
        loadNextPage();
    }


    /**
     * @param throwable required for {@link #fetchErrorMessage(Throwable)}
     * @return
     */
    private void showErrorView(Throwable throwable) {

        if (errorLayout.getVisibility() == View.GONE) {
            errorLayout.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);

            //txtError.setText(fetchErrorMessage(throwable));
        }
    }

    /**
     * @param throwable to identify the type of error
     * @return appropriate error message
     */
    private String fetchErrorMessage(Throwable throwable) {
        String errorMsg = getResources().getString(R.string.error_msg_unknown);

        if (!isNetworkConnected()) {
            errorMsg = getResources().getString(R.string.error_msg_no_internet);
        } else if (throwable instanceof TimeoutException) {
            errorMsg = getResources().getString(R.string.error_msg_timeout);
        }

        return errorMsg;
    }

    // Helpers -------------------------------------------------------------------------------------


    private void hideErrorView() {
        if (errorLayout.getVisibility() == View.VISIBLE) {
            errorLayout.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
        }
    }
    private void ShowErrorView() {
        if (errorLayout.getVisibility() == View.GONE) {
            errorLayout.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
        }
    }
    /**
     * Remember to add android.permission.ACCESS_NETWORK_STATE permission.
     *
     * @return
     */
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    //for bottom navigation
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment;
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    Intent i = new Intent(Home.this, Home.class);
                    startActivity(i);
                    // onRefresh();

                    return true;
                case R.id.navigation_doubts:
                    setTitle("Doubts");
                    frame_container.setVisibility(View.VISIBLE);
                    mainFrame.setVisibility(View.GONE);
//                    fragment = new GiftsFragment();
                    DoubtsFragment doubtFragment = new DoubtsFragment();
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.add(R.id.frame_container_home, doubtFragment);
                    transaction.addToBackStack("doubt");
                    sharedPreference_main.setDoubtStatus("0");
                    getSupportFragmentManager().popBackStackImmediate();
                    transaction.commit();
                    floating_layout.setVisibility(View.GONE);
                    return true;

//                    fragment = new DoubtsFragment();
//                    loadFragment(fragment);
//                    setTitle("Doubts");
//                    floating_layout.setVisibility(View.GONE);
//
//                    //  toolbar.setTitle("Cart");
//                    return true;
                case R.id.navigation_testseries:
                    fragment = new TestseriesFragment();
                    loadFragment(fragment);
                    setTitle("Tests");
                    floating_layout.setVisibility(View.GONE);
                    //toolbar.setTitle("Profile");
                    return true;
                case R.id.navigation_course:
                    fragment = new CourseFragment();
                    loadFragment(fragment);
                    setTitle("Course");
                    floating_layout.setVisibility(View.GONE);
                    //toolbar.setTitle("Profile");
                    return true;
                case R.id.navigation_classroom:
                    fragment = new ClassroomFragment();
                    loadFragment(fragment);
                    setTitle("Live Class");
                    floating_layout.setVisibility(View.GONE);
                    //toolbar.setTitle("Profile");
                    return true;
            }
            return false;

        }
    };

    //** for removing padding form bottomnavigation bec Long text cutting off like live class**//
    public void removePaddingFromNavigationItem() {
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) navigation.getChildAt(0);

        for (int i = 0; i < menuView.getChildCount(); i++) {
            BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
            View activeLabel = item.findViewById(R.id.largeLabel);
            if (activeLabel instanceof TextView) {
                activeLabel.setPadding(0, 0, 0, 0);
            }
        }
    }

    private void loadFragment(Fragment fragment) {
        //switching fragment
        if (fragment != null) {
            mainFrame.setVisibility(View.GONE);
            frame_container.setVisibility(View.VISIBLE);
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frame_container_home, fragment);
            transaction.addToBackStack("CalenderWebview");
            getSupportFragmentManager().popBackStackImmediate();
            transaction.commit();

        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();

    /*    int count = getSupportFragmentManager().getBackStackEntryCount();

        if (count != 0) {
//            onStart();
            startActivity(new Intent(this, Home.class));
            finish();
            //additional code
        } else {
            super.onBackPressed();
        }*/

        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
           // getSupportFragmentManager().popBackStack();
            startActivity(new Intent(this, Home.class));
            finish();
        } else if (!doubleBackToExitPressedOnce) {
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this,"Please click BACK again to exit.", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        } else {
            super.onBackPressed();
            return;
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);

        final MenuItem menuItem = menu.findItem(R.id.action_notify);

        View actionView = menuItem.getActionView();
        textCartItemCount = (TextView) actionView.findViewById(R.id.notifications_badge);

        setupBadge();

        actionView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(menuItem);
            }
        });

        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_notify: {
                // Do something
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupBadge() {

        if (textCartItemCount != null) {
            if (mCartItemCount == 0) {
                if (textCartItemCount.getVisibility() != View.GONE) {
                    textCartItemCount.setVisibility(View.GONE);
                }
            } else {
                textCartItemCount.setText(String.valueOf(Math.min(mCartItemCount, 99)));
                if (textCartItemCount.getVisibility() != View.VISIBLE) {
                    textCartItemCount.setVisibility(View.VISIBLE);
                }
            }
        }
    }

}

