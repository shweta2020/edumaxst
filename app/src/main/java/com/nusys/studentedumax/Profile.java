package com.nusys.studentedumax;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.nusys.studentedumax.adapter.AdapterPost;
import com.nusys.studentedumax.adapter.AdapterProfilePost;
import com.nusys.studentedumax.adapter.AdapterTeacher;
import com.nusys.studentedumax.commonModules.PaginationAdapterCallback;
import com.nusys.studentedumax.commonModules.PaginationScrollListener;
import com.nusys.studentedumax.model.HomePostModel;
import com.nusys.studentedumax.model.PostModel;
import com.nusys.studentedumax.commonModules.SharedPreference_main;
import com.nusys.studentedumax.model.ProfilePostModel;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeoutException;

import static com.nusys.studentedumax.Constants.BASE_URL;
import static com.nusys.studentedumax.Constants.PROFILE;

public class Profile extends BaseActivity implements PaginationAdapterCallback, SwipeRefreshLayout.OnRefreshListener {
    RecyclerView rv_post_profile_list;
    AdapterProfilePost myAdapter;
    SharedPreference_main sharedPreference_main;
    LinearLayoutManager linearLayoutManager;
    private static int TOTAL_PAGES;
    ProgressBar progressBar;
    LinearLayout errorLayout;
    Button btnRetry;
    SwipeRefreshLayout swipeRefreshLayout;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int currentPage = 1;
    TextView btn_edit_profile, userName, userEmail;
    TextView tvDoubts, tv_member_since, tv_user_following, tv_user_follower, tv_user_post;


    ImageView ivUserImage;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_profile, frameLayout);
        setTitle("Profile");

        context = Profile.this;
        sharedPreference_main = SharedPreference_main.getInstance(this);
        tv_user_post = findViewById(R.id.user_post);
        tv_user_follower = findViewById(R.id.user_follower);
        tv_user_following = findViewById(R.id.user_following);
        tv_member_since = findViewById(R.id.member_since);
        tvDoubts = findViewById(R.id.tv_doubts);

        progressBar = findViewById(R.id.main_progress);
        errorLayout = findViewById(R.id.error_layout);
        btnRetry = findViewById(R.id.error_btn_retry);
        rv_post_profile_list = findViewById(R.id.rvPostProfileList);
        // txtError = findViewById(R.id.error_txt_cause);
        swipeRefreshLayout = findViewById(R.id.main_swiperefresh);
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rv_post_profile_list.setLayoutManager(linearLayoutManager);
        rv_post_profile_list.setItemAnimator(new DefaultItemAnimator());

        myAdapter = new AdapterProfilePost(context);
        rv_post_profile_list.setAdapter(myAdapter);
        loadFirstPage();
        rv_post_profile_list.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;

                loadNextPage();
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });


        btnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadFirstPage();
            }
        });

        swipeRefreshLayout.setOnRefreshListener(this);
        userName = findViewById(R.id.user_name);
        userEmail = findViewById(R.id.user_email);
        ivUserImage = findViewById(R.id.iv_user_image);
        userName.setText(sharedPreference_main.getUsername());
        userEmail.setText(sharedPreference_main.getUserEmail());
        loadProfileData();

        //Toast.makeText(this, ""+sharedPreference_main.getUserId(), Toast.LENGTH_SHORT).show();

        btn_edit_profile = findViewById(R.id.edit_profile);
        btn_edit_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Profile.this, UpdateProfile.class);
                startActivity(i);
            }
        });

    }

    public void onRefresh() {
        // TODO: Check if data is stale.
        //  Execute network request if cache is expired; otherwise do not update data.
        myAdapter.clear();
        myAdapter.notifyDataSetChanged();
        progressBar.setVisibility(View.VISIBLE);
        isLastPage = false;
        swipeRefreshLayout.setRefreshing(false);
        loadFirstPage();
    }

    private void loadFirstPage() {
        currentPage = 1;
        //relative.setVisibility(View.VISIBLE);
        errorLayout.setVisibility(View.VISIBLE);
        //   frame_container.setVisibility(View.GONE);

        // To ensure list is visible when retry button in error view is clicked
        hideErrorView();

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        final JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("id", sharedPreference_main.getUserId());
            jsonBody.put("page", currentPage);
            jsonBody.put("user_type", sharedPreference_main.getUserType());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        // Enter the correct url for your api service site
        // String url = getResources().getString(R.string.url);
        //String URL = "http://13.233.162.24/nusys(UAT)/api/student/profile.php";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL+PROFILE, jsonBody,
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("response", response.toString());
                        try {
                            myAdapter.clear();
                            myAdapter.notifyDataSetChanged();
                            Boolean status = response.getBoolean("status");
                            //Toast.makeText(Profile.this, "", Toast.LENGTH_SHORT).show();
                            // Toast.makeText(Profile.this, "abcd"+status, Toast.LENGTH_SHORT).show();
                            if (status) {

                                final List<ProfilePostModel> ab = new ArrayList<>();
                                //JSONObject jsonObj = response.getJSONObject("data");
                                JSONObject jsonObj = response.getJSONObject("data");
                                tv_user_post.setText(jsonObj.getString("post_count"));
                                tv_user_follower.setText(jsonObj.getString("followers_count"));
                                tv_user_following.setText(jsonObj.getString("following_count"));
                                tv_member_since.setText(jsonObj.getString("member_since") + " Member Since");
                                tvDoubts.setText(jsonObj.getString("doubts_count") + " Doubts");


                                JSONArray jsonArray = jsonObj.getJSONArray("posts");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    ProfilePostModel entity = new ProfilePostModel();
                                    //entity.setData(response.getString("namezz"));
                                    entity.setUploader_name(jsonObject.getString("uploader_name"));
                                    entity.setHeading(jsonObject.getString("heading"));
                                    entity.setContent(jsonObject.getString("content"));
                                    entity.setFilename(jsonObject.getString("filename"));
                                    entity.setTime(jsonObject.getString("time"));
                                    entity.setLikes(jsonObject.getString("likes"));
                                    entity.setLike_status(jsonObject.getString("like_status"));
                                    entity.setTotal_comments(jsonObject.getString("total_comments"));
                                    entity.setUploader_image(jsonObject.getString("uploader_image"));
                                    entity.setUploader_id(jsonObject.getString("uploader_id"));
                                    entity.setUser_type(jsonObject.getString("user_type"));
                                    entity.setPost_id(jsonObject.getString("post_id"));

                                    ab.add(entity);
                                }
                                //reprog.setVisibility(View.GONE);
                                //myAdapter = new PaginationAdapter(MainActivity.this, ab);
                                //rv_post_list.setAdapter(myAdapter);
                                if (ab != null) {

                                    TOTAL_PAGES = ab.size();
                                    Log.e("totalPage", String.valueOf(ab.size()));
                                    myAdapter.addAll(ab);
//                                    rv_post_list.setAdapter(myAdapter);

                                } else {

                                    Toast.makeText(context, "Something is wrong try again later", Toast.LENGTH_SHORT).show();
                                }
                                progressBar.setVisibility(View.GONE);
                                if (currentPage != TOTAL_PAGES) myAdapter.addLoadingFooter();
                                else isLastPage = true;
                            } else {
                                Toast.makeText(context, "Something is wrong try again later", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        // Toast.makeText(SignUpStepTwo.this, "" + response.toString(), Toast.LENGTH_SHORT).show();
                        //resultTextView.setText("String Response : "+ response.toString());
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //  resultTextView.setText("Error getting response");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json; charset=UTF-8");
                params.put("Authorization", "Bearer " + sharedPreference_main.getToken());
                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);
    }


    @Override
    protected void onStart() {
        super.onStart();
        loadProfileData();
        //loadFirstPage();
//        onRefresh();
    }

    private void loadNextPage() {
        Log.d("page", "loadNextPage: " + currentPage);
        Log.e("total", String.valueOf(TOTAL_PAGES));
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                final JSONObject jsonBody = new JSONObject();
                try {
                    jsonBody.put("id", sharedPreference_main.getUserId());
                    jsonBody.put("page", currentPage);
                    jsonBody.put("user_type", sharedPreference_main.getUserType());

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                // Enter the correct url for your api service site
                // String url = getResources().getString(R.string.url);
              //  String URL = "http://13.233.162.24/nusys(UAT)/api/student/profile.php";
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL+PROFILE, jsonBody,
                        new com.android.volley.Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                Log.e("response", response.toString());
                                try {

                                    isLoading = false;
                                    Boolean status = response.getBoolean("status");
                                    //Toast.makeText(Profile.this, "", Toast.LENGTH_SHORT).show();
                                    // Toast.makeText(Profile.this, "abcd"+status, Toast.LENGTH_SHORT).show();
                                    if (status) {
                                        final List<ProfilePostModel> ab = new ArrayList<>();
                                        //JSONObject jsonObj = response.getJSONObject("data");
                                        JSONObject jsonObj = response.getJSONObject("data");
                                        tv_user_post.setText(jsonObj.getString("post_count"));
                                        tv_user_follower.setText(jsonObj.getString("followers_count"));
                                        tv_user_following.setText(jsonObj.getString("following_count"));
                                        tv_member_since.setText(jsonObj.getString("member_since") + " Member Since");
                                        tvDoubts.setText(jsonObj.getString("doubts_count") + " Doubts");

                                        JSONArray jsonArray = jsonObj.getJSONArray("posts");
                                        for (int i = 0; i < jsonArray.length(); i++) {

                                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                                            ProfilePostModel entity = new ProfilePostModel();
                                            //  entity.setData(response.getString("namezz"));
                                            entity.setUploader_name(jsonObject.getString("uploader_name"));
                                            entity.setHeading(jsonObject.getString("heading"));
                                            entity.setContent(jsonObject.getString("content"));
                                            entity.setFilename(jsonObject.getString("filename"));
                                            entity.setTime(jsonObject.getString("time"));
                                            entity.setLikes(jsonObject.getString("likes"));
                                            entity.setLike_status(jsonObject.getString("like_status"));
                                            entity.setTotal_comments(jsonObject.getString("total_comments"));
                                            entity.setPost_id(jsonObject.getString("post_id"));
                                            entity.setUploader_id(jsonObject.getString("uploader_id"));
                                            entity.setUploader_image(jsonObject.getString("uploader_image"));
                                            entity.setUser_type(jsonObject.getString("user_type"));
                                            ab.add(entity);
                                        }
                                        //reprog.setVisibility(View.GONE);
                                        //myAdapter = new PaginationAdapter(MainActivity.this, ab);
                                        // rv_post_list.setAdapter(myAdapter);

                                        if (ab != null) {
                                            //TOTAL_PAGES = ab.size();
                                            myAdapter.addAll(ab);
                                            myAdapter.removeLoadingFooter();

                                        }
                                        if (currentPage != TOTAL_PAGES)
                                            myAdapter.addLoadingFooter();
                                        else isLastPage = true;
                                    } else {
                                        //Toast.makeText(Home.this, "Something is wrong try again later", Toast.LENGTH_SHORT).show();

                                    }


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                // Toast.makeText(SignUpStepTwo.this, "" + response.toString(), Toast.LENGTH_SHORT).show();
                                //resultTextView.setText("String Response : "+ response.toString());
                            }
                        }, new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  resultTextView.setText("Error getting response");
                    }
                }) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("Content-Type", "application/json; charset=UTF-8");
                        params.put("Authorization", "Bearer " + sharedPreference_main.getToken());
                        return params;
                    }
                };
                requestQueue.add(jsonObjectRequest);

            }
        }, 2000);

    }


    @Override
    public void retryPageLoad() {
        loadNextPage();
    }


    /**
     * @param throwable required for {@link #fetchErrorMessage(Throwable)}
     * @return
     */
    private void showErrorView(Throwable throwable) {

        if (errorLayout.getVisibility() == View.GONE) {
            errorLayout.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);

            //txtError.setText(fetchErrorMessage(throwable));
        }
    }

    /**
     * @param throwable to identify the type of error
     * @return appropriate error message
     */
    private String fetchErrorMessage(Throwable throwable) {
        String errorMsg = getResources().getString(R.string.error_msg_unknown);

        if (!isNetworkConnected()) {
            errorMsg = getResources().getString(R.string.error_msg_no_internet);
        } else if (throwable instanceof TimeoutException) {
            errorMsg = getResources().getString(R.string.error_msg_timeout);
        }

        return errorMsg;
    }

    // Helpers -------------------------------------------------------------------------------------


    private void hideErrorView() {
        if (errorLayout.getVisibility() == View.VISIBLE) {
            errorLayout.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Remember to add android.permission.ACCESS_NETWORK_STATE permission.
     *
     * @return
     */
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    private void loadProfileData() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                final JSONObject jsonBody = new JSONObject();
                try {
                    jsonBody.put("id", sharedPreference_main.getUserId());
                    jsonBody.put("page", currentPage);
                    jsonBody.put("user_type", sharedPreference_main.getUserType());

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                // Enter the correct url for your api service site
                // String url = getResources().getString(R.string.url);
                //  String URL = "http://13.233.162.24/nusys(UAT)/api/student/profile.php";
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL+PROFILE, jsonBody,
                        new com.android.volley.Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                Log.e("response", response.toString());
                                try {

                                    isLoading = false;
                                    Boolean status = response.getBoolean("status");
                                    //Toast.makeText(Profile.this, "", Toast.LENGTH_SHORT).show();
                                    // Toast.makeText(Profile.this, "abcd"+status, Toast.LENGTH_SHORT).show();
                                    if (status) {
                                        final List<ProfilePostModel> ab = new ArrayList<>();
                                        //JSONObject jsonObj = response.getJSONObject("data");
                                        JSONObject jsonObj = response.getJSONObject("data");

                                        Glide.with(getApplicationContext())
                                                .load(jsonObj.getString("image"))
                                                .placeholder(R.drawable.user_icon)
                                                .into(ivUserImage);


                                    } else {
                                        //Toast.makeText(Home.this, "Something is wrong try again later", Toast.LENGTH_SHORT).show();

                                    }


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                // Toast.makeText(SignUpStepTwo.this, "" + response.toString(), Toast.LENGTH_SHORT).show();
                                //resultTextView.setText("String Response : "+ response.toString());
                            }
                        }, new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  resultTextView.setText("Error getting response");
                    }
                }) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("Content-Type", "application/json; charset=UTF-8");
                        params.put("Authorization", "Bearer " + sharedPreference_main.getToken());
                        return params;
                    }
                };
                requestQueue.add(jsonObjectRequest);

            }
        }, 2000);

    }
}
