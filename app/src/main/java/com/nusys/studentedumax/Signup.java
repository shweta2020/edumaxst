package com.nusys.studentedumax;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nusys.studentedumax.model.ExamModel;
import com.nusys.studentedumax.commonModules.SharedPreference_main;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.nusys.studentedumax.Constants.BASE_URL_TEACHER;
import static com.nusys.studentedumax.Constants.SELECT_EXAM;

public class Signup extends AppCompatActivity {
    EditText st_name, st_mobile, st_password, st_email;
    Button btn_next;
    SharedPreference_main sharedPreference_main;
    TextView multiSpin;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";


    RecyclerView exam_list;
    Signup.AdapterExamSpinner myAdapter;
    List<String> get_ID = new ArrayList<String>();
    List<String> Show_select_jobName = new ArrayList<String>();
    TextView text;
    Dialog dialog;
    String ExamID;
    private List<ExamModel> ruleslist;
    String exam_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        st_name = findViewById(R.id.st_fullname);
        st_mobile = findViewById(R.id.st_mobile_num);
        st_email = findViewById(R.id.st_emailid);
        st_password = findViewById(R.id.stPasswordSign);
        btn_next = findViewById(R.id.btnNext);
        multiSpin = findViewById(R.id.spinnerMulti);


        sharedPreference_main = SharedPreference_main.getInstance(this);


        multiSpin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hitExamApi();

            }
        });
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(st_name.getText().toString())) {
                    st_name.setError("field can't be empty");

                } else if (TextUtils.isEmpty(st_mobile.getText().toString())) {
                    st_mobile.setError("field can't be empty");

                } else if (st_mobile.getText().toString().length() < 10 || st_mobile.getText().toString().length() > 13) {
                    Toast.makeText(Signup.this, "Please enter valid phone number", Toast.LENGTH_SHORT).show();

                } else if (TextUtils.isEmpty(st_email.getText().toString())) {
                    st_email.setError("field can't be empty");

                } else if (!st_email.getText().toString().matches(emailPattern)) {
                    Toast.makeText(Signup.this, "Please Enter The Correct Email", Toast.LENGTH_SHORT).show();

                } else if (TextUtils.isEmpty(st_password.getText().toString())) {
                    st_password.setError("field can't be empty");

                } else if (TextUtils.isEmpty(multiSpin.getText().toString())) {
                    multiSpin.setError("field can't be empty");

                } else {


                    Intent intentSign = new Intent(Signup.this, SignUpStepTwo.class);
                    intentSign.putExtra("name", st_name.getText().toString());
                    intentSign.putExtra("mobile", st_mobile.getText().toString());
                    intentSign.putExtra("email", st_email.getText().toString());
                    intentSign.putExtra("password", st_password.getText().toString());
//                    intentSign.putExtra("exam", ExamID);
                    intentSign.putExtra("exam", "1");
                    startActivity(intentSign);
                }

            }
        });


    }
    private void hitExamApi() {

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        JSONObject jsonBody = new JSONObject();
        try {
            //input your API parameters
            jsonBody.put("teacher_id", "");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL_TEACHER+SELECT_EXAM, jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        final List<ExamModel> examModels = new ArrayList<>();
                        try {
                            // Toast.makeText(ChangePassword.this, "true", Toast.LENGTH_SHORT).show();

                            Boolean status = response.getBoolean("status");
                            if (status) {
                                JSONArray jArray = response.getJSONArray("data");
                                // Toast.makeText(context, "ccccc", Toast.LENGTH_SHORT).show();
                                //Toast.makeText(Home.this, "abc"+jArray, Toast.LENGTH_SHORT).show();
                                for (int i = 0; i < jArray.length(); i++) {
                                    JSONObject jsonObject = jArray.getJSONObject(i);
                                    Log.d("question_obj", " " + jsonObject);
                                    ExamModel entity = new ExamModel();
                                    entity.setName(jsonObject.getString("name"));
                                    entity.setId(jsonObject.getString("id"));
//
                                    examModels.add(entity);
                                }
                                get_dailog();
                                //LinearLayoutManager layoutManager = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
                                exam_list.setLayoutManager(new LinearLayoutManager(Signup.this));
                                myAdapter = new Signup.AdapterExamSpinner(Signup.this, examModels);
                                exam_list.setAdapter(myAdapter);


                            } else {
                                Toast.makeText(Signup.this, "" + response.getString("message"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {

                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Toast.makeText(Signup.this, "Server Error", Toast.LENGTH_SHORT).show();
            }
        });


        requestQueue.add(jsonObjectRequest);

    }


    private void get_dailog() {

        dialog = new Dialog(Signup.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_spinner_multiple);
        exam_list = (RecyclerView) dialog.findViewById(R.id.exam_list);

        final LinearLayoutManager layoutManager = new LinearLayoutManager(Signup.this);
        //layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        exam_list.setLayoutManager(layoutManager);
        exam_list.setItemAnimator(new DefaultItemAnimator());
        exam_list.setNestedScrollingEnabled(false);
        myAdapter = new Signup.AdapterExamSpinner(Signup.this, ruleslist);
        exam_list.setAdapter(myAdapter);
        multiSpin.setText("");
        get_ID.clear(); // this list which you hava passed in Adapter for your listview
        Show_select_jobName.clear(); // this list which you hava passed in Adapter for your listview
        myAdapter.notifyDataSetChanged(); // notify to listview for refresh

        Button button = dialog.findViewById(R.id.btn_ok);
        //Button button_cancel = dialog.findViewById(R.id.btn_cancel);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String P_id = "";
                StringBuilder pd = new StringBuilder();
                for (Object mPI : get_ID) {
                    pd.append(P_id).append(mPI);
                    P_id = ",";
                }
                ExamID = pd.toString();

                //System.out.println(IDs.toString());

                String S_Name = "";
                StringBuilder SB = new StringBuilder();
                for (Object mPI : Show_select_jobName) {
                    SB.append(S_Name).append(mPI);
                    S_Name = ",";
                }
                String Name = SB.toString();
                //System.out.println(IDs.toString());

                multiSpin.setText(Name);

                //Toast.makeText(BusinessUpload.this, "" + IDs, Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
       /* button_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });*/
        dialog.getWindow().getDecorView().setTop(100);
        dialog.getWindow().getDecorView().setLeft(100);
        dialog.show();
    }

    class AdapterExamSpinner extends RecyclerView.Adapter<Signup.AdapterExamSpinner.ItemViewHolder> {

        List<ExamModel> question_list;
        private Context a;

        public AdapterExamSpinner(Context context, List<ExamModel> question_list) {
            this.a = context;
            this.question_list = question_list;
        }

        @Override
        public Signup.AdapterExamSpinner.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.multi_adapter, parent, false);
            Signup.AdapterExamSpinner.ItemViewHolder viewHolder = new Signup.AdapterExamSpinner.ItemViewHolder(v);
            return viewHolder;
        }


        @Override
        public void onBindViewHolder(Signup.AdapterExamSpinner.ItemViewHolder holder, final int position) {

            holder.main_category.setText(question_list.get(position).getName());
            //holder.id.setText(question_list.get(position).getId());

        }

        @Override
        public int getItemCount() {
            //return question_list.size();

            return (null != question_list ? question_list.size() : 0);
        }

        public class ItemViewHolder extends RecyclerView.ViewHolder {
            TextView main_category, id;
            CheckBox checkBox;

            public ItemViewHolder(View itemView) {
                super(itemView);
                main_category = itemView.findViewById(R.id.exam_name);
                checkBox = itemView.findViewById(R.id.exam);
                //id = itemView.findViewById(R.id.name_id);

                checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override

                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        int pos = getAdapterPosition();
                        if (b) {
                            //True condition
                            String id = question_list.get(pos).getId();
                            String name = question_list.get(pos).getName();
                            get_ID.add(id);
                            Show_select_jobName.add(name);
                            //Job_name = name;
                        }
                        if (checkBox.isChecked() == false) {

                            get_ID.remove(question_list.get(pos).getId());
                            Show_select_jobName.remove(question_list.get(pos).getName());
                            // false condition
                        }
                    }
                });

            }
        }

    }

}


