package com.nusys.studentedumax;

import android.app.Dialog;
import android.os.Bundle;

import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.navigation.NavigationView;
import com.nusys.studentedumax.adapter.AdapterBatch;
import com.nusys.studentedumax.adapter.AdapterDoubtComment;
import com.nusys.studentedumax.adapter.AdapterQuesNav;
import com.nusys.studentedumax.commonModules.NetworkUtil;
import com.nusys.studentedumax.commonModules.SharedPreference_main;
import com.nusys.studentedumax.interfaces.GetQuesIds;
import com.nusys.studentedumax.model.HomePostModel;
import com.nusys.studentedumax.model.QuizOptions;
import com.nusys.studentedumax.model.ResponseQuestionList2Method;
import com.nusys.studentedumax.model.ResponseTestStart;
import com.nusys.studentedumax.retrofit.ApiClient;
import com.nusys.studentedumax.retrofit.ServiceInterface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

import static com.nusys.studentedumax.Constants.BASE_URL;
import static com.nusys.studentedumax.Constants.Content_Type;
import static com.nusys.studentedumax.Constants.FETCH_QUIZ_QUES;
import static com.nusys.studentedumax.Constants.RESUME_QUIZ_QUES;
import static com.nusys.studentedumax.commonModules.Extension.showErrorDialog;


public class QuesNevigation extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, GetQuesIds {
    DrawerLayout drawerLayout;
    Toolbar toolbar;

    public FrameLayout frameLayout;
    FrameLayout drawerButton;
    NavigationView navigationView;
    SharedPreference_main sharedPreference_main;
    RecyclerView recycleOptions;
    AdapterQuesNav Adapter;

    TextView navTestName, navNumberOfQues;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ques_nevigation);
        init();
    }

    private void init() {
        sharedPreference_main = SharedPreference_main.getInstance(getApplicationContext());
        toolbar = findViewById(R.id.toolbar);
        frameLayout = findViewById(R.id.content_frame);
        drawerLayout = findViewById(R.id.drawer_layouts);
        drawerButton = findViewById(R.id.drawer_button);
        navigationView = findViewById(R.id.nav_views);

        View header = navigationView.getHeaderView(0);
        navTestName = header.findViewById(R.id.nav_test_name);
        navNumberOfQues = header.findViewById(R.id.nav_number_of_questions);
        recycleOptions = header.findViewById(R.id.rv_options);
        // common_data_fetch_quiz_start();

        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        setSupportActionBar(toolbar);


        drawerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // open right drawer
                get_Quiz_ques();
                if (drawerLayout.isDrawerOpen(GravityCompat.END)) {
                    drawerLayout.closeDrawer(GravityCompat.END);
                } else
                    drawerLayout.openDrawer(GravityCompat.END);
            }
        });

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.setDrawerIndicatorEnabled(false);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {

        if (drawerLayout.isDrawerOpen(GravityCompat.END)) {
            drawerLayout.closeDrawer(GravityCompat.END);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        get_Quiz_ques();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        drawerLayout.closeDrawer(GravityCompat.END);
        return true;
    }

    private void get_Quiz_ques() {

        JSONObject parms = new JSONObject();
        try {

            parms.put("student_id",  sharedPreference_main.getUserId());
            parms.put("test_id", getIntent().getStringExtra("testId"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //creating json obeject request
        final RequestQueue requestQueue = Volley.newRequestQueue(this);
        JsonObjectRequest objectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL + RESUME_QUIZ_QUES, parms,
                new Response.Listener<JSONObject>() {
                    @Override
                    //if response is successful
                    public void onResponse(JSONObject response) {

                        Log.e("Quiz_response", String.valueOf(response));
                        try {
                            final List<ResponseQuestionList2Method> ab = new ArrayList<>();

                            //navNumberOfQues.setText("Questions: " + response.getString("total_questions"));
                            navNumberOfQues.setText("Questions: " + response.getInt("total_question"));
                            navTestName.setText(response.getString("test_name"));

                            JSONArray jsonarray = response.getJSONArray("data");
                            for (int i = 0; i < jsonarray.length(); i++) {

                                JSONObject object = jsonarray.getJSONObject(i);
                                ResponseQuestionList2Method setQus = new ResponseQuestionList2Method();

                                setQus.setQuestion(Html.fromHtml(object.getString("question")).toString());
                                setQus.setQuestion_id(object.getString("question_id"));
                                setQus.setAttempt_status(object.getInt("attempt_status"));
                                setQus.setReview_status(object.getInt("review_status"));
                                ab.add(setQus);

                                //String qua = object.getString("question");

                            }
                            Adapter = new AdapterQuesNav(QuesNevigation.this, ab, QuesNevigation.this);

                            recycleOptions.setLayoutManager(new GridLayoutManager(QuesNevigation.this, 5));
                            recycleOptions.setItemAnimator(new DefaultItemAnimator());
                            recycleOptions.setAdapter(Adapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.e("Rest Response", error.toString());
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0aW1lIjoxNTg3MzYxMDE4LCJkYXRhIjp7ImlkIjoiMTkiLCJuYW1lIjoiQWduZXNoIEFyeWEiLCJlbWFpbCI6ImFyeWEuYWduZXNoQGdtYWlsLmNvbSJ9fQ.3sg5FgeauiVcKfRbTpmnhUMPdsKjWg3plJxFGTeeOCk");
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        requestQueue.add(objectRequest);

    }
  /*  private void get_Quiz_ques() {

        JSONObject parms = new JSONObject();
        try {
            parms.put("test_id", getIntent().getStringExtra("testId"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //creating json obeject request
        final RequestQueue requestQueue = Volley.newRequestQueue(this);
        JsonObjectRequest objectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL + FETCH_QUIZ_QUES, parms,
                new Response.Listener<JSONObject>() {
                    @Override
                    //if response is successful
                    public void onResponse(JSONObject response) {

                        Log.e("Quiz_response", String.valueOf(response));
                        try {
                            final List<ResponseQuestionList2Method> ab = new ArrayList<>();

                            navNumberOfQues.setText("Questions: " + response.getString("total_questions"));
                            navTestName.setText(response.getString("test_name"));

                            JSONArray jsonarray = response.getJSONArray("data");
                            for (int i = 0; i < jsonarray.length(); i++) {

                                JSONObject object = jsonarray.getJSONObject(i);
                                ResponseQuestionList2Method setQus = new ResponseQuestionList2Method();

                                setQus.setQuestion(Html.fromHtml(object.getString("question")).toString());
                                setQus.setQuestion_id(object.getString("question_id"));
                                ab.add(setQus);

                                //String qua = object.getString("question");

                            }
                            Adapter = new AdapterQuesNav(QuesNevigation.this, ab, QuesNevigation.this);

                            recycleOptions.setLayoutManager(new GridLayoutManager(QuesNevigation.this, 5));
                            recycleOptions.setItemAnimator(new DefaultItemAnimator());
                            recycleOptions.setAdapter(Adapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.e("Rest Response", error.toString());
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0aW1lIjoxNTg3MzYxMDE4LCJkYXRhIjp7ImlkIjoiMTkiLCJuYW1lIjoiQWduZXNoIEFyeWEiLCJlbWFpbCI6ImFyeWEuYWduZXNoQGdtYWlsLmNvbSJ9fQ.3sg5FgeauiVcKfRbTpmnhUMPdsKjWg3plJxFGTeeOCk");
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        requestQueue.add(objectRequest);

    }*/

    @Override
    public void getQuesId(String n) {
        if (n.isEmpty()) {

        } else {
            drawerLayout.closeDrawer(GravityCompat.END);
        }
    }
}
