package com.nusys.studentedumax;

public class Constants {
 /*   public static final String BASE_URL = "http://13.233.162.24/nusys(UAT)/api/student/";
    public static final String BASE_URL_TEACHER = "http://13.233.162.24/nusys(UAT)/api/teacher/";*/
    public static final String BASE_URL = "https://edumaxe.com/api/student/";
    public static final String BASE_URL_TEACHER = "https://edumaxe.com/api/teacher/";
    public static final String WEBVIEW_BASE_URL = "https://edumaxe.com/student/";

    public static final int GALLERY_REQUEST_CODE = 101;
    public static final int CAMERA_REQUEST_CODE = 102;
    public static final String Content_Type = "application/json";

    public static final String LOGIN = "login.php";//Checked
    public static final String SIGNUP = "register.php";//Checked
    public static final String SELECT_EXAM = "select_exanination.php";//Checked
    public static final String TEACH_FOLLOW = "show_teacher_follow.php";//Checked
    public static final String CHANGE_PWD = "change_password.php";//checked
    public static final String STUDENT_FOLLOWER = "student_followers.php";//checked
    public static final String STUDENT_EXAM = "student_exam.php";//checked
    public static final String STUDENT_SUB = "select_subject.php";//checked
    public static final String STUDENT_UNIT = "select_unit.php";//checked
    public static final String STUDENT_CHAP = "GetChapterData.php";//checked
    public static final String STUDENT_DOUBT = "fetch_student_doubt.php";//checked
    public static final String STUDENT_DOUBT_SATISFY = "student_doubt_satsify.php";//checked
    public static final String STUDENT_DOUBT_ADD = "add_doubt.php";//checked
    public static final String STUDENT_DOUBT_COMMENT_VIEW = "fetch_doubts_comment.php";//checked
    public static final String STUDENT_DOUBT_COMMENT_ADD = "add_doubt_comment.php";//checked
    public static final String CREATE_POST = "create_post.php";//Checked
    public static final String VIEW_POST = "view_post.php";//Checked
    public static final String SINGLE_POST = "single_post.php";//Checked
    public static final String MAKE_LIKE = "make_like.php";//Checked
    public static final String POST_COMMENT = "post_comment.php";//Checked
    public static final String MAKE_COMMENT = "make_comment.php";//Checked
    public static final String PROFILE = "profile.php";//Checked
    public static final String GET_USER_PROFILE = "getUserProfile.php";//Checked
    public static final String PROFILE_PIC_UPDATE = "update_profile_pic.php";//checked
    public static final String WEBVIEW_LOGIN = "auto_login.php";//Checked
    public static final String UPDATE_PROFILE = "updateprofile.php";//checked
    public static final String GET_ALL_STU_GROUP = "get_all_studentGroup.php";//checked
    public static final String VIEW_GROUP = "viewGroup.php";//checked
    public static final String DELETE_STU_GROUP = "deleteStudentGroup.php";//checked
    public static final String UPDATE_GROUP_NAME = "updateGroupname.php";//checked
    public static final String UPDATE_GROUP_PIC = "updateGroupPic.php";//checked
    public static final String REMOVE_STU_GROUP = "removeStudentGroup.php";//checked
    public static final String GET_STU_BATCH = "getStudentBatch.php";//checked
    public static final String GET_STU_LIVECLASS = "getStudentLiveClass.php";//checked
    public static final String GET_BATCH_STU = "getBatchStudent.php";//checked
    public static final String GET_LIVECLASS_STU = "getLiveClassStudent.php";//checked
    public static final String CREATE_STU_GROUP = "create_student_group.php";//checked
    public static final String STU_BY_GROUPTYPE = "getallStudentByGroupType.php";//checked
    public static final String ADD_STU_TO_GROUP = "addStudentToGroup.php";//checked
    public static final String STU_TEST_LIST = "allstudentTestList.php";//checked
    public static final String TEST_START_BUTTON = "testStartButtonAction.php";//checked
    public static final String QUES_SUBMIT = "submitques.php";
    public static final String FETCH_QUIZ_QUES = "FetchQuizQuestion.php";//checked
    public static final String RESUME_QUIZ_QUES = "exam_resume.php";//checked
    public static final String FETCH_QUES_COUNT = "answered_unanswered_question_count.php";//checked
    public static final String FINAL_QUIZ_SUBMIT = "submit_exam.php";//checked
    public static final String QUIZ_RESULT = "exam_result.php";//checked
    public static final String QUIZ_REVIEW = "exam_review.php";//checked
    public static final String QUES_REVIEW = "submit_review.php";//checked
    public static final String VIEW_COURSE = "viewCourse.php";//checked
    public static final String FORGOT_PWD = "forgot_password.php";


    //for capitalize each word
    public static String getCapsSentences(String tagName) {
        String[] splits = tagName.toLowerCase().split(" ");
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < splits.length; i++) {
            String eachWord = splits[i];
            if (i > 0 && eachWord.length() > 0) {
                sb.append(" ");
            }
            String cap = eachWord.substring(0, 1).toUpperCase()
                    + eachWord.substring(1);
            sb.append(cap);
        }
        //Toast.makeText(context, ""+sb.toString(), Toast.LENGTH_SHORT).show();
        //capText=sb.toString();
        return sb.toString();
    }
}
