package com.nusys.studentedumax;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.animation.Animator;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import com.nusys.studentedumax.commonModules.DialogProgress;
import com.nusys.studentedumax.commonModules.NetworkUtil;
import com.nusys.studentedumax.commonModules.SharedPreference_main;
import com.nusys.studentedumax.model.QuizOptions;
import com.nusys.studentedumax.model.ResponseQuesReview;
import com.nusys.studentedumax.model.ResponseQuesSubmit;
import com.nusys.studentedumax.model.ResponseQuestionList;
import com.nusys.studentedumax.model.ResponseQuestionList2Method;
import com.nusys.studentedumax.model.ResponseQuizSubmit;
import com.nusys.studentedumax.model.ResponseTestStart;
import com.nusys.studentedumax.retrofit.ApiClient;
import com.nusys.studentedumax.retrofit.ServiceInterface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

import static com.nusys.studentedumax.Constants.BASE_URL;
import static com.nusys.studentedumax.Constants.Content_Type;
import static com.nusys.studentedumax.Constants.FETCH_QUIZ_QUES;
import static com.nusys.studentedumax.Constants.RESUME_QUIZ_QUES;
import static com.nusys.studentedumax.commonModules.Extension.showErrorDialog;

public class PlayQuiz2Method extends QuesNevigation {

    SharedPreference_main sharedPreference_main;
    Context context;
    private ProgressDialog pd;
    //for timer
    TextView tv_test_name, tv_number_of_questions, tv_timer, mButtonStartPause;
    private TextView mTextViewCountDown;
    private CountDownTimer mCountDownTimer;
    private boolean mTimerRunning;
    private long mStartTimeInMillis;
    private long mTimeLeftInMillis;
    private long mEndTime;
    String input;
    String timeLeftFormatted;
    String timeLeftFormattedSet, totalTime, calculateTime, startTime1, timeLeftFormattedSetNav = "", timeLeftFormattedSetSkip="";
    long startTime;
    java.util.Date dateStart;
    //for quiz
    FloatingActionButton fabSkip;
    TextView tvOption1, tvOption2, tvOption3, tvOption, tvNumIndicator, tvReview, tvReviewMarked, tvNotReviewed, tvClearResponse;
    LinearLayout llClickMark;
    TextView tvSubmitQuiz, tvQuesMarking;
    TextView tvQuestions, tvSubmitQues;
    LinearLayout optionsContainer;
    int count = 0;
    int countReview = 0;
    ArrayList<ResponseQuestionList2Method> list;
    ResponseQuestionList2Method setQus;
    QuizOptions setOpt;
    int position = 0;
    String optionIdsubmit, questionId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_play_quiz2_method, frameLayout);
        //setContentView(R.layout.activity_play_quiz2_method);

        init();
    }

    public void init() {
        sharedPreference_main = SharedPreference_main.getInstance(this);
        context = PlayQuiz2Method.this;
        pd = new DialogProgress(context, "");
        pd.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        pd.setCancelable(false);


        list = new ArrayList<>();
        tv_test_name = findViewById(R.id.tv_test_name);
        tv_number_of_questions = findViewById(R.id.tv_number_of_questions);
        // mTextViewCountDown = findViewById(R.id.tv_timer);
        mButtonStartPause = findViewById(R.id.tv_submit_ques);

        fabSkip = findViewById(R.id.fab_skip);
        tvQuestions = findViewById(R.id.tv_questions);
        tvOption1 = findViewById(R.id.tv_option1);
        tvOption2 = findViewById(R.id.tv_option2);
        tvOption3 = findViewById(R.id.tv_option3);
        tvOption = findViewById(R.id.tv_option);
        tvNumIndicator = findViewById(R.id.tv_num_indicator);
        tvClearResponse = findViewById(R.id.tv_clear_response);
        tvReview = findViewById(R.id.tv_review);
        tvReviewMarked = findViewById(R.id.tv_review_marked);
        tvNotReviewed = findViewById(R.id.tv_not_reviewed);
        tvQuesMarking = findViewById(R.id.tv_ques_marking);
        tvSubmitQuiz = findViewById(R.id.tv_submit_quiz);
        // llClickMark = findViewById(R.id.ll_click_mark);
        optionsContainer = findViewById(R.id.options_container);

        /* for skip question
         * Created Date: 12-06-2020
         */
        fabSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //for marked options visibility
                enableOption(true);
                // setting date1 for finding difference in time
                timeLeftFormattedSetSkip = timeLeftFormattedSet;

                //increment of position and goes to other
                position++;
                count = 0;
                if (position < list.size()) {
                    playAnim(tvQuestions, 0, list.get(position).getQuestion());

                } else {

                    String countReviewInString = String.valueOf(countReview);
                    String countTotalQuesInString = String.valueOf(list.size());
                    Intent i = new Intent(context, ViewFinalSubmitQuiz.class);
                    i.putExtra("reviewCount", countReviewInString);
                    i.putExtra("totalQuesCount", countTotalQuesInString);
                    i.putExtra("examId", getIntent().getStringExtra("testId"));

                    startActivity(i);
                }
            }
        });


        /* for submit quiz directly
         * Created Date: 11-06-2020
         * Updated Date: 26-06-2020(add final submit quiz)
         */
        tvSubmitQuiz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.delete);
                dialog.getWindow().getDecorView().setTop(100);
                dialog.getWindow().getDecorView().setLeft(100);
                dialog.show();

                TextView tvText = dialog.findViewById(R.id.tv_text);
                tvText.setText("Are you sure you want to Submit?");
                Button bt_delete = dialog.findViewById(R.id.delete);
                bt_delete.setText("Submit");
                Button bt_cancel = dialog.findViewById(R.id.cancel);
                bt_delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        submit_final_quiz();
                        dialog.dismiss();

                    }
                });
                bt_cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        dialog.dismiss();
                    }
                });
                dialog.show();

            }
        });


        /* for visibility of Review
         * Created Date: 11-06-2020
         * Updated Date:25-06-2020(add question_review method)
         */
        tvNotReviewed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                question_review();

                tvReviewMarked.setText("Marked");
                tvReview.setVisibility(View.VISIBLE);
                tvNotReviewed.setVisibility(View.GONE);
                countReview++;
                //  Toast.makeText(context, ""+countReview, Toast.LENGTH_SHORT).show();

            }
        });
        /* for visibility of Review
         * Created Date: 11-06-2020
         * Updated Date:25-06-2020(add question_review method)
         */
        tvReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                question_review();
                tvReviewMarked.setText("Review");
                tvReview.setVisibility(View.GONE);
                tvNotReviewed.setVisibility(View.VISIBLE);

                countReview--;

            }
        });

        /* for clear response
         * Created Date: 10-06-2020
         */
        tvClearResponse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvClearResponse.setAlpha(1);
                mButtonStartPause.setEnabled(false);
                mButtonStartPause.setAlpha((float) 0.7);
                enableOption(true);
            }
        });
        /* method for fetching common data and start quiz
         * Created Date: 08-06-2020
         */
        common_data_fetch_quiz_start();
        /* method for fetching quiz questions and options
         * Created Date: 07-06-2020
         * Updated Date: 09-06-2020
         */
        get_Quiz(position);


        /* onclick on save and next button
         * Created Date: 05-06-2020
         * Updated Date: 08-06-2020, 09-06-2020,15-06-2020(setting timer in else condition)
         */

        mButtonStartPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // for visibility of button
                mButtonStartPause.setEnabled(false);
                mButtonStartPause.setAlpha((float) 0.7);
                // for submit individual quetion
                question_submit();

                //for marked options visibility
                enableOption(true);

                //increment of position and goes to other
                position++;
                count = 0;
                if (position < list.size()) {
                    playAnim(tvQuestions, 0, list.get(position).getQuestion());

                } else {
                    //for setting timer
                    if (mTimerRunning) {
                        pauseTimer();
                    } else {
                        startTimer();
                    }
                    String countReviewInString = String.valueOf(countReview);
                    String countTotalQuesInString = String.valueOf(list.size());
                    Intent i = new Intent(context, ViewFinalSubmitQuiz.class);
                    i.putExtra("reviewCount", countReviewInString);
                    i.putExtra("totalQuesCount", countTotalQuesInString);
                    i.putExtra("examId", getIntent().getStringExtra("testId"));
                    startActivity(i);
                }
            }
        });


        for (int i = 0; i < 4; i++) {
            final int finalI = i;
            optionsContainer.getChildAt(i).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    optionIdsubmit = list.get(position).getQuizOptions().get(finalI).getOption_id();
                    checkAnswer((TextView) v);
                }
            });
        }

    }
    /* method for setting options and sets different things
     * Created Date: 05-06-2020
     * Updated Date: 24-06-2020(add if condition for attempted questions)
     * Updated Date: 25-06-2020(1.remove questionId from if conditions and add outside to condition 2. remove quetionId and optionId from conditions )
     * Updated Date: 25-06-2020(3. add optionIdsubmit for option value when not click on option and do direct submit)
     */
    private void playAnim(final View view, final int value, final String data) {
        view.animate().alpha(value).scaleX(value).scaleY(value).setDuration(500).setStartDelay(100)
                .setInterpolator(new DecelerateInterpolator()).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

                if (value == 0 && count < 4) {
                    String option = "";
                    questionId = list.get(position).getQuestion_id();
                    if (list.get(position).getAttempt_status().equals(1)) {

                        if (count == 0) {
                            if (list.get(position).getQuizOptions().get(0).getYour_answer().equals(1)) {
                                optionIdsubmit=list.get(position).getQuizOptions().get(0).getOption_id();
                                checkAnswer((TextView) optionsContainer.getChildAt(count));
                            } else {
                                optionsContainer.getChildAt(0).setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#FF0099CC")));
                            }
                            option = list.get(position).getQuizOptions().get(0).getOption();

                        } else if (count == 1) {
                            if (list.get(position).getQuizOptions().get(1).getYour_answer().equals(1)) {
                                optionIdsubmit=list.get(position).getQuizOptions().get(1).getOption_id();

                                checkAnswer((TextView) optionsContainer.getChildAt(count));
                            } else {
                                optionsContainer.getChildAt(1).setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#FF0099CC")));
                            }
                            option = list.get(position).getQuizOptions().get(1).getOption();


                        } else if (count == 2) {
                            if (list.get(position).getQuizOptions().get(2).getYour_answer().equals(1)) {
                                optionIdsubmit=list.get(position).getQuizOptions().get(2).getOption_id();

                                checkAnswer((TextView) optionsContainer.getChildAt(count));
                            } else {
                                optionsContainer.getChildAt(2).setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#FF0099CC")));
                            }
                            option = list.get(position).getQuizOptions().get(2).getOption();


                        } else if (count == 3) {
                            if (list.get(position).getQuizOptions().get(3).getYour_answer().equals(1)) {
                                optionIdsubmit=list.get(position).getQuizOptions().get(3).getOption_id();

                                checkAnswer((TextView) optionsContainer.getChildAt(count));
                            } else {
                                optionsContainer.getChildAt(3).setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#FF0099CC")));
                            }
                            option = list.get(position).getQuizOptions().get(3).getOption();


                        }
                    } else {

                        if (count == 0) {
                            option = list.get(position).getQuizOptions().get(0).getOption();

                        } else if (count == 1) {
                            option = list.get(position).getQuizOptions().get(1).getOption();

                        } else if (count == 2) {
                            option = list.get(position).getQuizOptions().get(2).getOption();

                        } else if (count == 3) {
                            option = list.get(position).getQuizOptions().get(3).getOption();

                        }
                        enableOption(true);
                    }
                    playAnim(optionsContainer.getChildAt(count), 0, option);
                    count++;
                }
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (value == 0) {
                    try {
                        ((TextView) view).setText(data);
                        // set number indicator
                        tvNumIndicator.setText(position + 1 + "/" + list.size());

                        //set marking of question acc to position
                        tvQuesMarking.setText("+" + list.get(position).getMarks() + "/-" + list.get(position).getNegative_mark());

                        /**set review when position is changed
                         * Updated Date:25-06-2020(fetch data from database
                         **/

                        if (list.get(position).getReview_status().equals(1)){
                            tvReviewMarked.setText("Marked");
                            tvReview.setVisibility(View.VISIBLE);
                            tvNotReviewed.setVisibility(View.GONE);
                        }else{
                            tvReviewMarked.setText("Review");
                            tvReview.setVisibility(View.GONE);
                            tvNotReviewed.setVisibility(View.VISIBLE);
                        }

                    } catch (ClassCastException ex) {
                        ((TextView) view).setText(data);
                    }
                    // view.setTag(data);
                    playAnim(view, 1, data);
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

    }

    private void checkAnswer(TextView selectedOption) {
        enableOption(false);
        mButtonStartPause.setEnabled(true);
        mButtonStartPause.setAlpha(1);
        //green
        selectedOption.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#FF99CC00")));

    }

    private void enableOption(boolean enable) {
        for (int i = 0; i < 4; i++) {
            // optionsContainer.getChildAt(i).setEnabled(enable);
            if (enable) {
                //gray
                optionsContainer.getChildAt(i).setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#FF000000")));

            } else {
                //blue
                optionsContainer.getChildAt(i).setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#FF0099CC")));

            }
        }
    }

    /**
     * method for fetching quiz questions and options
     * Created Date: 07-06-2020
     * Updated Date: 24-06-2020(change api name fetch_quiz_ques to resume_quiz_ques)
     * Updated Date: 25-06-2020(1.add pos argument in a get_quiz method 2. call playAnim method outside from loop )
     **/
    private void get_Quiz(final int pos) {
        if (NetworkUtil.isConnected(context)) {
            pd.show();
            JSONObject parms = new JSONObject();

            try {
                parms.put("student_id", sharedPreference_main.getUserId());
                parms.put("test_id", getIntent().getStringExtra("testId"));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            //creating json obeject request
            final RequestQueue requestQueue = Volley.newRequestQueue(this);
            JsonObjectRequest objectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL + RESUME_QUIZ_QUES, parms,
                    new Response.Listener<JSONObject>() {
                        @Override
                        //if response is successful
                        public void onResponse(JSONObject response) {

                            list.clear();
                            Log.e("Quiz_response", String.valueOf(response));
                            try {
                                //get from strDescription of 0th row of ingridients
                                JSONArray jsonarray = response.getJSONArray("data");
                                for (int i = 0; i < jsonarray.length(); i++) {
                                    JSONObject object = jsonarray.getJSONObject(i);
                                    setQus = new ResponseQuestionList2Method();
                                    setQus.setMarks(object.getString("marks"));
                                    setQus.setNegative_mark(object.getString("negative_mark"));
                                    setQus.setAttempt_status(object.getInt("attempt_status"));
                                    setQus.setReview_status(object.getInt("review_status"));


                                    setQus.setQuestion(Html.fromHtml(object.getString("question")).toString());
                                    setQus.setQuestion_id(object.getString("question_id"));

                                    JSONArray jsonArray = object.getJSONArray("options");
                                    List<QuizOptions> quizOptionsList = new ArrayList<>();
                                    for (int j = 0; j < jsonArray.length(); j++) {
                                        JSONObject jb = jsonArray.getJSONObject(j);

                                        setOpt = new QuizOptions();
                                        setOpt.setOption(Html.fromHtml(jb.getString("option")).toString());
                                        setOpt.setOption_id(jb.getString("option_id"));
                                        setOpt.setQuestion_id(jb.getString("question_id"));
                                        setOpt.setYour_answer(jb.getInt("your_answer"));
                                        quizOptionsList.add(setOpt);

                                    }
                                    setQus.setQuizOptions(quizOptionsList);
                                    list.add(setQus);

                                    //String qua = object.getString("question");

                                }
                                playAnim(tvQuestions, 0, list.get(pos).getQuestion());

                                pd.dismiss();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            pd.dismiss();
                            Log.e("Rest Response", error.toString());
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0aW1lIjoxNTg3MzYxMDE4LCJkYXRhIjp7ImlkIjoiMTkiLCJuYW1lIjoiQWduZXNoIEFyeWEiLCJlbWFpbCI6ImFyeWEuYWduZXNoQGdtYWlsLmNvbSJ9fQ.3sg5FgeauiVcKfRbTpmnhUMPdsKjWg3plJxFGTeeOCk");
                    params.put("Content-Type", "application/json");
                    return params;
                }
            };
            requestQueue.add(objectRequest);
        } else {
            showErrorDialog(context, new Dialog(context));
        }
    }


    private void common_data_fetch_quiz_start() {

        HashMap<String, String> map = new HashMap<>();

        //map.put("test_id", "2");
        map.put("student_id", sharedPreference_main.getUserId());
        map.put("test_id", getIntent().getStringExtra("testId"));
        map.put("language", getIntent().getStringExtra("language"));

        // Toast.makeText(context, "" + getIntent().getStringExtra("testId"), Toast.LENGTH_SHORT).show();
        //   if (NetworkUtils.isConnected(getActivity())) {
        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<ResponseTestStart> call = serviceInterface.quiz_start("Bearer " + sharedPreference_main.getToken(), Content_Type, map);
        call.enqueue(new Callback<ResponseTestStart>() {
            @Override
            public void onResponse(Call<ResponseTestStart> call, retrofit2.Response<ResponseTestStart> response) {

                if (response.isSuccessful()) {

                    ResponseTestStart bean = response.body();

                    // Toast.makeText(GroupActivity.this, "sucess", Toast.LENGTH_SHORT).show();
                    if (bean.getStatus()) {
                        ResponseTestStart view = response.body();
                        //Toast.makeText(AddNewMembersToGroup.this, "gfcfdhgjkd", Toast.LENGTH_SHORT).show();

                        // tv_timer.setText(bean.getData().getStartTime());
                        tv_test_name.setText(bean.getTestName());
                        //Toast.makeText(PlayQuiz.this, ""+bean.getTotalQuestions(), Toast.LENGTH_SHORT).show();

                        tv_number_of_questions.setText("Questions: " + bean.getTotalQuestions().toString());
                        //  Toast.makeText(PlayQuiz.this, "abc" + bean.getSessionTime(), Toast.LENGTH_SHORT).show();
                        input = bean.getSessionTime();
                        // input = "60";
                        startTime = Long.parseLong(input) * 60000;
                        long millisInput = Long.parseLong(input) * 60000;
                        setTime(millisInput);
                        startTimer();


                        Log.e("test_response", view.toString());

                    } else {
                        Toast.makeText(context, bean.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                } else {

                    Toast.makeText(context, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseTestStart> call, Throwable t) {
                Log.e("error", t.getMessage());
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void question_submit() {

        HashMap<String, String> map = new HashMap<>();

        String calTime = calculateTime();

        //map.put("test_id", "2");
        map.put("student_id", sharedPreference_main.getUserId());
        map.put("exam_id", getIntent().getStringExtra("testId"));
        map.put("question_id", questionId);
        map.put("option_id", optionIdsubmit);
        map.put("time", calTime);


        //   if (NetworkUtils.isConnected(getActivity())) {
        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<ResponseQuesSubmit> call = serviceInterface.ques_submit("Bearer " + sharedPreference_main.getToken(), Content_Type, map);
        call.enqueue(new Callback<ResponseQuesSubmit>() {
            @Override
            public void onResponse(Call<ResponseQuesSubmit> call, retrofit2.Response<ResponseQuesSubmit> response) {

                if (response.isSuccessful()) {

                    ResponseQuesSubmit bean = response.body();

                    // Toast.makeText(GroupActivity.this, "sucess", Toast.LENGTH_SHORT).show();
                    if (bean.getStatus()) {
                        ResponseQuesSubmit view = response.body();

                        Toast.makeText(context, "" + bean.getMessage(), Toast.LENGTH_SHORT).show();


                    } else {
                        Toast.makeText(context, bean.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                } else {

                    Toast.makeText(context, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseQuesSubmit> call, Throwable t) {
                Log.e("error", t.getMessage());
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    /** method for review question
     * Created Date:25-06-2020
     */
    private void question_review() {

        HashMap<String, String> map = new HashMap<>();

        map.put("student_id", sharedPreference_main.getUserId());
        map.put("exam_id", getIntent().getStringExtra("testId"));
        map.put("question_id", questionId);
        map.put("option_id", "0");

        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<ResponseQuesReview> call = serviceInterface.ques_review("Bearer " + sharedPreference_main.getToken(), Content_Type, map);
        call.enqueue(new Callback<ResponseQuesReview>() {
            @Override
            public void onResponse(Call<ResponseQuesReview> call, retrofit2.Response<ResponseQuesReview> response) {

                if (response.isSuccessful()) {

                    ResponseQuesReview bean = response.body();
                    if (bean.getStatus()) {
                        ResponseQuesReview view = response.body();

                        Toast.makeText(context, "" + bean.getMessage(), Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(context, bean.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                } else {

                    Toast.makeText(context, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseQuesReview> call, Throwable t) {
                Log.e("error", t.getMessage());
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    /* for final submit quiz
     * Created Date: 26-06-2020
     * Updated Date:
     */
    private void submit_final_quiz() {

        HashMap<String, String> map = new HashMap<>();

        map.put("student_id", sharedPreference_main.getUserId());
        map.put("exam_id", getIntent().getStringExtra("testId"));
        map.put("q_id", "");


        // Toast.makeText(context, "" + getIntent().getStringExtra("testId"), Toast.LENGTH_SHORT).show();
        //   if (NetworkUtils.isConnected(getActivity())) {
        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<ResponseQuizSubmit> call = serviceInterface.final_quiz_submit("Bearer " + sharedPreference_main.getToken(), Content_Type, map);
        call.enqueue(new Callback<ResponseQuizSubmit>() {
            @Override
            public void onResponse(Call<ResponseQuizSubmit> call, retrofit2.Response<ResponseQuizSubmit> response) {

                if (response.isSuccessful()) {

                    ResponseQuizSubmit bean = response.body();

                    // Toast.makeText(GroupActivity.this, "sucess", Toast.LENGTH_SHORT).show();
                    if (bean.getStatus()) {
                        ResponseQuizSubmit view = response.body();

                        Toast.makeText(context, bean.getMessage(), Toast.LENGTH_SHORT).show();
                        if (sharedPreference_main.getTest_type().equals("exam")){
                            Intent i = new Intent(context, ExamTest.class);
                            i.putExtra("test_type", "Exam");
                            startActivity(i);
                        }else if (sharedPreference_main.getTest_type().equals("practice")){
                            Intent i = new Intent(context, PracticeTest.class);
                            i.putExtra("test_type", "Practice");
                            startActivity(i);
                        }





                    } else {
                        Toast.makeText(context, bean.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                } else {

                    Toast.makeText(context, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseQuizSubmit> call, Throwable t) {
                Log.e("error", t.getMessage());
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    /* method for calculate time taken by particular question when click on submit button
     *Created Date:15-06-2020
     * Updated Date:24-06-2020(add else if condition for timeLeftFormattedSetNav and timeLeftFormattedSetSkip)
     */
    private String calculateTime() {

        startTime1 = convertInFormat(startTime);


        java.text.DateFormat df = new java.text.SimpleDateFormat("hh:mm:ss");
        java.util.Date date1 = null;
        try {
            if (position == 0) {
                date1 = df.parse(startTime1);
            } else if (!timeLeftFormattedSetNav.isEmpty()) {
                date1 = df.parse(timeLeftFormattedSetNav);
            } else if (!timeLeftFormattedSetSkip.isEmpty()) {
                date1 = df.parse(timeLeftFormattedSetSkip);
            } else {
                date1 = dateStart;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }


        java.util.Date date2 = null;
        try {
            date2 = df.parse(timeLeftFormattedSet);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long diff = date1.getTime() - date2.getTime();

        calculateTime = convertInFormat(diff);
       // Toast.makeText(PlayQuiz2Method.this, "a" + calculateTime, Toast.LENGTH_SHORT).show();
        dateStart = date2;
        return calculateTime;

    }

    //** for convert time in hh:mm:ss format **//
    private String convertInFormat(long millisInput) {
        int hours = (int) (millisInput / 1000) / 3600;
        int minutes = (int) ((millisInput / 1000) % 3600) / 60;
        int seconds = (int) (millisInput / 1000) % 60;
        totalTime = String.format(Locale.getDefault(),
                "%d:%02d:%02d", hours, minutes, seconds);
        return totalTime;
    }

    //** for setting timer 7 methods**//
    private void setTime(long milliseconds) {
        mStartTimeInMillis = milliseconds;
        resetTimer();
        closeKeyboard();
    }

    private void startTimer() {
        mEndTime = System.currentTimeMillis() + mTimeLeftInMillis;

        mCountDownTimer = new CountDownTimer(mTimeLeftInMillis, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                mTimeLeftInMillis = millisUntilFinished;
                updateCountDownText();
            }

            @Override
            public void onFinish() {
                mTimerRunning = false;
                updateWatchInterface();
                //** add by me bec when end timers time then automatically finish quiz
                finish();
                //submit_quiz();

            }
        }.start();

        mTimerRunning = true;
        updateWatchInterface();
    }

    private void pauseTimer() {
        mCountDownTimer.cancel();
        mTimerRunning = false;
        updateWatchInterface();
        // Toast.makeText(context, "left timing" + timeLeftFormatted, Toast.LENGTH_SHORT).show();
        //** call method for submit quiz partially
        // partially_submit_quiz();
    }

    private void resetTimer() {
        mTimeLeftInMillis = mStartTimeInMillis;
        updateCountDownText();
        updateWatchInterface();
    }

    private void updateCountDownText() {
        int hours = (int) (mTimeLeftInMillis / 1000) / 3600;
        int minutes = (int) ((mTimeLeftInMillis / 1000) % 3600) / 60;
        int seconds = (int) (mTimeLeftInMillis / 1000) % 60;
        timeLeftFormattedSet = String.format(Locale.getDefault(),
                "%d:%02d:%02d", hours, minutes, seconds);

        if (hours > 0) {
            timeLeftFormatted = String.format(Locale.getDefault(),
                    "%d:%02d:%02d", hours, minutes, seconds);
        } else {
            timeLeftFormatted = String.format(Locale.getDefault(),
                    "%02d:%02d", minutes, seconds);
        }

        setTitle(timeLeftFormatted);

        // Toast.makeText(context, "left timing"+timeLeftFormatted, Toast.LENGTH_SHORT).show();
    }

    private void updateWatchInterface() {
        if (mTimerRunning) {
            //mEditTextInput.setVisibility(View.INVISIBLE);
            //mButtonSet.setVisibility(View.INVISIBLE);
            //  mButtonReset.setVisibility(View.INVISIBLE);
            //mButtonStartPause.setText("Pause");
        } else {
            // mEditTextInput.setVisibility(View.VISIBLE);
            //mButtonSet.setVisibility(View.VISIBLE);
            //  mButtonStartPause.setText("Start");

            if (mTimeLeftInMillis < 1000) {
                mButtonStartPause.setVisibility(View.INVISIBLE);
            } else {
                mButtonStartPause.setVisibility(View.VISIBLE);
            }

            if (mTimeLeftInMillis < mStartTimeInMillis) {
                //mButtonReset.setVisibility(View.VISIBLE);
            } else {
                // mButtonReset.setVisibility(View.INVISIBLE);
            }
        }
    }

    private void closeKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    /* for fetching question id from navigation adapter
     * Created Date: 17-06-2020
     * Updated Date: 24-06-2020(add timeLeftFormattedSetNav for taking time in ques submit)
     * Updated Date: 25-06-2020(add get_Quiz method with position value for refreshing question with latest updated value)
     */
    @Override
    public void getQuesId(String n) {
        super.getQuesId(n);

        position = Integer.parseInt(n);
        timeLeftFormattedSetNav = timeLeftFormattedSet;
        count = 0;

        get_Quiz(position);
        //playAnim(tvQuestions, 0, list.get(position).getQuestion());
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Toast.makeText(context, "Quiz paused", Toast.LENGTH_SHORT).show();
        if (sharedPreference_main.getTest_type().equals("exam")){
            Intent i = new Intent(context, ExamTest.class);
            i.putExtra("test_type", "Exam");
            startActivity(i);
        }else if (sharedPreference_main.getTest_type().equals("practice")){
            Intent i = new Intent(context, PracticeTest.class);
            i.putExtra("test_type", "Practice");
            startActivity(i);
        }
        finish();
    }
}
