package com.nusys.studentedumax;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.nusys.studentedumax.adapter.AdapterBatch;
import com.nusys.studentedumax.adapter.AdapterCourseExam;
import com.nusys.studentedumax.commonModules.DialogProgress;
import com.nusys.studentedumax.commonModules.NetworkUtil;
import com.nusys.studentedumax.commonModules.SharedPreference_main;
import com.nusys.studentedumax.model.GetExamResponseModel;
import com.nusys.studentedumax.model.ResponeStudentBatch;
import com.nusys.studentedumax.retrofit.ApiClient;
import com.nusys.studentedumax.retrofit.ServiceInterface;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;

import static com.nusys.studentedumax.Constants.Content_Type;
import static com.nusys.studentedumax.commonModules.Extension.showErrorDialog;

public class CourseFragment extends Fragment{
    SwipeRefreshLayout refreshLayout;

    ProgressBar mainProgress;
    LinearLayout errorLayout;
    TextView errorTxtCause;
    Button errorBtnRetry;
    Context context;

    SharedPreference_main sharedPreference_main;
    RecyclerView recyclerView_course_view;
    AdapterCourseExam Adapter;
    private ProgressDialog pd;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.activity_course_fragment, container, false);


        sharedPreference_main = SharedPreference_main.getInstance(getActivity());
        refreshLayout = view.findViewById(R.id.refresh_layout);
        mainProgress = view.findViewById(R.id.main_progress);
        errorLayout = view.findViewById(R.id.error_layout);
        errorTxtCause = view.findViewById(R.id.error_txt_cause);
        errorBtnRetry = view.findViewById(R.id.error_btn_retry);

        pd = new DialogProgress(getContext(), "");
        pd.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        pd.setCancelable(false);

        recyclerView_course_view = view.findViewById(R.id.rv_course_view);
        getExams();
        return view;

    }
    private void getExams() {
        if (NetworkUtil.isConnected(getContext())) {

            pd.show();
        HashMap<String, String> map = new HashMap<>();
        map.put("student_id", sharedPreference_main.getUserId());
        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<GetExamResponseModel> call = serviceInterface.getMyExams("Bearer " + sharedPreference_main.getToken(), Content_Type, map);
        call.enqueue(new Callback<GetExamResponseModel>() {
            @Override
            public void onResponse(Call<GetExamResponseModel> call, retrofit2.Response<GetExamResponseModel> response) {
                if (response.isSuccessful()) {
                    if (response.isSuccessful()) {

                        GetExamResponseModel bean = response.body();

                        //Toast.makeText(batch_Activity.this, "sucess", Toast.LENGTH_SHORT).show();
                        if (bean.getStatus()) {


                            Adapter = new AdapterCourseExam(getActivity(), bean.getData());
                           // recyclerView_course_view.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
                            recyclerView_course_view.setLayoutManager(new GridLayoutManager(getActivity(), 2));
                            recyclerView_course_view.setItemAnimator(new DefaultItemAnimator());
                            recyclerView_course_view.setAdapter(Adapter);
                            pd.dismiss();
                        } else {
                            pd.dismiss();
                            Toast.makeText(getActivity(), bean.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        pd.dismiss();
                        Toast.makeText(getActivity(), "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    pd.dismiss();
                    Toast.makeText(getActivity(), "Something is wrong please try again later", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<GetExamResponseModel> call, Throwable t) {
                pd.dismiss();
            }
        });


    } else {
            showErrorDialog(getContext(), new Dialog(getContext()));
        }
    }
}
