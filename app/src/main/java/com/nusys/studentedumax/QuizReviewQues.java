package com.nusys.studentedumax;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.nusys.studentedumax.adapter.AdapterQuestionListReview;
import com.nusys.studentedumax.commonModules.SharedPreference_main;
import com.nusys.studentedumax.model.ResponseQuestionList;
import com.nusys.studentedumax.retrofit.ApiClient;
import com.nusys.studentedumax.retrofit.ServiceInterface;
import java.util.ArrayList;
import java.util.HashMap;
import retrofit2.Call;
import retrofit2.Callback;
import static com.nusys.studentedumax.Constants.Content_Type;

public class QuizReviewQues extends AppCompatActivity {


    SharedPreference_main sharedPreference_main;
    RecyclerView recycleQuestions;
    AdapterQuestionListReview Adapter;
    Context context;
    TextView tvTestName, tvSessionTime,tvSubmitTime,tvMarksFrom,tvScore;

    private ArrayList<String> ques_list = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_review_ques);


        sharedPreference_main = SharedPreference_main.getInstance(this);
        context = QuizReviewQues.this;

        recycleQuestions = findViewById(R.id.recycleQuestions);
        tvTestName = findViewById(R.id.tv_test_name);
        tvSessionTime = findViewById(R.id.tv_session_time);
        tvSubmitTime = findViewById(R.id.tv_submit_time);
        tvMarksFrom = findViewById(R.id.tv_marks_from);
        tvScore = findViewById(R.id.tv_score);


        view_question_list();
    }


    private void view_question_list() {

        HashMap<String, String> map = new HashMap<>();

        //map.put("test_id", "2");
        map.put("student_id",sharedPreference_main.getUserId());
        map.put("test_id", getIntent().getStringExtra("testId"));


        //   if (NetworkUtils.isConnected(getActivity())) {
        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<ResponseQuestionList> call = serviceInterface.quiz_review("Bearer " + sharedPreference_main.getToken(), Content_Type, map);
        call.enqueue(new Callback<ResponseQuestionList>() {
            @Override
            public void onResponse(Call<ResponseQuestionList> call, retrofit2.Response<ResponseQuestionList> response) {

                if (response.isSuccessful()) {

                    ResponseQuestionList bean = response.body();

                    // Toast.makeText(GroupActivity.this, "sucess", Toast.LENGTH_SHORT).show();
                    if (bean.getStatus()) {
                        tvScore.setText("Score:  "+bean.getYourMark());
                        tvMarksFrom.setText(bean.getYourMark()+"/"+bean.getTotalExamMark());
                        tvTestName.setText(bean.getTestName());
                        tvSubmitTime.setText("Submited On:"+bean.getSubmitTime());
                        tvSessionTime.setText("Schedule:"+bean.getSessionTime());
                        Adapter = new AdapterQuestionListReview(context, bean.getData());
                        //recyclerView_group_view.setLayoutManager(new GridLayoutManager(getBaseContext(), 2));
                        recycleQuestions.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
                        recycleQuestions.setItemAnimator(new DefaultItemAnimator());
                        recycleQuestions.setAdapter(Adapter);



                    } else {
                        Toast.makeText(context, bean.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                } else {

                    Toast.makeText(context, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseQuestionList> call, Throwable t) {
                Log.e("error", t.getMessage());
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


}
