package com.nusys.studentedumax;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import com.nusys.studentedumax.adapter.AdapterCourseChap;
import com.nusys.studentedumax.adapter.AdapterCourseSubject;
import com.nusys.studentedumax.adapter.AdapterCourseUnit;
import com.nusys.studentedumax.commonModules.SharedPreference_main;
import com.nusys.studentedumax.interfaces.GetChapIds;
import com.nusys.studentedumax.interfaces.GetSubjectIds;
import com.nusys.studentedumax.interfaces.GetTeacherIds;
import com.nusys.studentedumax.interfaces.GetUnitIds;
import com.nusys.studentedumax.model.GetChapResponseModel;
import com.nusys.studentedumax.model.GetExamResponseModel;
import com.nusys.studentedumax.model.GetSubjectResponseModel;
import com.nusys.studentedumax.model.GetUnitResponseModel;
import com.nusys.studentedumax.model.HomePostModel;
import com.nusys.studentedumax.retrofit.ApiClient;
import com.nusys.studentedumax.retrofit.ServiceInterface;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

import static com.nusys.studentedumax.Constants.Content_Type;

public class CourseSubject extends AppCompatActivity implements GetSubjectIds, GetUnitIds, GetChapIds {
    SwipeRefreshLayout refreshLayout;
    ProgressBar mainProgress;
    LinearLayout errorLayout;
    TextView errorTxtCause;
    Button errorBtnRetry;

    Context context;

    TextView tv_toolbar_head1;
    LinearLayout ll_back_activity;

    SharedPreference_main sharedPreference_main;
    RecyclerView recyclerView_subject_view, recyclerView_unit_view;
    AdapterCourseSubject Adapter;
    AdapterCourseUnit AdapterUnit;
    AdapterCourseChap AdapterChap;
    RecyclerView recyclerView_chap_view;
    Dialog dialog;
    String chapId, unitId, subId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_subject);
        //Toast.makeText(context, ""+subId, Toast.LENGTH_SHORT).show();
        initialization();
        action();
    }

    public void initialization() {
        context = CourseSubject.this;
        sharedPreference_main = SharedPreference_main.getInstance(context);
        tv_toolbar_head1 = findViewById(R.id.tv_toolbar_head);
        ll_back_activity = findViewById(R.id.ll_back_activity);

        refreshLayout = findViewById(R.id.refresh_layout);
        mainProgress = findViewById(R.id.main_progress);
        errorLayout = findViewById(R.id.error_layout);
        errorTxtCause = findViewById(R.id.error_txt_cause);
        errorBtnRetry = findViewById(R.id.error_btn_retry);
        recyclerView_subject_view = findViewById(R.id.rv_subject_view);
        recyclerView_unit_view = findViewById(R.id.rv_unit_view);


    }

    public void action() {
        tv_toolbar_head1.setText("Subjects");

        ll_back_activity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        getSubjects();
    }

    private void getSubjects() {

        HashMap<String, String> map = new HashMap<>();
        map.put("category_id", getIntent().getStringExtra("exam_id"));
        ServiceInterface serviceInterface = ApiClient.getClientTeacher().create(ServiceInterface.class);
        Call<GetSubjectResponseModel> call = serviceInterface.getMySubject("Bearer " + sharedPreference_main.getToken(), Content_Type, map);
        call.enqueue(new Callback<GetSubjectResponseModel>() {
            @Override
            public void onResponse(Call<GetSubjectResponseModel> call, retrofit2.Response<GetSubjectResponseModel> response) {
                if (response.isSuccessful()) {
                    if (response.isSuccessful()) {

                        GetSubjectResponseModel bean = response.body();

                        //Toast.makeText(batch_Activity.this, "sucess", Toast.LENGTH_SHORT).show();
                        if (bean.getStatus()) {
                            GetSubjectResponseModel view = response.body();

                            Adapter = new AdapterCourseSubject(context, bean.getData(), CourseSubject.this);
                            // recyclerView_subject_view.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
                            recyclerView_subject_view.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                           // recyclerView_subject_view.setLayoutManager(new GridLayoutManager(context, 3));
                            recyclerView_subject_view.setItemAnimator(new DefaultItemAnimator());
                            recyclerView_subject_view.setAdapter(Adapter);

                        } else {
                            Toast.makeText(context, bean.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    } else {

                        Toast.makeText(context, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                    }

                } else {

                    Toast.makeText(context, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<GetSubjectResponseModel> call, Throwable t) {

            }
        });


    }

    @Override
    public void getSubId(String n) {
        getUnits(n);
        subId=n;
        //    Toast.makeText(context, ""+n, Toast.LENGTH_SHORT).show();
    }

    private void getUnits(String n) {

        HashMap<String, String> map = new HashMap<>();
        map.put("exam_id", getIntent().getStringExtra("exam_id"));
        map.put("subject_id", n);
        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<GetUnitResponseModel> call = serviceInterface.getMyUnit("Bearer " + sharedPreference_main.getToken(), Content_Type, map);
        call.enqueue(new Callback<GetUnitResponseModel>() {
            @Override
            public void onResponse(Call<GetUnitResponseModel> call, retrofit2.Response<GetUnitResponseModel> response) {
                if (response.isSuccessful()) {
                    if (response.isSuccessful()) {

                        GetUnitResponseModel bean = response.body();

                        //Toast.makeText(batch_Activity.this, "sucess", Toast.LENGTH_SHORT).show();

                        if (bean.getStatus()) {
                            GetUnitResponseModel view = response.body();

                            AdapterUnit = new AdapterCourseUnit(context, bean.getData(), CourseSubject.this);


                        } else {
                            final List<GetUnitResponseModel.Datum> ab = new ArrayList<>();
                            AdapterUnit = new AdapterCourseUnit(context, ab, CourseSubject.this);

                        }
                        recyclerView_unit_view.setLayoutManager(new LinearLayoutManager(context));
                        recyclerView_unit_view.setItemAnimator(new DefaultItemAnimator());
                        recyclerView_unit_view.setAdapter(AdapterUnit);
                        //  Toast.makeText(context, bean.getMessage(), Toast.LENGTH_SHORT).show();
                    } else {

                        Toast.makeText(context, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                    }


                } else {

                    Toast.makeText(context, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<GetUnitResponseModel> call, Throwable t) {

            }
        });


    }

    @Override
    public void getUnitId(String n) {
        unitId=n;
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.chap_list_layout);
        recyclerView_chap_view = dialog.findViewById(R.id.rv_chap_list);
        getChap(n);

        //** SET DIALOG'S SCREEN WIDTH AND HEIGHT
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int displayWidth = displayMetrics.widthPixels;
        int displayHeight = displayMetrics.heightPixels;
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(dialog.getWindow().getAttributes());

        int dialogWindowWidth = (int) (displayWidth * 0.7f);
        int dialogWindowHeight = (int) (displayHeight * 0.7f);

        layoutParams.width = dialogWindowWidth;
        layoutParams.height = dialogWindowHeight;
        dialog.getWindow().setAttributes(layoutParams);

        dialog.show();
    }

    private void getChap(String n) {

        HashMap<String, String> map = new HashMap<>();
        map.put("unit_id", n);
        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<GetChapResponseModel> call = serviceInterface.getMyChap("Bearer " + sharedPreference_main.getToken(), Content_Type, map);
        call.enqueue(new Callback<GetChapResponseModel>() {
            @Override
            public void onResponse(Call<GetChapResponseModel> call, retrofit2.Response<GetChapResponseModel> response) {
                if (response.isSuccessful()) {
                    if (response.isSuccessful()) {

                        GetChapResponseModel bean = response.body();

                        if (bean.getStatus()) {
                            GetChapResponseModel view = response.body();

                            AdapterChap = new AdapterCourseChap(context, bean.getData(),CourseSubject.this);


                        } else {
                            final List<GetChapResponseModel.Datum> ab = new ArrayList<>();
                            AdapterChap = new AdapterCourseChap(context, ab, CourseSubject.this);

                        }
                        recyclerView_chap_view.setLayoutManager(new LinearLayoutManager(context));
                        recyclerView_chap_view.setItemAnimator(new DefaultItemAnimator());
                        recyclerView_chap_view.setAdapter(AdapterChap);
                        //  Toast.makeText(context, bean.getMessage(), Toast.LENGTH_SHORT).show();
                    } else {

                        Toast.makeText(context, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                    }


                } else {

                    Toast.makeText(context, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<GetChapResponseModel> call, Throwable t) {

            }
        });


    }

    @Override
    public void getChapId(String n) {
        Intent i = new Intent(context, CourseView.class);
        i.putExtra("examId", getIntent().getStringExtra("exam_id"));
        i.putExtra("subId", subId);
        i.putExtra("unitId", unitId);
        i.putExtra("chapId", n);
        startActivity(i);
        dialog.dismiss();
    }
}
