package com.nusys.studentedumax;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.nusys.studentedumax.adapter.AdapterBatch;
import com.nusys.studentedumax.adapter.AdpaterViewGroupList;
import com.nusys.studentedumax.commonModules.SharedPreference_main;
import com.nusys.studentedumax.model.ResponeViewGroupList;
import com.nusys.studentedumax.retrofit.ApiClient;
import com.nusys.studentedumax.retrofit.ServiceInterface;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;

import static com.nusys.studentedumax.Constants.Content_Type;

public class ViewGroupActivity extends AppCompatActivity {
    SharedPreference_main sharedPreference_main;
    RecyclerView recycle_group_view;
    AdpaterViewGroupList Adapter;
    Context context;
    ImageView iv_collapsing_toolbar;
    Toolbar collapsingToolbar;
    String createdId;
    String userId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_group);

        sharedPreference_main = SharedPreference_main.getInstance(this);

        context = ViewGroupActivity.this;
        recycle_group_view = findViewById(R.id.rv_group_view);
        iv_collapsing_toolbar = findViewById(R.id.collapsing_toolbar_image_view);
        collapsingToolbar = findViewById(R.id.collapsing_toolbar);
        setSupportActionBar(collapsingToolbar);
        collapsingToolbar.setTitle(getIntent().getStringExtra("name"));
        setTitle(getIntent().getStringExtra("name"));

       // Toast.makeText(context, "group"+getIntent().getStringExtra("group_id"), Toast.LENGTH_SHORT).show();

        //collapsingToolbar = findViewById(R.id.collapsing_toolbar);
        Glide.with(context)
                .load(getIntent().getStringExtra("image"))
                .placeholder(R.drawable.user_icon)
                .into(iv_collapsing_toolbar);
        //  Toast.makeText(context, ""+getIntent().getStringExtra("group_id"), Toast.LENGTH_SHORT).show();
        student_group();

    }

    private void student_group() {

        HashMap<String, String> map = new HashMap<>();

        map.put("group_id", getIntent().getStringExtra("group_id"));

        //   if (NetworkUtils.isConnected(getActivity())) {
        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<ResponeViewGroupList> call = serviceInterface.view_group_list("Bearer " + sharedPreference_main.getToken(), Content_Type, map);
        call.enqueue(new Callback<ResponeViewGroupList>() {


            @Override
            public void onResponse(Call<ResponeViewGroupList> call, retrofit2.Response<ResponeViewGroupList> response) {

                if (response.isSuccessful()) {

                    ResponeViewGroupList bean = response.body();

                    //Toast.makeText(batch_Activity.this, "sucess", Toast.LENGTH_SHORT).show();
                    if (bean.getStatus()) {
                        // bean.getdata().getPosition(0).getGroupId()

                        Adapter = new AdpaterViewGroupList(context, bean.getData().get(0).getStudents(), bean.getData().get(0).getGroupId(), bean.getData().get(0).getCreaterId());
                        //  createdId = bean.getData().get(0).getCreaterId();
                        //   Toast.makeText(ViewGroupActivity.this, ""+createdId, Toast.LENGTH_SHORT).show();
                        recycle_group_view.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
                        // recyclerView_group_view.setLayoutManager(new GridLayoutManager(getBaseContext(), 2));
                        recycle_group_view.setItemAnimator(new DefaultItemAnimator());
                        recycle_group_view.setAdapter(Adapter);
                        Log.e("Group_response", bean.toString());

                    } else {
                        Toast.makeText(context, bean.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                } else {

                    Toast.makeText(context, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponeViewGroupList> call, Throwable t) {
                Log.e("error", t.getMessage());
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    //** foe creation option menu in toolbar**//
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();

        // Toast.makeText(context, ""+createdId, Toast.LENGTH_SHORT).show();
        //  Toast.makeText(context, ""+sharedPreference_main.getUserId(), Toast.LENGTH_SHORT).show();
        userId = sharedPreference_main.getUserId();
        createdId = sharedPreference_main.getCreatedId();
        //Toast.makeText(context, ""+createdId, Toast.LENGTH_SHORT).show();
        if (createdId.equals(userId)) {
            //Toast.makeText(context, "equal", Toast.LENGTH_SHORT).show();
            inflater.inflate(R.menu.edit_menu, menu);
        } else {
            //Toast.makeText(context, "equal no", Toast.LENGTH_SHORT).show();
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() == R.id.action_edit) {

            Intent i = new Intent(context, AddNewMembersToGroup.class);
            i.putExtra("groupId", getIntent().getStringExtra("group_id"));
            i.putExtra("groupType", getIntent().getStringExtra("group_type"));
            i.putExtra("classId", getIntent().getStringExtra("class_id"));
            i.putExtra("batchId", getIntent().getStringExtra("batch_id"));

            startActivity(i);
            // Toast.makeText(context, "edit", Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }
}
