package com.nusys.studentedumax;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.nusys.studentedumax.commonModules.SharedPreference_main;
import com.nusys.studentedumax.model.ResponeProfileUpdate;
import com.nusys.studentedumax.model.profile.ResponeProfile;
import com.nusys.studentedumax.retrofit.ApiClient;
import com.nusys.studentedumax.retrofit.ServiceInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

import static androidx.core.content.FileProvider.getUriForFile;
import static com.nusys.studentedumax.Constants.BASE_URL;
import static com.nusys.studentedumax.Constants.CAMERA_REQUEST_CODE;
import static com.nusys.studentedumax.Constants.Content_Type;
import static com.nusys.studentedumax.Constants.GALLERY_REQUEST_CODE;
import static com.nusys.studentedumax.Constants.PROFILE_PIC_UPDATE;

public class UpdateProfile extends AppCompatActivity {
    ImageView img_profile, img_pickfrom;
    RadioButton rd_male, rd_female, rd_other;
    TextView st_edit_dob, st_name;
    private int mYear, mMonth, mDay;
    private String fileName;
    private String userImageBase64;
    SharedPreference_main sharedPreference_main;
    LinearLayout back_activity;
    Context context;
    Button bt_update;
    String Gender, str_email, str_name, str_location, str_dob;
    RadioGroup rd_Sex;
    EditText et_email, et_location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile);
        context = UpdateProfile.this;
        init();
        listener();


    }


    private void init() {
        sharedPreference_main = SharedPreference_main.getInstance(this);
        img_profile = findViewById(R.id.img_Profile);
        img_pickfrom = findViewById(R.id.img_pickFrom);
        st_edit_dob = findViewById(R.id.st_edit_dob);
        back_activity = findViewById(R.id.back_activity);
        rd_Sex = findViewById(R.id.radioSex);
        rd_male = findViewById(R.id.radioMale);
        rd_female = findViewById(R.id.radioFemale);
        rd_other = findViewById(R.id.radio_other);
        bt_update = findViewById(R.id.btn_update);
        et_location = findViewById(R.id.st_edit_location);
        et_email = findViewById(R.id.st_edit_email);
        et_email.setText(sharedPreference_main.getUserEmail());
        st_name = findViewById(R.id.st_edit_name);
        st_name.setText(sharedPreference_main.getUsername());
        et_location.setText(sharedPreference_main.getaddress());
        st_edit_dob.setText(sharedPreference_main.getdob());

    }

    private void listener() {

        user_profile();
        back_activity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        img_pickfrom.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                openChooser(CAMERA_REQUEST_CODE, GALLERY_REQUEST_CODE);

            }
        });


        st_edit_dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(UpdateProfile.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                                st_edit_dob.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }

        });
      /*  rd_Sex.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radioMale:
                        // do operations specific to this selection
                        Gender = "Male";
                        break;
                    case R.id.radioFemale:
                        // do operations specific to this selection
                        Gender = "Female";
                        break;
                    case R.id.radio_other:
                        // do operations specific to this selection
                        Gender = "Other";
                        break;

                }
            }
        });*/
        bt_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rd_male.isChecked()) {
                    Gender = rd_male.getText().toString();
                   // Toast.makeText(context, "abc", Toast.LENGTH_SHORT).show();
                } else if (rd_female.isChecked()) {
                    Gender = rd_female.getText().toString();
                   // Toast.makeText(context, "abc2", Toast.LENGTH_SHORT).show();
                } else if (rd_other.isChecked()) {
                    Gender = rd_other.getText().toString();
                   // Toast.makeText(context, "abc3", Toast.LENGTH_SHORT).show();
                }
                //  Log.e("Gen", Gender);

                user_profile_update();
            }


        });

    }

    private void chooseImageFromGallery(final int code) {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(pickPhoto, code);
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();

    }

    private void takeCameraImage(final int code) {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            fileName = System.currentTimeMillis() + ".jpg";

                            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(cameraIntent, code);
//                            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, getCacheImagePath(fileName));
//                            if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
//                                startActivityForResult(takePictureIntent, code);
//                            }
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();

    }

    private Uri getCacheImagePath(String fileName) {
        File path = new File(getExternalCacheDir(), "camera");
        if (!path.exists()) path.mkdirs();
        File image = new File(path, fileName);
        return getUriForFile(UpdateProfile.this, getPackageName() + ".provider", image);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {

            case CAMERA_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    Bitmap photo = (Bitmap) data.getExtras().get("data");
                    img_profile.setImageBitmap(photo);
                    getEncoded64ImageStringFromBitmap(photo);
                    userImageBase64 = getEncoded64ImageStringFromBitmap(photo);
                    Log.e("excam", userImageBase64);
                    post_Json_Request_Image();

                    // hitapi();


//                    tv_userImage.setText(getCacheImagePath(fileName).toString());
//                  getCacheImagePath(fileName);
                }
                break;
            case GALLERY_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    Uri imageUri = data.getData();
                    img_profile.setImageURI(imageUri);
                    Bitmap bitmap;
                    try {
                        bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(imageUri));
                        Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, 500, 500, false);
                        userImageBase64 = getEncoded64ImageStringFromBitmap(resizedBitmap);
                        Log.e("exp", userImageBase64);
                        post_Json_Request_Image();

                    } catch (FileNotFoundException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
//                    tv_userImage.setText(imageUri.getPath());

                break;

        }
    }

    public String getEncoded64ImageStringFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteFormat = stream.toByteArray();
        String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);
        Log.e("", imgString);
        return imgString;
    }

    private void openChooser(final int code1, final int code2) {

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.chooser);
        TextView camera = dialog.findViewById(R.id.camera);
        TextView gallery = dialog.findViewById(R.id.gallery);
        dialog.show();
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takeCameraImage(code1);
                dialog.dismiss();
            }
        });
        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseImageFromGallery(code2);
                dialog.dismiss();


            }
        });
    }


    private void post_Json_Request_Image() {
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        JSONObject jsonBody = new JSONObject();
        try {
            //input your API parameters
            jsonBody.put("id", sharedPreference_main.getUserId());
            jsonBody.put("photo", userImageBase64);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        // Enter the correct url for your api service site
        // String url = getResources().getString(R.string.url);
       // String URL = "http://13.233.162.24/nusys(UAT)/api/student/update_profile_pic.php";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL+PROFILE_PIC_UPDATE, jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            // Toast.makeText(ChangePassword.this, "true", Toast.LENGTH_SHORT).show();

                            Boolean status = response.getBoolean("status");
                            if (status) {
                                //sharedPreference_main.setUserImage(response.getString("image"));
                                JSONObject jsonObject2 = response.getJSONObject("data");
                                sharedPreference_main.setUserImage(jsonObject2.getString("image"));
                                Toast.makeText(context, "" + response.getString("message"), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(context, "" + response.getString("message"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        // Toast.makeText(ChangePassword.this, ""+response.toString(), Toast.LENGTH_SHORT).show();
                        //resultTextView.setText("String Response : "+ response.toString());
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //  resultTextView.setText("Error getting response");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json; charset=UTF-8");
                params.put("Authorization", "Bearer " + sharedPreference_main.getToken());
                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);


    }

    private void user_profile_update() {

        HashMap<String, String> map = new HashMap<>();
        str_email = et_email.getText().toString();
        str_location = et_location.getText().toString();
        //  str_name = st_name.getText().toString();
        str_dob = st_edit_dob.getText().toString();

        map.put("id", sharedPreference_main.getUserId());
        //  map.put("photo", "data:image/jpeg;base64," + userImageBase64);
        map.put("address", str_location);
        map.put("email", str_email);
        map.put("gender", Gender);
        map.put("dob", str_dob);

        //   if (NetworkUtils.isConnected(getActivity())) {
        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<ResponeProfileUpdate> call = serviceInterface.update_profile("Bearer " + sharedPreference_main.getToken(), Content_Type, map);
        call.enqueue(new Callback<ResponeProfileUpdate>() {
            @Override
            public void onResponse(Call<ResponeProfileUpdate> call, retrofit2.Response<ResponeProfileUpdate> response) {

                if (response.isSuccessful()) {

                    ResponeProfileUpdate bean = response.body();
                    if (bean.getStatus()) {
                        ResponeProfileUpdate update = response.body();

                        Toast.makeText(UpdateProfile.this, update.getMessage(), Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(UpdateProfile.this, Profile.class);
                        startActivity(intent);

                    } else {
                        Toast.makeText(UpdateProfile.this, bean.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                } else {

                    Toast.makeText(UpdateProfile.this, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponeProfileUpdate> call, Throwable t) {
                Toast.makeText(UpdateProfile.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void user_profile() {

        HashMap<String, String> map = new HashMap<>();

        map.put("id", sharedPreference_main.getUserId());
        map.put("user_type", "student");
        map.put("page", "1");

        //   if (NetworkUtils.isConnected(getActivity())) {
        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<ResponeProfile> call = serviceInterface.profile("Bearer " + sharedPreference_main.getToken(), Content_Type, map);
        call.enqueue(new Callback<ResponeProfile>() {
            @Override
            public void onResponse(Call<ResponeProfile> call, retrofit2.Response<ResponeProfile> response) {

                if (response.isSuccessful()) {

                    ResponeProfile bean = response.body();
                    if (bean.getStatus()) {
                        ResponeProfile update = response.body();
                        sharedPreference_main.setaddress(bean.getData().getAddress());
                        sharedPreference_main.setUserEmail(bean.getData().getEmail());
                        sharedPreference_main.setgender(bean.getData().getGender());
                        sharedPreference_main.setdob(bean.getData().getDob());

                       // Toast.makeText(UpdateProfile.this, ""+bean.getData().getGender(), Toast.LENGTH_SHORT).show();
                        if (bean.getData().getGender().equals("Male")){
                            rd_male.setChecked(true);
                            rd_female.setChecked(false);
                            rd_other.setChecked(false);
                        }else  if (bean.getData().getGender().equals("Female")){
                            rd_male.setChecked(false);
                            rd_female.setChecked(true);
                            rd_other.setChecked(false);
                        }else if (bean.getData().getGender().equals("Other")){
                            rd_male.setChecked(false);
                            rd_female.setChecked(false);
                            rd_other.setChecked(true);
                        }
                        et_email.setText(bean.getData().getEmail());
                        et_location.setText(bean.getData().getAddress());
                        st_edit_dob.setText(bean.getData().getDob());
                        Glide.with(getApplicationContext())
                                .load(bean.getData().getImage())
                                .placeholder(R.drawable.user_icon)
                                .into(img_profile);


                    } else {
                        Toast.makeText(UpdateProfile.this, bean.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                } else {

                    Toast.makeText(UpdateProfile.this, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponeProfile> call, Throwable t) {
                Log.e("error", t.getMessage());
                Toast.makeText(UpdateProfile.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}



