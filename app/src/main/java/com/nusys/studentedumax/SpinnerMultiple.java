package com.nusys.studentedumax;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nusys.studentedumax.model.ExamModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SpinnerMultiple extends AppCompatActivity {
    RecyclerView exam_list;
    AdapterExamSpinner myAdapter;
    List<String> get_ID = new ArrayList<String>();
    List<String> Show_select_jobName = new ArrayList<String>();
    TextView multiSpin;
    Dialog dialog;
    String ID;

    private List<ExamModel> ruleslist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.demo);
        multiSpin = findViewById(R.id.spinnerMulti);
        multiSpin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hitExamApi();
            }
        });

    }

    private void hitExamApi() {
        try {
            RequestQueue requestQueue = Volley.newRequestQueue(SpinnerMultiple.this);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://13.233.162.24/nusys(UAT)/api/teacher/select_exanination.php",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Toast.makeText(SpinnerMultiple.this, response, Toast.LENGTH_LONG).show();
                            final List<ExamModel> examModels = new ArrayList<>();
                            try {
                                // Toast.makeText(Home.this, "aaaaaaaa", Toast.LENGTH_LONG).show();
                                JSONObject jsonObject1 = new JSONObject(response);
                                //Toast.makeText(Home.this, ""+jsonObject1, Toast.LENGTH_SHORT).show();
                                JSONArray jArray = jsonObject1.getJSONArray("data");
                                Log.d("ARray", " " + jArray);
                                //Toast.makeText(Home.this, "abc"+jArray, Toast.LENGTH_SHORT).show();
                                for (int i = 0; i < jArray.length(); i++) {
                                    JSONObject jsonObject = jArray.getJSONObject(i);
                                    Log.d("question_obj", " " + jsonObject);
                                    ExamModel entity = new ExamModel();
                                    entity.setName(jsonObject.getString("name"));
                                    entity.setId(jsonObject.getString("id"));
//
                                    examModels.add(entity);
                                }
                                get_dailog();
                                //LinearLayoutManager layoutManager = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
                                exam_list.setLayoutManager(new LinearLayoutManager(SpinnerMultiple.this));
                                myAdapter = new AdapterExamSpinner(SpinnerMultiple.this, examModels);
                                exam_list.setAdapter(myAdapter);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    Toast.makeText(SpinnerMultiple.this, "Server Error", Toast.LENGTH_SHORT).show();
                    // progressDialog.dismiss();
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();

                    return params;
                }
            };

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    5000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(stringRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void get_dailog() {

        dialog = new Dialog(SpinnerMultiple.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_spinner_multiple);
        exam_list = (RecyclerView) dialog.findViewById(R.id.exam_list);

        final LinearLayoutManager layoutManager = new LinearLayoutManager(SpinnerMultiple.this);
        //layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        exam_list.setLayoutManager(layoutManager);
        exam_list.setItemAnimator(new DefaultItemAnimator());
        exam_list.setNestedScrollingEnabled(false);
        myAdapter = new AdapterExamSpinner(SpinnerMultiple.this, ruleslist);
        exam_list.setAdapter(myAdapter);

        Button button = dialog.findViewById(R.id.btn_ok);
        // Button button_cancel = dialog.findViewById(R.id.btn_cancel);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String P_id = "";
                StringBuilder pd = new StringBuilder();
                for (Object mPI : get_ID) {
                    pd.append(P_id).append(mPI);
                    P_id = ",";
                }
                ID = pd.toString();
                //System.out.println(IDs.toString());

                String S_Name = "";
                StringBuilder SB = new StringBuilder();
                for (Object mPI : Show_select_jobName) {
                    SB.append(S_Name).append(mPI);
                    S_Name = ",";
                }
                String Name = SB.toString();
                //System.out.println(IDs.toString());

                multiSpin.setText(Name);

                //Toast.makeText(BusinessUpload.this, "" + IDs, Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
       /* button_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });*/
        dialog.getWindow().getDecorView().setTop(100);
        dialog.getWindow().getDecorView().setLeft(100);
        dialog.show();
    }


    class AdapterExamSpinner extends RecyclerView.Adapter<AdapterExamSpinner.ItemViewHolder> {

        List<ExamModel> question_list;
        private Context a;

        public AdapterExamSpinner(Context context, List<ExamModel> question_list) {
            this.a = context;
            this.question_list = question_list;
        }

        @Override
        public AdapterExamSpinner.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.multi_adapter, parent, false);
            AdapterExamSpinner.ItemViewHolder viewHolder = new AdapterExamSpinner.ItemViewHolder(v);
            return viewHolder;
        }


        @Override
        public void onBindViewHolder(AdapterExamSpinner.ItemViewHolder holder, final int position) {

            holder.main_category.setText(question_list.get(position).getName());
            //holder.id.setText(question_list.get(position).getId());

        }

        @Override
        public int getItemCount() {
            //return question_list.size();

            return (null != question_list ? question_list.size() : 0);
        }

        public class ItemViewHolder extends RecyclerView.ViewHolder {
            TextView main_category, id;
            CheckBox checkBox;

            public ItemViewHolder(View itemView) {
                super(itemView);
                main_category = itemView.findViewById(R.id.exam_name);
                checkBox = itemView.findViewById(R.id.exam);
                //id = itemView.findViewById(R.id.name_id);

                checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override

                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        int pos = getAdapterPosition();
                        if (b) {
                            //True condition
                            String id = question_list.get(pos).getId();
                            String name = question_list.get(pos).getName();
                            get_ID.add(id);
                            Show_select_jobName.add(name);
                            //Job_name = name;
                        }
                        if (checkBox.isChecked() == false) {

                            get_ID.remove(question_list.get(pos).getId());
                            Show_select_jobName.remove(question_list.get(pos).getName());
                            // false condition
                        }
                    }
                });

            }
        }

    }

}
