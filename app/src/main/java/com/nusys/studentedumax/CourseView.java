package com.nusys.studentedumax;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nusys.studentedumax.adapter.AdapterBatch;
import com.nusys.studentedumax.adapter.AdapterCourseImage;
import com.nusys.studentedumax.commonModules.SharedPreference_main;
import com.nusys.studentedumax.model.ResponeStudentBatch;
import com.nusys.studentedumax.model.ResponeViewCourse;
import com.nusys.studentedumax.retrofit.ApiClient;
import com.nusys.studentedumax.retrofit.ServiceInterface;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;

import static com.nusys.studentedumax.Constants.Content_Type;

public class CourseView extends AppCompatActivity {

    Context context;
    LinearLayout llBackActivity;
    TextView tvToolbarHead, tv_content, tv_heading;
    SharedPreference_main sharedPreference_main;
    RecyclerView rv_image_view;
    AdapterCourseImage Adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_view);


        init();
        action();
    }

    public void init() {
        context = CourseView.this;
        sharedPreference_main = SharedPreference_main.getInstance(context);
        llBackActivity = findViewById(R.id.ll_back_activity);
        tvToolbarHead = findViewById(R.id.tv_toolbar_head);
        tv_content = findViewById(R.id.tvContent);
        tv_heading = findViewById(R.id.tvHeading);
        rv_image_view = findViewById(R.id.rv_course_image_view);

    }

    public void action() {

        tvToolbarHead.setText("Course View");

        llBackActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        viewCourse();
    }

    private void viewCourse() {

        HashMap<String, String> map = new HashMap<>();

        map.put("course_id", getIntent().getStringExtra("examId"));
        map.put("sub_id", getIntent().getStringExtra("subId"));
        map.put("unit_id", getIntent().getStringExtra("unitId"));
        map.put("chapter_id", getIntent().getStringExtra("chapId"));

        //   if (NetworkUtils.isConnected(getActivity())) {
        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<ResponeViewCourse> call = serviceInterface.view_course("Bearer " + sharedPreference_main.getToken(), Content_Type, map);
        call.enqueue(new Callback<ResponeViewCourse>() {
            @Override
            public void onResponse(Call<ResponeViewCourse> call, retrofit2.Response<ResponeViewCourse> response) {

                if (response.isSuccessful()) {

                    ResponeViewCourse bean = response.body();


                    if (bean.getStatus()) {
                        ResponeViewCourse view = response.body();

                        tv_content.setText(Html.fromHtml(bean.getData().get(0).getTopicDesc()));
                        tv_heading.setText(Html.fromHtml(bean.getData().get(0).getTopicName()));
                        student_group();


                    } else {
                        Toast.makeText(context, bean.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                } else {

                    Toast.makeText(context, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponeViewCourse> call, Throwable t) {
                Log.e("error", t.getMessage());
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void student_group() {

        HashMap<String, String> map = new HashMap<>();

        map.put("course_id", getIntent().getStringExtra("examId"));
        map.put("sub_id", getIntent().getStringExtra("examId"));
        map.put("unit_id", getIntent().getStringExtra("unitId"));
        map.put("chapter_id", getIntent().getStringExtra("chapId"));

        //   if (NetworkUtils.isConnected(getActivity())) {
        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<ResponeViewCourse> call = serviceInterface.view_course("Bearer " + sharedPreference_main.getToken(), Content_Type, map);
        call.enqueue(new Callback<ResponeViewCourse>() {


            @Override
            public void onResponse(Call<ResponeViewCourse> call, retrofit2.Response<ResponeViewCourse> response) {

                if (response.isSuccessful()) {

                    ResponeViewCourse bean = response.body();


                    if (bean.getStatus()) {
                        ResponeViewCourse view = response.body();

                        Adapter = new AdapterCourseImage(context, bean.getData().get(0).getTopicImage());
                        rv_image_view.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
                        // recyclerView_group_view.setLayoutManager(new GridLayoutManager(getBaseContext(), 2));
                        rv_image_view.setItemAnimator(new DefaultItemAnimator());
                        rv_image_view.setAdapter(Adapter);
                        // Log.e("Group_response", view.toString());

                    } else {
                        Toast.makeText(context, bean.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                } else {

                    Toast.makeText(context, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponeViewCourse> call, Throwable t) {
                Log.e("error", t.getMessage());
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
