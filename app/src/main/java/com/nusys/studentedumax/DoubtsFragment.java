package com.nusys.studentedumax;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.nusys.studentedumax.adapter.AdapterDoubtComment;
import com.nusys.studentedumax.adapter.AdapterDoubts;

import com.nusys.studentedumax.commonModules.DialogProgress;
import com.nusys.studentedumax.commonModules.NetworkUtil;
import com.nusys.studentedumax.commonModules.PaginationAdapterCallback;
import com.nusys.studentedumax.commonModules.PaginationScrollListener;
import com.nusys.studentedumax.commonModules.SharedPreference_main;
import com.nusys.studentedumax.interfaces.GetDoubtIds;
import com.nusys.studentedumax.model.DoubtCommentModel;
import com.nusys.studentedumax.model.DoubtsModel;

import com.nusys.studentedumax.model.TeacherModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeoutException;

import static android.app.Activity.RESULT_OK;
import static com.nusys.studentedumax.Constants.BASE_URL;
import static com.nusys.studentedumax.Constants.BASE_URL_TEACHER;
import static com.nusys.studentedumax.Constants.CAMERA_REQUEST_CODE;
import static com.nusys.studentedumax.Constants.GALLERY_REQUEST_CODE;
import static com.nusys.studentedumax.Constants.STUDENT_DOUBT;
import static com.nusys.studentedumax.Constants.STUDENT_DOUBT_COMMENT_ADD;
import static com.nusys.studentedumax.Constants.STUDENT_DOUBT_COMMENT_VIEW;
import static com.nusys.studentedumax.commonModules.Extension.showErrorDialog;

public class DoubtsFragment extends Fragment implements PaginationAdapterCallback, SwipeRefreshLayout.OnRefreshListener, GetDoubtIds {
    RecyclerView rv_doubts_list;
    AdapterDoubts myAdapter;
    SharedPreference_main sharedPreference_main;
    LinearLayoutManager linearLayoutManager;
    private static int TOTAL_PAGES;
    ProgressBar progressBar;
    LinearLayout errorLayout;
    Button btnRetry;
    SwipeRefreshLayout swipeRefreshLayout;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int currentPage = 1;
    Dialog dialog;
    String doubt_id;

    RecyclerView recycler_commment;
    AdapterDoubtComment myAdapter1;
    private List<DoubtCommentModel> ruleslist;
    private Context context;
    LinearLayout relative_noComment;
    EditText et_comment;
    ImageView openGalley;
    ImageView img_send;
    private String userImageBase64;
    String mediaType;
    private ProgressDialog pd;

    //Context context;
//    private List<DoubtsModel> ab;


    TextView st_ask_doubts, tv_completed, tv_inprogress, tv_all;
    String progress_count;
    // AdapterDoubts myAdapter;
    //private RecyclerView doubts_list;
    //  ArrayList<DoubtsModel> ruleslist = new ArrayList<DoubtsModel>();

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_doubts_fragment, container, false);
//        ab = new ArrayList<>();
        sharedPreference_main = SharedPreference_main.getInstance(getActivity());

        pd = new DialogProgress(getContext(), "");
        pd.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        pd.setCancelable(false);

        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_comment_doubt);
        recycler_commment = dialog.findViewById(R.id.recycler_commment);
        relative_noComment = dialog.findViewById(R.id.relative_noComment);
        et_comment = dialog.findViewById(R.id.et_comment);
        openGalley = dialog.findViewById(R.id.openGalley);
        et_comment = dialog.findViewById(R.id.et_comment);
        img_send = dialog.findViewById(R.id.img_send);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);
        recycler_commment.setLayoutManager(linearLayoutManager);
        //recycler_commment.setLayoutManager(new LinearLayoutManager(getActivity()));
        myAdapter1 = new AdapterDoubtComment(context, ruleslist);
        recycler_commment.setAdapter(myAdapter1);


        tv_all = rootView.findViewById(R.id.tvAll);
        tv_completed = rootView.findViewById(R.id.tvCompleted);
        tv_inprogress = rootView.findViewById(R.id.tvInprogress);
        st_ask_doubts = rootView.findViewById(R.id.tvAskDoubts);
        st_ask_doubts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), DoubtsAsk.class);
                startActivity(i);

            }
        });
        /* starting 5 lines of this method for applying delay for displaying buttons
         * Created Date:12-06-2020
         */
        tv_all.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!getActivity().isFinishing()) { // make checks to see if activity is still available
                    tv_all.setEnabled(true);
                    tv_all.setAlpha(1);
                    tv_all.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //onStart();
                            sharedPreference_main.setDoubtStatus("0");

                            FragmentTransaction ft = getFragmentManager().beginTransaction();
                            ft.detach(DoubtsFragment.this).attach(DoubtsFragment.this).commit();


                        }
                    });
                }
            }
        }, 1000 * 3);    // 3 seconds

        tv_inprogress.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!getActivity().isFinishing()) { // make checks to see if activity is still available
                    tv_inprogress.setEnabled(true);
                    tv_inprogress.setAlpha(1);
                    tv_inprogress.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {


                            sharedPreference_main.setDoubtStatus("1");

                            // Toast.makeText(getActivity(), ""+sharedPreference_main.getDoubtStatus(), Toast.LENGTH_SHORT).show();
                            FragmentTransaction ft = getFragmentManager().beginTransaction();
                            ft.detach(DoubtsFragment.this).attach(DoubtsFragment.this).commit();

                        }
                    });

                }
            }
        }, 1000 * 3);    // 3 seconds


        tv_completed.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!getActivity().isFinishing()) { // make checks to see if activity is still available
                    tv_completed.setEnabled(true);
                    tv_completed.setAlpha(1);
                    tv_completed.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            sharedPreference_main.setDoubtStatus("2");
                            FragmentTransaction ft = getFragmentManager().beginTransaction();
                            ft.detach(DoubtsFragment.this).attach(DoubtsFragment.this).commit();

                        }
                    });
                }
            }
        }, 1000 * 3);    // 3 seconds


        progressBar = rootView.findViewById(R.id.main_progress);
        errorLayout = rootView.findViewById(R.id.error_layout);
        btnRetry = rootView.findViewById(R.id.error_btn_retry);
        rv_doubts_list = rootView.findViewById(R.id.rvDoubtsList);
        swipeRefreshLayout = rootView.findViewById(R.id.main_swiperefresh);
        linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rv_doubts_list.setLayoutManager(linearLayoutManager);
        rv_doubts_list.setItemAnimator(new DefaultItemAnimator());


        myAdapter = new AdapterDoubts(getActivity(), this);
        rv_doubts_list.setAdapter(myAdapter);

        rv_doubts_list.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;

                loadNextPage();
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });


        btnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadFirstPage();
            }
        });

        swipeRefreshLayout.setOnRefreshListener(this);
        //swipeRefreshLayout.setOnRefreshListener((SwipeRefreshLayout.OnRefreshListener) getActivity());


        // hitReviewApi();
        return rootView;
    }

    public void onRefresh() {
        // TODO: Check if data is stale.
        //  Execute network request if cache is expired; otherwise do not update data.
     /*   myAdapter.clear();
        //  currentPage = 1;
        myAdapter.notifyDataSetChanged();*/
        progressBar.setVisibility(View.VISIBLE);
        isLastPage = false;

        swipeRefreshLayout.setRefreshing(false);
        loadFirstPage();
    }

    private void loadFirstPage() {
        currentPage = 1;
        //relative.setVisibility(View.VISIBLE);
        errorLayout.setVisibility(View.VISIBLE);
        //   frame_container.setVisibility(View.GONE);
        if (NetworkUtil.isConnected(getContext())) {
            // To ensure list is visible when retry button in error view is clicked
            hideErrorView();
            pd.show();

            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            final JSONObject jsonBody = new JSONObject();
            try {
                //   jsonBody.put("id", sharedPreference_main.getUserId());
                jsonBody.put("id", sharedPreference_main.getUserId());
                jsonBody.put("page", currentPage);
                jsonBody.put("progress", sharedPreference_main.getDoubtStatus());


            } catch (JSONException e) {
                e.printStackTrace();
            }
            // Enter the correct url for your api service site
            // String url = getResources().getString(R.string.url);
            // String URL = "http://13.233.162.24/nusys(UAT)/api/student/fetch_student_doubt.php";
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL + STUDENT_DOUBT, jsonBody,
                    new com.android.volley.Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e("response1", response.toString());
                            try {

                                Boolean status = response.getBoolean("status");
                                //Toast.makeText(Profile.this, "", Toast.LENGTH_SHORT).show();
                                //Toast.makeText(getActivity(), "abcd" + status, Toast.LENGTH_SHORT).show();
                                if (status) {
                                    errorLayout.setVisibility(View.GONE);
                                    myAdapter.clear();
                                    myAdapter.notifyDataSetChanged();
                                    final List<DoubtsModel> ab = new ArrayList<>();
                                    //JSONObject jsonObj = response.getJSONObject("data");
                                    JSONArray jsonArray = response.getJSONArray("data");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                                        DoubtsModel entity = new DoubtsModel();
                                        //  entity.setData(response.getString("namezz"));
                                        entity.setUser_name(jsonObject.getString("user_name"));
                                        entity.setCreate_at(jsonObject.getString("create_at"));
                                        entity.setRemark(jsonObject.getString("remark"));
                                        entity.setDescription(jsonObject.getString("description"));
                                        entity.setUser_profile(jsonObject.getString("user_profile"));
                                        entity.setImage(jsonObject.getString("image"));
                                        entity.setSubject_name(jsonObject.getString("subject_name"));
                                        entity.setUnit_name(jsonObject.getString("unit_name"));
                                        entity.setUploader_student_id(jsonObject.getString("uploader_student_id"));
                                        entity.setDoubt_id(jsonObject.getString("doubt_id"));
                                        entity.setProgress_status(jsonObject.getInt("progress_status"));
                                        ab.add(entity);
                                    }
                                    //reprog.setVisibility(View.GONE);
                                    //myAdapter = new PaginationAdapter(MainActivity.this, ab);
                                    if (ab != null) {
                                        TOTAL_PAGES = ab.size();
                                        Log.e("totalPages", String.valueOf(ab.size()));
                                        myAdapter.addAll(ab);
                                        //rv_doubts_list.setAdapter(myAdapter);

                                    } else {

                                        Toast.makeText(getActivity(), "Something is wrong try again later", Toast.LENGTH_SHORT).show();
                                    }
                                    progressBar.setVisibility(View.GONE);
                                    if (currentPage <= TOTAL_PAGES) myAdapter.addLoadingFooter();
                                    else isLastPage = true;
                                    pd.dismiss();
                                } else {
                                    errorLayout.setVisibility(View.VISIBLE);
                                    pd.dismiss();
                                    ShowErrorView();
                                    Toast.makeText(getActivity(), "Record not found", Toast.LENGTH_SHORT).show();

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            // Toast.makeText(SignUpStepTwo.this, "" + response.toString(), Toast.LENGTH_SHORT).show();
                            //resultTextView.setText("String Response : "+ response.toString());
                        }
                    }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    errorLayout.setVisibility(View.VISIBLE);
                    pd.dismiss();
                    ShowErrorView();
                    //  resultTextView.setText("Error getting response");
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/json; charset=UTF-8");
                    params.put("Authorization", "Bearer " + sharedPreference_main.getToken());
                    return params;
                }
            };
            requestQueue.add(jsonObjectRequest);
        } else {
            showErrorDialog(getContext(), new Dialog(getContext()));
            errorLayout.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        loadFirstPage();

    }

    private void loadNextPage() {
        Log.d("page", "loadNextPage: " + currentPage);
        Log.e("total", String.valueOf(TOTAL_PAGES));
        // Log.d(TAG, "loadNextPage: " + currentPage);
        /*new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {*/
        //  RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity().getApplicationContext());
        final JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("id", sharedPreference_main.getUserId());
            //jsonBody.put("id", sharedPreference_main.getUserId());
            jsonBody.put("page", String.valueOf(currentPage));
            jsonBody.put("progress", sharedPreference_main.getDoubtStatus());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        // Enter the correct url for your api service site
        // String url = getResources().getString(R.string.url);
        //  String URL = "http://13.233.162.24/nusys(UAT)/api/student/fetch_student_doubt.php";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL + STUDENT_DOUBT, jsonBody,
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("response", response.toString());
                        try {
                            myAdapter.removeLoadingFooter();
                            isLoading = false;
                            Boolean status = response.getBoolean("status");
                            //Toast.makeText(Profile.this, "", Toast.LENGTH_SHORT).show();
                            // Toast.makeText(Profile.this, "abcd"+status, Toast.LENGTH_SHORT).show();
                            if (status) {

                                //   isLoading = false;
                                final List<DoubtsModel> ab = new ArrayList<>();
                                //JSONObject jsonObj = response.getJSONObject("data");
                                JSONArray jsonArray = response.getJSONArray("data");
                                for (int i = 0; i < jsonArray.length(); i++) {

                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    DoubtsModel entity = new DoubtsModel();
                                    //  entity.setData(response.getString("namezz"));
                                    entity.setUser_name(jsonObject.getString("user_name"));
                                    entity.setCreate_at(jsonObject.getString("create_at"));
                                    entity.setRemark(jsonObject.getString("remark"));
                                    entity.setDescription(jsonObject.getString("description"));
                                    entity.setUser_profile(jsonObject.getString("user_profile"));
                                    entity.setImage(jsonObject.getString("image"));
                                    entity.setSubject_name(jsonObject.getString("subject_name"));
                                    entity.setProgress_status(jsonObject.getInt("progress_status"));
                                    entity.setUploader_student_id(jsonObject.getString("uploader_student_id"));
                                    entity.setDoubt_id(jsonObject.getString("doubt_id"));
                                    entity.setUnit_name(jsonObject.getString("unit_name"));
                                    ab.add(entity);
                                }
                                //reprog.setVisibility(View.GONE);
                                //myAdapter = new PaginationAdapter(MainActivity.this, ab);
//                                        rv_doubts_list.setAdapter(myAdapter);

                                if (ab != null) {
                                    //TOTAL_PAGES = ab.size();
                                    myAdapter.addAll(ab);
                                    //   myAdapter.removeLoadingFooter();
                                    if (currentPage != TOTAL_PAGES)
                                        myAdapter.addLoadingFooter();
                                    else isLastPage = true;
                                    //myAdapter.removeLoadingFooter();

                                } else {
                                    isLastPage = true;
                                }
                                       /* if (currentPage != TOTAL_PAGES)
                                            myAdapter.addLoadingFooter();
                                        else isLastPage = true;*/
                            } else {
                                isLastPage = true;
                                //Toast.makeText(getActivity(), "Something is wrong try again later", Toast.LENGTH_SHORT).show();

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            isLastPage = true;
                        }

                        // Toast.makeText(SignUpStepTwo.this, "" + response.toString(), Toast.LENGTH_SHORT).show();
                        //resultTextView.setText("String Response : "+ response.toString());
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                isLastPage = true;
                //  resultTextView.setText("Error getting response");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json; charset=UTF-8");
                params.put("Authorization", "Bearer " + sharedPreference_main.getToken());
                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);

        /*    }
        }, 2000);*/

    }


    @Override
    public void retryPageLoad() {
        loadNextPage();
    }


    /**
     * @param throwable required for {@link #fetchErrorMessage(Throwable)}
     * @return
     */
    private void showErrorView(Throwable throwable) {

        if (errorLayout.getVisibility() == View.GONE) {
            errorLayout.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);

            //txtError.setText(fetchErrorMessage(throwable));
        }
    }

    /**
     * @param throwable to identify the type of error
     * @return appropriate error message
     */
    private String fetchErrorMessage(Throwable throwable) {
        String errorMsg = getResources().getString(R.string.error_msg_unknown);

        if (!isNetworkConnected()) {
            errorMsg = getResources().getString(R.string.error_msg_no_internet);
        } else if (throwable instanceof TimeoutException) {
            errorMsg = getResources().getString(R.string.error_msg_timeout);
        }

        return errorMsg;
    }

    // Helpers -------------------------------------------------------------------------------------


    private void hideErrorView() {
        if (errorLayout.getVisibility() == View.VISIBLE) {
            errorLayout.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    private void ShowErrorView() {
        if (errorLayout.getVisibility() == View.GONE) {
            errorLayout.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Remember to add android.permission.ACCESS_NETWORK_STATE permission.
     *
     * @return
     */
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }


    @Override
    public void getDoubtIds(String doubtId) {
        this.doubt_id = doubtId;
/*        dialog.getWindow().getDecorView().setTop(100);
        dialog.getWindow().getDecorView().setLeft(100);*/
        dialog.show();


        //Window window = dialog.getWindow();
        dialog.getWindow().setLayout(750, 1200);
        getComments(doubt_id);
        openGalley.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openChooser(CAMERA_REQUEST_CODE, GALLERY_REQUEST_CODE);
            }
        });
        img_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(et_comment.getText().toString())) {
                    et_comment.setError("field can't be empty");
                } else {
                    DoComment(doubt_id);
                }
            }
        });
    }


    private void getComments(String doubt_id) {

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        final JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("doubt_id", doubt_id);
            jsonBody.put("user_id", sharedPreference_main.getUserId());

            // Toast.makeText(context, ""+sharedPreference_main.getUserId(), Toast.LENGTH_SHORT).show();

        } catch (JSONException e) {
            e.printStackTrace();
        }
        // Enter the correct url for your api service site
        // String url = getResources().getString(R.string.url);
        //String URL = "http://13.233.162.24/nusys(UAT)/api/teacher/fetch_doubts_comment.php";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL_TEACHER + STUDENT_DOUBT_COMMENT_VIEW, jsonBody,
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Boolean status = response.getBoolean("status");
                            //Toast.makeText(Profile.this, "", Toast.LENGTH_SHORT).show();
                            // Toast.makeText(Profile.this, "abcd"+status, Toast.LENGTH_SHORT).show();
                            if (status) {
                                final List<DoubtCommentModel> ab = new ArrayList<>();
                                JSONArray jsonArray = response.getJSONArray("data");
                                for (int i = 0; i < jsonArray.length(); i++) {

                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    DoubtCommentModel entity = new DoubtCommentModel();
                                    //  entity.setData(response.getString("namezz"));
                                    entity.setCommenter_name(jsonObject.getString("commenter_name"));
                                    entity.setComment(jsonObject.getString("comment"));
                                    entity.setCommenter_photo(jsonObject.getString("commenter_photo"));
                                    entity.setTime(jsonObject.getString("time"));
                                    entity.setMedia_name(jsonObject.getString("media_name"));
                                    entity.setParent_comment_id(jsonObject.getInt("parent_comment_id"));
                                    entity.setMode(jsonObject.getInt("mode"));

                                    ab.add(entity);
                                }
                                //reprog.setVisibility(View.GONE);
                                recycler_commment.setVisibility(View.VISIBLE);
                                relative_noComment.setVisibility(View.GONE);
                                myAdapter1 = new AdapterDoubtComment(context, ab);
                                recycler_commment.setAdapter(myAdapter1);


                            } else {

                                recycler_commment.setVisibility(View.GONE);
                                relative_noComment.setVisibility(View.VISIBLE);
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        // Toast.makeText(SignUpStepTwo.this, "" + response.toString(), Toast.LENGTH_SHORT).show();
                        //resultTextView.setText("String Response : "+ response.toString());
                    }

                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //  resultTextView.setText("Error getting response");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json; charset=UTF-8");
                params.put("Authorization", "Bearer " + sharedPreference_main.getToken());
                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);


    }

    private void DoComment(final String doubt_id) {

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        final JSONObject jsonBody = new JSONObject();
        try {


            jsonBody.put("doubt_id", doubt_id);
            jsonBody.put("commenter_id", sharedPreference_main.getUserId());
            jsonBody.put("comment", et_comment.getText().toString());
            jsonBody.put("commenter_type", sharedPreference_main.getUserType());
            jsonBody.put("media_name", userImageBase64);
            jsonBody.put("media_type", mediaType);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        // String URL = "http://13.233.162.24/nusys(UAT)/api/teacher/add_doubt_comment.php";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL_TEACHER + STUDENT_DOUBT_COMMENT_ADD, jsonBody,
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Boolean status = response.getBoolean("status");

                            if (status) {
                                et_comment.setText("");
                                userImageBase64 = "";
                                mediaType = "";
                                openGalley.setImageResource(R.drawable.ic_camera_alt_red_24dp);
                                getComments(doubt_id);
                                //  Toast.makeText(context, "" + response.getString("message"), Toast.LENGTH_SHORT).show();

                            } else {
                                Toast.makeText(context, "error" + response.getString("message"), Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //  resultTextView.setText("Error getting response");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json; charset=UTF-8");
                params.put("Authorization", "Bearer " + sharedPreference_main.getToken());
                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);


    }


    private void chooseImageFromGallery(final int code) {
        Dexter.withActivity(getActivity())
                .withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(pickPhoto, code);
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    private void takeCameraImage(final int code) {
        Dexter.withActivity(getActivity())
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
//                            fileName = System.currentTimeMillis() + ".jpg";

                            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(cameraIntent, code);
//                            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, getCacheImagePath(fileName));
//                            if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
//                                startActivityForResult(takePictureIntent, code);
//                            }
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {

            case CAMERA_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    Bitmap photo = (Bitmap) data.getExtras().get("data");
                    openGalley.setImageBitmap(photo);
                    getEncoded64ImageStringFromBitmap(photo);
                    userImageBase64 = getEncoded64ImageStringFromBitmap(photo);
                    // printLog(userImageBase64);
                    if (userImageBase64.equals("")) {
                        mediaType = "";
                    } else {
                        mediaType = "image";
                    }
//                    tv_userImage.setText(getCacheImagePath(fileName).toString());

//                  getCacheImagePath(fileName);
                }
                break;
            case GALLERY_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    Uri imageUri = data.getData();
                    openGalley.setImageURI(imageUri);
                    Bitmap bitmap;
                    try {
                        bitmap = BitmapFactory.decodeStream(getActivity().getContentResolver().openInputStream(imageUri));
                        Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, 500, 500, false);
                        userImageBase64 = getEncoded64ImageStringFromBitmap(resizedBitmap);
                        Log.e("exp", userImageBase64);
                        if (userImageBase64.equals("")) {
                            mediaType = "";
                        } else {
                            mediaType = "image";
                        }

                    } catch (FileNotFoundException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
//                    tv_userImage.setText(imageUri.getPath());
                break;


        }
    }

    public String getEncoded64ImageStringFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteFormat = stream.toByteArray();
        String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);
        //  printLog(imgString);
        return imgString;
    }

    private void openChooser(final int code1, final int code2) {

        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.chooser);
        TextView camera = dialog.findViewById(R.id.camera);
        TextView gallery = dialog.findViewById(R.id.gallery);
        dialog.show();
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takeCameraImage(code1);
                dialog.dismiss();
            }
        });
        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseImageFromGallery(code2);
                dialog.dismiss();


            }
        });
    }
}
