package com.nusys.studentedumax;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.nusys.studentedumax.model.LoginModel;
import com.nusys.studentedumax.commonModules.SharedPreference_main;
import com.nusys.studentedumax.retrofit.ApiClient;
import com.nusys.studentedumax.retrofit.ServiceInterface;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.nusys.studentedumax.Constants.CAMERA_REQUEST_CODE;

public class Login extends AppCompatActivity {

    EditText st_mobileNumber, st_password;
    Context context;
    SharedPreference_main sharedPreference_main;
    private TextView st_signUp, st_forgot_password;
    Button btn_logIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        init();
        listener();


    }

    private void init() {
        context = Login.this;
        st_mobileNumber = findViewById(R.id.st_mobile_num);
        st_password = findViewById(R.id.stPassword);
        btn_logIn = findViewById(R.id.st_login);
        st_signUp = findViewById(R.id.signup);
        st_forgot_password = findViewById(R.id.stForgotPassword);
        sharedPreference_main = SharedPreference_main.getInstance(context);

    }


    private void listener() {

        btn_logIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(st_mobileNumber.getText().toString())) {
                    st_mobileNumber.setError("field can't be empty");

                } else if (st_mobileNumber.getText().toString().length() < 10 || st_mobileNumber.getText().toString().length() > 13) {
                    Toast.makeText(Login.this, "Please enter valid phone number", Toast.LENGTH_SHORT).show();

                } else if (TextUtils.isEmpty(st_password.getText().toString())) {
                    st_password.setError("field can't be empty");

                } else {
                    doLogin();



                }
            }
        });
        st_signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Login.this, Signup.class);
                startActivity(i);
            }
        });
        st_forgot_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(Login.this, ForgotPassword.class));

            }
        });
    }


    private void doLogin() {


//        apiPresenter=new ApiPresenter(this,this);
        HashMap<String, String> map = new HashMap<>();
        map.put("mobile", st_mobileNumber.getText().toString());
        map.put("password", st_password.getText().toString());

        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<LoginModel> call = serviceInterface.doLogin(map);
        call.enqueue(new Callback<LoginModel>() {
            @Override
            public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                if (response.isSuccessful()) {
                    LoginModel bean = response.body();
                    if (bean.getStatus()) {
                        // Toast.makeText(context, "" + bean.getMessage(), Toast.LENGTH_SHORT).show();

                        sharedPreference_main.setIs_LoggedIn(true);
                        sharedPreference_main.setUserId(bean.getData().getId());
                        //Toast.makeText(context, "" + sharedPreference_main.getUserId(), Toast.LENGTH_SHORT).show();
                        // Toast.makeText(context, "" + bean.getData().getId(), Toast.LENGTH_SHORT).show();
                        sharedPreference_main.setUsername(bean.getData().getName());
                        sharedPreference_main.setUserEmail(bean.getData().getEmail());
                        sharedPreference_main.setUserType(bean.getData().getUser_type());
                        sharedPreference_main.setToken(bean.getJwt());
                        sharedPreference_main.setUserImage(bean.getData().getImage());
                        sharedPreference_main.setUserType("student");
                        takePermission();




                    } else {
                        Toast.makeText(context, "" + bean.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(context, "Something is wrong", Toast.LENGTH_SHORT).show();
                }
            }


            @Override
            public void onFailure(Call<LoginModel> call, Throwable t) {
                Toast.makeText(context, "Something is wrongs", Toast.LENGTH_SHORT).show();

            }
        });


//        apiPresenter.makeRequest(SIGNUP,map);
    }



    private void takePermission() {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.RECORD_AUDIO,Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {


                        finish();
                        startActivity(new Intent(Login.this, Home.class));
                        finish();
                    }


                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();

    }
}
