package com.nusys.studentedumax;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.nusys.studentedumax.adapter.AdapterTeacher;
import com.nusys.studentedumax.interfaces.GetTeacherIds;
import com.nusys.studentedumax.commonModules.SharedPreference_main;
import com.nusys.studentedumax.model.TeacherModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.nusys.studentedumax.Constants.BASE_URL;
import static com.nusys.studentedumax.Constants.SIGNUP;
import static com.nusys.studentedumax.Constants.TEACH_FOLLOW;

public class SignUpStepTwo extends AppCompatActivity implements GetTeacherIds {
    // RecyclerView order_list;
    Button btn_SignUp;
    SharedPreference_main sharedPreference_main;
    AdapterTeacher myAdapter;
    private RecyclerView select_teache;
    //    ProgressBar reprog;
    LinearLayout reapi;
    ArrayList<TeacherModel> ruleslist = new ArrayList<TeacherModel>();
    private String URLstring = "http://13.233.162.24/nusys(UAT)/api/student/show_teacher_follow.php";
    String i_name, i_email, i_pwd, i_mobile, i_exam;
    TextView tv_no_record;
    ArrayList<String> ids;
    private String ExamID;
    ProgressDialog pd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_step_two);

        sharedPreference_main = SharedPreference_main.getInstance(this);

        Intent intentSign = getIntent();
        i_name = intentSign.getStringExtra("name");
        i_email = intentSign.getStringExtra("email");
        i_pwd = intentSign.getStringExtra("password");
        i_mobile = intentSign.getStringExtra("mobile");
        i_exam = intentSign.getStringExtra("exam");
        ids = new ArrayList<>();
        pd = new ProgressDialog(this);
        pd.setMessage("loading");
        hitTeacherApi();


        //Toast.makeText(this, ""+i_exam, Toast.LENGTH_LONG).show();
        select_teache = findViewById(R.id.selectTeach);
        select_teache.setLayoutManager(new LinearLayoutManager(this));

        //   reapi = findViewById(R.id.linearhit);
//        reprog = findViewById(R.id.linearprog);
        tv_no_record = findViewById(R.id.no_record);
        btn_SignUp = findViewById(R.id.btnSignup);

        btn_SignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                post_Json_Request_signup();
            }
        });

    }


    // json request for signup for row data
    private void post_Json_Request_signup() {

        pd.show();

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        JSONObject jsonBody = new JSONObject();
        try {
            //input your API parameters

            jsonBody.put("name", getIntent().getStringExtra("name"));
            jsonBody.put("email", getIntent().getStringExtra("email"));
            jsonBody.put("mobile", getIntent().getStringExtra("mobile"));
            jsonBody.put("course_id", getIntent().getStringExtra("exam"));
            jsonBody.put("password", getIntent().getStringExtra("password"));
            jsonBody.put("teacher_id", ExamID);

           /* jsonBody.put("email", sharedPreference_main.getemail());
            jsonBody.put("mobile", sharedPreference_main.getmobile());
            jsonBody.put("course_id", "1,2");
            jsonBody.put("password", sharedPreference_main.getpassword());
            jsonBody.put("teacher_id", "1,2");*/

        } catch (JSONException e) {
            e.printStackTrace();
        }
        // Enter the correct url for your api service site
        // String url = getResources().getString(R.string.url);
        //String URL = "http://13.233.162.24/nusys(UAT)/api/student/register.php";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL+SIGNUP, jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Boolean status = response.getBoolean("status");
                            if (status) {
                                pd.dismiss();

                                //Toast.makeText(SignUpStepTwo.this, "true"+response.getString("message"), Toast.LENGTH_SHORT).show();
                              //  Toast.makeText(SignUpStepTwo.this, "" + response.getString("message"), Toast.LENGTH_SHORT).show();
                                Intent i = new Intent(SignUpStepTwo.this, Login.class);
                                startActivity(i);
                            } else {
                                pd.dismiss();
                                Toast.makeText(SignUpStepTwo.this, "false", Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            pd.dismiss();

                            e.printStackTrace();
                        }
                       // Toast.makeText(SignUpStepTwo.this, "" + response.toString(), Toast.LENGTH_LONG).show();
                        Log.e("response1", response.toString());
                        //resultTextView.setText("String Response : "+ response.toString());
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();

                //  resultTextView.setText("Error getting response");
            }
        });
        requestQueue.add(jsonObjectRequest);


    }

    private void hitTeacherApi() {
        pd.show();


        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        final JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("subject_id", getIntent().getStringExtra("exam"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        // Enter the correct url for your api service site
        // String url = getResources().getString(R.string.url);
       // String URL = "http://13.233.162.24/nusys(UAT)/api/student/show_teacher_follow.php";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL+TEACH_FOLLOW, jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Boolean status = response.getBoolean("status");
                            Log.e("response", response.toString());
                            if (status) {
                                JSONArray jsonArray = response.getJSONArray("data");

                                for (int i = 0; i < jsonArray.length(); i++) {

                                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                                    TeacherModel entity = new TeacherModel();
                                    //  entity.setData(response.getString("namezz"));
                                    entity.setName(jsonObject.getString("name"));
                                    entity.setId(jsonObject.getString("id"));
                                    entity.setProfile_image(jsonObject.getString("profile_image"));
                                    entity.setExpr_year("Experience Year:" + jsonObject.getString("expr_year"));
                                    //entity.setExperience(jsonObject.getString("experience"));

                                    ruleslist.add(entity);
                                }
//                                reprog.setVisibility(View.GONE);
                                myAdapter = new AdapterTeacher(SignUpStepTwo.this, ruleslist, SignUpStepTwo.this);
                                select_teache.setAdapter(myAdapter);
                                pd.dismiss();


                                //Toast.makeText(SignUpStepTwo.this, "true"+response.getString("message"), Toast.LENGTH_SHORT).show();
                                //  Toast.makeText(SignUpStepTwo.this, "true", Toast.LENGTH_SHORT).show();
                            } else {
                                pd.dismiss();

//                                reprog.setVisibility(View.GONE);
                                tv_no_record.setVisibility(View.VISIBLE);

                                // Toast.makeText(SignUpStepTwo.this, "false", Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            pd.dismiss();

                            e.printStackTrace();
                        }

                        // Toast.makeText(SignUpStepTwo.this, "" + response.toString(), Toast.LENGTH_SHORT).show();
                        //resultTextView.setText("String Response : "+ response.toString());
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();

                //  resultTextView.setText("Error getting response");
            }
        });
        requestQueue.add(jsonObjectRequest);


    }

    @Override
    public void getTeacher(ArrayList<String> ids) {
        this.ids = ids;
        Log.e("ids", ids.toString());
        String P_id = "";
        StringBuilder pd = new StringBuilder();
        for (Object mPI : ids) {
            pd.append(P_id).append(mPI);
            P_id = ",";
        }
        ExamID = pd.toString();
    }
}
