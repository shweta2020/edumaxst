package com.nusys.studentedumax;

import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.nusys.studentedumax.commonModules.SharedPreference_main;

public class ClassroomFragment extends Fragment {
    SharedPreference_main sharedPreference_main;
    LinearLayout llLiveSchedule;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_classroom_fragment, container, false);


        llLiveSchedule =rootView.findViewById(R.id.ll_live_schedule);
        llLiveSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(getActivity(), CalenderWebview.class);
                startActivity(i);

            }
        });
        return rootView;
    }

}
