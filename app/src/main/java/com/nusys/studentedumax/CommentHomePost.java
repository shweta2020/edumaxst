package com.nusys.studentedumax;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.transition.TransitionInflater;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.commit451.elasticdragdismisslayout.ElasticDragDismissFrameLayout;
import com.commit451.elasticdragdismisslayout.ElasticDragDismissLinearLayout;
import com.commit451.elasticdragdismisslayout.ElasticDragDismissListener;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.nusys.studentedumax.adapter.AdapterShowComment;
import com.nusys.studentedumax.commonModules.SharedPreference_main;
import com.nusys.studentedumax.model.ShowCommentModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.nusys.studentedumax.Constants.BASE_URL_TEACHER;
import static com.nusys.studentedumax.Constants.CAMERA_REQUEST_CODE;
import static com.nusys.studentedumax.Constants.GALLERY_REQUEST_CODE;
import static com.nusys.studentedumax.Constants.MAKE_COMMENT;
import static com.nusys.studentedumax.Constants.POST_COMMENT;

public class CommentHomePost extends AppCompatActivity {
    RecyclerView rv_comment_view;
    AdapterShowComment myAdapter;
    private List<ShowCommentModel> ruleslist;
    Context context;
    String i_id;
    ImageView ivCommentImg, postComment;
    private String userImageBase64;
    SharedPreference_main sharedPreference_main;
    EditText etComment;
    String media_type;
    LinearLayout llComments, relativeNoComment;

    ElasticDragDismissLinearLayout mDraggableFrame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment_home_post);

        sharedPreference_main = SharedPreference_main.getInstance(context);
        etComment = findViewById(R.id.et_comment);
        llComments = findViewById(R.id.ll_comments);

        mDraggableFrame = findViewById(R.id.draggable_frame);

        relativeNoComment = findViewById(R.id.relative_noComment);

        /*showing activity like dialoge box**/
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.x = -20;
        params.height = 1300;
        params.width = 750;
        params.y = -10;

        this.getWindow().setAttributes(params);

        /* for closing activity using gragging*/
        mDraggableFrame.addListener(new ElasticDragDismissListener() {
            @Override
            public void onDrag(float elasticOffset, float elasticOffsetPixels, float rawOffset, float rawOffsetPixels) {
            }

            @Override
            public void onDragDismissed() {
                //if you are targeting 21+ you might want to finish after transition
                finish();
            }
        });
        //post comment
        postComment = findViewById(R.id.post_comment);
        postComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postCommentApi();
            }
        });


        //open camera and set image
        ivCommentImg = findViewById(R.id.iv_comment_img);

        ivCommentImg.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                // media_type= "image";
                openChooser(CAMERA_REQUEST_CODE, GALLERY_REQUEST_CODE);

            }
        });

       // view comments
        context = CommentHomePost.this;
        Intent in = getIntent();
        i_id = in.getStringExtra("postId");

        rv_comment_view = findViewById(R.id.rvCommentView);
        //LinearLayoutManager layoutManager = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);
        rv_comment_view.setLayoutManager(linearLayoutManager);

        myAdapter = new AdapterShowComment(context, ruleslist);
        rv_comment_view.setAdapter(myAdapter);
        viewCommentApi();

    }

    private void viewCommentApi() {

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        final JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("post_id", i_id);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        // String URL = "http://13.233.162.24/nusys(UAT)/api/teacher/post_comment.php";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL_TEACHER + POST_COMMENT, jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Boolean status = response.getBoolean("status");

                            if (status) {
                                final List<ShowCommentModel> ab = new ArrayList<>();
                                JSONArray jsonArray = response.getJSONArray("data");
                                for (int i = 0; i < jsonArray.length(); i++) {

                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    ShowCommentModel entity = new ShowCommentModel();
                                    //  entity.setData(response.getString("namezz"));
                                    entity.setCommenter_name(jsonObject.getString("commenter_name"));
                                    entity.setComment(jsonObject.getString("comment"));
                                    entity.setCommenter_photo(jsonObject.getString("commenter_photo"));
                                    entity.setTime(jsonObject.getString("time"));
                                    entity.setMedia_name(jsonObject.getString("media_name"));

                                    ab.add(entity);
                                }
                                rv_comment_view.setVisibility(View.VISIBLE);
                                relativeNoComment.setVisibility(View.GONE);
                                //reprog.setVisibility(View.GONE);
                                myAdapter = new AdapterShowComment(context, ab);
                                rv_comment_view.setAdapter(myAdapter);

                            } else {
                                rv_comment_view.setVisibility(View.GONE);
                                relativeNoComment.setVisibility(View.VISIBLE);
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        // Toast.makeText(SignUpStepTwo.this, "" + response.toString(), Toast.LENGTH_SHORT).show();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //  resultTextView.setText("Error getting response");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json; charset=UTF-8");
                params.put("Authorization", "Bearer " + sharedPreference_main.getToken());
                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);


    }

    private void postCommentApi() {

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        final JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("post_id", i_id);
            jsonBody.put("commenter_id", sharedPreference_main.getUserId());
            jsonBody.put("comment", etComment.getText().toString());
            jsonBody.put("commenter_type", sharedPreference_main.getUserType());
            jsonBody.put("media_name", userImageBase64);
            jsonBody.put("media_type", media_type);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        //String URL = "http://13.233.162.24/nusys(UAT)/api/teacher/make_comment.php";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL_TEACHER + MAKE_COMMENT, jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Boolean status = response.getBoolean("status");

                            if (status) {
                                etComment.setText("");
                                userImageBase64 = "";
                                media_type = "";
                                ivCommentImg.setImageResource(R.drawable.ic_camera_alt_red_24dp);
                                viewCommentApi();
                                //  Toast.makeText(context, "" + response.getString("message"), Toast.LENGTH_SHORT).show();

                            } else {
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //  resultTextView.setText("Error getting response");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json; charset=UTF-8");
                params.put("Authorization", "Bearer " + sharedPreference_main.getToken());
                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);


    }

    private void chooseImageFromGallery(final int code) {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(pickPhoto, code);
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();

    }

    private void takeCameraImage(final int code) {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
//                            fileName = System.currentTimeMillis() + ".jpg";

                            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(cameraIntent, code);

                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {

            case CAMERA_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    Bitmap photo = (Bitmap) data.getExtras().get("data");
                    ivCommentImg.setImageBitmap(photo);
                    getEncoded64ImageStringFromBitmap(photo);
                    userImageBase64 = getEncoded64ImageStringFromBitmap(photo);
                    if (userImageBase64.equals(null)) {
                        media_type = null;
                    } else {
                        media_type = "image";
                    }
                    Log.e("excam", userImageBase64);
//                    tv_userImage.setText(getCacheImagePath(fileName).toString());

//                  getCacheImagePath(fileName);
                }
                break;
            case GALLERY_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    Uri imageUri = data.getData();
                    ivCommentImg.setImageURI(imageUri);
                    Bitmap bitmap;
                    try {
                        bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(imageUri));
                        Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, 500, 500, false);
                        userImageBase64 = getEncoded64ImageStringFromBitmap(resizedBitmap);
                        Log.e("exp", userImageBase64);
                        if (userImageBase64.equals(null)) {
                            media_type = null;
                        } else {
                            media_type = "image";
                        }


                    } catch (FileNotFoundException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
//                    tv_userImage.setText(imageUri.getPath());

                break;
        }
    }

    public String getEncoded64ImageStringFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteFormat = stream.toByteArray();
        String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);
        Log.e("", imgString);
        return imgString;
    }

    private void openChooser(final int code1, final int code2) {

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.chooser);
        TextView camera_Comment = dialog.findViewById(R.id.camera);
        TextView gallery_Comment = dialog.findViewById(R.id.gallery);
        dialog.show();
        camera_Comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takeCameraImage(code1);
                dialog.dismiss();
            }
        });
        gallery_Comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseImageFromGallery(code2);
                dialog.dismiss();


            }
        });
    }
}
