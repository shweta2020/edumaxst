package com.nusys.studentedumax;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.nusys.studentedumax.commonModules.SharedPreference_main;
import com.nusys.studentedumax.model.LoginModel;
import com.nusys.studentedumax.model.ResponseTestList;
import com.nusys.studentedumax.retrofit.ApiClient;
import com.nusys.studentedumax.retrofit.ServiceInterface;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InstructionTest extends AppCompatActivity {
    TextView tv_toolbar_head1, tv_instructions, tv_start_exam;
    LinearLayout ll_back_activity;
    ImageView back, close;
    String[] language = {"English", "Hindi"};
    private ArrayList<String> spin_language_id = new ArrayList<String>();
    Spinner sp_lang;
    SharedPreference_main sharedPreference_main;
    Context context;
    String lang_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instruction_test);

        context = InstructionTest.this;
        back = findViewById(R.id.back);
        close = findViewById(R.id.close);
        tv_toolbar_head1 = findViewById(R.id.tv_toolbar_head);
        ll_back_activity = findViewById(R.id.ll_back_activity);
        SharedPreference_main.getInstance(context);

        tv_toolbar_head1.setText("Instruction");
        back.setVisibility(View.GONE);
        close.setVisibility(View.VISIBLE);

        ll_back_activity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        sp_lang = findViewById(R.id.sp_language);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, language);
        sp_lang.setAdapter(adapter);
        sp_lang.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (sp_lang.getItemIdAtPosition(position) == 0) {
                    lang_id = "1";
                } else {
                    lang_id = "2";
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        tv_instructions = findViewById(R.id.tv_instructions);
        //tv_instructions.setText(getIntent().getStringExtra("test_dis"));

        tv_instructions.setText(Html.fromHtml(Html.fromHtml(getIntent().getStringExtra("test_dis")).toString()));

        tv_start_exam = findViewById(R.id.tv_start_exam);
        tv_start_exam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, PlayQuiz2Method.class);
                i.putExtra("testId", getIntent().getStringExtra("test_id"));
                i.putExtra("language", lang_id);
                startActivity(i);
                finish();
            }
        });

    }

}
