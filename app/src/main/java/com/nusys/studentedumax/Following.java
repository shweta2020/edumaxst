package com.nusys.studentedumax;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.nusys.studentedumax.adapter.AdapterFollowing;
import com.nusys.studentedumax.commonModules.PaginationAdapterCallback;
import com.nusys.studentedumax.commonModules.PaginationScrollListener;
import com.nusys.studentedumax.model.FollowingModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeoutException;

import static com.nusys.studentedumax.Constants.BASE_URL;
import static com.nusys.studentedumax.Constants.STUDENT_FOLLOWER;

public class Following extends BaseActivity implements PaginationAdapterCallback, SwipeRefreshLayout.OnRefreshListener {
    RecyclerView rv_following_view;
    AdapterFollowing myAdapter;
    Context context;
    LinearLayoutManager linearLayoutManager;
    private static int TOTAL_PAGES;
    ProgressBar progressBar;
    LinearLayout errorLayout;
    Button btnRetry;
    SwipeRefreshLayout swipeRefreshLayout;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int currentPage = 1;


    private List<FollowingModel> ruleslist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_following, frameLayout);
        setTitle("Following");

        context = Following.this;
        progressBar = findViewById(R.id.main_progress);
        errorLayout = findViewById(R.id.error_layout);
        btnRetry = findViewById(R.id.error_btn_retry);
        rv_following_view = findViewById(R.id.rvFollowingView);
        swipeRefreshLayout = findViewById(R.id.refresh_layout);
        //frame_container = findViewById(R.id.frame_container);
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rv_following_view.setLayoutManager(linearLayoutManager);
        rv_following_view.setItemAnimator(new DefaultItemAnimator());
        myAdapter = new AdapterFollowing(context);
        rv_following_view.setAdapter(myAdapter);






        loadFirstPage();

        rv_following_view.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;

                loadNextPage();
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        //loadFirstPage();
        btnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadFirstPage();
            }
        });

        swipeRefreshLayout.setOnRefreshListener(this);


        // hitFollowingApi();


        // setContentView(R.layout.activity_following);
    }

    public void onRefresh() {
        // TODO: Check if data is stale.
        //  Execute network request if cache is expired; otherwise do not update data.
        myAdapter.clear();
        myAdapter.notifyDataSetChanged();
        progressBar.setVisibility(View.VISIBLE);
        isLastPage = false;
        loadFirstPage();
        swipeRefreshLayout.setRefreshing(false);
    }




    private void loadFirstPage() {

        currentPage = 1;
        hideErrorView();

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        final JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("user_id", sharedPreference_main.getUserId());
            // jsonBody.put("page", "1");
            jsonBody.put("page", currentPage);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        //String URL = "http://13.233.162.24/nusys(UAT)/api/student/student_followers.php";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL+STUDENT_FOLLOWER, jsonBody,
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("response", response.toString());
                        try {
                            myAdapter.clear();
                            myAdapter.notifyDataSetChanged();
                            Boolean status = response.getBoolean("status");
                            //Toast.makeText(Profile.this, "", Toast.LENGTH_SHORT).show();
                            // Toast.makeText(Profile.this, "abcd"+status, Toast.LENGTH_SHORT).show();
                            if (status) {

                                final List<FollowingModel> ab = new ArrayList<>();
                                //JSONObject jsonObj = response.getJSONObject("data");
                                JSONArray jsonArray = response.getJSONArray("data");
                                for (int i = 0; i < jsonArray.length(); i++) {

                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    FollowingModel entity = new FollowingModel();
                                    entity.setName(jsonObject.getString("name"));
                                    entity.setTeacher_image(jsonObject.getString("teacher_image"));
                                    entity.setExp_year("Experience Year " + jsonObject.getString("exp_year"));
                                    entity.setTeacher_id(jsonObject.getString("teacher_id"));
                                    entity.setTeacher_image(jsonObject.getString("teacher_image"));

                                    ab.add(entity);
                                }
                                //reprog.setVisibility(View.GONE);
                                //myAdapter = new PaginationAdapter(MainActivity.this, ab);
                                //rv_post_list.setAdapter(myAdapter);
                                if (ab != null) {

                                    TOTAL_PAGES = ab.size();
                                    Log.e("totalPage", String.valueOf(ab.size()));
                                    myAdapter.addAll(ab);
//                                    rv_post_list.setAdapter(myAdapter);

                                } else {
                                    errorLayout.setVisibility(View.VISIBLE);
                                    Toast.makeText(context, "Something is wrong try again later", Toast.LENGTH_SHORT).show();
                                }
                                progressBar.setVisibility(View.GONE);
                                if (currentPage <= TOTAL_PAGES) myAdapter.addLoadingFooter();
                                else isLastPage = true;
                            } else {
                                errorLayout.setVisibility(View.VISIBLE);
                                Toast.makeText(context, "Something is wrong try again later", Toast.LENGTH_SHORT).show();

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        // Toast.makeText(SignUpStepTwo.this, "" + response.toString(), Toast.LENGTH_SHORT).show();
                        //resultTextView.setText("String Response : "+ response.toString());
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                errorLayout.setVisibility(View.VISIBLE);
                //  resultTextView.setText("Error getting response");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json; charset=UTF-8");
                params.put("Authorization", "Bearer " + sharedPreference_main.getToken());
                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);
    }

    @Override
    protected void onStart() {
        super.onStart();

//        loadFirstPage();
//        onRefresh();
    }

    private void loadNextPage() {

        Log.d("page", "loadNextPage: " + currentPage);
        Log.e("total", String.valueOf(TOTAL_PAGES));
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                final JSONObject jsonBody = new JSONObject();
                try {
                    jsonBody.put("user_id", sharedPreference_main.getUserId());
                    jsonBody.put("page", String.valueOf(currentPage));


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                // Enter the correct url for your api service site
                // String url = getResources().getString(R.string.url);
               // String URL = "http://13.233.162.24/nusys(UAT)/api/student/student_followers.php";
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL+STUDENT_FOLLOWER, jsonBody,
                        new com.android.volley.Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                Log.e("response", response.toString());
                                try {

                                    Boolean status = response.getBoolean("status");
                                    //Toast.makeText(Profile.this, "", Toast.LENGTH_SHORT).show();
                                    // Toast.makeText(Profile.this, "abcd"+status, Toast.LENGTH_SHORT).show();
                                    if (status) {
                                        myAdapter.removeLoadingFooter();
                                        isLoading = false;

                                        final List<FollowingModel> ab = new ArrayList<>();
                                        //JSONObject jsonObj = response.getJSONObject("data");
                                        JSONArray jsonArray = response.getJSONArray("data");
                                        for (int i = 0; i < jsonArray.length(); i++) {

                                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                                            FollowingModel entity = new FollowingModel();
                                            entity.setName(jsonObject.getString("name"));
                                            entity.setTeacher_image(jsonObject.getString("teacher_image"));
                                            entity.setExp_year("Experience Year " + jsonObject.getString("exp_year"));
                                            entity.setTeacher_id(jsonObject.getString("teacher_id"));
                                            entity.setTeacher_image(jsonObject.getString("teacher_image"));
                                            ab.add(entity);
                                        }
                                        //reprog.setVisibility(View.GONE);
                                        //myAdapter = new PaginationAdapter(MainActivity.this, ab);
                                        // rv_post_list.setAdapter(myAdapter);

                                        if (ab != null) {
                                            //TOTAL_PAGES = ab.size();
                                            myAdapter.addAll(ab);
                                            if (currentPage != TOTAL_PAGES)
                                                myAdapter.addLoadingFooter();
                                            else isLastPage = true;

                                        } else {
                                            myAdapter.removeLoadingFooter();
                                            isLastPage = true;
                                            //Toast.makeText(Home.this, "Something is wrong try again later", Toast.LENGTH_SHORT).show();

                                        }

                                    } else {
                                        myAdapter.removeLoadingFooter();
                                        isLastPage = true;
                                        //Toast.makeText(Home.this, "Something is wrong try again later", Toast.LENGTH_SHORT).show();

                                    }


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    myAdapter.removeLoadingFooter();

                                    isLastPage = true;
                                }

                                // Toast.makeText(SignUpStepTwo.this, "" + response.toString(), Toast.LENGTH_SHORT).show();
                                //resultTextView.setText("String Response : "+ response.toString());
                            }
                        }, new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        isLastPage = true;
                        myAdapter.removeLoadingFooter();
                        Log.e("error", error.getMessage());
                        //  resultTextView.setText("Error getting response");
                    }
                }) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("Content-Type", "application/json; charset=UTF-8");
                        params.put("Authorization", "Bearer " + sharedPreference_main.getToken());
                        return params;
                    }
                };
                requestQueue.add(jsonObjectRequest);

            }
        }, 2000);

    }


    @Override
    public void retryPageLoad() {
        loadNextPage();
    }


    /**
     * @param throwable required for {@link #fetchErrorMessage(Throwable)}
     * @return
     */
    private void showErrorView(Throwable throwable) {

        if (errorLayout.getVisibility() == View.GONE) {
            errorLayout.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);

            //txtError.setText(fetchErrorMessage(throwable));
        }
    }

    /**
     * @param throwable to identify the type of error
     * @return appropriate error message
     */
    private String fetchErrorMessage(Throwable throwable) {
        String errorMsg = getResources().getString(R.string.error_msg_unknown);

        if (!isNetworkConnected()) {
            errorMsg = getResources().getString(R.string.error_msg_no_internet);
        } else if (throwable instanceof TimeoutException) {
            errorMsg = getResources().getString(R.string.error_msg_timeout);
        }

        return errorMsg;
    }

    // Helpers -------------------------------------------------------------------------------------


    private void hideErrorView() {
        if (errorLayout.getVisibility() == View.VISIBLE) {
            errorLayout.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Remember to add android.permission.ACCESS_NETWORK_STATE permission.
     *
     * @return
     */
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }


}
