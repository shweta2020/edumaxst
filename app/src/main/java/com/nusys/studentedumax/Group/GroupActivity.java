package com.nusys.studentedumax.Group;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.nusys.studentedumax.BaseActivity;
import com.nusys.studentedumax.R;
import com.nusys.studentedumax.UpdateProfile;
import com.nusys.studentedumax.ViewAnimation;
import com.nusys.studentedumax.adapter.Group_Adapter.Adapter_group_view;
import com.nusys.studentedumax.commonModules.SharedPreference_main;
import com.nusys.studentedumax.interfaces.GetGroupIds;
import com.nusys.studentedumax.model.ResponeDeleteGroup;
import com.nusys.studentedumax.model.ResponeUpdateGroupImage;
import com.nusys.studentedumax.model.ResponeUpdateGroupName;
import com.nusys.studentedumax.model.ResponeViewGroup;
import com.nusys.studentedumax.retrofit.ApiClient;
import com.nusys.studentedumax.retrofit.ServiceInterface;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

import static androidx.core.content.FileProvider.getUriForFile;
import static com.nusys.studentedumax.Constants.CAMERA_REQUEST_CODE;
import static com.nusys.studentedumax.Constants.Content_Type;
import static com.nusys.studentedumax.Constants.GALLERY_REQUEST_CODE;

public class GroupActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener, GetGroupIds {
    SharedPreference_main sharedPreference_main;
    RecyclerView recyclerView_group_view;
    Adapter_group_view Adapter;
    FloatingActionButton fab_group;

    SwipeRefreshLayout refresh_layout;
    private String fileName;
    ProgressBar main_progress;
    LinearLayout errorLayout;
    TextView error_txt_cause;
    Button error_btn_retry;
    Context context;
    private String userImageBase64;
    Dialog dialog;
    ImageView grpImage;
    EditText etGroupName;
    ImageView imgGroup;
    TextView tv_UpdateGroup;
    String grp_id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_group, frameLayout);
        setTitle("Group");
        sharedPreference_main = SharedPreference_main.getInstance(this);
        refresh_layout = findViewById(R.id.refresh_layout_group);
       // main_progress = findViewById(R.id.main_progress);
        errorLayout = findViewById(R.id.error_layout);

        refresh_layout.setOnRefreshListener(this);
        refresh_layout.setColorSchemeColors(Color.RED, Color.YELLOW, Color.BLUE);

        context = GroupActivity.this;
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_update_group);
        grpImage = dialog.findViewById(R.id.cv_grpImage);
        etGroupName = dialog.findViewById(R.id.et_GroupName);
        imgGroup = dialog.findViewById(R.id.iv_imgGroup);
        tv_UpdateGroup = dialog.findViewById(R.id.tv_update_group);


        recyclerView_group_view = findViewById(R.id.rv_group_view);
        fab_group = findViewById(R.id.fab);

        fab_group.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(GroupActivity.this, Create_Group.class));
            }
        });
        view_group();

    }

    @Override
    protected void onStart() {
        super.onStart();
        view_group();

    }

    private void view_group() {

        HashMap<String, String> map = new HashMap<>();

        map.put("student_id", sharedPreference_main.getUserId());

        //   if (NetworkUtils.isConnected(getActivity())) {
        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<ResponeViewGroup> call = serviceInterface.view_group("Bearer " + sharedPreference_main.getToken(), Content_Type, map);
        call.enqueue(new Callback<ResponeViewGroup>() {


            @Override
            public void onResponse(Call<ResponeViewGroup> call, retrofit2.Response<ResponeViewGroup> response) {

                if (response.isSuccessful()) {

                    ResponeViewGroup bean = response.body();

                    // Toast.makeText(GroupActivity.this, "sucess", Toast.LENGTH_SHORT).show();
                    if (bean.getStatus()) {
                        errorLayout.setVisibility(View.GONE);
                        recyclerView_group_view.setVisibility(View.VISIBLE);
                        ResponeViewGroup view = response.body();
                        Adapter = new Adapter_group_view(GroupActivity.this, bean.getData(), GroupActivity.this);
//                        recyclerView_group_view.setLayoutManager(new GridLayoutManager(GroupActivity.this, 2));
//                        recyclerView_group_view.setItemAnimator(new DefaultItemAnimator());
                        recyclerView_group_view.setAdapter(Adapter);

                        Log.e("Group_response", view.toString());

                    } else {
                        errorLayout.setVisibility(View.VISIBLE);
                        recyclerView_group_view.setVisibility(View.GONE);
                        Toast.makeText(GroupActivity.this, bean.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    errorLayout.setVisibility(View.VISIBLE);
                    recyclerView_group_view.setVisibility(View.GONE);
                    Toast.makeText(GroupActivity.this, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponeViewGroup> call, Throwable t) {
                errorLayout.setVisibility(View.VISIBLE);
                recyclerView_group_view.setVisibility(View.GONE);
                Log.e("error", t.getMessage());
                Toast.makeText(GroupActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }



    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                view_group();
                refresh_layout.setRefreshing(false);
            }
        }, 2000);

    }

    @Override
    public void getGroupIds(final String groupId, final String groupName, String groupImage) {

        this.grp_id = groupId;
        etGroupName.setText(groupName);
        Glide.with(context)
                .load(groupImage)
                .placeholder(R.drawable.user_icon)
                .into(grpImage);

        imgGroup.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                openChooser(CAMERA_REQUEST_CODE, GALLERY_REQUEST_CODE);

            }
        });

        tv_UpdateGroup.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                update_group_name(groupId, etGroupName.getText().toString());

            }
        });
        dialog.getWindow().getDecorView().setTop(100);
        dialog.getWindow().getDecorView().setLeft(100);
        dialog.show();


    }

    private void update_group_name(String groupId, String groupName) {

        HashMap<String, String> map = new HashMap<>();

        map.put("id", groupId);
        map.put("group_name", groupName);

        //   if (NetworkUtils.isConnected(getActivity())) {
        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<ResponeUpdateGroupName> call = serviceInterface.update_group_name("Bearer " + sharedPreference_main.getToken(), Content_Type, map);
        call.enqueue(new Callback<ResponeUpdateGroupName>() {


            @Override
            public void onResponse(Call<ResponeUpdateGroupName> call, retrofit2.Response<ResponeUpdateGroupName> response) {

                if (response.isSuccessful()) {

                    ResponeUpdateGroupName bean = response.body();
                    Toast.makeText(context, bean.getMessage(), Toast.LENGTH_LONG).show();
                    // Toast.makeText(context, "sucess", Toast.LENGTH_SHORT).show();
                    if (bean.getStatus()) {

                        ResponeUpdateGroupName view = response.body();
                        Intent intent = new Intent(context, GroupActivity.class);
                        context.startActivity(intent);
                        dialog.dismiss();

                        Log.e("Group_response", view.toString());

                    } else {

                        Toast.makeText(context, bean.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {

                    Toast.makeText(context, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponeUpdateGroupName> call, Throwable t) {

                Log.e("error", t.getMessage());
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void chooseImageFromGallery(final int code) {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(pickPhoto, code);
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();

    }

    private void takeCameraImage(final int code) {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            fileName = System.currentTimeMillis() + ".jpg";

                            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(cameraIntent, code);
//                            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, getCacheImagePath(fileName));
//                            if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
//                                startActivityForResult(takePictureIntent, code);
//                            }
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();

    }

    private Uri getCacheImagePath(String fileName) {
        File path = new File(getExternalCacheDir(), "camera");
        if (!path.exists()) path.mkdirs();
        File image = new File(path, fileName);
        return getUriForFile(this, getPackageName() + ".provider", image);
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {

            case CAMERA_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    Bitmap photo = (Bitmap) data.getExtras().get("data");
                    grpImage.setImageBitmap(photo);
                    getEncoded64ImageStringFromBitmap(photo);
                    userImageBase64 = getEncoded64ImageStringFromBitmap(photo);
                    Log.e("excam", userImageBase64);
                    update_group_image(grp_id, userImageBase64);
                }
                break;
            case GALLERY_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    Uri imageUri = data.getData();
                    grpImage.setImageURI(imageUri);
                    Bitmap bitmap;
                    try {
                        bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(imageUri));
                        Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, 500, 500, false);
                        userImageBase64 = getEncoded64ImageStringFromBitmap(resizedBitmap);
                        Log.e("exp", userImageBase64);
                        update_group_image(grp_id, userImageBase64);

                    } catch (FileNotFoundException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }

                break;

        }
    }

    public String getEncoded64ImageStringFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteFormat = stream.toByteArray();
        String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);
        Log.e("", imgString);
        return imgString;
    }

    private void openChooser(final int code1, final int code2) {

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.chooser);
        TextView tv_camera = dialog.findViewById(R.id.camera);
        TextView tv_gallery = dialog.findViewById(R.id.gallery);
        dialog.show();
        tv_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takeCameraImage(code1);
                dialog.dismiss();
            }
        });
        tv_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseImageFromGallery(code2);
                dialog.dismiss();

            }
        });
    }

    private void update_group_image(String groupId, String groupImage) {

        HashMap<String, String> map = new HashMap<>();

        map.put("group_id", groupId);
        map.put("filename", groupImage);

        //   if (NetworkUtils.isConnected(getActivity())) {
        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<ResponeUpdateGroupImage> call = serviceInterface.update_group_image("Bearer " + sharedPreference_main.getToken(), Content_Type, map);
        call.enqueue(new Callback<ResponeUpdateGroupImage>() {


            @Override
            public void onResponse(Call<ResponeUpdateGroupImage> call, retrofit2.Response<ResponeUpdateGroupImage> response) {

                if (response.isSuccessful()) {

                    ResponeUpdateGroupImage bean = response.body();
                    Toast.makeText(context, bean.getMessage(), Toast.LENGTH_LONG).show();
                    // Toast.makeText(context, "sucess", Toast.LENGTH_SHORT).show();
                    if (bean.getStatus()) {
                        ResponeUpdateGroupImage view = response.body();
                        Intent intent = new Intent(context, GroupActivity.class);
                        context.startActivity(intent);


                        Log.e("Group_response", view.toString());

                    } else {
                        Toast.makeText(context, bean.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {

                    Toast.makeText(context, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponeUpdateGroupImage> call, Throwable t) {
                Log.e("error", t.getMessage());
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

}
