package com.nusys.studentedumax.Group;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.nusys.studentedumax.R;
import com.nusys.studentedumax.adapter.Group_Adapter.Adapter_batch;
import com.nusys.studentedumax.commonModules.SharedPreference_main;
import com.nusys.studentedumax.model.ResponeStudentBatch;
import com.nusys.studentedumax.retrofit.ApiClient;
import com.nusys.studentedumax.retrofit.ServiceInterface;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;

import static com.nusys.studentedumax.Constants.Content_Type;

public class batch_Activity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    SwipeRefreshLayout refresh_layout;

    ProgressBar main_progress;
    LinearLayout errorLayout;
    TextView error_txt_cause;
    Button error_btn_retry;
    Context context;

    SharedPreference_main sharedPreference_main;
    RecyclerView recyclerView_group_view;
    Adapter_batch Adapter;
    TextView tv_toolbar_head1;
    LinearLayout ll_back_activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_batch_);
        sharedPreference_main = SharedPreference_main.getInstance(this);
        refresh_layout=findViewById(R.id.refreshLayoutBatch);
        main_progress=findViewById(R.id.mainProgressBatch);
        errorLayout=findViewById(R.id.error_layout);
       /* error_txt_cause=findViewById(R.id.error_txt_cause_batch);
        error_btn_retry=findViewById(R.id.error_btn_retry_batch);*/
        tv_toolbar_head1 = findViewById(R.id.tv_toolbar_head);
        ll_back_activity = findViewById(R.id.ll_back_activity);
        tv_toolbar_head1.setText("Batch");

        ll_back_activity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        refresh_layout.setOnRefreshListener(this);
        refresh_layout.setColorSchemeColors(Color.RED, Color.YELLOW, Color.BLUE);

        recyclerView_group_view = findViewById(R.id.rv_group_view_batch);
        sharedPreference_main.setGroupTypeId("0");
        student_group();
    }

    private void student_group() {

        HashMap<String, String> map = new HashMap<>();

        map.put("student_id", sharedPreference_main.getUserId());

        //   if (NetworkUtils.isConnected(getActivity())) {
        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<ResponeStudentBatch> call = serviceInterface.student_batch("Bearer " + sharedPreference_main.getToken(), Content_Type, map);
        call.enqueue(new Callback<ResponeStudentBatch>() {


            @Override
            public void onResponse(Call<ResponeStudentBatch> call, retrofit2.Response<ResponeStudentBatch> response) {

                if (response.isSuccessful()) {

                    ResponeStudentBatch bean = response.body();

                    //Toast.makeText(batch_Activity.this, "sucess", Toast.LENGTH_SHORT).show();
                    if (bean.getStatus()) {
                        errorLayout.setVisibility(View.GONE);
                        recyclerView_group_view.setVisibility(View.VISIBLE);
                        ResponeStudentBatch view = response.body();

                        Adapter = new Adapter_batch(batch_Activity.this, bean.getData());
                        recyclerView_group_view.setLayoutManager(new LinearLayoutManager(batch_Activity.this, LinearLayoutManager.VERTICAL, false));
                       // recyclerView_group_view.setLayoutManager(new GridLayoutManager(getBaseContext(), 2));
                        recyclerView_group_view.setItemAnimator(new DefaultItemAnimator());
                        recyclerView_group_view.setAdapter(Adapter);
                        Log.e("Group_response", view.toString());

                    } else {
                        errorLayout.setVisibility(View.VISIBLE);
                        recyclerView_group_view.setVisibility(View.GONE);
                        Toast.makeText(batch_Activity.this, bean.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    errorLayout.setVisibility(View.VISIBLE);
                    recyclerView_group_view.setVisibility(View.GONE);
                    Toast.makeText(batch_Activity.this, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponeStudentBatch> call, Throwable t) {
                errorLayout.setVisibility(View.VISIBLE);
                recyclerView_group_view.setVisibility(View.GONE);
                Log.e("error", t.getMessage());
                Toast.makeText(batch_Activity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                student_group();
                refresh_layout.setRefreshing(false);
            }
        },2000);

    }
}


