package com.nusys.studentedumax.Group;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.animation.Animator;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.nusys.studentedumax.R;
import com.nusys.studentedumax.commonModules.SharedPreference_main;

import de.hdodenhof.circleimageview.CircleImageView;

public class Create_Group extends AppCompatActivity {
    ConstraintLayout action_container;
    CardView bt_class, bt_courses, bt_batch;
    private String fileName;
    private CircleImageView userIMG;
    TextView tx_create;
    RadioButton radio_batch, radio_live_classes, radio_courses;
    private String userImageBase64;
    SharedPreference_main sharedPreference_main;
    LinearLayout linearLayout;
    TextView tv_toolbar_head1;
    LinearLayout ll_back_activity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create__group);

        tv_toolbar_head1 = findViewById(R.id.tv_toolbar_head);
        ll_back_activity = findViewById(R.id.ll_back_activity);
        bt_batch = findViewById(R.id.btn_batch);
        bt_class = findViewById(R.id.btn_live_class);
        bt_courses = findViewById(R.id.btn_courses);
        //tx_create = findViewById(R.id.txt_create);
        linearLayout = findViewById(R.id.linear);
        action_container = findViewById(R.id.actionContainer);
        sharedPreference_main = SharedPreference_main.getInstance(this);

   /*     tx_create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linearLayout.setVisibility(View.VISIBLE);
            }


        });*/
        tv_toolbar_head1.setText("Select Group Type");

        ll_back_activity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if (savedInstanceState == null) {
            action_container.setVisibility(View.INVISIBLE);
            ViewTreeObserver viewTreeObserver = action_container.getViewTreeObserver();
            if (viewTreeObserver.isAlive()) {
                viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        circularRevealTransition(); //

                        action_container.getViewTreeObserver().removeOnGlobalLayoutListener(this);

                    }
                });
            }
        }

        bt_courses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        bt_class.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Create_Group.this, Live_Class_Activity.class));
            }
        });
        bt_batch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Create_Group.this, batch_Activity.class));
            }
        });
    }

    private void circularRevealTransition() {
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Set the 'X' and 'Y' values for your requirement, Here it is set for the fab being as the source of the circle reveal //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        int X = 9 * action_container.getWidth() / 10;
        int Y = 9 * action_container.getHeight() / 10;
        int Duration = 800;
        float finalRadius = Math.max(action_container.getWidth(), action_container.getHeight()); //The final radius must be the end points of the current activity
        // create the animator for this view, with the start radius as zero
        Animator circularReveal = ViewAnimationUtils.createCircularReveal(action_container, X, Y, 0, finalRadius);
        circularReveal.setDuration(Duration);

        // set the view visible and start the animation
        action_container.setVisibility(View.VISIBLE);
        // start the animation
        circularReveal.start();
//        action_container.setBackgroundResource(android.R.color.white);
    }

}
