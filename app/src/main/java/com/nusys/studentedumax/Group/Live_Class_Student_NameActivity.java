package com.nusys.studentedumax.Group;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.nusys.studentedumax.R;
import com.nusys.studentedumax.adapter.Group_Adapter.Adpater_student_name;
import com.nusys.studentedumax.commonModules.SharedPreference_main;
import com.nusys.studentedumax.model.ResponeBatchStudent;
import com.nusys.studentedumax.retrofit.ApiClient;
import com.nusys.studentedumax.retrofit.ServiceInterface;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;

import static com.nusys.studentedumax.Constants.Content_Type;

public class Live_Class_Student_NameActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    SharedPreference_main sharedPreference_main;
    RecyclerView recycleStudents;
    Adpater_student_name Adapter;
    String id;
    TextView tv_create;

    SwipeRefreshLayout refresh_layout;

    ProgressBar main_progress;
    LinearLayout error_layout;
    TextView error_txt_cause;
    Button error_btn_retry;
    Context context;
    TextView tv_toolbar_head1;
    LinearLayout ll_back_activity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live__class__student__name);
        sharedPreference_main = SharedPreference_main.getInstance(this);
        refresh_layout = findViewById(R.id.refresh_layout_live_stu);
        main_progress = findViewById(R.id.main_progress_live_stu);
        error_layout = findViewById(R.id.error_layout_live_stu);
        error_txt_cause = findViewById(R.id.error_txt_cause);
        error_btn_retry = findViewById(R.id.error_btn_retry_live);
        refresh_layout.setOnRefreshListener(this);
        refresh_layout.setColorSchemeColors(Color.RED, Color.YELLOW, Color.BLUE);

        tv_toolbar_head1 = findViewById(R.id.tv_toolbar_head);
        ll_back_activity = findViewById(R.id.ll_back_activity);
        tv_toolbar_head1.setText("Create new group");

        ll_back_activity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        recycleStudents = findViewById(R.id.rvStudentsLive);
        id = getIntent().getStringExtra("Batch_id");
        tv_create = findViewById(R.id.tv_create_live);
        sharedPreference_main.seteducation(getIntent().getStringExtra("Batch_id"));
        tv_create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent intent = new Intent(context, Group_Create_SumitActivity.class);
               // intent.putExtra("Batchid", id);
                context.startActivity(intent);
*/

              /*  if (sharedPreference_main.getStudent_id() == "" ) {
                    Toast.makeText(context, "Please select teachers first to create group ", Toast.LENGTH_SHORT).show();


                } else {*/
                    startActivity(new Intent(Live_Class_Student_NameActivity.this, Group_Create_SumitActivity.class));

                    //  showtoast(context, "Please select teachers first to create group ");
                //}

            }
        });
        live_student_name();
    }

    private void live_student_name() {

        HashMap<String, String> map = new HashMap<>();
        map.put("class_id", getIntent().getStringExtra("Batch_id"));
//        Toast.makeText(context, "hd"+getIntent().getStringExtra("Batch_id"), Toast.LENGTH_SHORT).show();
        //   if (NetworkUtils.isConnected(getActivity())) {
        ServiceInterface serviceInterface = ApiClient.getClientTeacher().create(ServiceInterface.class);
        Call<ResponeBatchStudent> call = serviceInterface.live_student_name("Bearer " + sharedPreference_main.getToken(), Content_Type, map);
        call.enqueue(new Callback<ResponeBatchStudent>() {

            @Override
            public void onResponse(Call<ResponeBatchStudent> call, retrofit2.Response<ResponeBatchStudent> response) {

                if (response.isSuccessful()) {

                    ResponeBatchStudent bean = response.body();

                    // Toast.makeText(GroupActivity.this, "sucess", Toast.LENGTH_SHORT).show();
                    if (bean.getStatus()) {
                        ResponeBatchStudent view = response.body();

                        Adapter = new Adpater_student_name(Live_Class_Student_NameActivity.this, bean.getData());
                        //recyclerView_group_view.setLayoutManager(new GridLayoutManager(getBaseContext(), 2));
                        recycleStudents.setLayoutManager(new LinearLayoutManager(Live_Class_Student_NameActivity.this, LinearLayoutManager.VERTICAL, false));
                        recycleStudents.setItemAnimator(new DefaultItemAnimator());
                        recycleStudents.setAdapter(Adapter);
                        Log.e("Group_response", view.toString());

                    } else {
                        Toast.makeText(Live_Class_Student_NameActivity.this, bean.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                } else {

                    Toast.makeText(Live_Class_Student_NameActivity.this, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponeBatchStudent> call, Throwable t) {
                Log.e("error", t.getMessage());
                Toast.makeText(Live_Class_Student_NameActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                live_student_name();
                refresh_layout.setRefreshing(false);
            }
        }, 2000);

    }
}
