package com.nusys.studentedumax.Group;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.nusys.studentedumax.R;
import com.nusys.studentedumax.adapter.Group_Adapter.Adpater_live_classes;
import com.nusys.studentedumax.commonModules.SharedPreference_main;
import com.nusys.studentedumax.model.ResponeStudentLiveClasses;
import com.nusys.studentedumax.retrofit.ApiClient;
import com.nusys.studentedumax.retrofit.ServiceInterface;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;

import static com.nusys.studentedumax.Constants.Content_Type;

public class Live_Class_Activity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    SharedPreference_main sharedPreference_main;
    RecyclerView recycleLiveClasses;
    Adpater_live_classes Adapter;

    SwipeRefreshLayout refresh_layout;
    ProgressBar main_progress;
    LinearLayout errorLayout;
    TextView error_txt_cause;
    Button error_btn_retry;
    TextView tv_toolbar_head1;
    LinearLayout ll_back_activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live__class_);

        tv_toolbar_head1 = findViewById(R.id.tv_toolbar_head);
        ll_back_activity = findViewById(R.id.ll_back_activity);
        sharedPreference_main = SharedPreference_main.getInstance(this);
        refresh_layout = findViewById(R.id.refresh_layout_live);
        main_progress = findViewById(R.id.main_progress_live);
        errorLayout = findViewById(R.id.error_layout);
     /*   error_txt_cause = findViewById(R.id.error_txt_cause_live);
        error_btn_retry = findViewById(R.id.errorBtnRetryLive);*/
        refresh_layout.setOnRefreshListener(this);
        refresh_layout.setColorSchemeColors(Color.RED, Color.YELLOW, Color.BLUE);

        tv_toolbar_head1.setText("Live classes");

        ll_back_activity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        recycleLiveClasses = findViewById(R.id.rvLiveClasses);
        sharedPreference_main.setGroupTypeId("1");
        liveClass_group();
    }

    private void liveClass_group() {

        HashMap<String, String> map = new HashMap<>();

        map.put("student_id", sharedPreference_main.getUserId());

        //   if (NetworkUtils.isConnected(getActivity())) {
        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<ResponeStudentLiveClasses> call = serviceInterface.student_live("Bearer " + sharedPreference_main.getToken(), Content_Type, map);
        call.enqueue(new Callback<ResponeStudentLiveClasses>() {


            @Override
            public void onResponse(Call<ResponeStudentLiveClasses> call, retrofit2.Response<ResponeStudentLiveClasses> response) {

                if (response.isSuccessful()) {

                    ResponeStudentLiveClasses bean = response.body();

                    //Toast.makeText(Live_Class_Activity.this, "sucess", Toast.LENGTH_SHORT).show();
                    if (bean.getStatus()) {
                        ResponeStudentLiveClasses view = response.body();
                        errorLayout.setVisibility(View.GONE);
                        recycleLiveClasses.setVisibility(View.VISIBLE);
                        Adapter = new Adpater_live_classes(Live_Class_Activity.this, bean.getData());
                        //  recyclerView_group_view.setLayoutManager(new GridLayoutManager(getBaseContext(), 2));
                        recycleLiveClasses.setLayoutManager(new LinearLayoutManager(Live_Class_Activity.this, LinearLayoutManager.VERTICAL, false));
                        recycleLiveClasses.setItemAnimator(new DefaultItemAnimator());
                        recycleLiveClasses.setAdapter(Adapter);
                        Log.e("Group_response", view.toString());

                    } else {
                        errorLayout.setVisibility(View.VISIBLE);
                        recycleLiveClasses.setVisibility(View.GONE);
                        Toast.makeText(Live_Class_Activity.this, bean.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    errorLayout.setVisibility(View.VISIBLE);
                    recycleLiveClasses.setVisibility(View.GONE);
                    Toast.makeText(Live_Class_Activity.this, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponeStudentLiveClasses> call, Throwable t) {
                errorLayout.setVisibility(View.VISIBLE);
                recycleLiveClasses.setVisibility(View.GONE);
                Log.e("error", t.getMessage());
                Toast.makeText(Live_Class_Activity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                liveClass_group();
                refresh_layout.setRefreshing(false);
            }
        }, 2000);
    }
}


