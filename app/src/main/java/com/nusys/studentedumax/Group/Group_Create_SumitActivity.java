package com.nusys.studentedumax.Group;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.nusys.studentedumax.R;
import com.nusys.studentedumax.commonModules.SharedPreference_main;
import com.nusys.studentedumax.model.ResponeCreateGroup;
import com.nusys.studentedumax.retrofit.ApiClient;
import com.nusys.studentedumax.retrofit.ServiceInterface;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

import static com.nusys.studentedumax.Constants.CAMERA_REQUEST_CODE;
import static com.nusys.studentedumax.Constants.Content_Type;
import static com.nusys.studentedumax.Constants.GALLERY_REQUEST_CODE;

public class Group_Create_SumitActivity extends AppCompatActivity {
    ImageView imgPickFrom, group_image;
    private String userImageBase64;
    private String fileName;
    TextView tvCreateGroup;
    EditText etGroupName;
    //   private ImageView userIMG;
    SharedPreference_main sharedPreference_main;
    String str_name, str_batch, str_class;
    String type;
    String batch_id;
    String class_id, exam_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group__create__sumit);
        imgPickFrom = findViewById(R.id.img_pickFrom);
        // userIMG = findViewById(R.id.img_image);
        sharedPreference_main = SharedPreference_main.getInstance(this);
        group_image = findViewById(R.id.groupImageCreate);
        etGroupName = findViewById(R.id.et_groupName);
        tvCreateGroup = findViewById(R.id.tv_createGroup);
        str_batch = sharedPreference_main.getbatch();
        str_class = sharedPreference_main.geteducation();
        // Toast.makeText(this, ""+sharedPreference_main.getGroupTypeId(), Toast.LENGTH_SHORT).show();
        imgPickFrom.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                openChooser(CAMERA_REQUEST_CODE, GALLERY_REQUEST_CODE);

            }
        });


        tvCreateGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(Group_Create_SumitActivity.this, ""+sharedPreference_main.getGroupTypeId(), Toast.LENGTH_SHORT).show();
                switch (sharedPreference_main.getGroupTypeId()) {

                    case "0":
                        create_group("Batch", sharedPreference_main.getbatch(), "", "");

                        break;

                    case "1":
                        create_group("live_classes", "", sharedPreference_main.getbatch(), "");
                        break;
                    case "2":
                        batch_id = "";
                        batch_id = "";
                        exam_id = "";
                        break;
                    default:
                        break;

                }

            }
        });
    }


    private void chooseImageFromGallery(final int code) {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(pickPhoto, code);
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();

    }

    private void takeCameraImage(final int code) {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {

                            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(cameraIntent, code);

                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {

            case CAMERA_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    Bitmap photo = (Bitmap) data.getExtras().get("data");
                    group_image.setImageBitmap(photo);
                    getEncoded64ImageStringFromBitmap(photo);
                    userImageBase64 = getEncoded64ImageStringFromBitmap(photo);
                    Log.e("excam", userImageBase64);

                }
                break;
            case GALLERY_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    Uri imageUri = data.getData();
                    group_image.setImageURI(imageUri);
                    Bitmap bitmap;
                    try {
                        bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(imageUri));
                        Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, 500, 500, false);
                        userImageBase64 = getEncoded64ImageStringFromBitmap(resizedBitmap);
                        Log.e("exp", userImageBase64);


                    } catch (FileNotFoundException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }

                break;
        }
    }

    public String getEncoded64ImageStringFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteFormat = stream.toByteArray();
        String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);
        Log.e("", imgString);
        return imgString;
    }

    private void openChooser(final int code1, final int code2) {

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.chooser);
        TextView tv_camera = dialog.findViewById(R.id.camera);
        TextView tv_gallery = dialog.findViewById(R.id.gallery);
        dialog.show();
        tv_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takeCameraImage(code1);
                dialog.dismiss();
            }
        });
        tv_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseImageFromGallery(code2);
                dialog.dismiss();


            }
        });
    }


    private void create_group(String type, String batch_id, String class_id, String exam_id) {
        str_name = etGroupName.getText().toString().trim();
        HashMap<String, String> map = new HashMap<>();
        map.put("batch_id", batch_id);
        map.put("class_id", class_id);
        map.put("exam_id", exam_id);
        map.put("created_id", sharedPreference_main.getUserId());
        map.put("created_by", "Student");
        map.put("student_id", sharedPreference_main.getStudent_id());
        map.put("group_name", str_name);
        map.put("type", type);
        map.put("filename", userImageBase64);
        //   if (NetworkUtils.isConnected(getActivity())) {
        ServiceInterface serviceInterface = ApiClient.getClientTeacher().create(ServiceInterface.class);
        Call<ResponeCreateGroup> call = serviceInterface.create_group("Bearer " + sharedPreference_main.getToken(), Content_Type, map);
        call.enqueue(new Callback<ResponeCreateGroup>() {

            @Override
            public void onResponse(Call<ResponeCreateGroup> call, retrofit2.Response<ResponeCreateGroup> response) {
                if (response.isSuccessful()) {
                    ResponeCreateGroup bean = response.body();
                    // Toast.makeText(GroupActivity.this, "sucess", Toast.LENGTH_SHORT).show();
                    if (bean.getStatus()) {
                        ResponeCreateGroup view = response.body();
                        Toast.makeText(Group_Create_SumitActivity.this, bean.getMessage(), Toast.LENGTH_SHORT).show();

                        Intent intent = new Intent(Group_Create_SumitActivity.this, GroupActivity.class);
                        startActivity(intent);
                        sharedPreference_main.seteducation(" ");
                        sharedPreference_main.setbatch("");
                        sharedPreference_main.setStudent_id("");
                        Log.e("Group_response", view.toString());

                    } else {
                        Toast.makeText(Group_Create_SumitActivity.this, bean.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                } else {

                    Toast.makeText(Group_Create_SumitActivity.this, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponeCreateGroup> call, Throwable t) {
                Log.e("error", t.getMessage());
                Toast.makeText(Group_Create_SumitActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

}
