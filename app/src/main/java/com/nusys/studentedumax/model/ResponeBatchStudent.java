package com.nusys.studentedumax.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponeBatchStudent {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private List<Databs> data = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<Databs> getData() {
        return data;
    }

    public void setData(List<Databs> data) {
        this.data = data;
    }

    public class Databs {
        @SerializedName("selected")
        @Expose
        private Boolean selected;
        @SerializedName("student_id")
        @Expose
        private String studentId;
        @SerializedName("student_name")
        @Expose
        private String studentName;
        @SerializedName("student_image")
        @Expose
        private String studentImage;

        public String getStudentId() {
            return studentId;
        }

        public void setStudentId(String studentId) {
            this.studentId = studentId;
        }

        public String getStudentName() {
            return studentName;
        }

        public void setStudentName(String studentName) {
            this.studentName = studentName;
        }

        public String getStudentImage() {
            return studentImage;
        }

        public void setStudentImage(String studentImage) {
            this.studentImage = studentImage;
        }
        public boolean getSelected() {
            return selected;
        }

        public void setSelected(boolean selected) {
            selected = selected;
        }
    }

}
