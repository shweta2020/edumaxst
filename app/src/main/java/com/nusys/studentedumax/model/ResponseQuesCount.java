package com.nusys.studentedumax.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseQuesCount {
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }
    public class Datum {

        @SerializedName("all_question")
        @Expose
        private Integer allQuestion;
        @SerializedName("answered_question")
        @Expose
        private Integer answeredQuestion;
        @SerializedName("unanswered_question")
        @Expose
        private Integer unansweredQuestion;

        public Integer getAllQuestion() {
            return allQuestion;
        }

        public void setAllQuestion(Integer allQuestion) {
            this.allQuestion = allQuestion;
        }

        public Integer getAnsweredQuestion() {
            return answeredQuestion;
        }

        public void setAnsweredQuestion(Integer answeredQuestion) {
            this.answeredQuestion = answeredQuestion;
        }

        public Integer getUnansweredQuestion() {
            return unansweredQuestion;
        }

        public void setUnansweredQuestion(Integer unansweredQuestion) {
            this.unansweredQuestion = unansweredQuestion;
        }

    }
}
