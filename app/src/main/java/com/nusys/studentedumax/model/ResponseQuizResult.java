package com.nusys.studentedumax.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseQuizResult {
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }
    public class Datum {

        @SerializedName("exam_name")
        @Expose
        private String examName;
        @SerializedName("session_time")
        @Expose
        private String sessionTime;
        @SerializedName("start_time")
        @Expose
        private String startTime;
        @SerializedName("submit_time")
        @Expose
        private String submitTime;
        @SerializedName("total_question")
        @Expose
        private Integer totalQuestion;
        @SerializedName("attempt_question")
        @Expose
        private Integer attemptQuestion;
        @SerializedName("total_exam_mark")
        @Expose
        private String totalExamMark;
        @SerializedName("your_mark")
        @Expose
        private String yourMark;
        @SerializedName("questions")
        @Expose
        private List<Question> questions = null;

        public String getExamName() {
            return examName;
        }

        public void setExamName(String examName) {
            this.examName = examName;
        }

        public String getSessionTime() {
            return sessionTime;
        }

        public void setSessionTime(String sessionTime) {
            this.sessionTime = sessionTime;
        }

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime;
        }

        public String getSubmitTime() {
            return submitTime;
        }

        public void setSubmitTime(String submitTime) {
            this.submitTime = submitTime;
        }

        public Integer getTotalQuestion() {
            return totalQuestion;
        }

        public void setTotalQuestion(Integer totalQuestion) {
            this.totalQuestion = totalQuestion;
        }

        public Integer getAttemptQuestion() {
            return attemptQuestion;
        }

        public void setAttemptQuestion(Integer attemptQuestion) {
            this.attemptQuestion = attemptQuestion;
        }

        public String getTotalExamMark() {
            return totalExamMark;
        }

        public void setTotalExamMark(String totalExamMark) {
            this.totalExamMark = totalExamMark;
        }

        public String getYourMark() {
            return yourMark;
        }

        public void setYourMark(String yourMark) {
            this.yourMark = yourMark;
        }

        public List<Question> getQuestions() {
            return questions;
        }

        public void setQuestions(List<Question> questions) {
            this.questions = questions;
        }
        public class Question {

            @SerializedName("group_type")
            @Expose
            private String groupType;
            @SerializedName("question_view")
            @Expose
            private String questionView;
            @SerializedName("answer_status")
            @Expose
            private String answerStatus;

            public String getGroupType() {
                return groupType;
            }

            public void setGroupType(String groupType) {
                this.groupType = groupType;
            }

            public String getQuestionView() {
                return questionView;
            }

            public void setQuestionView(String questionView) {
                this.questionView = questionView;
            }

            public String getAnswerStatus() {
                return answerStatus;
            }

            public void setAnswerStatus(String answerStatus) {
                this.answerStatus = answerStatus;
            }

        }
    }
}
