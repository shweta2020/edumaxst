package com.nusys.studentedumax.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseQuesSubmit {
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private Data data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("student_id")
        @Expose
        private String studentId;
        @SerializedName("exam_id")
        @Expose
        private String examId;
        @SerializedName("q_id")
        @Expose
        private String qId;
        @SerializedName("option_id")
        @Expose
        private String optionId;
        @SerializedName("quiz_result_date")
        @Expose
        private String quizResultDate;

        public String getStudentId() {
            return studentId;
        }

        public void setStudentId(String studentId) {
            this.studentId = studentId;
        }

        public String getExamId() {
            return examId;
        }

        public void setExamId(String examId) {
            this.examId = examId;
        }

        public String getQId() {
            return qId;
        }

        public void setQId(String qId) {
            this.qId = qId;
        }

        public String getOptionId() {
            return optionId;
        }

        public void setOptionId(String optionId) {
            this.optionId = optionId;
        }

        public String getQuizResultDate() {
            return quizResultDate;
        }

        public void setQuizResultDate(String quizResultDate) {
            this.quizResultDate = quizResultDate;
        }

    }
}
