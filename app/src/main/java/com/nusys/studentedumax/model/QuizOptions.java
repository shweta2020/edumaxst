package com.nusys.studentedumax.model;

public class QuizOptions {

    private String question_id;
    private String option_id;
    private String option;
    private Integer your_answer;

    public String getQuestion_id() {
        return question_id;
    }

    public void setOption_id(String option_id) {
        this.option_id = option_id;
    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }

    public String getOption_id() {
        return option_id;
    }

    public void setQuestion_id(String question_id) {
        this.question_id = question_id;
    }
    public Integer getYour_answer() {
        return your_answer;
    }

    public void setYour_answer(Integer your_answer) {
        this.your_answer = your_answer;
    }
}
