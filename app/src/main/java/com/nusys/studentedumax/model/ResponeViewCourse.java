
package com.nusys.studentedumax.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponeViewCourse {
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("topic_name")
        @Expose
        private String topicName;
        @SerializedName("topic_desc")
        @Expose
        private String topicDesc;
        @SerializedName("topic_image")
        @Expose
        private List<TopicImage> topicImage = null;

        public String getTopicName() {
            return topicName;
        }

        public void setTopicName(String topicName) {
            this.topicName = topicName;
        }

        public String getTopicDesc() {
            return topicDesc;
        }

        public void setTopicDesc(String topicDesc) {
            this.topicDesc = topicDesc;
        }

        public List<TopicImage> getTopicImage() {
            return topicImage;
        }

        public void setTopicImage(List<TopicImage> topicImage) {
            this.topicImage = topicImage;
        }

        public class TopicImage {

            @SerializedName("image")
            @Expose
            private String image;

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

        }
    }


}
