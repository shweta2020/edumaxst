package com.nusys.studentedumax.model;

public class DoubtsModel {


    Boolean status;
    String message;
    String user_name;
    public String user_profile;
    String subject_id;
    String subject_name;
    String unit_id;
    String unit_name;
    String uploader_student_id;
    String description;
    String priority;
    String remark;
    String image;
    String accept_status;
    String doubt_id;
    int progress_status;
    String create_at;

    public String getDoubt_id() {
        return doubt_id;
    }

    public void setDoubt_id(String doubt_id) {
        this.doubt_id = doubt_id;
    }

    public String getUploader_student_id() {
        return uploader_student_id;
    }

    public void setUploader_student_id(String uploader_student_id) {
        this.uploader_student_id = uploader_student_id;
    }

    public int getProgress_status() {
        return progress_status;
    }

    public void setProgress_status(int progress_status) {
        this.progress_status = progress_status;
    }


    public String getSubject_name() {
        return subject_name;
    }

    public void setSubject_name(String subject_name) {
        this.subject_name = subject_name;
    }

    public String getUnit_name() {
        return unit_name;
    }

    public void setUnit_name(String unit_name) {
        this.unit_name = unit_name;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_profile() {
        return user_profile;
    }

    public void setUser_profile(String user_profile) {
        this.user_profile = user_profile;
    }

    public String getSubject_id() {
        return subject_id;
    }

    public void setSubject_id(String subject_id) {
        this.subject_id = subject_id;
    }

    public String getUnit_id() {
        return unit_id;
    }

    public void setUnit_id(String unit_id) {
        this.unit_id = unit_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getAccept_status() {
        return accept_status;
    }

    public void setAccept_status(String accept_status) {
        this.accept_status = accept_status;
    }


    public String getCreate_at() {
        return create_at;
    }

    public void setCreate_at(String create_at) {
        this.create_at = create_at;
    }


}
