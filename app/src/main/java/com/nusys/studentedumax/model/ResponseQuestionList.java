package com.nusys.studentedumax.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseQuestionList {
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("test_name")
    @Expose
    private String testName;
    @SerializedName("session_time")
    @Expose
    private String sessionTime;
    @SerializedName("start_time")
    @Expose
    private String startTime;
    @SerializedName("submit_time")
    @Expose
    private String submitTime;
    @SerializedName("total_question")
    @Expose
    private Integer totalQuestion;
    @SerializedName("attempt_question")
    @Expose
    private Integer attemptQuestion;
    @SerializedName("total_exam_mark")
    @Expose
    private String totalExamMark;
    @SerializedName("your_mark")
    @Expose
    private String yourMark;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

    public String getSessionTime() {
        return sessionTime;
    }

    public void setSessionTime(String sessionTime) {
        this.sessionTime = sessionTime;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getSubmitTime() {
        return submitTime;
    }

    public void setSubmitTime(String submitTime) {
        this.submitTime = submitTime;
    }

    public Integer getTotalQuestion() {
        return totalQuestion;
    }

    public void setTotalQuestion(Integer totalQuestion) {
        this.totalQuestion = totalQuestion;
    }

    public Integer getAttemptQuestion() {
        return attemptQuestion;
    }

    public void setAttemptQuestion(Integer attemptQuestion) {
        this.attemptQuestion = attemptQuestion;
    }

    public String getTotalExamMark() {
        return totalExamMark;
    }

    public void setTotalExamMark(String totalExamMark) {
        this.totalExamMark = totalExamMark;
    }

    public String getYourMark() {
        return yourMark;
    }

    public void setYourMark(String yourMark) {
        this.yourMark = yourMark;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }
    public class Datum {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("question_id")
        @Expose
        private String questionId;
        @SerializedName("your_mark")
        @Expose
        private String yourMark;
        @SerializedName("time")
        @Expose
        private Object time;
        @SerializedName("question")
        @Expose
        private String question;
        @SerializedName("solution")
        @Expose
        private String solution;
        @SerializedName("options")
        @Expose
        private List<Option> options = null;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getQuestionId() {
            return questionId;
        }

        public void setQuestionId(String questionId) {
            this.questionId = questionId;
        }

        public String getYourMark() {
            return yourMark;
        }

        public void setYourMark(String yourMark) {
            this.yourMark = yourMark;
        }

        public Object getTime() {
            return time;
        }

        public void setTime(Object time) {
            this.time = time;
        }

        public String getQuestion() {
            return question;
        }

        public void setQuestion(String question) {
            this.question = question;
        }

        public String getSolution() {
            return solution;
        }

        public void setSolution(String solution) {
            this.solution = solution;
        }

        public List<Option> getOptions() {
            return options;
        }

        public void setOptions(List<Option> options) {
            this.options = options;
        }
        public class Option {

            @SerializedName("question_id")
            @Expose
            private String questionId;
            @SerializedName("option_id")
            @Expose
            private String optionId;
            @SerializedName("correct")
            @Expose
            private String correct;
            @SerializedName("your_answer")
            @Expose
            private Integer yourAnswer;
            @SerializedName("option")
            @Expose
            private String option;
            private boolean selected;

            public boolean isSelected() {
                return selected;
            }

            public void setSelected(boolean selected) {
                this.selected = selected;
            }
            public String getQuestionId() {
                return questionId;
            }

            public void setQuestionId(String questionId) {
                this.questionId = questionId;
            }

            public String getOptionId() {
                return optionId;
            }

            public void setOptionId(String optionId) {
                this.optionId = optionId;
            }

            public String getCorrect() {
                return correct;
            }

            public void setCorrect(String correct) {
                this.correct = correct;
            }

            public Integer getYourAnswer() {
                return yourAnswer;
            }

            public void setYourAnswer(Integer yourAnswer) {
                this.yourAnswer = yourAnswer;
            }

            public String getOption() {
                return option;
            }

            public void setOption(String option) {
                this.option = option;
            }

        }
    }
}
