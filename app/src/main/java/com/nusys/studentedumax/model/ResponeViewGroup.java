
package com.nusys.studentedumax.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponeViewGroup {
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;
    @SerializedName("group_strength")
    @Expose
    private Integer groupStrength;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public Integer getGroupStrength() {
        return groupStrength;
    }

    public void setGroupStrength(Integer groupStrength) {
        this.groupStrength = groupStrength;
    }
    public class Datum {

        @SerializedName("group_id")
        @Expose
        private String groupId;
        @SerializedName("group_name")
        @Expose
        private String groupName;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("group_by")
        @Expose
        private Integer groupBy;
        @SerializedName("group_type")
        @Expose
        private String groupType;
        @SerializedName("class_id")
        @Expose
        private String classId;
        @SerializedName("batch_id")
        @Expose
        private String batchId;
        @SerializedName("strength")
        @Expose
        private Integer strength;
        @SerializedName("created_id")
        @Expose
        private String createdId;

        public String getGroupId() {
            return groupId;
        }

        public void setGroupId(String groupId) {
            this.groupId = groupId;
        }

        public String getGroupName() {
            return groupName;
        }

        public void setGroupName(String groupName) {
            this.groupName = groupName;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public Integer getGroupBy() {
            return groupBy;
        }

        public void setGroupBy(Integer groupBy) {
            this.groupBy = groupBy;
        }

        public String getGroupType() {
            return groupType;
        }

        public void setGroupType(String groupType) {
            this.groupType = groupType;
        }

        public String getClassId() {
            return classId;
        }

        public void setClassId(String classId) {
            this.classId = classId;
        }

        public String getBatchId() {
            return batchId;
        }

        public void setBatchId(String batchId) {
            this.batchId = batchId;
        }

        public Integer getStrength() {
            return strength;
        }

        public void setStrength(Integer strength) {
            this.strength = strength;
        }

        public String getCreatedId() {
            return createdId;
        }

        public void setCreatedId(String createdId) {
            this.createdId = createdId;
        }

    }
}
