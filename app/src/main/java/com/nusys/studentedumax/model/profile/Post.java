
package com.nusys.studentedumax.model.profile;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Post {

    @SerializedName("post_id")
    @Expose
    private String postId;
    @SerializedName("uploader_id")
    @Expose
    private String uploaderId;
    @SerializedName("uploader_name")
    @Expose
    private String uploaderName;
    @SerializedName("uploader_image")
    @Expose
    private String uploaderImage;
    @SerializedName("heading")
    @Expose
    private String heading;
    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("exam_id")
    @Expose
    private String examId;
    @SerializedName("post_type")
    @Expose
    private String postType;
    @SerializedName("file_type")
    @Expose
    private String fileType;
    @SerializedName("user_type")
    @Expose
    private String userType;
    @SerializedName("filename")
    @Expose
    private String filename;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("likes")
    @Expose
    private Integer likes;
    @SerializedName("like_status")
    @Expose
    private String likeStatus;
    @SerializedName("total_comments")
    @Expose
    private Integer totalComments;
    @SerializedName("comment")
    @Expose
    private List<Comment> comment = null;

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getUploaderId() {
        return uploaderId;
    }

    public void setUploaderId(String uploaderId) {
        this.uploaderId = uploaderId;
    }

    public String getUploaderName() {
        return uploaderName;
    }

    public void setUploaderName(String uploaderName) {
        this.uploaderName = uploaderName;
    }

    public String getUploaderImage() {
        return uploaderImage;
    }

    public void setUploaderImage(String uploaderImage) {
        this.uploaderImage = uploaderImage;
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getExamId() {
        return examId;
    }

    public void setExamId(String examId) {
        this.examId = examId;
    }

    public String getPostType() {
        return postType;
    }

    public void setPostType(String postType) {
        this.postType = postType;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Integer getLikes() {
        return likes;
    }

    public void setLikes(Integer likes) {
        this.likes = likes;
    }

    public String getLikeStatus() {
        return likeStatus;
    }

    public void setLikeStatus(String likeStatus) {
        this.likeStatus = likeStatus;
    }

    public Integer getTotalComments() {
        return totalComments;
    }

    public void setTotalComments(Integer totalComments) {
        this.totalComments = totalComments;
    }

    public List<Comment> getComment() {
        return comment;
    }

    public void setComment(List<Comment> comment) {
        this.comment = comment;
    }

}
