package com.nusys.studentedumax.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ResponseQuestionList2Method {

    String question;
    Integer attempt_status;

    public Integer getReview_status() {
        return review_status;
    }

    public void setReview_status(Integer review_status) {
        this.review_status = review_status;
    }

    Integer review_status;
    String test_name;
    Integer total_questions;
    String negative_mark;
    String question_id;
    String marks;
    String partial_marking;
    private ArrayList<QuizOptions> quizOptions;


    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getTest_name() {
        return test_name;
    }

    public void setTest_name(String test_name) {
        this.test_name = test_name;
    }


    public Integer getTotal_questions() {
        return total_questions;
    }

    public void setTotal_questions(Integer total_questions) {
        this.total_questions = total_questions;
    }

    public String getNegative_mark() {
        return negative_mark;
    }

    public void setNegative_mark(String negative_mark) {
        this.negative_mark = negative_mark;
    }

    public String getQuestion_id() {
        return question_id;
    }

    public void setQuestion_id(String question_id) {
        this.question_id = question_id;
    }

    public String getMarks() {
        return marks;
    }

    public void setMarks(String marks) {
        this.marks = marks;
    }

    public String getPartial_marking() {
        return partial_marking;
    }

    public void setPartial_marking(String partial_marking) {
        this.partial_marking = partial_marking;
    }

    public ArrayList<QuizOptions> getQuizOptions() {
        return quizOptions;
    }

    public void setQuizOptions(List<QuizOptions> quizOptions) {
        this.quizOptions = (ArrayList<QuizOptions>) quizOptions;
    }

    public Integer getAttempt_status() {
        return attempt_status;
    }

    public void setAttempt_status(Integer attempt_status) {
        this.attempt_status = attempt_status;
    }

}

