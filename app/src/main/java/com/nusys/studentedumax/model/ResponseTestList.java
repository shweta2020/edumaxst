package com.nusys.studentedumax.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseTestList {


    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }
    public class Datum {

        @SerializedName("test_id")
        @Expose
        private String testId;
        @SerializedName("test_name")
        @Expose
        private String testName;
        @SerializedName("test_type")
        @Expose
        private String testType;
        @SerializedName("test_description")
        @Expose
        private String testDescription;
        @SerializedName("test_image")
        @Expose
        private String testImage;
        @SerializedName("start_date")
        @Expose
        private String startDate;
        @SerializedName("end_date")
        @Expose
        private String endDate;

        @SerializedName("exam_id")
        @Expose
        private String examId;

        public String getStartStatus() {
            return startStatus;
        }

        public void setStartStatus(String startStatus) {
            this.startStatus = startStatus;
        }

        @SerializedName("start_status")
        @Expose
        private String startStatus;


        @SerializedName("exam_name")
        @Expose
        private String examName;
        @SerializedName("created_id")
        @Expose
        private String createdId;
        @SerializedName("created_by")
        @Expose
        private String createdBy;

        public String getTestId() {
            return testId;
        }

        public void setTestId(String testId) {
            this.testId = testId;
        }

        public String getTestName() {
            return testName;
        }

        public void setTestName(String testName) {
            this.testName = testName;
        }

        public String getTestType() {
            return testType;
        }

        public void setTestType(String testType) {
            this.testType = testType;
        }

        public String getTestDescription() {
            return testDescription;
        }

        public void setTestDescription(String testDescription) {
            this.testDescription = testDescription;
        }

        public String getTestImage() {
            return testImage;
        }

        public void setTestImage(String testImage) {
            this.testImage = testImage;
        }

        public String getStartDate() {
            return startDate;
        }

        public void setStartDate(String startDate) {
            this.startDate = startDate;
        }

        public String getEndDate() {
            return endDate;
        }

        public void setEndDate(String endDate) {
            this.endDate = endDate;
        }

        public String getExamId() {
            return examId;
        }

        public void setExamId(String examId) {
            this.examId = examId;
        }

        public String getExamName() {
            return examName;
        }

        public void setExamName(String examName) {
            this.examName = examName;
        }

        public String getCreatedId() {
            return createdId;
        }

        public void setCreatedId(String createdId) {
            this.createdId = createdId;
        }

        public String getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(String createdBy) {
            this.createdBy = createdBy;
        }

    }
}
