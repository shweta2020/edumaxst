
package com.nusys.studentedumax.model.profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Comment {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("parent_comment_id")
    @Expose
    private String parentCommentId;
    @SerializedName("post_id")
    @Expose
    private String postId;
    @SerializedName("commenter_id")
    @Expose
    private String commenterId;
    @SerializedName("commenter_name")
    @Expose
    private String commenterName;
    @SerializedName("commenter_photo")
    @Expose
    private String commenterPhoto;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("commenter_type")
    @Expose
    private String commenterType;
    @SerializedName("time")
    @Expose
    private String time;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getParentCommentId() {
        return parentCommentId;
    }

    public void setParentCommentId(String parentCommentId) {
        this.parentCommentId = parentCommentId;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getCommenterId() {
        return commenterId;
    }

    public void setCommenterId(String commenterId) {
        this.commenterId = commenterId;
    }

    public String getCommenterName() {
        return commenterName;
    }

    public void setCommenterName(String commenterName) {
        this.commenterName = commenterName;
    }

    public String getCommenterPhoto() {
        return commenterPhoto;
    }

    public void setCommenterPhoto(String commenterPhoto) {
        this.commenterPhoto = commenterPhoto;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCommenterType() {
        return commenterType;
    }

    public void setCommenterType(String commenterType) {
        this.commenterType = commenterType;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

}
