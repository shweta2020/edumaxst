package com.nusys.studentedumax.model;

public class TeacherModel {
    String name;
    Boolean status;
    String message;
    String expr_year;
    String profile_image;
    String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }


    public String getExpr_year() {
        return expr_year;
    }

    public void setExpr_year(String expr_year) {
        this.expr_year = expr_year;
    }




    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
