
package com.nusys.studentedumax.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class ResponeViewGroupList {
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }


    public class Datum {

        @SerializedName("group_id")
        @Expose
        private String groupId;
        @SerializedName("group_name")
        @Expose
        private String groupName;
        @SerializedName("creater_id")
        @Expose
        private String createrId;
        @SerializedName("created_by")
        @Expose
        private Object createdBy;
        @SerializedName("creater_type")
        @Expose
        private Object createrType;
        @SerializedName("creater_image")
        @Expose
        private Object createrImage;
        @SerializedName("students")
        @Expose
        private List<Student> students = null;

        public String getGroupId() {
            return groupId;
        }

        public void setGroupId(String groupId) {
            this.groupId = groupId;
        }

        public String getGroupName() {
            return groupName;
        }

        public void setGroupName(String groupName) {
            this.groupName = groupName;
        }

        public String getCreaterId() {
            return createrId;
        }

        public void setCreaterId(String createrId) {
            this.createrId = createrId;
        }

        public Object getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(Object createdBy) {
            this.createdBy = createdBy;
        }

        public Object getCreaterType() {
            return createrType;
        }

        public void setCreaterType(Object createrType) {
            this.createrType = createrType;
        }

        public Object getCreaterImage() {
            return createrImage;
        }

        public void setCreaterImage(Object createrImage) {
            this.createrImage = createrImage;
        }

        public List<Student> getStudents() {
            return students;
        }

        public void setStudents(List<Student> students) {
            this.students = students;
        }


        public class Student {

            @SerializedName("student_id")
            @Expose
            private String studentId;
            @SerializedName("student_name")
            @Expose
            private String studentName;
            @SerializedName("student_image")
            @Expose
            private String studentImage;

            public String getStudentId() {
                return studentId;
            }

            public void setStudentId(String studentId) {
                this.studentId = studentId;
            }

            public String getStudentName() {
                return studentName;
            }

            public void setStudentName(String studentName) {
                this.studentName = studentName;
            }

            public String getStudentImage() {
                return studentImage;
            }

            public void setStudentImage(String studentImage) {
                this.studentImage = studentImage;
            }

        }
    }
}



//
//    @SerializedName("message")
//    @Expose
//    private String message;
//    @SerializedName("status")
//    @Expose
//    private Boolean status;
//    @SerializedName("data")
//    @Expose
//    private List<Datum> data = null;
//    @SerializedName("group_strength")
//    @Expose
//    private Integer groupStrength;
//
//    public String getMessage() {
//        return message;
//    }
//
//    public void setMessage(String message) {
//        this.message = message;
//    }
//
//    public Boolean getStatus() {
//        return status;
//    }
//
//    public void setStatus(Boolean status) {
//        this.status = status;
//    }
//
//    public List<Datum> getData() {
//        return data;
//    }
//
//    public void setData(List<Datum> data) {
//        this.data = data;
//    }
//
//    public Integer getGroupStrength() {
//        return groupStrength;
//    }
//
//    public void setGroupStrength(Integer groupStrength) {
//        this.groupStrength = groupStrength;
//    }
//
//    public class Datum {
//
//        @SerializedName("group_id")
//        @Expose
//        private String groupId;
//        @SerializedName("group_name")
//        @Expose
//        private String groupName;
//        @SerializedName("image")
//        @Expose
//        private String image;
//        @SerializedName("group_by")
//        @Expose
//        private Integer groupBy;
//        @SerializedName("strength")
//        @Expose
//        private Integer strength;
//
//        public String getGroupId() {
//            return groupId;
//        }
//
//        public void setGroupId(String groupId) {
//            this.groupId = groupId;
//        }
//
//        public String getGroupName() {
//            return groupName;
//        }
//
//        public void setGroupName(String groupName) {
//            this.groupName = groupName;
//        }
//
//        public String getImage() {
//            return image;
//        }
//
//        public void setImage(String image) {
//            this.image = image;
//        }
//
//        public Integer getGroupBy() {
//            return groupBy;
//        }
//
//        public void setGroupBy(Integer groupBy) {
//            this.groupBy = groupBy;
//        }
//
//        public Integer getStrength() {
//            return strength;
//        }
//
//        public void setStrength(Integer strength) {
//            this.strength = strength;
//        }
//
//    }
//}
