package com.nusys.studentedumax;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nusys.studentedumax.adapter.AdapterCourseExam;
import com.nusys.studentedumax.adapter.AdapterQuizResult;
import com.nusys.studentedumax.commonModules.SharedPreference_main;
import com.nusys.studentedumax.model.ResponseQuesCount;
import com.nusys.studentedumax.model.ResponseQuizResult;
import com.nusys.studentedumax.retrofit.ApiClient;
import com.nusys.studentedumax.retrofit.ServiceInterface;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;

import static com.nusys.studentedumax.Constants.Content_Type;

public class QuizResult extends AppCompatActivity {
    Context context;
    SharedPreference_main sharedPreference_main;
    TextView tvReviewQues;
    TextView tvToolbarHead;
    ImageView backChangeNameImage;
    AdapterQuizResult Adapter;
    RecyclerView recycle_questions;

    TextView tvScore,tvOutOfScore,tvQues,tvOutOfQues,tvStartDate,tvSubmitDate,tvExamName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_result);
        initialization();
        action();
    }

    public void initialization() {
        context = QuizResult.this;
        sharedPreference_main = SharedPreference_main.getInstance(context);
        tvToolbarHead = findViewById(R.id.tv_toolbar_head);
        backChangeNameImage = findViewById(R.id.back);

        tvScore = findViewById(R.id.tv_score);
        tvOutOfScore = findViewById(R.id.tv_out_of_score);
        tvQues = findViewById(R.id.tv_ques);
        tvOutOfQues = findViewById(R.id.tv_out_of_ques);
        tvStartDate = findViewById(R.id.tv_start_date);
        tvSubmitDate = findViewById(R.id.tv_submit_date);
        tvExamName = findViewById(R.id.tv_exam_name);
        recycle_questions = findViewById(R.id.recycleQuestions);

        tvReviewQues = findViewById(R.id.tv_review_ques);
    }

    public void action() {
        /* set text for toolbar header
         * Created Date: 16-03-2020
         */
        tvToolbarHead.setText("Result");
        /* back intent for toolbar header
         * Created Date: 16-03-2020
         */
        backChangeNameImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        /* for fetching quiz result
         * Created date: 18-06-2020
         */

        quiz_result();
        /* for hit change update basic info api on onclick button
         * Created date: 16-03-2020
         */

        tvReviewQues.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(context, QuizReviewQues.class);
                i.putExtra("testId",getIntent().getStringExtra("test_id"));
                startActivity(i);
            }
        });


    }

    /* for fetching quiz result
     * Created Date: 18-06-2020
     * Updated Date:19-06-2020(add language parameter in query and change response model)
     */
    private void quiz_result() {

        HashMap<String, String> map = new HashMap<>();

        map.put("student_id", sharedPreference_main.getUserId());
        map.put("exam_id", getIntent().getStringExtra("test_id"));
        map.put("language","2");



        // Toast.makeText(context, "" + getIntent().getStringExtra("testId"), Toast.LENGTH_SHORT).show();
        //   if (NetworkUtils.isConnected(getActivity())) {
        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<ResponseQuizResult> call = serviceInterface.quiz_result("Bearer " + sharedPreference_main.getToken(), Content_Type, map);
        call.enqueue(new Callback<ResponseQuizResult>() {
            @Override
            public void onResponse(Call<ResponseQuizResult> call, retrofit2.Response<ResponseQuizResult> response) {

                if (response.isSuccessful()) {

                    ResponseQuizResult bean = response.body();

                    // Toast.makeText(GroupActivity.this, "sucess", Toast.LENGTH_SHORT).show();
                    if (bean.getStatus()) {

                        tvExamName.setText(bean.getData().get(0).getExamName());
                        tvSubmitDate.setText(bean.getData().get(0).getSubmitTime());
                        tvStartDate.setText(bean.getData().get(0).getStartTime());
                        tvScore.setText(bean.getData().get(0).getYourMark());
                        tvQues.setText(bean.getData().get(0).getAttemptQuestion().toString());
                        tvOutOfScore.setText("Out of "+bean.getData().get(0).getTotalExamMark());
                        tvOutOfQues.setText("Out of "+bean.getData().get(0).getTotalQuestion().toString());

                        Adapter = new AdapterQuizResult(context, bean.getData().get(0).getQuestions());
                        // recyclerView_course_view.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
                        recycle_questions.setLayoutManager(new GridLayoutManager(context, 5));
                        recycle_questions.setItemAnimator(new DefaultItemAnimator());
                        recycle_questions.setAdapter(Adapter);

                    } else {
                        Toast.makeText(context, bean.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                } else {

                    Toast.makeText(context, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseQuizResult> call, Throwable t) {
                Log.e("error", t.getMessage());
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
