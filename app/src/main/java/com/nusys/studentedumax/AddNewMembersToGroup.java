package com.nusys.studentedumax;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.nusys.studentedumax.Group.Batch_student_name_Activity;
import com.nusys.studentedumax.Group.Group_Create_SumitActivity;
import com.nusys.studentedumax.adapter.AdpaterAddNewMemberGroupList;
import com.nusys.studentedumax.adapter.Group_Adapter.Adpater_student_name;
import com.nusys.studentedumax.commonModules.SharedPreference_main;
import com.nusys.studentedumax.model.ResponeAddStudentInGroup;
import com.nusys.studentedumax.model.ResponeBatchStudent;
import com.nusys.studentedumax.model.ResponeDeleteStudentFromGroup;
import com.nusys.studentedumax.model.ResponeViewGroupMemberList;
import com.nusys.studentedumax.retrofit.ApiClient;
import com.nusys.studentedumax.retrofit.ServiceInterface;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.nusys.studentedumax.Constants.Content_Type;

public class AddNewMembersToGroup extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    SharedPreference_main sharedPreference_main;
    RecyclerView recycleStudents;
    AdpaterAddNewMemberGroupList Adapter;
    String id;
    TextView tv_create;

    SwipeRefreshLayout refreshLayout;

    ProgressBar mainProgress;
    LinearLayout errorLayout;
    TextView errorTxtCause;
    Button errorBtnRetry;
    Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_members_to_group);

        sharedPreference_main = SharedPreference_main.getInstance(this);
        refreshLayout = findViewById(R.id.refresh_layout);
        mainProgress = findViewById(R.id.main_progress);
        errorLayout = findViewById(R.id.error_layout);
        errorTxtCause = findViewById(R.id.error_txt_cause);
        errorBtnRetry = findViewById(R.id.error_btn_retry);
        refreshLayout.setOnRefreshListener(this);
        refreshLayout.setColorSchemeColors(Color.RED, Color.YELLOW, Color.BLUE);

        context = AddNewMembersToGroup.this;
        recycleStudents = findViewById(R.id.rv_add_member);
        id = getIntent().getStringExtra("Batch_id");
        //Toast.makeText(context, "iggg"+sharedPreference_main.getCreatedId(), Toast.LENGTH_SHORT).show();
        tv_create = findViewById(R.id.tv_create_new_mem);
        sharedPreference_main.setbatch(getIntent().getStringExtra("Batch_id"));
        tv_create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                update_student_group_list();


            }
        });
        view_all_group_member();
    }

    private void view_all_group_member() {

        HashMap<String, String> map = new HashMap<>();

        map.put("group_id", getIntent().getStringExtra("groupId"));
        map.put("group_type", getIntent().getStringExtra("groupType"));
        map.put("class_id", getIntent().getStringExtra("classId"));
        map.put("batch_id", getIntent().getStringExtra("batchId"));

        //   if (NetworkUtils.isConnected(getActivity())) {
        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<ResponeViewGroupMemberList> call = serviceInterface.view_all_group_member("Bearer " + sharedPreference_main.getToken(), Content_Type, map);
        call.enqueue(new Callback<ResponeViewGroupMemberList>() {
            @Override
            public void onResponse(Call<ResponeViewGroupMemberList> call, retrofit2.Response<ResponeViewGroupMemberList> response) {

                if (response.isSuccessful()) {

                    ResponeViewGroupMemberList bean = response.body();

                    // Toast.makeText(GroupActivity.this, "sucess", Toast.LENGTH_SHORT).show();
                    if (bean.getStatus()) {
                        ResponeViewGroupMemberList view = response.body();
                        //Toast.makeText(AddNewMembersToGroup.this, "gfcfdhgjkd", Toast.LENGTH_SHORT).show();
                        Adapter = new AdpaterAddNewMemberGroupList(AddNewMembersToGroup.this, bean.getData());
                        //recyclerView_group_view.setLayoutManager(new GridLayoutManager(getBaseContext(), 2));
                        recycleStudents.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
                        recycleStudents.setItemAnimator(new DefaultItemAnimator());
                        recycleStudents.setAdapter(Adapter);

                        Log.e("Group_response", view.toString());

                    } else {
                        Toast.makeText(context, bean.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                } else {

                    Toast.makeText(context, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponeViewGroupMemberList> call, Throwable t) {
                Log.e("error", t.getMessage());
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void update_student_group_list(){
            HashMap<String,String> map=new HashMap<>();

        map.put("group_id", getIntent().getStringExtra("groupId"));
        map.put("user_id", sharedPreference_main.getCreatedId());
        map.put("students_id", sharedPreference_main.getStudent_id());

            ServiceInterface serviceInterface= ApiClient.getClient().create(ServiceInterface.class);
            Call<ResponeAddStudentInGroup> call=serviceInterface.add_sudent_group_in_group("Bearer " + sharedPreference_main.getToken(), Content_Type, map);
            call.enqueue(new Callback<ResponeAddStudentInGroup>() {
                @Override
                public void onResponse(Call<ResponeAddStudentInGroup> call, Response<ResponeAddStudentInGroup> response) {
                    if (response.isSuccessful()){
                        ResponeAddStudentInGroup bean=response.body();
                        if (bean.getStatus()) {
                            //**refresh activity on adapter**//
                            ((ViewGroupActivity) context).recreate();
                            Log.e("Group_response", bean.toString());
                        }
                        else {
                            Toast.makeText(context, bean.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }

                    else {
                        Toast.makeText(context, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponeAddStudentInGroup> call, Throwable t) {
                    Log.e("error", t.getMessage());
                    Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });





    }


    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                view_all_group_member();
                refreshLayout.setRefreshing(false);
            }
        }, 2000);

    }
}