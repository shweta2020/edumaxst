package com.nusys.studentedumax;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class ForgotPasswordFinal extends AppCompatActivity {
    TextView tv_generatePassword;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password_final);
        tv_generatePassword=findViewById(R.id.tvGeneratePassword);

        tv_generatePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ForgotPasswordFinal.this,Login.class));
                finish();
            }
        });
    }
}
