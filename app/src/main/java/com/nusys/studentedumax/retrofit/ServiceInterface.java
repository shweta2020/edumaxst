package com.nusys.studentedumax.retrofit;

import com.nusys.studentedumax.model.GetChapResponseModel;
import com.nusys.studentedumax.model.GetExamResponseModel;
import com.nusys.studentedumax.model.GetSubjectResponseModel;
import com.nusys.studentedumax.model.GetUnitResponseModel;
import com.nusys.studentedumax.model.LoginModel;
import com.nusys.studentedumax.model.ResponeAddStudentInGroup;
import com.nusys.studentedumax.model.ResponeForgotPassword;
import com.nusys.studentedumax.model.ResponeStartTestAction;
import com.nusys.studentedumax.model.ResponeViewCourse;
import com.nusys.studentedumax.model.ResponeViewGroupMemberList;
import com.nusys.studentedumax.model.ResponeBatchStudent;
import com.nusys.studentedumax.model.ResponeCreateGroup;
import com.nusys.studentedumax.model.ResponeDeleteGroup;
import com.nusys.studentedumax.model.ResponeDeleteStudentFromGroup;
import com.nusys.studentedumax.model.ResponeProfileUpdate;
import com.nusys.studentedumax.model.ResponeStudentBatch;
import com.nusys.studentedumax.model.ResponeStudentLiveClasses;
import com.nusys.studentedumax.model.ResponeUpdateGroupImage;
import com.nusys.studentedumax.model.ResponeUpdateGroupName;
import com.nusys.studentedumax.model.ResponeViewGroup;
import com.nusys.studentedumax.model.ResponeViewGroupList;
import com.nusys.studentedumax.model.ResponseQuesCount;
import com.nusys.studentedumax.model.ResponseQuesReview;
import com.nusys.studentedumax.model.ResponseQuesSubmit;
import com.nusys.studentedumax.model.ResponseQuestionList;
import com.nusys.studentedumax.model.ResponseQuizResult;
import com.nusys.studentedumax.model.ResponseQuizSubmit;
import com.nusys.studentedumax.model.ResponseTestList;
import com.nusys.studentedumax.model.ResponseTestStart;
import com.nusys.studentedumax.model.profile.ResponeProfile;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

import static com.nusys.studentedumax.Constants.ADD_STU_TO_GROUP;
import static com.nusys.studentedumax.Constants.CREATE_STU_GROUP;
import static com.nusys.studentedumax.Constants.DELETE_STU_GROUP;
import static com.nusys.studentedumax.Constants.FETCH_QUES_COUNT;
import static com.nusys.studentedumax.Constants.FETCH_QUIZ_QUES;
import static com.nusys.studentedumax.Constants.FINAL_QUIZ_SUBMIT;
import static com.nusys.studentedumax.Constants.FORGOT_PWD;
import static com.nusys.studentedumax.Constants.GET_ALL_STU_GROUP;
import static com.nusys.studentedumax.Constants.GET_BATCH_STU;
import static com.nusys.studentedumax.Constants.GET_LIVECLASS_STU;
import static com.nusys.studentedumax.Constants.GET_STU_BATCH;
import static com.nusys.studentedumax.Constants.GET_STU_LIVECLASS;
import static com.nusys.studentedumax.Constants.LOGIN;

import static com.nusys.studentedumax.Constants.PROFILE;
import static com.nusys.studentedumax.Constants.QUES_REVIEW;
import static com.nusys.studentedumax.Constants.QUES_SUBMIT;
import static com.nusys.studentedumax.Constants.QUIZ_RESULT;
import static com.nusys.studentedumax.Constants.QUIZ_REVIEW;
import static com.nusys.studentedumax.Constants.REMOVE_STU_GROUP;
import static com.nusys.studentedumax.Constants.SIGNUP;
import static com.nusys.studentedumax.Constants.STUDENT_CHAP;
import static com.nusys.studentedumax.Constants.STUDENT_EXAM;
import static com.nusys.studentedumax.Constants.STUDENT_SUB;
import static com.nusys.studentedumax.Constants.STUDENT_UNIT;
import static com.nusys.studentedumax.Constants.STU_BY_GROUPTYPE;
import static com.nusys.studentedumax.Constants.STU_TEST_LIST;
import static com.nusys.studentedumax.Constants.TEST_START_BUTTON;
import static com.nusys.studentedumax.Constants.UPDATE_GROUP_NAME;
import static com.nusys.studentedumax.Constants.UPDATE_GROUP_PIC;
import static com.nusys.studentedumax.Constants.UPDATE_PROFILE;
import static com.nusys.studentedumax.Constants.VIEW_COURSE;
import static com.nusys.studentedumax.Constants.VIEW_GROUP;

public interface ServiceInterface {

    @POST(LOGIN)
    Call<LoginModel> doLogin(@Body HashMap<String, String> map);


    @POST(SIGNUP)
    Call<LoginModel> doSignUp(@Body HashMap<String, String> map);


    @POST(UPDATE_PROFILE)
    Call<ResponeProfileUpdate> update_profile(@Header("Authorization") String token, @Header("Content_Type") String header, @Body HashMap<String, String> map);


    @POST(PROFILE)
    Call<ResponeProfile> profile(@Header("Authorization") String token, @Header("Content_Type") String header, @Body HashMap<String, String> map);


    //View all Group
    @POST(GET_ALL_STU_GROUP)
    Call<ResponeViewGroup> view_group(@Header("Authorization") String token, @Header("Content_Type") String header, @Body HashMap<String, String> map);


    //View Group List
    @POST(VIEW_GROUP)
    Call<ResponeViewGroupList> view_group_list(@Header("Authorization") String token, @Header("Content_Type") String header, @Body HashMap<String, String> map);

    //Delete student Group
    @POST(DELETE_STU_GROUP)
    Call<ResponeDeleteGroup> delete_group(@Header("Authorization") String token, @Header("Content_Type") String header, @Body HashMap<String, String> map);

    //Update Group Name
    @POST(UPDATE_GROUP_NAME)
    Call<ResponeUpdateGroupName> update_group_name(@Header("Authorization") String token, @Header("Content_Type") String header, @Body HashMap<String, String> map);

    //Update Group Image
    @POST(UPDATE_GROUP_PIC)
    Call<ResponeUpdateGroupImage> update_group_image(@Header("Authorization") String token, @Header("Content_Type") String header, @Body HashMap<String, String> map);

    //remove student Group
    @POST(REMOVE_STU_GROUP)
    Call<ResponeDeleteStudentFromGroup> delete_student_from_group(@Header("Authorization") String token, @Header("Content_Type") String header, @Body HashMap<String, String> map);


    //Student Batch Group
    @POST(GET_STU_BATCH)
    Call<ResponeStudentBatch> student_batch(@Header("Authorization") String token, @Header("Content_Type") String header, @Body HashMap<String, String> map);


    //Student Live Classes
    @POST(GET_STU_LIVECLASS)
    Call<ResponeStudentLiveClasses> student_live(@Header("Authorization") String token, @Header("Content_Type") String header, @Body HashMap<String, String> map);

    //Batch Student Name
    @POST(GET_BATCH_STU)
    Call<ResponeBatchStudent> batch_student_name(@Header("Authorization") String token, @Header("Content_Type") String header, @Body HashMap<String, String> map);


    //Batch Live Student Name
    @POST(GET_LIVECLASS_STU)
    Call<ResponeBatchStudent> live_student_name(@Header("Authorization") String token, @Header("Content_Type") String header, @Body HashMap<String, String> map);

    //Create Group
    @POST(CREATE_STU_GROUP)
    Call<ResponeCreateGroup> create_group(@Header("Authorization") String token, @Header("Content_Type") String header, @Body HashMap<String, String> map);

    //get all students by group
    @POST(STU_BY_GROUPTYPE)
    Call<ResponeViewGroupMemberList> view_all_group_member(@Header("Authorization") String token, @Header("Content_Type") String header, @Body HashMap<String, String> map);


    //add student in a group update
    @POST(ADD_STU_TO_GROUP)
    Call<ResponeAddStudentInGroup> add_sudent_group_in_group(@Header("Authorization") String token, @Header("Content_Type") String header, @Body HashMap<String, String> map);

    //view tests
    @POST(STU_TEST_LIST)
    Call<ResponseTestList> view_test_list(@Header("Authorization") String token, @Header("Content_Type") String header, @Body HashMap<String, String> map);

    /*//start tests
    @POST("testStartButtonAction.php")
    Call<ResponeStartTestAction> start_test(@Header("Authorization") String token, @Header("Content_Type") String header, @Body HashMap<String, String> map);
*/
    //test start button
    @POST(TEST_START_BUTTON)
    Call<ResponseTestStart> quiz_start(@Header("Authorization") String token, @Header("Content_Type") String header, @Body HashMap<String, String> map);

    /*//fetch quiz questions
    @POST(FETCH_QUIZ_QUES)
    Call<ResponseQuestionList> quiz_question(@Header("Authorization") String token, @Header("Content_Type") String header, @Body HashMap<String, String> map);
*/
    //submit individual questions
    @POST(QUES_SUBMIT)
    Call<ResponseQuesSubmit> ques_submit(@Header("Authorization") String token, @Header("Content_Type") String header, @Body HashMap<String, String> map);

    //fetch ques count
    @POST(FETCH_QUES_COUNT)
    Call<ResponseQuesCount> fect_ques_count(@Header("Authorization") String token, @Header("Content_Type") String header, @Body HashMap<String, String> map);

    //for submit quiz
    @POST(FINAL_QUIZ_SUBMIT)
    Call<ResponseQuizSubmit> final_quiz_submit(@Header("Authorization") String token, @Header("Content_Type") String header, @Body HashMap<String, String> map);

    //for quiz result
    @POST(QUIZ_RESULT)
    Call<ResponseQuizResult> quiz_result(@Header("Authorization") String token, @Header("Content_Type") String header, @Body HashMap<String, String> map);

    //complete quiz review quiz
    @POST(QUIZ_REVIEW)
    Call<ResponseQuestionList> quiz_review(@Header("Authorization") String token, @Header("Content_Type") String header, @Body HashMap<String, String> map);

    //review ques
    @POST(QUES_REVIEW)
    Call<ResponseQuesReview> ques_review(@Header("Authorization") String token, @Header("Content_Type") String header, @Body HashMap<String, String> map);

    //view course detail
    @POST(VIEW_COURSE)
    Call<ResponeViewCourse> view_course(@Header("Authorization") String token, @Header("Content_Type") String header, @Body HashMap<String, String> map);

    //forgot password
    @POST(FORGOT_PWD)
    Call<ResponeForgotPassword> forgot_pwd(@Body HashMap<String, String> map);

    @POST(STUDENT_EXAM)
    Call<GetExamResponseModel> getMyExams(@Header("Authorization") String header, @Header("Content-Type") String contentType, @Body HashMap<String, String> map);

    @POST(STUDENT_SUB)
    Call<GetSubjectResponseModel> getMySubject(@Header("Authorization") String header, @Header("Content-Type") String contentType, @Body HashMap<String, String> map);

    @POST(STUDENT_UNIT)
    Call<GetUnitResponseModel> getMyUnit(@Header("Authorization") String header, @Header("Content-Type") String contentType, @Body HashMap<String, String> map);

    @POST(STUDENT_CHAP)
    Call<GetChapResponseModel> getMyChap(@Header("Authorization") String header, @Header("Content-Type") String contentType, @Body HashMap<String, String> map);


}
