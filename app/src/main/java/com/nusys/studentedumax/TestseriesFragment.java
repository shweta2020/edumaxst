package com.nusys.studentedumax;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.nusys.studentedumax.commonModules.NetworkUtil;
import com.nusys.studentedumax.commonModules.SharedPreference_main;

import static com.nusys.studentedumax.commonModules.Extension.showErrorDialog;

public class TestseriesFragment extends Fragment {
    CardView practice_test, exam_test;
    LinearLayout errorLayout, llCompleteTestview;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_testseries_fragment, container, false);

        practice_test = rootView.findViewById(R.id.practiceTest);
        exam_test = rootView.findViewById(R.id.examTest);
        errorLayout = rootView.findViewById(R.id.error_layout);
        llCompleteTestview = rootView.findViewById(R.id.ll_complete_testview);

        if (NetworkUtil.isConnected(getContext())) {
          /*  errorLayout.setVisibility(View.GONE);
            llCompleteTestview.setVisibility(View.VISIBLE);*/
            practice_test.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent i = new Intent(getActivity(), PracticeTest.class);
                    i.putExtra("test_type", "Practice");
                    startActivity(i);

                }
            });

            exam_test.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(getActivity(), ExamTest.class);
                    i.putExtra("test_type", "Exam");
                    startActivity(i);

                }
            });
        } else {
           /* errorLayout.setVisibility(View.VISIBLE);
            llCompleteTestview.setVisibility(View.GONE);*/
            showErrorDialog(getContext(), new Dialog(getContext()));
        }

        return rootView;


    }


}
