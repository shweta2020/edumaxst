package com.nusys.studentedumax;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.webkit.PermissionRequest;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.nusys.studentedumax.commonModules.Extension;
import com.nusys.studentedumax.commonModules.MyReceiver;
import com.nusys.studentedumax.commonModules.NetworkUtil;
import com.nusys.studentedumax.commonModules.SharedPreference_main;

import static com.nusys.studentedumax.Constants.BASE_URL;
import static com.nusys.studentedumax.Constants.WEBVIEW_BASE_URL;
import static com.nusys.studentedumax.Constants.WEBVIEW_LOGIN;
import static com.nusys.studentedumax.commonModules.TextModule.getContext;

public class CalenderWebview extends AppCompatActivity {
    SharedPreference_main sharedPreference_main;
    Context context;
    WebView wv1;
    Dialog dialog;
    private BroadcastReceiver MyReceiver = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tests);

        init();
        action(savedInstanceState);
        broadcastIntent();
        //isNetworkConnectionAvailable();

    }

    public void init() {
        context = CalenderWebview.this;
        sharedPreference_main = SharedPreference_main.getInstance(context);
        wv1 = findViewById(R.id.webview);
        dialog = new Dialog(this);
        MyReceiver = new MyReceiver();

    }

    public void action(Bundle savedInstanceState) {
        if (NetworkUtil.isConnected(this)) {
            wv1.setWebViewClient(new MyWebViewClient());
            wv1.getSettings().setLoadsImagesAutomatically(true);
            wv1.getSettings().setJavaScriptEnabled(true);

            wv1.getSettings().setAllowFileAccessFromFileURLs(true);
            wv1.getSettings().setAllowUniversalAccessFromFileURLs(true);
            wv1.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
            wv1.getSettings().setPluginState(WebSettings.PluginState.ON);
            wv1.setWebChromeClient(new WebChromeClient() {

                @Override
                public void onPermissionRequest(final PermissionRequest request) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        request.grant(request.getResources());
                    }
                }

            });
            /** for-when change mode from landscape to portrait or vice-versa not goes in back, keep on same screen
             * Created Date: 18-06-2020
             **/
            if (savedInstanceState == null) {
                wv1.loadUrl(WEBVIEW_BASE_URL + WEBVIEW_LOGIN + "?user_id=\"" + sharedPreference_main.getUserId() + "\"&token=\"" + sharedPreference_main.getToken() + "\"");

            }

            //  wv1.loadUrl(WEBVIEW_BASE_URL + WEBVIEW_LOGIN + "?user_id=\"" + sharedPreference_main.getUserId() + "\"&token=\"" + sharedPreference_main.getToken() + "\"");

            /** used for keep screen on **/
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        } else {
            Extension.showErrorDialog(this, dialog);
//            showPopup();
        }
    }


    static class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }


    /*
      broadcastIntent(),onPause() both are used for broadcast msg for internet connection
       */
    public void broadcastIntent() {
        registerReceiver(MyReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (MyReceiver != null) {
            unregisterReceiver(MyReceiver);
            MyReceiver = null;
        }

    }
    /** for-when change mode from landscape to portrait or vice-versa not goes in back, keep on same screen
     * Created Date: 18-06-2020
     **/
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        wv1.saveState(outState);
    }
    /** for-when change mode from landscape to portrait or vice-versa not goes in back, keep on same screen
     * Created Date: 18-06-2020
     **/
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        wv1.restoreState(savedInstanceState);
    }
}
