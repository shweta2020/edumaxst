package com.nusys.studentedumax;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.nusys.studentedumax.commonModules.ExpandableTextView;
import com.nusys.studentedumax.commonModules.SharedPreference_main;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static com.nusys.studentedumax.Constants.BASE_URL_TEACHER;
import static com.nusys.studentedumax.Constants.SINGLE_POST;

public class ViewImage extends AppCompatActivity {
    SharedPreference_main sharedPreference_main;
    Context context;
    ImageView ivImage;
    /* variable for zoomin and zoomout*/
    private ScaleGestureDetector mScaleGestureDetector;
    private float mScaleFactor = 1.0f;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_image);

        sharedPreference_main = SharedPreference_main.getInstance(context);
        context= ViewImage.this;
        ivImage= findViewById(R.id.iv_image);
        /* detecting gesture for zoomin and zoomout*/
        mScaleGestureDetector = new ScaleGestureDetector(this, new ScaleListener());

        viewCommentApi();
    }

    private void viewCommentApi() {

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        final JSONObject jsonBody = new JSONObject();
        try {

            jsonBody.put("id", getIntent().getStringExtra("postIds"));
            jsonBody.put("user_id", sharedPreference_main.getUserId());
            jsonBody.put("user_type", sharedPreference_main.getUserType());


        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL_TEACHER+SINGLE_POST, jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Boolean status = response.getBoolean("status");
                            //Toast.makeText(Profile.this, "", Toast.LENGTH_SHORT).show();
                            // Toast.makeText(Profile.this, "abcd"+status, Toast.LENGTH_SHORT).show();
                            if (status) {
                                JSONArray jsonArray = response.getJSONArray("data");
                                for (int i = 0; i < jsonArray.length(); i++) {

                                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                                    Glide.with(getApplicationContext())
                                            .load(jsonObject.getString("filename"))
                                            .into(ivImage);

                                }


                            } else {

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        // Toast.makeText(SignUpStepTwo.this, "" + response.toString(), Toast.LENGTH_SHORT).show();
                        //resultTextView.setText("String Response : "+ response.toString());
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //  resultTextView.setText("Error getting response");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json; charset=UTF-8");
                params.put("Authorization", "Bearer " + sharedPreference_main.getToken());
                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);


    }

    /* touch event for zoomin and zoomout*/
    public boolean onTouchEvent(MotionEvent motionEvent) {
        mScaleGestureDetector.onTouchEvent(motionEvent);
        return true;
    }

    /* scaling for zoomin and zoomout*/
    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        @Override
        public boolean onScale(ScaleGestureDetector scaleGestureDetector){
            mScaleFactor *= scaleGestureDetector.getScaleFactor();
            mScaleFactor = Math.max(0.1f,
                    Math.min(mScaleFactor, 10.0f));
            ivImage.setScaleX(mScaleFactor);
            ivImage.setScaleY(mScaleFactor);
            return true;
        }
    }
}
