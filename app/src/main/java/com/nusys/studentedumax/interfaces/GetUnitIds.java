package com.nusys.studentedumax.interfaces;

import java.util.ArrayList;

public interface GetUnitIds {

    public void getUnitId(String n);
}
