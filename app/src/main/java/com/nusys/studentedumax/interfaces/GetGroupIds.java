package com.nusys.studentedumax.interfaces;

import java.util.ArrayList;

public interface GetGroupIds {

    public void getGroupIds(String groupId, String groupName, String groupImage);
}
