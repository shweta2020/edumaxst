package com.nusys.studentedumax.commonModules;

import android.content.Context;

public class TextModule {


    private static Context context;

    public static Context getContext() {

        return context;
    }

    public static void setContext(Context context) {

        TextModule.context = context;
    }
}
