package com.nusys.studentedumax.commonModules;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import com.nusys.studentedumax.R;

import java.util.Calendar;

public class Extension {


    //    public static void showToastMessage(Context context, String message){
//
//        LayoutInflater inflater = ((Activity)context).getLayoutInflater();
//        View layout=inflater.inflate(R.layout.custom_toast,(ViewGroup)( ((Activity) context).findViewById(R.id.relativelayout)));
//
////        View layout = inflater.inflate(R.layout.custom_toast,
////                (ViewGroup) ((Activity) context).findViewById(R.id.relativelayout));
////        // set a message
//        TextView text = (TextView) layout.findViewById(R.id.toast);
//        text.setText(message);
//
//        // Toast...
//        Toast toast = new Toast(context);
//        toast.setGravity(Gravity.BOTTOM, 0, 0);
//        toast.setDuration(Toast.LENGTH_LONG);
//        toast.setView(layout);
//        toast.show();
//    }
    public static void showtoast(Context context, String message) {
        Toast.makeText(context, "" + message, Toast.LENGTH_SHORT).show();
    }

    public static void printLog(String msg) {
        Log.e("error", msg);
    }

    public static void printResponse(String msg) {
        Log.e("response", msg);
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    public static void openCelender(Context context, int year, int month, int day, final TextView textView) {
        final Calendar c;
        c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        final DatePickerDialog mDatePicker = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                textView.setText(new StringBuilder().append(selectedday).append("-").append(selectedmonth + 1).append("-").append(selectedyear));
            }
        }, year, month, day);
        mDatePicker.setTitle("Please select date");
//                mDatePicker.getDatePicker().setMaxDate(System.currentTimeMillis());
        mDatePicker.show();


    }


    public static void openTimer(Context context , int mHour, int mMinute, final TextView textView){


            // Get Current Time
            final Calendar c = Calendar.getInstance();
            mHour = c.get(Calendar.HOUR_OF_DAY);
            mMinute = c.get(Calendar.MINUTE);

            // Launch Time Picker Dialog
            TimePickerDialog timePickerDialog = new TimePickerDialog(context,
                    new TimePickerDialog.OnTimeSetListener() {

                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay,
                                              int minute) {

                            textView.setText(hourOfDay + ":" + minute);
                        }
                    }, mHour, mMinute, false);
            timePickerDialog.show();


    }

    public static void showErrorDialog(Context context, final Dialog dialog) {

//        dialog=new Dialog(context);
        dialog.setContentView(R.layout.error_dialog);
        TextView tvOk = dialog.findViewById(R.id.text_ok);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();

        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


    }

 /*   public static void showResponseErrorDialog(Context context, final Dialog dialog) {

//        dialog=new Dialog(context);
        dialog.setContentView(R.layout.error_dialog);
        TextView tvOk = dialog.findViewById(R.id.text_ok);
        TextView text = dialog.findViewById(R.id.text);
        text.setText("Something is wrong please try again later");
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();

        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


    }*/

    public static View makeMeBlink(View view, int duration, int offset) {

        Animation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(duration);
        anim.setStartOffset(offset);
        anim.setRepeatMode(Animation.REVERSE);
        anim.setRepeatCount(Animation.INFINITE);
        view.startAnimation(anim);
        return view;


    }

}

