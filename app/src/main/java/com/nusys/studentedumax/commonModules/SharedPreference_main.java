package com.nusys.studentedumax.commonModules;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreference_main {

    Context mContext;
    private static SharedPreference_main sharedPreference_main;
    private SharedPreferences sharedPreference;
    private SharedPreferences.Editor editor;

    public SharedPreference_main(Context context) {
        mContext = context;
        sharedPreference = context.getSharedPreferences("Shared_Pre", Context.MODE_PRIVATE);
        editor = sharedPreference.edit();
    }

    public static SharedPreference_main getInstance(Context ctx) {
        if (sharedPreference_main == null) {
            sharedPreference_main = new SharedPreference_main(ctx);
            return sharedPreference_main;
        }
        return sharedPreference_main;
    }

    public void removePreference() {
        editor = sharedPreference.edit();
        editor.clear().apply();
    }

    public void setIs_LoggedIn(boolean Is_LoggedIn) {
        editor = sharedPreference.edit();
        editor.putBoolean("Is_LoggedIn", Is_LoggedIn);
        editor.commit();
    }

    public boolean getIs_LoggedIn() {
        return sharedPreference.getBoolean("Is_LoggedIn", false);
    }

    public void setUsername(String username) {
        editor = sharedPreference.edit();
        editor.putString("username", username);
        editor.commit();
    }

    public String getUsername() {
        return sharedPreference.getString("username", "");
    }


    public void setUserEmail(String Email) {
        editor = sharedPreference.edit();
        editor.putString("Email", Email);
        editor.commit();
    }

    public String getUserEmail() {
        return sharedPreference.getString("Email", "");
    }


    public void setUserId(String Id) {
        editor = sharedPreference.edit();
        editor.putString("id", Id);
        editor.commit();
    }

    public String getUserId() {
        return sharedPreference.getString("id", "");
    }


    public void setToken(String Token) {
        editor = sharedPreference.edit();
        editor.putString("Token", Token);
        editor.commit();
    }

    public String getToken() {
        return sharedPreference.getString("Token", "");
    }


    public void setUserImage(String Image) {
        editor = sharedPreference.edit();
        editor.putString("Image", Image);
        editor.commit();
    }

    public String getUserImage() {
        return sharedPreference.getString("Image", "");
    }

    public void setUserType(String user_type) {
        editor = sharedPreference.edit();
        editor.putString("user_type", user_type);
        editor.commit();
    }

    public String getUserType() {
        return sharedPreference.getString("user_type", "");
    }


    // setDAta for signUP

    public void setname(String name) {
        editor = sharedPreference.edit();
        editor.putString("name", name);
        editor.commit();
    }

    public String getname() {
        return sharedPreference.getString("name", "");
    }

    public void setemail(String email) {
        editor = sharedPreference.edit();
        editor.putString("email", email);
        editor.commit();
    }

    public String getemail() {
        return sharedPreference.getString("email", "");
    }

    public void setStudent_id(String Student_id) {
        editor = sharedPreference.edit();
        editor.putString("Student_id", Student_id);
        editor.commit();
    }

    public String getStudent_id() {
        return sharedPreference.getString("Student_id", "");
    }

    public void setTest_id(String Test_id) {
        editor = sharedPreference.edit();
        editor.putString("Test_id", Test_id);
        editor.commit();
    }

    public String getTest_id() {
        return sharedPreference.getString("Test_id", "");
    }

    public String getTest_type() {
        return sharedPreference.getString("Test_type", "");
    }

    public void setTest_type(String Test_type) {
        editor = sharedPreference.edit();
        editor.putString("Test_type", Test_type);
        editor.commit();
    }


    public void seteducation(String education) {
        editor = sharedPreference.edit();
        editor.putString("education", education);
        editor.commit();
    }

    public String geteducation() {
        return sharedPreference.getString("education", "");
    }

    public void setaddress(String address) {
        editor = sharedPreference.edit();
        editor.putString("address", address);
        editor.commit();
    }

    public String getaddress() {
        return sharedPreference.getString("address", "");
    }

    public void setInititateSignUp(Boolean inititateSignUp) {
        editor = sharedPreference.edit();
        editor.putBoolean("inititateSignUp", inititateSignUp);
        editor.commit();
    }

    public Boolean getinititateSignUp() {
        return sharedPreference.getBoolean("inititateSignUp", false);
    }

    public void setIsClicked(Boolean IsClicked) {
        editor = sharedPreference.edit();
        editor.putBoolean("IsClicked", IsClicked);
        editor.commit();
    }

    public Boolean getIsClicked() {
        return sharedPreference.getBoolean("IsClicked", false);
    }


    public void setdob(String dob) {
        editor = sharedPreference.edit();
        editor.putString("dob", dob);
        editor.commit();
    }

    public String getdob() {
        return sharedPreference.getString("dob", "");
    }

    public void setgender(String gen) {
        editor = sharedPreference.edit();
        editor.putString("gen", gen);
        editor.commit();
    }

    public String getgender() {
        return sharedPreference.getString("gen", "");
    }


    public void setbatch(String batch) {
        editor = sharedPreference.edit();
        editor.putString("batch", batch);
        editor.commit();
    }

    public String getbatch() {
        return sharedPreference.getString("batch", "");
    }

    public void setGroupTypeId(String GroupTypeId) {
        editor = sharedPreference.edit();
        editor.putString("GroupTypeId", GroupTypeId);
        editor.commit();
    }

    public String getGroupTypeId() {
        return sharedPreference.getString("GroupTypeId", "");
    }

    public void setCreatedId(String CreatedId) {
        editor = sharedPreference.edit();
        editor.putString("CreatedId", CreatedId);
        editor.commit();
    }

    public String getCreatedId() {
        return sharedPreference.getString("CreatedId", "");
    }

    //** 0= for all, 1= for completed, 2= for inprogress**//
    public void setDoubtStatus(String DoubtStatus) {
        editor = sharedPreference.edit();
        editor.putString("DoubtStatus", DoubtStatus);
        editor.commit();
    }

    public String getDoubtStatus() {
        return sharedPreference.getString("DoubtStatus", "");
    }

    public void setGroupId(String GroupId) {
        editor = sharedPreference.edit();
        editor.putString("GroupId", GroupId);
        editor.commit();
    }

    public String getGroupId() {
        return sharedPreference.getString("GroupId", "");
    }

}
