package com.nusys.studentedumax.commonModules;

import android.content.Context;
import android.content.SharedPreferences;

import com.nusys.studentedumax.R;

import java.util.Random;

public class Utils {

    static int randomAndroidColor;
    static int color = -1;
    private static int[] androidColors = TextModule.getContext().getResources().getIntArray(R.array.androidcolors);

    //Generate Random Color
    public static String genRandomColor() {
        randomAndroidColor = androidColors[new Random().nextInt(androidColors.length)];
        String hexColor = "#" + Integer.toHexString(randomAndroidColor).substring(2);
        return hexColor.toString();
    }

    //Save Circle Color in Shared Prefs
    public static void setCircleColor(int position, int color) {
        SharedPreferences preferences;
        SharedPreferences.Editor editor;
        preferences = TextModule.getContext().getSharedPreferences("colors", Context.MODE_PRIVATE);
        editor = preferences.edit().putInt(Integer.toString(position), color);
        editor.commit();
    }

    //Retrieve Circle Color from Shared Prefs
    public static int LoadCircleColor(int position) {
        SharedPreferences preferences = TextModule.getContext().getSharedPreferences("colors", Context.MODE_PRIVATE);
        color = preferences.getInt(Integer.toString(position), -1);
        return color;
    }
}
