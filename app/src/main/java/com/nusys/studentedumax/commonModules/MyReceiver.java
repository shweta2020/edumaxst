package com.nusys.studentedumax.commonModules;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.nusys.studentedumax.R;
/*
this class is used for checking internet connection on webview via BroadcastReceiver
 */
public class MyReceiver extends BroadcastReceiver {
    Dialog dialog;
    TextView text_error;
    @Override
    public void onReceive(Context context, Intent intent) {
        dialog=new Dialog(context);
        String status = NetworkUtil.getConnectivityStatusString(context);
        if(status.isEmpty()) {
            status="No Internet Connection";
        }


     /*   dialog.setContentView(R.layout.error_dialog);
        text_error= dialog.findViewById(R.id.text);
        TextView tvOk = dialog.findViewById(R.id.text_ok);
       // text_error.setText(status);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();

        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });*/
        Toast.makeText(context, status, Toast.LENGTH_LONG).show();
    }
}