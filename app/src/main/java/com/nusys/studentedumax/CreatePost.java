package com.nusys.studentedumax;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.Manifest;
import android.animation.Animator;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.nusys.studentedumax.Group.GroupActivity;
import com.nusys.studentedumax.Group.batch_Activity;
import com.nusys.studentedumax.adapter.Group_Adapter.Adapter_batch;
import com.nusys.studentedumax.adapter.Group_Adapter.Adapter_group_view;
import com.nusys.studentedumax.commonModules.DialogProgress;
import com.nusys.studentedumax.commonModules.SharedPreference_main;
import com.nusys.studentedumax.model.GetExamResponseModel;
import com.nusys.studentedumax.model.ResponeStudentBatch;
import com.nusys.studentedumax.model.ResponeViewGroup;
import com.nusys.studentedumax.retrofit.ApiClient;
import com.nusys.studentedumax.retrofit.ServiceInterface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

import static com.nusys.studentedumax.Constants.BASE_URL;
import static com.nusys.studentedumax.Constants.BASE_URL_TEACHER;
import static com.nusys.studentedumax.Constants.CAMERA_REQUEST_CODE;
import static com.nusys.studentedumax.Constants.CREATE_POST;
import static com.nusys.studentedumax.Constants.Content_Type;
import static com.nusys.studentedumax.Constants.GALLERY_REQUEST_CODE;
import static com.nusys.studentedumax.Constants.STUDENT_EXAM;

public class CreatePost extends AppCompatActivity {
    ConstraintLayout action_container;
    LinearLayout back_acti, img_layout;
    ImageView img_gallery, profile_img;
    private String userImageBase64;
    SharedPreference_main sharedPreference_main;
    TextView text_userName, text_email;
    EditText edt_heading, edt_post;
    Context context;
    Button btn_post;

    TextView text_ApplyPrivacy;

    RadioGroup rdgprivacyType;
    RadioButton rdpublic, idgroup, rdbatch, rdexam;
    Spinner spinprivcy;
    private Dialog dialog;
    String contentType;
    ProgressDialog pd;
    List<String> examList, courseList, groupList, batchList, examListId, courseListId, groupListId, batchListId;
    String privacyType="0";
    Dialog privacyDialog;

    String selectedId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_post);

        //for animation
        action_container = findViewById(R.id.action_container_create_post);
        if (savedInstanceState == null) {
            action_container.setVisibility(View.INVISIBLE);
            ViewTreeObserver viewTreeObserver = action_container.getViewTreeObserver();
            if (viewTreeObserver.isAlive()) {
                viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        circularRevealTransition(); //
                        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                            action_container.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                        } else {
                            action_container.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        }
                    }
                });
            }
        }
        init();
        listener();

    }

    private void init() {
        context = CreatePost.this;
        sharedPreference_main = SharedPreference_main.getInstance(this);
        privacyDialog = new Dialog(this);
        privacyDialog.setContentView(R.layout.privacy_dialog);
        privacyDialog.getWindow().setLayout(650, 1000);
        edt_heading = findViewById(R.id.et_heading);
        edt_post = findViewById(R.id.et_post);
        btn_post = findViewById(R.id.btn__create_post);

        text_ApplyPrivacy = findViewById(R.id.textApplyPrivacy);

        rdgprivacyType = privacyDialog.findViewById(R.id.rdg_privacyType);
        rdpublic = privacyDialog.findViewById(R.id.rd_public);
        idgroup = privacyDialog.findViewById(R.id.id_group);
        rdbatch = privacyDialog.findViewById(R.id.rd_batch);
        rdexam = privacyDialog.findViewById(R.id.rd_exam);
        spinprivcy = privacyDialog.findViewById(R.id.spin_privcy);

        dialog = new Dialog(context);
        pd = new DialogProgress(this, "");
        pd.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        pd.setCancelable(false);
        examList = new ArrayList<>();
        courseList = new ArrayList<>();
        groupList = new ArrayList<>();
        batchList = new ArrayList<>();
        examListId = new ArrayList<>();
        courseListId = new ArrayList<>();
        groupListId = new ArrayList<>();
        batchListId = new ArrayList<>();

        profile_img = findViewById(R.id.profileImg);
        text_userName = findViewById(R.id.tv_userName_post);
        text_email = findViewById(R.id.tv_email_createPost);

        Glide.with(CreatePost.this)
                .load(sharedPreference_main.getUserImage())
                .placeholder(R.drawable.user_icon)
                .into(profile_img);
        back_acti = findViewById(R.id.back_activity);

        //open camera and set image
        img_gallery = findViewById(R.id.iv_gallery);
        img_layout = findViewById(R.id.iv_layout);

    }

    private void listener() {

        btn_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(edt_heading.getText().toString())) {
                    edt_heading.setError("Field can't be empty");
                } else if (TextUtils.isEmpty(edt_post.getText().toString())) {
                    edt_post.setError("Field can't be empty");
                } else {
                    switch (privacyType) {


                        case "0":
                            createPost_Json_Request(privacyType, "", "", "");
                            break;

                        case "1":
                            createPost_Json_Request(privacyType, "", selectedId, "");
                            break;

                        case "2":
                            createPost_Json_Request(privacyType, "", "", selectedId);

                            break;
                        case "3":
                            createPost_Json_Request(privacyType, selectedId, "", "");
                            break;
                    }


                }
            }
        });


        text_userName.setText(sharedPreference_main.getUsername());
        text_email.setText(sharedPreference_main.getUserEmail());

        img_layout.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                openChooser(CAMERA_REQUEST_CODE, GALLERY_REQUEST_CODE);

            }
        });

        back_acti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        text_ApplyPrivacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPrivacyDialog(privacyDialog);
            }
        });

        rdgprivacyType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                switch (checkedId) {

                    case R.id.rd_public:
                        privacyType = "0";

                        text_ApplyPrivacy.setText("with public");
                        privacyDialog.dismiss();
                        spinprivcy.setVisibility(View.GONE);
                       // privacyDialog.dismiss();
                        break;
                    case R.id.id_group:
                        privacyType = "2";

                        text_ApplyPrivacy.setText("with group");
                        getGroups();
                        spinprivcy.setVisibility(View.VISIBLE);
                        break;
                    case R.id.rd_batch:
                        privacyType = "3";
                        text_ApplyPrivacy.setText("with batch");
                        getBatches();
                        spinprivcy.setVisibility(View.VISIBLE);
                        break;
                    case R.id.rd_exam:
                        privacyType = "1";
                        text_ApplyPrivacy.setText("with exam");
                        getExams();
                        spinprivcy.setVisibility(View.VISIBLE);
                        break;
                }
            }
        });

        spinprivcy.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (privacyType) {


                    case "0":
                        break;
                    case "1":
                        if (examListId.get(position) == "-1") {

                        } else {
                            selectedId = examListId.get(position);
                            text_ApplyPrivacy.setText("with " + examList.get(position) + " exam");

                            privacyDialog.dismiss();
                        }
                        break;
                    case "2":
                        if (groupListId.get(position) == "-1") {

                        } else {
                            selectedId = groupListId.get(position);
                            text_ApplyPrivacy.setText("with " + groupList.get(position) + " group");

                            privacyDialog.dismiss();
                        }
                        break;
                    case "3":
                        if (batchListId.get(position) == "-1") {

                        } else {
                            selectedId = batchListId.get(position);
                            text_ApplyPrivacy.setText("with " + (batchList.get(position)) + " batch");

                            privacyDialog.dismiss();
                        }
                        break;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



    }


    private void createPost_Json_Request(String privacy, String batchId, String examId, String groupId) {


        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        JSONObject jsonBody = new JSONObject();
        try {
            //input your API parameters

            jsonBody.put("uploader_id", sharedPreference_main.getUserId());
            jsonBody.put("heading", edt_heading.getText().toString());
            jsonBody.put("content", edt_post.getText().toString());
            jsonBody.put("post_type", "post");
            jsonBody.put("upload_type", "image");
            jsonBody.put("uploader_type", "student");
            jsonBody.put("filename", userImageBase64);
            jsonBody.put("privacy", privacy);
            jsonBody.put("exam_id", examId);
            jsonBody.put("group_id", groupId);
            jsonBody.put("batch_id", batchId);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        // Enter the correct url for your api service site
        // String url = getResources().getString(R.string.url);
        //String URL = "http://13.233.162.24/nusys(UAT)/api/teacher/create_post.php";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL_TEACHER + CREATE_POST, jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            // Toast.makeText(ChangePassword.this, "true", Toast.LENGTH_SHORT).show();

                            Boolean status = response.getBoolean("status");
                            if (status) {
                                Toast.makeText(context, "" + response.getString("message"), Toast.LENGTH_SHORT).show();
                                Intent i = new Intent(context, Home.class);
                                startActivity(i);

                            } else {
                                Toast.makeText(context, "" + response.getString("message"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        // Toast.makeText(ChangePassword.this, ""+response.toString(), Toast.LENGTH_SHORT).show();
                        //resultTextView.setText("String Response : "+ response.toString());
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //  resultTextView.setText("Error getting response");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json; charset=UTF-8");
                params.put("Authorization", "Bearer " + sharedPreference_main.getToken());
                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);


    }

    private void chooseImageFromGallery(final int code) {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(pickPhoto, code);
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();

    }

    private void takeCameraImage(final int code) {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
//                            fileName = System.currentTimeMillis() + ".jpg";

                            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(cameraIntent, code);
//                            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, getCacheImagePath(fileName));
//                            if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
//                                startActivityForResult(takePictureIntent, code);
//                            }
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {

            case CAMERA_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    Bitmap photo = (Bitmap) data.getExtras().get("data");
                    img_gallery.setImageBitmap(photo);
                    getEncoded64ImageStringFromBitmap(photo);
                    userImageBase64 = getEncoded64ImageStringFromBitmap(photo);
                    Log.e("excam", userImageBase64);
//                    tv_userImage.setText(getCacheImagePath(fileName).toString());

//                  getCacheImagePath(fileName);
                }
                break;
            case GALLERY_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    Uri imageUri = data.getData();
                    img_gallery.setImageURI(imageUri);
                    Bitmap bitmap;
                    try {
                        bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(imageUri));
                        Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, 500, 500, false);
                        userImageBase64 = getEncoded64ImageStringFromBitmap(resizedBitmap);
                        Log.e("exp", userImageBase64);


                    } catch (FileNotFoundException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
//                    tv_userImage.setText(imageUri.getPath());

                break;
        }
    }

    public String getEncoded64ImageStringFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteFormat = stream.toByteArray();
        String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);
        Log.e("", imgString);
        return imgString;
    }

    private void openChooser(final int code1, final int code2) {

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.chooser);
        TextView camera_cp = dialog.findViewById(R.id.camera);
        TextView gallery_cp = dialog.findViewById(R.id.gallery);
        dialog.show();
        camera_cp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takeCameraImage(code1);
                dialog.dismiss();
            }
        });
        gallery_cp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseImageFromGallery(code2);
                dialog.dismiss();


            }
        });
    }

    private void circularRevealTransition() {
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Set the 'X' and 'Y' values for your requirement, Here it is set for the fab being as the source of the circle reveal //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        int X = 9 * action_container.getWidth() / 10;
        int Y = 9 * action_container.getHeight() / 10;
        int Duration = 800;
        float finalRadius = Math.max(action_container.getWidth(), action_container.getHeight()); //The final radius must be the end points of the current activity
        // create the animator for this view, with the start radius as zero
        Animator circularReveal = ViewAnimationUtils.createCircularReveal(action_container, X, Y, 0, finalRadius);
        circularReveal.setDuration(Duration);

        // set the view visible and start the animation
        action_container.setVisibility(View.VISIBLE);
        // start the animation
        circularReveal.start();
//        action_container.setBackgroundResource(android.R.color.white);
    }


    private void openPrivacyDialog(Dialog dialog) {
//        dialog.setContentView(R.layout.privacy_dialog);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        rdgprivacyType = dialog.findViewById(R.id.rdg_privacyType);
        rdpublic = dialog.findViewById(R.id.rd_public);
        idgroup = dialog.findViewById(R.id.id_group);
        rdbatch = dialog.findViewById(R.id.rd_batch);
        rdexam = dialog.findViewById(R.id.rd_exam);
        spinprivcy = dialog.findViewById(R.id.spin_privcy);
        dialog.show();
    }

    private void getExams() {
        examList.clear();
        examListId.clear();
        examList.add("Select Exam");
        examListId.add("-1");
        HashMap<String, String> map = new HashMap<>();
        map.put("student_id", sharedPreference_main.getUserId());
        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<GetExamResponseModel> call = serviceInterface.getMyExams("Bearer " + sharedPreference_main.getToken(), Content_Type, map);
        call.enqueue(new Callback<GetExamResponseModel>() {
            @Override
            public void onResponse(Call<GetExamResponseModel> call, retrofit2.Response<GetExamResponseModel> response) {
                if (response.isSuccessful()) {

                    GetExamResponseModel bean = response.body();
                    for (int i = 0; i < bean.getData().size(); i++) {

                        examList.add(bean.getData().get(i).getExamName());
                        examListId.add(bean.getData().get(i).getExamId());
                    }
                    if (bean.getStatus()) {

                        spinprivcy.setAdapter(new ArrayAdapter<String>(context, R.layout.recycle_item, examList));
                    } else {
                        Toast.makeText(context, bean.getMessage(), Toast.LENGTH_SHORT).show();
                        examList.clear();
                        examListId.clear();
                        examList.add("Select Exam");
                        examListId.add("-1");
                    }

                } else {

                    Toast.makeText(context, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                    examList.clear();
                    examListId.clear();
                    examList.add("Select Exam");
                    examListId.add("-1");
                }
            }

            @Override
            public void onFailure(Call<GetExamResponseModel> call, Throwable t) {
                examList.clear();
                examListId.clear();
                examList.add("Select Exam");
                examListId.add("-1");
            }
        });


    }


    private void getBatches() {
        batchList.clear();
        batchListId.clear();
        batchList.add("Select Batch");
        batchListId.add("-1");
        HashMap<String, String> map = new HashMap<>();

        map.put("student_id", sharedPreference_main.getUserId());

        //   if (NetworkUtils.isConnected(getActivity())) {
        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<ResponeStudentBatch> call = serviceInterface.student_batch("Bearer " + sharedPreference_main.getToken(), Content_Type, map);
        call.enqueue(new Callback<ResponeStudentBatch>() {


            @Override
            public void onResponse(Call<ResponeStudentBatch> call, retrofit2.Response<ResponeStudentBatch> response) {

                if (response.isSuccessful()) {

                    ResponeStudentBatch bean = response.body();

                    //Toast.makeText(batch_Activity.this, "sucess", Toast.LENGTH_SHORT).show();
                    if (bean.getStatus()) {
                        for (int i = 0; i < bean.getData().size(); i++) {

                            batchList.add(bean.getData().get(i).getBatchName());
                            batchListId.add(bean.getData().get(i).getBatchId());
                        }
                        if (bean.getStatus()) {

                            spinprivcy.setAdapter(new ArrayAdapter<String>(context, R.layout.recycle_item, batchList));
                        } else {
                            Toast.makeText(context, bean.getMessage(), Toast.LENGTH_SHORT).show();
                            batchList.clear();
                            batchListId.clear();
                            batchList.add("Select Batch");
                            batchListId.add("-1");
                        }

                    } else {
                        Toast.makeText(context, bean.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                } else {

                    Toast.makeText(context, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                    batchList.clear();
                    batchListId.clear();
                    batchList.add("Select Batch");
                    batchListId.add("-1");
                }
            }

            @Override
            public void onFailure(Call<ResponeStudentBatch> call, Throwable t) {
                Log.e("error", t.getMessage());
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getGroups() {
        groupList.clear();
        groupListId.clear();
        groupList.add("Select Group");
        groupListId.add("-1");
        HashMap<String, String> map = new HashMap<>();

        map.put("student_id", sharedPreference_main.getUserId());

        //   if (NetworkUtils.isConnected(getActivity())) {
        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<ResponeViewGroup> call = serviceInterface.view_group("Bearer " + sharedPreference_main.getToken(), Content_Type, map);
        call.enqueue(new Callback<ResponeViewGroup>() {


            @Override
            public void onResponse(Call<ResponeViewGroup> call, retrofit2.Response<ResponeViewGroup> response) {

                if (response.isSuccessful()) {

                    ResponeViewGroup bean = response.body();

                    // Toast.makeText(GroupActivity.this, "sucess", Toast.LENGTH_SHORT).show();
                    if (bean.getStatus()) {


                        for (int i = 0; i < bean.getData().size(); i++) {
                            groupList.add(bean.getData().get(i).getGroupName());
                            groupListId.add(bean.getData().get(i).getGroupId());
                        }
                        if (bean.getStatus()) {

                            spinprivcy.setAdapter(new ArrayAdapter<String>(context, R.layout.recycle_item, groupList));
                        } else {
                            Toast.makeText(context, bean.getMessage(), Toast.LENGTH_SHORT).show();
                            groupList.clear();
                            groupListId.clear();
                            groupList.add("Select Group");
                            groupListId.add("-1");
                        }
                    } else {
                        Toast.makeText(context, bean.getMessage(), Toast.LENGTH_SHORT).show();

                        groupList.clear();
                        groupListId.clear();
                        groupList.add("Select Group");
                        groupListId.add("-1");
                    }

                } else {

                    Toast.makeText(context, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponeViewGroup> call, Throwable t) {
                groupList.clear();
                groupListId.clear();
                groupList.add("Select Group");
                groupListId.add("-1");
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


}
