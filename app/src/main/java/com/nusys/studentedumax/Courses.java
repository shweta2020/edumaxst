package com.nusys.studentedumax;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.nusys.studentedumax.commonModules.SharedPreference_main;
import com.nusys.studentedumax.model.LoginModel;
import com.nusys.studentedumax.model.ResponeViewCourse;
import com.nusys.studentedumax.model.ResponseTestStart;
import com.nusys.studentedumax.retrofit.ApiClient;
import com.nusys.studentedumax.retrofit.ServiceInterface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

import static com.nusys.studentedumax.Constants.BASE_URL;
import static com.nusys.studentedumax.Constants.BASE_URL_TEACHER;

import static com.nusys.studentedumax.Constants.Content_Type;
import static com.nusys.studentedumax.Constants.STUDENT_CHAP;
import static com.nusys.studentedumax.Constants.STUDENT_EXAM;
import static com.nusys.studentedumax.Constants.STUDENT_SUB;
import static com.nusys.studentedumax.Constants.STUDENT_UNIT;

public class Courses extends Fragment {


    TextView tv_view;
    Spinner sp_sub, sp_unit, sp_exam, sp_chap;
    private ArrayList<String> spin_exam_list = new ArrayList<String>();
    private ArrayList<String> spin_exam_list_id = new ArrayList<String>();

    private ArrayList<String> spin_sub_list = new ArrayList<String>();
    private ArrayList<String> spin_sub_list_id = new ArrayList<String>();

    private ArrayList<String> spin_unit_list = new ArrayList<String>();
    private ArrayList<String> spin_unit_list_id = new ArrayList<String>();

    private ArrayList<String> spin_chap_list = new ArrayList<String>();
    private ArrayList<String> spin_chap_list_id = new ArrayList<String>();

    String id_exam, id_sub, id_unit, id_chap;
    SharedPreference_main sharedPreference_main;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_courses, container, false);
        sharedPreference_main = SharedPreference_main.getInstance(getActivity());
        tv_view = rootView.findViewById(R.id.tvView);
        sp_sub = rootView.findViewById(R.id.spSub);
        sp_exam = rootView.findViewById(R.id.spExam);
        sp_unit = rootView.findViewById(R.id.spUnit);
        sp_chap = rootView.findViewById(R.id.spChap);

        spin_exam_list.add("Select Exam");
        spin_exam_list_id.add("-1");

        sp_exam.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                id_exam = spin_exam_list_id.get(position);

                hitSubjectSpinApi(spin_exam_list_id.get(position));

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        sp_sub.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(context, ""+parent.getSelectedItemId(), Toast.LENGTH_SHORT).show();
                id_sub = spin_sub_list_id.get(position);
                //   Toast.makeText(context, "Subjectclick"+id_sub1, Toast.LENGTH_SHORT).show();
                hitUnitSpinApi(id_exam, id_sub);


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        sp_unit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                id_unit = spin_unit_list_id.get(position);

                hitChapSpinApi(id_unit);


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        sp_chap.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(context, ""+parent.getSelectedItemId(), Toast.LENGTH_SHORT).show();

                id_chap = spin_chap_list_id.get(position);

                if (id_chap.equals("-1")) {
                    tv_view.setVisibility(View.GONE);
                } else {
                    tv_view.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        hitExamSpinApi();

        tv_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), CourseView.class);
                i.putExtra("examId", id_exam);
                i.putExtra("subId", id_sub);
                i.putExtra("unitId", id_unit);
                i.putExtra("chapId", id_chap);
                startActivity(i);
            }
        });

        return rootView;
    }


    private void hitExamSpinApi() {

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        JSONObject jsonBody = new JSONObject();
        try {
            //input your API parameters
            jsonBody.put("student_id", sharedPreference_main.getUserId());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        // String URL = "http://13.233.162.24/nusys(UAT)/api/student/student_exam.php";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL + STUDENT_EXAM, jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            // Toast.makeText(ChangePassword.this, "true", Toast.LENGTH_SHORT).show();

                            Boolean status = response.getBoolean("status");
                            if (status) {
                                JSONArray jsonArray = response.getJSONArray("data");

                                for (int i = 0; i < jsonArray.length(); i++) {

                                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                                    String exam_name = jsonObject.getString("exam_name");
                                    String exam_id = jsonObject.getString("exam_id");
                                    //  Toast.makeText(context, "" + jsonObject.getString("exam_name"), Toast.LENGTH_SHORT).show();
                                    spin_exam_list.add(exam_name);
                                    spin_exam_list_id.add(exam_id);


                                }
                                sp_exam.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, spin_exam_list));

                            } else {
                                Toast.makeText(getActivity(), "" + response.getString("message"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {

                            e.printStackTrace();
                        }
                        // Toast.makeText(ChangePassword.this, ""+response.toString(), Toast.LENGTH_SHORT).show();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });


        requestQueue.add(jsonObjectRequest);

    }


    private void hitSubjectSpinApi(final String id_exam) {

        spin_sub_list.clear();
        spin_sub_list_id.clear();
        spin_sub_list.add("Select Subject");
        spin_sub_list_id.add("-1");
        // Toast.makeText(context, "hdjhdhfj", Toast.LENGTH_SHORT).show();
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        JSONObject jsonBody = new JSONObject();
        try {
            //input your API parameters
            jsonBody.put("category_id", id_exam);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        // Enter the correct url for your api service site
        // String url = getResources().getString(R.string.url);
        //  String URL = "http://13.233.162.24/nusys(UAT)/api/teacher/select_subject.php";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL_TEACHER + STUDENT_SUB, jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            //Toast.makeText(getActivity(), "true", Toast.LENGTH_SHORT).show();

                            Boolean status = response.getBoolean("status");
                            if (status) {
                                JSONArray jsonArray = response.getJSONArray("data");
                                //  Toast.makeText(getActivity(), "ccccc", Toast.LENGTH_SHORT).show();
                                for (int i = 0; i < jsonArray.length(); i++) {

                                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                                    String sub_id = jsonObject.getString("sub_id");
                                    String name = jsonObject.getString("name");
                                    // Toast.makeText(getActivity(), "fjhgjhg" + jsonObject.getString("name"), Toast.LENGTH_SHORT).show();
                                    spin_sub_list.add(name);
                                    spin_sub_list_id.add(sub_id);

                                }
                                sp_sub.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, spin_sub_list));

                            } else {


                                sp_sub.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, spin_sub_list));

//                                Toast.makeText(getActivity(), "" + response.getString("message"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                           /* spin_sub_list.add("Select Subject");
                            spin_sub_list_id.add("-1");*/
                            e.printStackTrace();
                        }
                        // Toast.makeText(ChangePassword.this, ""+response.toString(), Toast.LENGTH_SHORT).show();
                        //resultTextView.setText("String Response : "+ response.toString());
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                sp_sub.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, spin_sub_list));

                //  resultTextView.setText("Error getting response");
            }
        });


        requestQueue.add(jsonObjectRequest);

    }

    private void hitUnitSpinApi(String id_exam, String id_sub) {
        spin_unit_list.clear();
        spin_unit_list_id.clear();
        spin_unit_list.add("Select Unit");
        spin_unit_list_id.add("-1");

        // Toast.makeText(getActivity(), "hdjhdhfj", Toast.LENGTH_SHORT).show();
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        JSONObject jsonBody = new JSONObject();
        try {
            //input your API parameters
            jsonBody.put("exam_id", id_exam);
            jsonBody.put("subject_id", id_sub);

            //Toast.makeText(getActivity(), "exam" + id_exam, Toast.LENGTH_SHORT).show();
            // Toast.makeText(getActivity(), "subject" + id_sub, Toast.LENGTH_SHORT).show();

        } catch (JSONException e) {
            e.printStackTrace();
        }
        // Enter the correct url for your api service site
        // String url = getResources().getString(R.string.url);
        // String URL = "http://13.233.162.24/nusys(UAT)/api/student/select_unit.php";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL + STUDENT_UNIT, jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            //Toast.makeText(getActivity(), "true", Toast.LENGTH_SHORT).show();

                            Boolean status = response.getBoolean("status");
                            if (status) {
                                JSONArray jsonArray = response.getJSONArray("data");
                                //  Toast.makeText(getActivity(), "ccccc", Toast.LENGTH_SHORT).show();
                                for (int i = 0; i < jsonArray.length(); i++) {

                                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                                    String unit_id = jsonObject.getString("unit_id");
                                    String unit_name = jsonObject.getString("unit_name");
                                    // Toast.makeText(getActivity(), "fjhgjhg" + jsonObject.getString("name"), Toast.LENGTH_SHORT).show();
                                    spin_unit_list.add(unit_name);
                                    spin_unit_list_id.add(unit_id);

                                }
                                sp_unit.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, spin_unit_list));
                            } else {


                                sp_unit.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, spin_unit_list));
                                //Toast.makeText(getActivity(), "" + response.getString("message"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        // Toast.makeText(ChangePassword.this, ""+response.toString(), Toast.LENGTH_SHORT).show();
                        //resultTextView.setText("String Response : "+ response.toString());
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                sp_unit.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, spin_unit_list));
                //  resultTextView.setText("Error getting response");
            }
        });


        requestQueue.add(jsonObjectRequest);

    }

    private void hitChapSpinApi(String id_unit) {
        spin_chap_list.clear();
        spin_chap_list_id.clear();
        spin_chap_list.add("Select Chapter");
        spin_chap_list_id.add("-1");


        // Toast.makeText(getActivity(), "hdjhdhfj", Toast.LENGTH_SHORT).show();
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        JSONObject jsonBody = new JSONObject();
        try {

            jsonBody.put("unit_id", id_unit);


        } catch (JSONException e) {
            e.printStackTrace();
        }
        // Enter the correct url for your api service site
        // String url = getResources().getString(R.string.url);
        //  String URL = "http://13.233.162.24/nusys(UAT)/api/student/GetChapterData.php";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL + STUDENT_CHAP, jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            //  Toast.makeText(getActivity(), "true", Toast.LENGTH_SHORT).show();

                            Boolean status = response.getBoolean("status");
                            if (status) {
                                JSONArray jsonArray = response.getJSONArray("data");
                                //  Toast.makeText(getActivity(), "ccccc", Toast.LENGTH_SHORT).show();
                                for (int i = 0; i < jsonArray.length(); i++) {

                                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                                    String chap_id = jsonObject.getString("chapter_id");
                                    String chap_name = jsonObject.getString("chapter_name");
                                    //  Toast.makeText(getActivity(), "fjhgjhg" + jsonObject.getString("chapter_id"), Toast.LENGTH_SHORT).show();
                                    spin_chap_list.add(chap_name);
                                    spin_chap_list_id.add(chap_id);


                                }
                                sp_chap.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, spin_chap_list));
                            } else {
                                //Toast.makeText(getActivity(), "a" , Toast.LENGTH_SHORT).show();


                                sp_chap.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, spin_chap_list));
                                //Toast.makeText(getActivity(), "" + response.getString("message"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        // Toast.makeText(ChangePassword.this, ""+response.toString(), Toast.LENGTH_SHORT).show();
                        //resultTextView.setText("String Response : "+ response.toString());
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //  Toast.makeText(Courses.this, "error", Toast.LENGTH_SHORT).show();

                sp_chap.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, spin_chap_list));
                //  resultTextView.setText("Error getting response");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json; charset=UTF-8");
                params.put("Authorization", "Bearer " + sharedPreference_main.getToken());
                return params;
            }
        };


        requestQueue.add(jsonObjectRequest);

    }


}
