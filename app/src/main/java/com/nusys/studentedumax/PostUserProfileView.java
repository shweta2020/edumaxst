package com.nusys.studentedumax;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.nusys.studentedumax.adapter.AdapterProfilePost;
import com.nusys.studentedumax.commonModules.ExpandableTextView;
import com.nusys.studentedumax.commonModules.PaginationAdapterCallback;
import com.nusys.studentedumax.commonModules.PaginationScrollListener;
import com.nusys.studentedumax.commonModules.SharedPreference_main;
import com.nusys.studentedumax.model.ProfilePostModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeoutException;

import static com.nusys.studentedumax.Constants.BASE_URL_TEACHER;
import static com.nusys.studentedumax.Constants.GET_USER_PROFILE;
import static com.nusys.studentedumax.Constants.SINGLE_POST;

public class PostUserProfileView extends AppCompatActivity implements PaginationAdapterCallback, SwipeRefreshLayout.OnRefreshListener {
    RecyclerView rv_post_profile_list;
    AdapterProfilePost myAdapter;
    SharedPreference_main sharedPreference_main;
    LinearLayoutManager linearLayoutManager;
    private static int TOTAL_PAGES;
    ProgressBar progressBar;
    LinearLayout errorLayout;
    Button btnRetry;
    SwipeRefreshLayout swipeRefreshLayout;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int currentPage = 1;

    TextView user_name, user_email, tvToolbarHead;
    TextView member_since, user_following, user_follower, user_post, user_doubts;
    ImageView user_image;
    String i_name, i__uploader_id;

    LinearLayout llBackActivity;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_user_profile_view);
        sharedPreference_main = SharedPreference_main.getInstance(context);
        context = PostUserProfileView.this;
        tvToolbarHead = findViewById(R.id.tv_toolbar_head);
        llBackActivity = findViewById(R.id.ll_back_activity);
        tvToolbarHead.setText("Profile");


        llBackActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        user_name = findViewById(R.id.user_name_post_profile);
        user_email = findViewById(R.id.user_email_post_profile);
        user_image = findViewById(R.id.user_image_post_profile);
        user_post = findViewById(R.id.tv_user_post);
        user_follower = findViewById(R.id.tv_user_follower);
        user_following = findViewById(R.id.tv_user_following);
        member_since = findViewById(R.id.tv_member_since);
        user_doubts = findViewById(R.id.tv_user_doubts);


        Intent intent = getIntent();
        i_name = intent.getStringExtra("name");
        //  Toast.makeText(context, "bame"+intent.getStringExtra("name"), Toast.LENGTH_SHORT).show();
        i__uploader_id = intent.getStringExtra("id");
        // user_name.setText(i_name);

        viewProfile();

        progressBar = findViewById(R.id.main_progress);
        errorLayout = findViewById(R.id.error_layout);
        btnRetry = findViewById(R.id.error_btn_retry);
        rv_post_profile_list = findViewById(R.id.rv_post_profile_list_user);
        // txtError = findViewById(R.id.error_txt_cause);
        swipeRefreshLayout = findViewById(R.id.main_swiperefresh);
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rv_post_profile_list.setLayoutManager(linearLayoutManager);
        rv_post_profile_list.setItemAnimator(new DefaultItemAnimator());

        myAdapter = new AdapterProfilePost(context);
        rv_post_profile_list.setAdapter(myAdapter);


        rv_post_profile_list.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;

                loadNextPage();
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });


        //loadFirstPage();
        btnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadFirstPage();
            }
        });


        swipeRefreshLayout.setOnRefreshListener(this);


    }

    public void onRefresh() {
        // TODO: Check if data is stale.
        //  Execute network request if cache is expired; otherwise do not update data.
        myAdapter.clear();
        myAdapter.notifyDataSetChanged();
        progressBar.setVisibility(View.VISIBLE);
        isLastPage = false;
        loadFirstPage();
        swipeRefreshLayout.setRefreshing(false);
    }

    private void loadFirstPage() {
        currentPage = 1;
        //relative.setVisibility(View.VISIBLE);
        errorLayout.setVisibility(View.VISIBLE);
        //   frame_container.setVisibility(View.GONE);

        // To ensure list is visible when retry button in error view is clicked
        hideErrorView();

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        final JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("id", getIntent().getStringExtra("id"));
            jsonBody.put("page", currentPage);
            jsonBody.put("user_type", getIntent().getStringExtra("user_type"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        // Enter the correct url for your api service site
        // String url = getResources().getString(R.string.url);
        //String URL = "http://13.233.162.24/nusys(UAT)/api/teacher/getUserProfile.php";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL_TEACHER+GET_USER_PROFILE, jsonBody,
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("response", response.toString());
                        try {
                            myAdapter.clear();
                            myAdapter.notifyDataSetChanged();
                            Boolean status = response.getBoolean("status");
                            //Toast.makeText(Profile.this, "", Toast.LENGTH_SHORT).show();
                            // Toast.makeText(Profile.this, "abcd"+status, Toast.LENGTH_SHORT).show();
                            if (status) {

                                final List<ProfilePostModel> ab = new ArrayList<>();
                                //JSONObject jsonObj = response.getJSONObject("data");
                                JSONObject jsonObj = response.getJSONObject("data");
                                JSONArray jsonArray = jsonObj.getJSONArray("posts");
                                for (int i = 0; i < jsonArray.length(); i++) {

                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    ProfilePostModel entity = new ProfilePostModel();
                                    //  entity.setData(response.getString("namezz"));
                                    entity.setUploader_name(jsonObject.getString("uploader_name"));
                                    entity.setHeading(jsonObject.getString("heading"));
                                    entity.setContent(jsonObject.getString("content"));
                                    entity.setFilename(jsonObject.getString("filename"));
                                    entity.setTime(jsonObject.getString("time"));
                                    entity.setLikes(jsonObject.getString("likes"));
                                    entity.setLike_status(jsonObject.getString("like_status"));
                                    entity.setTotal_comments(jsonObject.getString("total_comments"));
                                    entity.setUploader_image(jsonObject.getString("uploader_image"));
                                    entity.setUploader_id(jsonObject.getString("uploader_id"));
                                    entity.setUser_type(jsonObject.getString("user_type"));
                                    entity.setPost_id(jsonObject.getString("post_id"));
                                    ab.add(entity);
                                }
                                //reprog.setVisibility(View.GONE);
                                //myAdapter = new PaginationAdapter(MainActivity.this, ab);
                                //rv_post_list.setAdapter(myAdapter);
                                if (ab != null) {

                                    TOTAL_PAGES = ab.size();
                                    Log.e("totalPage", String.valueOf(ab.size()));
                                    myAdapter.addAll(ab);
//                                    rv_post_list.setAdapter(myAdapter);

                                } else {

                                    Toast.makeText(context, "Something is wrong try again later", Toast.LENGTH_SHORT).show();
                                }
                                progressBar.setVisibility(View.GONE);
                                if (currentPage <= TOTAL_PAGES) myAdapter.addLoadingFooter();
                                else isLastPage = true;
                            } else {
                                Toast.makeText(context, "Something is wrong try again later", Toast.LENGTH_SHORT).show();

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        // Toast.makeText(SignUpStepTwo.this, "" + response.toString(), Toast.LENGTH_SHORT).show();
                        //resultTextView.setText("String Response : "+ response.toString());
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //  resultTextView.setText("Error getting response");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json; charset=UTF-8");
                params.put("Authorization", "Bearer " + sharedPreference_main.getToken());
                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);
    }


    @Override
    protected void onStart() {
        super.onStart();

        loadFirstPage();
//        onRefresh();
    }

    private void loadNextPage() {
        Log.d("page", "loadNextPage: " + currentPage);
        Log.e("total", String.valueOf(TOTAL_PAGES));
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                final JSONObject jsonBody = new JSONObject();
                try {
                    jsonBody.put("id", getIntent().getStringExtra("id"));
                    jsonBody.put("page", currentPage);
                    jsonBody.put("user_type", getIntent().getStringExtra("user_type"));


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                // Enter the correct url for your api service site
                // String url = getResources().getString(R.string.url);
               // String URL = "http://13.233.162.24/nusys(UAT)/api/teacher/getUserProfile.php";
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL_TEACHER+GET_USER_PROFILE, jsonBody,
                        new com.android.volley.Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                Log.e("response", response.toString());
                                try {
                                    myAdapter.removeLoadingFooter();
                                    isLoading = false;
                                    Boolean status = response.getBoolean("status");
                                    //Toast.makeText(Profile.this, "", Toast.LENGTH_SHORT).show();
                                    // Toast.makeText(Profile.this, "abcd"+status, Toast.LENGTH_SHORT).show();
                                    if (status) {
                                        final List<ProfilePostModel> ab = new ArrayList<>();
                                        //JSONObject jsonObj = response.getJSONObject("data");
                                        JSONObject jsonObj = response.getJSONObject("data");
                                        JSONArray jsonArray = jsonObj.getJSONArray("posts");
                                        for (int i = 0; i < jsonArray.length(); i++) {

                                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                                            ProfilePostModel entity = new ProfilePostModel();
                                            //  entity.setData(response.getString("namezz"));
                                            entity.setUploader_name(jsonObject.getString("uploader_name"));

                                            entity.setHeading(jsonObject.getString("heading"));
                                            entity.setContent(jsonObject.getString("content"));
                                            entity.setFilename(jsonObject.getString("filename"));
                                            entity.setTime(jsonObject.getString("time"));
                                            entity.setLikes(jsonObject.getString("likes"));
                                            entity.setLike_status(jsonObject.getString("like_status"));
                                            entity.setTotal_comments(jsonObject.getString("total_comments"));
                                            entity.setPost_id(jsonObject.getString("post_id"));
                                            entity.setUploader_id(jsonObject.getString("uploader_id"));
                                            entity.setUploader_image(jsonObject.getString("uploader_image"));
                                            entity.setUser_type(jsonObject.getString("user_type"));
                                            ab.add(entity);
                                        }
                                        //reprog.setVisibility(View.GONE);
                                        //myAdapter = new PaginationAdapter(MainActivity.this, ab);
                                        // rv_post_list.setAdapter(myAdapter);

                                        if (ab != null) {
                                            //TOTAL_PAGES = ab.size();
                                            myAdapter.addAll(ab);
                                            if (currentPage != TOTAL_PAGES)
                                                myAdapter.addLoadingFooter();
                                            else isLastPage = true;

                                        } else {
                                            isLastPage = true;
                                            //Toast.makeText(Home.this, "Something is wrong try again later", Toast.LENGTH_SHORT).show();

                                        }

                                    } else {
                                        isLastPage = true;
                                        //Toast.makeText(Home.this, "Something is wrong try again later", Toast.LENGTH_SHORT).show();

                                    }


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    isLastPage = true;
                                }

                                // Toast.makeText(SignUpStepTwo.this, "" + response.toString(), Toast.LENGTH_SHORT).show();
                                //resultTextView.setText("String Response : "+ response.toString());
                            }
                        }, new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        isLastPage = true;
                        //  resultTextView.setText("Error getting response");
                    }
                }) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("Content-Type", "application/json; charset=UTF-8");
                        params.put("Authorization", "Bearer " + sharedPreference_main.getToken());
                        return params;
                    }
                };
                requestQueue.add(jsonObjectRequest);

            }
        }, 2000);

    }


    @Override
    public void retryPageLoad() {
        loadNextPage();
    }


    /**
     * @param throwable required for {@link #fetchErrorMessage(Throwable)}
     * @return
     */
    private void showErrorView(Throwable throwable) {

        if (errorLayout.getVisibility() == View.GONE) {
            errorLayout.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);

            //txtError.setText(fetchErrorMessage(throwable));
        }
    }

    /**
     * @param throwable to identify the type of error
     * @return appropriate error message
     */
    private String fetchErrorMessage(Throwable throwable) {
        String errorMsg = getResources().getString(R.string.error_msg_unknown);

        if (!isNetworkConnected()) {
            errorMsg = getResources().getString(R.string.error_msg_no_internet);
        } else if (throwable instanceof TimeoutException) {
            errorMsg = getResources().getString(R.string.error_msg_timeout);
        }

        return errorMsg;
    }

    // Helpers -------------------------------------------------------------------------------------


    private void hideErrorView() {
        if (errorLayout.getVisibility() == View.VISIBLE) {
            errorLayout.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Remember to add android.permission.ACCESS_NETWORK_STATE permission.
     *
     * @return
     */
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }


    private void viewProfile() {
        // Toast.makeText(context, "gdgdkhgdfjkghdfjk", Toast.LENGTH_SHORT).show();
        int page = 1;
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        final JSONObject jsonBody = new JSONObject();
        try {

            jsonBody.put("id", getIntent().getStringExtra("id"));
            jsonBody.put("page", page);
            jsonBody.put("user_type", getIntent().getStringExtra("user_type"));

            //  Toast.makeText(context, ""+getIntent().getStringExtra("user_type"), Toast.LENGTH_SHORT).show();
            //   Toast.makeText(context, "id"+getIntent().getStringExtra("id"), Toast.LENGTH_SHORT).show();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        // Enter the correct url for your api service site
        // String url = getResources().getString(R.string.url);
        //String URL = "http://13.233.162.24/nusys(UAT)/api/teacher/getUserProfile.php";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL_TEACHER+GET_USER_PROFILE, jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Boolean status = response.getBoolean("status");
                            //Toast.makeText(Profile.this, "", Toast.LENGTH_SHORT).show();
                            //Toast.makeText(context, "abcd"+status, Toast.LENGTH_SHORT).show();
                            if (status) {
                                // Toast.makeText(context, "aaaaaaaaaad", Toast.LENGTH_SHORT).show();
                                JSONObject jsonObject = response.getJSONObject("data");
                                //   user_name.setText(jsonObject.getString("name"));
                                //  user_email.setText(jsonObject.getString("email"));
                                //  user_post.setText(jsonObject.getString("post_count"));
                                //Toast.makeText(context, "image"+jsonObject.getString("image"), Toast.LENGTH_SHORT).show();
                                // Toast.makeText(context, "name"+jsonObject.getString("name"), Toast.LENGTH_SHORT).show();


                                Glide.with(getApplicationContext())
                                        .load(jsonObject.getString("image"))
                                        .placeholder(R.drawable.user_icon)
                                        .into(user_image);
                                user_name.setText(jsonObject.getString("name"));
                                user_email.setText(jsonObject.getString("email"));
                                user_post.setText(jsonObject.getString("post_count"));
                                user_follower.setText(jsonObject.getString("followers_count"));
                                user_following.setText(jsonObject.getString("following_count"));
                                member_since.setText(jsonObject.getString("member_since") + " Member Since");
                                user_doubts.setText(jsonObject.getString("doubts_count") + " Doubts");


                            } else {
                                // Toast.makeText(context, "bbbbbbbbbbbbbbbb", Toast.LENGTH_SHORT).show();

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        // Toast.makeText(SignUpStepTwo.this, "" + response.toString(), Toast.LENGTH_SHORT).show();
                        //resultTextView.setText("String Response : "+ response.toString());
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //  resultTextView.setText("Error getting response");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json; charset=UTF-8");
                params.put("Authorization", "Bearer " + sharedPreference_main.getToken());
                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);


    }
}
