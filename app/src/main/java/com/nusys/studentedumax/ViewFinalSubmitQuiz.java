package com.nusys.studentedumax;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nusys.studentedumax.commonModules.SharedPreference_main;
import com.nusys.studentedumax.model.ResponseQuesCount;
import com.nusys.studentedumax.model.ResponseQuizSubmit;
import com.nusys.studentedumax.model.ResponseTestStart;
import com.nusys.studentedumax.retrofit.ApiClient;
import com.nusys.studentedumax.retrofit.ServiceInterface;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;

import static com.nusys.studentedumax.Constants.Content_Type;

public class ViewFinalSubmitQuiz extends AppCompatActivity {
    SharedPreference_main sharedPreference_main;
    Context context;
    TextView tvReview, tvTotalQuestions, tvAns, tvUnans;
    TextView tv_toolbar_head1, tvSubmitQuiz;
    LinearLayout ll_back_activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_final_submit_quiz);

        init();

    }

    public void init() {
        sharedPreference_main = SharedPreference_main.getInstance(context);
        context = ViewFinalSubmitQuiz.this;
        tv_toolbar_head1 = findViewById(R.id.tv_toolbar_head);
        ll_back_activity = findViewById(R.id.ll_back_activity);
        tv_toolbar_head1.setText("Final Submit");

        ll_back_activity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        tvTotalQuestions = findViewById(R.id.tv_number_of_questions);
        tvAns = findViewById(R.id.tv_ans);
        tvUnans = findViewById(R.id.tv_unans);
        tvReview = findViewById(R.id.tv_review);
        tvSubmitQuiz = findViewById(R.id.tv_final_submit_quiz);

        fetch_count();
        tvReview.setText(getIntent().getStringExtra("reviewCount"));

        /* for final submit quiz
         * Created Date: 18-06-2020
         */
        tvSubmitQuiz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                submit_final_quiz();
            }
        });

    }

    /* for fetching different counts
     * Created Date: 15-06-2020
     */
    private void fetch_count() {

        HashMap<String, String> map = new HashMap<>();
        //map.put("test_id", "2");
        map.put("student_id", sharedPreference_main.getUserId());
        map.put("exam_id", getIntent().getStringExtra("examId"));


        // Toast.makeText(context, "" + getIntent().getStringExtra("testId"), Toast.LENGTH_SHORT).show();
        //   if (NetworkUtils.isConnected(getActivity())) {
        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<ResponseQuesCount> call = serviceInterface.fect_ques_count("Bearer " + sharedPreference_main.getToken(), Content_Type, map);
        call.enqueue(new Callback<ResponseQuesCount>() {
            @Override
            public void onResponse(Call<ResponseQuesCount> call, retrofit2.Response<ResponseQuesCount> response) {

                if (response.isSuccessful()) {

                    ResponseQuesCount bean = response.body();

                    // Toast.makeText(GroupActivity.this, "sucess", Toast.LENGTH_SHORT).show();
                    if (bean.getStatus()) {
                        ResponseQuesCount view = response.body();

                        // tv_timer.setText(bean.getData().getStartTime());
                        tvTotalQuestions.setText(bean.getData().get(0).getAllQuestion().toString());
                        tvUnans.setText(bean.getData().get(0).getUnansweredQuestion().toString());
                        tvAns.setText(bean.getData().get(0).getAnsweredQuestion().toString());
                        Log.e("test_response", view.toString());

                    } else {
                        Toast.makeText(context, bean.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                } else {

                    Toast.makeText(context, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseQuesCount> call, Throwable t) {
                Log.e("error", t.getMessage());
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    /* for final submit quiz
     * Created Date: 18-06-2020
     * Updated Date:26-06-2020(change intent home to practice test)
     */
    private void submit_final_quiz() {

        HashMap<String, String> map = new HashMap<>();

        map.put("student_id", sharedPreference_main.getUserId());
        map.put("exam_id", getIntent().getStringExtra("examId"));
        map.put("q_id", "");


        // Toast.makeText(context, "" + getIntent().getStringExtra("testId"), Toast.LENGTH_SHORT).show();
        //   if (NetworkUtils.isConnected(getActivity())) {
        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<ResponseQuizSubmit> call = serviceInterface.final_quiz_submit("Bearer " + sharedPreference_main.getToken(), Content_Type, map);
        call.enqueue(new Callback<ResponseQuizSubmit>() {
            @Override
            public void onResponse(Call<ResponseQuizSubmit> call, retrofit2.Response<ResponseQuizSubmit> response) {

                if (response.isSuccessful()) {

                    ResponseQuizSubmit bean = response.body();

                    // Toast.makeText(GroupActivity.this, "sucess", Toast.LENGTH_SHORT).show();
                    if (bean.getStatus()) {
                        ResponseQuizSubmit view = response.body();

                        Toast.makeText(context, bean.getMessage(), Toast.LENGTH_SHORT).show();
                        if (sharedPreference_main.getTest_type().equals("exam")){
                            Intent i = new Intent(context, ExamTest.class);
                            i.putExtra("test_type", "Exam");
                            startActivity(i);
                        }else if (sharedPreference_main.getTest_type().equals("practice")){
                            Intent i = new Intent(context, PracticeTest.class);
                            i.putExtra("test_type", "Practice");
                            startActivity(i);
                        }
                    /*    Intent i = new Intent(context, PracticeTest.class);
                        i.putExtra("test_type", "Practice");
                        startActivity(i);*/


                    } else {
                        Toast.makeText(context, bean.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                } else {

                    Toast.makeText(context, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseQuizSubmit> call, Throwable t) {
                Log.e("error", t.getMessage());
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
