package com.nusys.studentedumax.adapter;

import android.content.Context;

import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.nusys.studentedumax.ChangePassword;
import com.nusys.studentedumax.CommentHomePost;
import com.nusys.studentedumax.PostUserProfileView;
import com.nusys.studentedumax.Profile;
import com.nusys.studentedumax.R;
import com.nusys.studentedumax.ViewIndividualPostDetails;
import com.nusys.studentedumax.commonModules.ExpandableTextView;
import com.nusys.studentedumax.commonModules.PaginationAdapterCallback;
import com.nusys.studentedumax.commonModules.SharedPreference_main;
import com.nusys.studentedumax.model.HomePostModel;
import com.nusys.studentedumax.model.LikeModel;
import com.nusys.studentedumax.model.ProfilePostModel;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static com.nusys.studentedumax.Constants.BASE_URL_TEACHER;
import static com.nusys.studentedumax.Constants.MAKE_LIKE;


public class AdapterProfilePost extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int ITEM = 0;
    private static final int LOADING = 1;

    private List<ProfilePostModel> post_list;
    private Context context;

    private boolean isLoadingAdded = false;
    private boolean retryPageLoad = false;

    private PaginationAdapterCallback mCallback;

    private String errorMsg;
    SharedPreference_main sharedPreference_main;


    public AdapterProfilePost(Context context) {
        this.context = context;
        this.mCallback = (PaginationAdapterCallback) context;
        post_list = new ArrayList<>();
        sharedPreference_main = SharedPreference_main.getInstance(context);
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case ITEM:
                View viewItem = inflater.inflate(R.layout.post_adapter, parent, false);
                viewHolder = new ItemVH(viewItem);
                break;
            case LOADING:
                View viewLoading = inflater.inflate(R.layout.item_progress, parent, false);
                viewHolder = new LoadingVH(viewLoading);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, int position) {
        final ProfilePostModel result = post_list.get(position); // Movie

        switch (getItemViewType(position)) {


            case ITEM:

                ///SARE SET VALUES
                final ItemVH holder1 = (ItemVH) holder;
                // ProfilePostModel profilePostM = question_list.get(position);


                holder1.user_name.setText(result.getUploader_name());
                holder1.post_time.setText(result.getTime());
                holder1.tv_heading.setText(result.getHeading());
                holder1.expandableText.setText(result.getContent());
                holder1.tv_likes.setText(result.getLikes());
                holder1.tv_comment.setText(result.getTotal_comments() + " COMMENT");
                Glide.with(context).load(result.getUploader_image()).into(holder1.user_profile);
                Glide.with(context).load(result.getFilename()).into(holder1.feed_image);

                holder1.linear_options.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, ViewIndividualPostDetails.class);
                        intent.putExtra("postId", result.getPost_id());
                        context.startActivity(intent);
                    }
                });


                if (Objects.equals(result.getLike_status(), "false")) {
                    //Toast.makeText(context, "", Toast.LENGTH_SHORT).show();
                    holder1.img_unlike.setVisibility(View.VISIBLE);
                    holder1.img_like.setVisibility(View.GONE);
                } else {
                    holder1.img_unlike.setVisibility(View.GONE);
                    holder1.img_like.setVisibility(View.VISIBLE);
                }

                holder1.img_unlike.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        doLike(holder1.img_like, holder1.img_unlike, holder1.tv_likes, result.getPost_id());
                    }
                });

                holder1.img_like.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        doLike(holder1.img_like, holder1.img_unlike, holder1.tv_likes, result.getPost_id());
                    }
                });

                holder1.tv_comment.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent in = new Intent(context, CommentHomePost.class);

                        in.putExtra("postId", result.getPost_id());
                        context.startActivity(in);
                        // Intent i= new Intent(context, CommentHomePost.class);

                    }
                });


                holder1.expandableText.setAnimationDuration(750L);
                holder1.expandableText.setInterpolator(new OvershootInterpolator());
                holder1.expandableText.setExpandInterpolator(new OvershootInterpolator());
                holder1.expandableText.setCollapseInterpolator(new OvershootInterpolator());
                holder1.tv_viewmore.setOnClickListener(new View.OnClickListener() {
                    @SuppressWarnings("ConstantConditions")
                    @Override
                    public void onClick(final View v) {
                        holder1.tv_viewmore.setText(holder1.expandableText.isExpanded() ? R.string.expand : R.string.collapse);
                        holder1.expandableText.toggle();
                    }
                });

                holder1.tv_viewmore.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View v) {
                        if (holder1.expandableText.isExpanded()) {
                            holder1.expandableText.collapse();
                            holder1.tv_viewmore.setText(R.string.expand);
                        } else {
                            holder1.expandableText.expand();
                            holder1.tv_viewmore.setText(R.string.collapse);
                        }
                    }
                });


                holder1.expandableText.addOnExpandListener(new ExpandableTextView.OnExpandListener() {
                    @Override
                    public void onExpand(@NonNull final ExpandableTextView view) {
                        Log.d("texzt", "ExpandableTextView expanded");
                    }

                    @Override
                    public void onCollapse(@NonNull final ExpandableTextView view) {
                        Log.d("texzt", "ExpandableTextView collapsed");
                    }
                });

                break;

            case LOADING:
                LoadingVH loadingVH = (LoadingVH) holder;

                if (retryPageLoad) {
                    loadingVH.mErrorLayout.setVisibility(View.VISIBLE);
                    loadingVH.mProgressBar.setVisibility(View.GONE);

                    loadingVH.mErrorTxt.setText(
                            errorMsg != null ?
                                    errorMsg :
                                    context.getString(R.string.error_msg_unknown));

                } else {
                    loadingVH.mErrorLayout.setVisibility(View.GONE);
                    loadingVH.mProgressBar.setVisibility(View.VISIBLE);
                }
                break;
        }
    }


    private void doLike(final ImageView like, final ImageView unlike, final TextView tv_likes, String post_id) {


        RequestQueue requestQueue = Volley.newRequestQueue(context.getApplicationContext());
        JSONObject jsonBody = new JSONObject();
        try {
            //input your API parameters
            jsonBody.put("post_id", post_id);
            jsonBody.put("liked_user_id", sharedPreference_main.getUserId());
            jsonBody.put("liker_type", sharedPreference_main.getUserType());


        } catch (JSONException e) {
            e.printStackTrace();
        }
        // Enter the correct url for your api service site
        // String url = getResources().getString(R.string.url);
      //  String URL = "http://13.233.162.24/nusys(UAT)/api/teacher/make_like.php";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL_TEACHER+MAKE_LIKE, jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            // Toast.makeText(ChangePassword.this, "true", Toast.LENGTH_SHORT).show();

                            Boolean status = response.getBoolean("status");
                            String key = response.getString("key");
                            String count = response.getString("count");
                            if (status) {

                                if (key.equals("UnLike")) {

                                    like.setVisibility(View.GONE);
                                    unlike.setVisibility(View.VISIBLE);
                                    tv_likes.setText(count);
                                } else {
                                    like.setVisibility(View.VISIBLE);
                                    unlike.setVisibility(View.GONE);
                                    tv_likes.setText(count);
                                }
                            } else {
                                Toast.makeText(context, "" + response.getString("message"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        // Toast.makeText(ChangePassword.this, ""+response.toString(), Toast.LENGTH_SHORT).show();
                        //resultTextView.setText("String Response : "+ response.toString());
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //  resultTextView.setText("Error getting response");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json; charset=UTF-8");
                params.put("Authorization", "Bearer " + sharedPreference_main.getToken());
                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);


    }


    @Override
    public int getItemCount() {

        return post_list == null ? 0 : post_list.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return ITEM;
        } else {
            return (position == post_list.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
        }
    }


    public void add(ProfilePostModel r) {
        post_list.add(r);
        notifyItemInserted(post_list.size() - 1);
    }


    public void addAll(List<ProfilePostModel> moveResults) {
        for (ProfilePostModel result : moveResults) {
            add(result);
        }
    }


    private void remove(ProfilePostModel r) {
        int position = post_list.indexOf(r);
        if (position > -1) {
            post_list.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        isLoadingAdded = false;
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }


    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new ProfilePostModel());
    }


    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = post_list.size() - 1;
        ProfilePostModel result = getItem(position);

        if (result != null) {
            post_list.remove(position);
            notifyItemRemoved(position);
        }
    }


    public ProfilePostModel getItem(int position) {
        return post_list.get(position);
    }


    public void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(post_list.size() - 1);

        if (errorMsg != null) this.errorMsg = errorMsg;
    }

    /**
     * Main list's content ViewHolder
     */
    protected class ItemVH extends RecyclerView.ViewHolder {
        private TextView user_name, post_time, tv_heading, tv_likes, tv_comment, tv_viewmore;
        private ImageView user_profile, feed_image, img_like, img_unlike;
        ExpandableTextView expandableText;
        LinearLayout linear_options;


        public ItemVH(View itemView) {
            super(itemView);
            user_profile = itemView.findViewById(R.id.uploader_img_hpost);
            user_name = itemView.findViewById(R.id.user_name_hpost);
            post_time = itemView.findViewById(R.id.post_time_hpost);
            tv_heading = itemView.findViewById(R.id.tv_heading_hpost);
            feed_image = itemView.findViewById(R.id.feed_image_hpost);
            expandableText = itemView.findViewById(R.id.tv_content_hpost);
            tv_viewmore = itemView.findViewById(R.id.tv_viewmore_hpost);
            tv_likes = itemView.findViewById(R.id.tv_likes_hpost);
            tv_comment = itemView.findViewById(R.id.tv_comment_hpost);
            img_like = itemView.findViewById(R.id.img_like_hpost);
            img_unlike = itemView.findViewById(R.id.img_unlike_hpost);
            linear_options = itemView.findViewById(R.id.linear_options_hpost);

        }
    }


    protected class LoadingVH extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ProgressBar mProgressBar;
        private ImageButton mRetryBtn;
        private TextView mErrorTxt;
        private LinearLayout mErrorLayout;

        public LoadingVH(View itemView) {
            super(itemView);

            mProgressBar = itemView.findViewById(R.id.loadmore_progress);
            mRetryBtn = itemView.findViewById(R.id.loadmore_retry);
            mErrorTxt = itemView.findViewById(R.id.loadmore_errortxt);
            mErrorLayout = itemView.findViewById(R.id.loadmore_errorlayout);

            mRetryBtn.setOnClickListener(this);
            mErrorLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.loadmore_retry:
                case R.id.loadmore_errorlayout:

                    showRetry(false, null);
                    mCallback.retryPageLoad();

                    break;
            }
        }
    }

}
