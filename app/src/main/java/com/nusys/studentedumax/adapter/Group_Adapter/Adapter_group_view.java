package com.nusys.studentedumax.adapter.Group_Adapter;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.nusys.studentedumax.Group.GroupActivity;
import com.nusys.studentedumax.Home;
import com.nusys.studentedumax.R;
import com.nusys.studentedumax.Signup;
import com.nusys.studentedumax.UpdateProfile;
import com.nusys.studentedumax.ViewGroupActivity;
import com.nusys.studentedumax.commonModules.SharedPreference_main;
import com.nusys.studentedumax.interfaces.GetGroupIds;
import com.nusys.studentedumax.model.ResponeDeleteGroup;
import com.nusys.studentedumax.model.ResponeUpdateGroupName;
import com.nusys.studentedumax.model.ResponeViewGroup;
import com.nusys.studentedumax.retrofit.ApiClient;
import com.nusys.studentedumax.retrofit.ServiceInterface;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

import static android.view.View.GONE;
import static androidx.core.content.FileProvider.getUriForFile;
import static com.nusys.studentedumax.Constants.CAMERA_REQUEST_CODE;
import static com.nusys.studentedumax.Constants.Content_Type;
import static com.nusys.studentedumax.Constants.GALLERY_REQUEST_CODE;
import static com.nusys.studentedumax.Constants.getCapsSentences;

public class Adapter_group_view extends RecyclerView.Adapter<Adapter_group_view.myholder> {
    SharedPreference_main sharedPreference_main;
    Context context;
    List<ResponeViewGroup.Datum> data;
    public String id;
    public String created_id;
    GetGroupIds getGroupIds;
    String capText;

    public Adapter_group_view(Context context, List<ResponeViewGroup.Datum> data, GetGroupIds getGroupIds) {
        this.context = context;
        this.data = data;
        this.getGroupIds = getGroupIds;
    }

    @NonNull
    @Override
    public myholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.show_groups, parent, false);
        sharedPreference_main = SharedPreference_main.getInstance(context);
        return new myholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final myholder holder, final int position) {
      /*  if (data.get(position).getGroupName()){
            capText="";
        }else{
            capText=getCapsSentences(data.get(position).getGroupName());
        }
*/
        holder.tx_group_name.setText(data.get(position).getGroupName());
        //holder.tx_strength.setText(data.get(position).getStrength());

        holder.tx_strength.setText(data.get(position).getStrength() + " Members");
        holder.ll_view_grp.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                sharedPreference_main.setCreatedId(data.get(position).getCreatedId());
                Intent i = new Intent(context, ViewGroupActivity.class);
                i.putExtra("group_id", data.get(position).getGroupId());
                i.putExtra("image", data.get(position).getImage());
                i.putExtra("name", data.get(position).getGroupName());
                i.putExtra("group_type", data.get(position).getGroupType());
                i.putExtra("class_id", data.get(position).getClassId());
                i.putExtra("batch_id", data.get(position).getBatchId());
                context.startActivity(i);


            }
        });
        holder.info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PopupMenu popup = new PopupMenu(context, v);

                popup.getMenuInflater().inflate(R.menu.poupup_menu, popup.getMenu());

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        if (item.getTitle().equals("Delete")) {
                            id = data.get(position).getGroupId();

                            final Dialog dialog = new Dialog(context);
                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            dialog.setContentView(R.layout.delete);
                            dialog.getWindow().getDecorView().setTop(100);
                            dialog.getWindow().getDecorView().setLeft(100);
                            dialog.show();

                            Button bt_delete = dialog.findViewById(R.id.delete);
                            Button bt_cancel = dialog.findViewById(R.id.cancel);
                            bt_delete.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    delete_group();
                                    dialog.dismiss();

                                }
                            });
                            bt_cancel.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    dialog.dismiss();
                                }
                            });
                            dialog.show();
                        } else if (item.getTitle().equals("Update")) {

                            getGroupIds.getGroupIds(data.get(position).getGroupId(), data.get(position).getGroupName(), data.get(position).getImage());

                        }
                        return true;
                    }
                });

                popup.show();

            }
        });


        Glide.with(context)
                .load(data.get(position).getImage())
                .placeholder(R.drawable.user_icon)
                .into(holder.im_image);


        if (data.get(position).getGroupBy().equals(0)) {
            holder.info.setVisibility(View.GONE);
            holder.tx_creater.setText(">>Admin");

        } else if (data.get(position).getGroupBy().equals(1)) {
            holder.info.setVisibility(View.GONE);
            holder.tx_creater.setText(">>Teacher");

        } else if (data.get(position).getGroupBy().equals(2)) {

            created_id = data.get(position).getCreatedId();
            id = sharedPreference_main.getUserId();
            if (created_id.equals(id)) {
                holder.info.setVisibility(View.VISIBLE);
                holder.tx_creater.setText(">>By You");

            } else {
                holder.info.setVisibility(View.GONE);
                holder.tx_creater.setText(">>Student");
            }

        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class myholder extends RecyclerView.ViewHolder {
        TextView tx_creater, tx_group_name, tx_strength;
        ImageView im_image, info;
        LinearLayout ll_view_grp;

        public myholder(@NonNull View itemView) {
            super(itemView);
            info = itemView.findViewById(R.id.info);
            tx_group_name = itemView.findViewById(R.id.txt_group_name);
            tx_strength = itemView.findViewById(R.id.txt_strength);
            tx_creater = itemView.findViewById(R.id.txt_creater);
            im_image = itemView.findViewById(R.id.img_icon);
            ll_view_grp = itemView.findViewById(R.id.ll_view);
        }
    }


    private void delete_group() {

        HashMap<String, String> map = new HashMap<>();

        map.put("group_id", id);

        //   if (NetworkUtils.isConnected(getActivity())) {
        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<ResponeDeleteGroup> call = serviceInterface.delete_group("Bearer " + sharedPreference_main.getToken(), Content_Type, map);
        call.enqueue(new Callback<ResponeDeleteGroup>() {


            @Override
            public void onResponse(Call<ResponeDeleteGroup> call, retrofit2.Response<ResponeDeleteGroup> response) {

                if (response.isSuccessful()) {

                    ResponeDeleteGroup bean = response.body();
                  //  Toast.makeText(context, bean.getMessage(), Toast.LENGTH_LONG).show();
                    // Toast.makeText(context, "sucess", Toast.LENGTH_SHORT).show();
                    if (bean.getStatus()) {
                        ResponeDeleteGroup view = response.body();
                        Intent intent = new Intent(context, Home.class);
                        context.startActivity(intent);


                        Log.e("Group_response", view.toString());

                    } else {
                        Toast.makeText(context, bean.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {

                    Toast.makeText(context, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponeDeleteGroup> call, Throwable t) {
                Log.e("error", t.getMessage());
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


}
