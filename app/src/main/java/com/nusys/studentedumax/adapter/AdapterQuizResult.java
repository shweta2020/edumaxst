package com.nusys.studentedumax.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nusys.studentedumax.R;
import com.nusys.studentedumax.interfaces.GetQuesIds;
import com.nusys.studentedumax.model.ResponseQuestionList2Method;
import com.nusys.studentedumax.model.ResponseQuizResult;

import java.util.List;

public class AdapterQuizResult extends RecyclerView.Adapter<AdapterQuizResult.myholder> {
    String batchId;
    Context context;
    List<ResponseQuizResult.Datum.Question> data;
    String capText;


    public AdapterQuizResult(Context context, List<ResponseQuizResult.Datum.Question> data) {
        this.context = context;
        this.data = data;

    }

    @NonNull
    @Override
    public AdapterQuizResult.myholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.multiple_choice_view_adapter, parent, false);
        return new AdapterQuizResult.myholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterQuizResult.myholder holder, final int i) {
        int h = holder.getAdapterPosition() + 1;

        holder.tvOptionNum.setText("" + h);
        if (data.get(i).getAnswerStatus().equals("Wrong")){
            holder.tvOptionNum.setBackgroundResource(R.drawable.circle_red);
        }else   if (data.get(i).getAnswerStatus().equals("Right")) {
            holder.tvOptionNum.setBackgroundResource(R.drawable.circle_green);

        }else{

        }
        }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class myholder extends RecyclerView.ViewHolder {
        TextView tvOptionNum;


        public myholder(@NonNull View itemView) {
            super(itemView);
            tvOptionNum = itemView.findViewById(R.id.tv_option_num);


        }
    }
}