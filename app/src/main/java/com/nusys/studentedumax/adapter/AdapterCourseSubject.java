package com.nusys.studentedumax.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nusys.studentedumax.CourseSubject;
import com.nusys.studentedumax.R;
import com.nusys.studentedumax.ViewGroupActivity;
import com.nusys.studentedumax.commonModules.CircularTextView;

import com.nusys.studentedumax.interfaces.GetSubjectIds;
import com.nusys.studentedumax.model.GetSubjectResponseModel;

import java.util.List;

public class AdapterCourseSubject extends RecyclerView.Adapter<AdapterCourseSubject.myholder> {
    String batchId;
    Context context;
    List<GetSubjectResponseModel.Datum> data;
    GetSubjectIds getSubjectIds;

    public AdapterCourseSubject(Context context, List<GetSubjectResponseModel.Datum> data, GetSubjectIds getSubjectIds) {
        this.context = context;
        this.data = data;
        this.getSubjectIds = getSubjectIds;
    }

    @NonNull
    @Override
    public AdapterCourseSubject.myholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.course_sub_list, parent, false);
        return new AdapterCourseSubject.myholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterCourseSubject.myholder holder, final int position) {

        holder.circleTextView.setCustomText(data.get(position).getName().substring(0, 1));
        holder.circleTextView.setSolidColor(position);
        holder.circleTextView.setTextColor(Color.WHITE);
        holder.circleTextView.setCustomTextSize(18F);
        holder.tvExamName.setText(data.get(position).getName());
        holder.llClickable.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                getSubjectIds.getSubId(data.get(position).getSubId());
            /*    Intent i = new Intent(context, CourseSubject.class);
                i.putExtra("sub_id", data.get(position).getSubId());
                context.startActivity(i);*/
            }
        });


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class myholder extends RecyclerView.ViewHolder {
        TextView tvExamName;
        CircularTextView circleTextView;
        LinearLayout llClickable;
        public myholder(@NonNull View itemView) {
            super(itemView);
            tvExamName = itemView.findViewById(R.id.tv_exam_name);
            llClickable = itemView.findViewById(R.id.ll_clickable);
            circleTextView = itemView.findViewById(R.id.circularTextView);

        }
    }
}