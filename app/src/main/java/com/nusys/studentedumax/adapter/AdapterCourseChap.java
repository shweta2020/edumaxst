package com.nusys.studentedumax.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nusys.studentedumax.R;
import com.nusys.studentedumax.commonModules.CircularTextView;
import com.nusys.studentedumax.interfaces.GetChapIds;
import com.nusys.studentedumax.interfaces.GetUnitIds;
import com.nusys.studentedumax.model.GetChapResponseModel;
import com.nusys.studentedumax.model.GetUnitResponseModel;

import java.util.List;

public class AdapterCourseChap extends RecyclerView.Adapter<AdapterCourseChap.myholder> {
    String batchId;
    Context context;
    List<GetChapResponseModel.Datum> data;
    GetChapIds getChapIds;


    public AdapterCourseChap(Context context, List<GetChapResponseModel.Datum> data, GetChapIds getChapIds) {
        this.context = context;
        this.data = data;
        this.getChapIds = getChapIds;


    }

    @NonNull
    @Override
    public AdapterCourseChap.myholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.chapter_adapter, parent, false);
        return new AdapterCourseChap.myholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterCourseChap.myholder holder, final int position) {

        holder.tvChapName.setText(data.get(position).getChapterName());

        holder.tvChapName.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                getChapIds.getChapId(data.get(position).getChapterId());
            /*    Intent i = new Intent(context, CourseSubject.class);
                i.putExtra("sub_id", data.get(position).getSubId());
                context.startActivity(i);*/
            }
        });


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class myholder extends RecyclerView.ViewHolder {
        TextView tvChapName;

        public myholder(@NonNull View itemView) {
            super(itemView);
            tvChapName = itemView.findViewById(R.id.tv_chap_name);


        }
    }
}