package com.nusys.studentedumax.adapter;

import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.nusys.studentedumax.R;
import com.nusys.studentedumax.commonModules.ExpandableTextView;
import com.nusys.studentedumax.commonModules.SharedPreference_main;
import com.nusys.studentedumax.model.ResponseQuestionList;
import java.util.ArrayList;
import java.util.List;


public class AdapterQuestionListReview extends RecyclerView.Adapter<AdapterQuestionListReview.myholder> {
    @NonNull
    Context context;
    ArrayList<ResponseQuestionList.Datum> data;
    String id;
    private Object List;
    String teacherId = "";
    private ArrayList<String> TeachersIds = new ArrayList<String>();
    SharedPreference_main sharedPreference_main;

    public AdapterQuestionListReview(@NonNull Context context, List<ResponseQuestionList.Datum> data) {
        this.context = context;
        this.data = (ArrayList<ResponseQuestionList.Datum>) data;
        sharedPreference_main = SharedPreference_main.getInstance(context);

    }


    public AdapterQuestionListReview.myholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.question_list_adapter, parent, false);
        //sharedPreference_main = SharedPreference_main.getInstance(context);
        return new AdapterQuestionListReview.myholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final AdapterQuestionListReview.myholder holder, final int position) {

        holder.recycle_options.setAdapter(new AdapterQuizOptionReview(context, data.get(position).getOptions()));
        holder.tv_ques_num.setText("Q" + data.get(position).getId());
        holder.tvYourMark.setText(data.get(position).getYourMark());
       // holder.tvQuesTime.setText(data.get(position).getTime());
        holder.tv_question.setText(Html.fromHtml(data.get(position).getQuestion()).toString());

        holder.expandableText.setText(Html.fromHtml(data.get(position).getSolution()).toString());
        holder.expandableText.setAnimationDuration(750L);
        holder.expandableText.setInterpolator(new OvershootInterpolator());
        holder.expandableText.setExpandInterpolator(new OvershootInterpolator());
        holder.expandableText.setCollapseInterpolator(new OvershootInterpolator());
        holder.tvViewmoreSolution.setOnClickListener(new View.OnClickListener() {
            @SuppressWarnings("ConstantConditions")
            @Override
            public void onClick(final View v) {
                holder.tvViewmoreSolution.setText(holder.expandableText.isExpanded() ? R.string.expand1 : R.string.collapse);
                holder.expandableText.toggle();
            }
        });


        holder.tvViewmoreSolution.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                if (holder.expandableText.isExpanded()) {
                    holder.expandableText.collapse();
                    holder.tvViewmoreSolution.setText(R.string.expand1);
                } else {
                    holder.expandableText.expand();
                    holder.tvViewmoreSolution.setText(R.string.collapse);
                }
            }
        });


        holder.expandableText.addOnExpandListener(new ExpandableTextView.OnExpandListener() {
            @Override
            public void onExpand(@NonNull final ExpandableTextView view) {
                Log.d("texzt", "ExpandableTextView expanded");
            }

            @Override
            public void onCollapse(@NonNull final ExpandableTextView view) {
                Log.d("texzt", "ExpandableTextView collapsed");
            }
        });
        //  holder.tv_question.setText(Html.fromHtml(Html.fromHtml(data.get(position).getQuestion()).toString()));


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class myholder extends RecyclerView.ViewHolder {
        TextView tv_ques_num, tv_question, tvQuesTime,tvYourMark,tvViewmoreSolution;
        ExpandableTextView expandableText;
        RecyclerView recycle_options;

        public myholder(@NonNull View itemView) {
            super(itemView);
            tv_ques_num = itemView.findViewById(R.id.tv_ques_num);
            tv_question = itemView.findViewById(R.id.tv_questions);
            recycle_options = itemView.findViewById(R.id.recycle_options);
            tvYourMark = itemView.findViewById(R.id.tv_your_mark);
            tvQuesTime = itemView.findViewById(R.id.tv_ques_time);
            expandableText = itemView.findViewById(R.id.etv_solution);
            tvViewmoreSolution = itemView.findViewById(R.id.tv_viewmore_solution);


        }

    }
}
