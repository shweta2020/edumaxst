package com.nusys.studentedumax.adapter;

import android.content.Context;

import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.nusys.studentedumax.ChangePassword;
import com.nusys.studentedumax.CommentHomePost;
import com.nusys.studentedumax.PostUserProfileView;
import com.nusys.studentedumax.Profile;
import com.nusys.studentedumax.R;
import com.nusys.studentedumax.ViewIndividualPostDetails;
import com.nusys.studentedumax.commonModules.ExpandableTextView;
import com.nusys.studentedumax.commonModules.PaginationAdapterCallback;
import com.nusys.studentedumax.commonModules.SharedPreference_main;
import com.nusys.studentedumax.model.FollowingModel;
import com.nusys.studentedumax.model.HomePostModel;
import com.nusys.studentedumax.model.LikeModel;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;


public class AdapterFollowing extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    // View Types
    private static final int ITEM = 0;
    private static final int LOADING = 1;

    private List<FollowingModel> post_list;
    private Context context;

    private boolean isLoadingAdded = false;
    private boolean retryPageLoad = false;

    private PaginationAdapterCallback mCallback;

    private String errorMsg;
    SharedPreference_main sharedPreference_main;
    //ExpandableTextView expandableText;


    public AdapterFollowing(Context context) {
        this.context = context;
        this.mCallback = (PaginationAdapterCallback) context;
        post_list = new ArrayList<>();
        sharedPreference_main = SharedPreference_main.getInstance(context);
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case ITEM:
                View viewItem = inflater.inflate(R.layout.following_adapter, parent, false);
                viewHolder = new ItemVH(viewItem);
                break;
            case LOADING:
                View viewLoading = inflater.inflate(R.layout.item_progress, parent, false);
                viewHolder = new LoadingVH(viewLoading);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, int position) {
        final FollowingModel result = post_list.get(position); // Movie

        switch (getItemViewType(position)) {

            case ITEM:

                //SARE SET VALUES
                final ItemVH holder1 = (ItemVH) holder;

                holder1.tv_teacher_name.setText(result.getName());
                holder1.tv_experience.setText(result.getExp_year());

                Glide.with(context)
                        .load(result.getTeacher_image())
                        .placeholder(R.drawable.user_icon)
                        .into(holder1.iv_teacher_image);
                holder1.iv_teacher_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
               /* if (result.getUser_type().equals("admin")) {
                    Toast.makeText(context, "This Post Is Posted By Admin", Toast.LENGTH_SHORT).show();
                } else {*/

                        Intent intent = new Intent(context, PostUserProfileView.class);
                        intent.putExtra("id", result.getTeacher_id());
                        intent.putExtra("name", result.getName());
                        intent.putExtra("user_type", "teacher");
                        //intent.putExtra("role", result.getUserType());
                        context.startActivity(intent);

                        //  }
                    }
                });


                break;

            case LOADING:
                LoadingVH loadingVH = (LoadingVH) holder;

                if (retryPageLoad) {
                    loadingVH.mErrorLayout.setVisibility(View.VISIBLE);
                    loadingVH.mProgressBar.setVisibility(View.GONE);

                    loadingVH.mErrorTxt.setText(
                            errorMsg != null ?
                                    errorMsg :
                                    context.getString(R.string.error_msg_unknown));

                } else {
                    loadingVH.mErrorLayout.setVisibility(View.GONE);
                    loadingVH.mProgressBar.setVisibility(View.GONE);
                }
                break;
        }
    }


    @Override
    public int getItemCount() {
        return post_list == null ? 0 : post_list.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return ITEM;
        } else {
            return (position == post_list.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
        }
    }


    public void add(FollowingModel r) {
        post_list.add(r);
        notifyItemInserted(post_list.size() - 1);
    }


    public void addAll(List<FollowingModel> moveResults) {
        for (FollowingModel result : moveResults) {
            add(result);
        }
    }


    private void remove(FollowingModel r) {
        int position = post_list.indexOf(r);
        if (position > -1) {
            post_list.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        isLoadingAdded = false;
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }


    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new FollowingModel());
    }


    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = post_list.size() - 1;
        FollowingModel result = getItem(position);

        if (result != null) {
            post_list.remove(position);
            notifyItemRemoved(position);
        }
    }


    public FollowingModel getItem(int position) {
        return post_list.get(position);
    }


    public void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(post_list.size() - 1);

        if (errorMsg != null) this.errorMsg = errorMsg;
    }

    /**
     * Main list's content ViewHolder
     */
    protected class ItemVH extends RecyclerView.ViewHolder {
        TextView tv_experience, tv_teacher_name;
        ImageView iv_teacher_image;


        public ItemVH(View itemView) {
            super(itemView);
            tv_teacher_name = itemView.findViewById(R.id.tvTeacherName);
            iv_teacher_image = itemView.findViewById(R.id.ivTeacherImage);
            tv_experience = itemView.findViewById(R.id.tvExperience);
        }
    }


    protected class LoadingVH extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ProgressBar mProgressBar;
        private ImageButton mRetryBtn;
        private TextView mErrorTxt;
        private LinearLayout mErrorLayout;

        public LoadingVH(View itemView) {
            super(itemView);

            mProgressBar = itemView.findViewById(R.id.loadmore_progress);
            mRetryBtn = itemView.findViewById(R.id.loadmore_retry);
            mErrorTxt = itemView.findViewById(R.id.loadmore_errortxt);
            mErrorLayout = itemView.findViewById(R.id.loadmore_errorlayout);

            mRetryBtn.setOnClickListener(this);
            mErrorLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.loadmore_retry:
                case R.id.loadmore_errorlayout:

                    showRetry(false, null);
                    mCallback.retryPageLoad();

                    break;
            }
        }
    }

}
