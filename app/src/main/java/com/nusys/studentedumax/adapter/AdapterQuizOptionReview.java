package com.nusys.studentedumax.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nusys.studentedumax.R;
import com.nusys.studentedumax.commonModules.SharedPreference_main;
import com.nusys.studentedumax.model.ResponseQuestionList;

import java.util.ArrayList;
import java.util.List;

public class AdapterQuizOptionReview extends RecyclerView.Adapter<AdapterQuizOptionReview.myholder> {
    @NonNull
    Context context;
    ArrayList<ResponseQuestionList.Datum.Option> data;
    String id;
    private Object List;
    String optionIdCor = "";
    String optionIdAns = "";

    SharedPreference_main sharedPreference_main;
    private int selected_position = -1;

    public AdapterQuizOptionReview(@NonNull Context context, List<ResponseQuestionList.Datum.Option> data) {
        this.context = context;
        this.data = (ArrayList<ResponseQuestionList.Datum.Option>) data;
        sharedPreference_main = SharedPreference_main.getInstance(context);

    }


    public myholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.multiple_choice_view_adapter, parent, false);
        //sharedPreference_main = SharedPreference_main.getInstance(context);
        return new myholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final myholder holder, final int position) {
        // holder.recycle_options.setAdapter("Q"+data.get(position).getId());

        holder.tv_option.setVisibility(View.VISIBLE);
        holder.tv_option.setText(Html.fromHtml(data.get(holder.getAdapterPosition()).getOption()));
        // holder.tv_option_num.setText("Q"+position);
        if (data.get(holder.getAdapterPosition()).getCorrect().equals("1")) {
            holder.tv_option_num.setBackgroundResource(R.drawable.circle_green);
            optionIdCor=data.get(holder.getAdapterPosition()).getOptionId();
        }
        if (data.get(holder.getAdapterPosition()).getYourAnswer().equals(1)) {
            optionIdAns=data.get(holder.getAdapterPosition()).getOptionId();
            if (optionIdCor.equals(optionIdAns)){

            }else{
                holder.tv_option_num.setBackgroundResource(R.drawable.circle_red);
            }
        }

        switch (holder.getAdapterPosition()) {
            case 0:
                holder.tv_option_num.setText("A");
                break;
            case 1:
                holder.tv_option_num.setText("B");

                break;
            case 2:
                holder.tv_option_num.setText("C");

                break;
            case 3:
                holder.tv_option_num.setText("D");

                break;
            case 4:
                holder.tv_option_num.setText("E");

                break;
            case 5:
                holder.tv_option_num.setText("F");

                break;
            case 6:
                holder.tv_option_num.setText("G");

                break;
            case 7:
                holder.tv_option_num.setText("H");

                break;
            case 8:
                holder.tv_option_num.setText("I");

                break;
            case 9:
                holder.tv_option_num.setText("J");

                break;
            case 10:
                holder.tv_option_num.setText("K");

                break;
        }

      /*  if(data.get(holder.getAdapterPosition()).isSelected()) {
            //Change selected item background color and Show sub item views
            holder.tv_option_num.setBackgroundResource(R.drawable.circle_blue);

        } else {
            //Change  unselected item background color and Hide sub item views
            holder.tv_option_num.setBackgroundResource(R.drawable.circle);
        }*/

        /*holder.ll_option.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (selected_position == holder.getAdapterPosition()) {
                    selected_position = -1;
                    data.get(holder.getAdapterPosition()).setSelected(false);
                    notifyDataSetChanged();
                    return;
                }
                selected_position = holder.getAdapterPosition();
                for (int i = 0; i < data.size(); i++){
                    data.get(i).setSelected(false);
                }
                data.get(holder.getAdapterPosition()).setSelected(true);
                Toast.makeText(context, ""+data.get(holder.getAdapterPosition()).getOptionId(), Toast.LENGTH_SHORT).show();
                Toast.makeText(context, ""+data.get(holder.getAdapterPosition()).getQuestionId(), Toast.LENGTH_SHORT).show();

                notifyDataSetChanged();
            }
        });*/
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class myholder extends RecyclerView.ViewHolder {
        TextView tv_option_num, tv_option;
        LinearLayout ll_option;


        public myholder(@NonNull View itemView) {
            super(itemView);
            tv_option_num = itemView.findViewById(R.id.tv_option_num);
            tv_option = itemView.findViewById(R.id.tv_option);
            ll_option = itemView.findViewById(R.id.ll_option);


        }

    }
}
