package com.nusys.studentedumax.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.nusys.studentedumax.InstructionTest;
import com.nusys.studentedumax.PlayQuiz2Method;
import com.nusys.studentedumax.QuizResult;
import com.nusys.studentedumax.R;

import com.nusys.studentedumax.commonModules.SharedPreference_main;
import com.nusys.studentedumax.model.ResponseTestList;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class AdapterTestListView extends RecyclerView.Adapter<AdapterTestListView.myholder> {
    @NonNull
    Context context;
    ArrayList<ResponseTestList.Datum> data;
    String id;
    private Object List;
    String teacherId = "";
    private ArrayList<String> TeachersIds = new ArrayList<String>();
    SharedPreference_main sharedPreference_main;

    public AdapterTestListView(@NonNull Context context, List<ResponseTestList.Datum> data) {
        this.context = context;
        this.data = (ArrayList<ResponseTestList.Datum>) data;
        sharedPreference_main = SharedPreference_main.getInstance(context);

    }


    public AdapterTestListView.myholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.tests_list_adapter, parent, false);
        //sharedPreference_main = SharedPreference_main.getInstance(context);
        return new AdapterTestListView.myholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final AdapterTestListView.myholder holder, final int position) {
        holder.tv_test_name.setText(data.get(position).getTestName());
        holder.tv_exam_name.setText(data.get(position).getExamName());
        holder.tv_date.setText("End Date:"+data.get(position).getEndDate());

        // holder.tv_experience.setText(data.get(position).getStudentName());
        Glide.with(context)
                .load(data.get(position).getTestImage())
                .placeholder(R.drawable.user_icon)
                .into(holder.test_img);


        if(data.get(position).getStartStatus().equals("0")){
            holder.tv_startExam.setVisibility(View.VISIBLE);
            holder.tv_resume.setVisibility(View.GONE);
            holder.tv_result.setVisibility(View.GONE);

        }else  if(data.get(position).getStartStatus().equals("1")){
            holder.tv_startExam.setVisibility(View.GONE);
            holder.tv_resume.setVisibility(View.VISIBLE);
            holder.tv_result.setVisibility(View.GONE);

        }else  if(data.get(position).getStartStatus().equals("2")){
            holder.tv_startExam.setVisibility(View.GONE);
            holder.tv_resume.setVisibility(View.GONE);
            holder.tv_result.setVisibility(View.VISIBLE);

        }
        holder.tv_startExam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i= new Intent(context, InstructionTest.class);

                i.putExtra("test_id",data.get(position).getTestId());
                i.putExtra("test_dis",data.get(position).getTestDescription());
                context.startActivity(i);
                ((Activity)context).finish();

            }

        });
        holder.tv_result.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i= new Intent(context, QuizResult.class);
              //  sharedPreference_main.setTest_id(data.get(position).getTestId());
                i.putExtra("test_id",data.get(position).getTestId());
                i.putExtra("start_status",data.get(position).getStartStatus());
                context.startActivity(i);

            }
        });
        holder.tv_resume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i= new Intent(context, PlayQuiz2Method.class);
                i.putExtra("testId",data.get(position).getTestId());
                i.putExtra("start_status",data.get(position).getStartStatus());
                i.putExtra("language", "1");
                context.startActivity(i);

            }
        });


        sharedPreference_main.setStudent_id(teacherId);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class myholder extends RecyclerView.ViewHolder {
        TextView tv_test_name, tv_exam_name, tv_date, tv_startExam, tv_result, tv_resume;
        CircleImageView test_img;

        public myholder(@NonNull View itemView) {
            super(itemView);
            tv_test_name = itemView.findViewById(R.id.tv_test_name);
            tv_exam_name = itemView.findViewById(R.id.tv_exam_name);
            test_img = itemView.findViewById(R.id.test_img);
            tv_date = itemView.findViewById(R.id.tv_date);
            tv_startExam = itemView.findViewById(R.id.tv_startExam);
            tv_result = itemView.findViewById(R.id.tv_result);
            tv_resume = itemView.findViewById(R.id.tv_resume);


        }

    }
}
