package com.nusys.studentedumax.adapter.Group_Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nusys.studentedumax.Group.Batch_student_name_Activity;
import com.nusys.studentedumax.R;
import com.nusys.studentedumax.commonModules.CircularTextView;
import com.nusys.studentedumax.commonModules.SharedPreference_main;
import com.nusys.studentedumax.model.ResponeStudentBatch.Datab;

import java.util.List;

import static com.nusys.studentedumax.Constants.getCapsSentences;

public class Adapter_batch extends RecyclerView.Adapter<Adapter_batch.myholder> {
    String batchId;
    Context context;
    List<Datab> data;
    SharedPreference_main sharedPreference_main;
    String capText;

    public Adapter_batch(Context context, List<Datab> data) {
        this.context = context;
        this.data = data;
        sharedPreference_main = SharedPreference_main.getInstance(context);
    }

    @NonNull
    @Override
    public Adapter_batch.myholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.batch_list, parent, false);
        //sharedPreference_main = SharedPreference_main.getInstance(context);
        return new Adapter_batch.myholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Adapter_batch.myholder holder, final int i) {

        capText=getCapsSentences(data.get(i).getBatchName());
        holder.circleTextView.setCustomText(capText.substring(0, 1));
        holder.circleTextView.setSolidColor(i);
        holder.circleTextView.setTextColor(Color.WHITE);
        holder.circleTextView.setCustomTextSize(18F);
        holder.tx_batch_name.setText(capText);
        holder.txt_startTiming.setText("Start at: " + data.get(i).getDateFrom());
        holder.txt_endTiming.setText("End on: " + data.get(i).getDateTo());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                batchId = data.get(i).getBatchId();
                sharedPreference_main.setbatch(data.get(i).getBatchId());

                Intent intent = new Intent(context, Batch_student_name_Activity.class);
                intent.putExtra("Batch_id", batchId);
                context.startActivity(intent);

            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class myholder extends RecyclerView.ViewHolder {
        TextView tx_batch_name, txt_startTiming, txt_endTiming;
        RadioButton radioButton;
        CircularTextView circleTextView;

        public myholder(@NonNull View itemView) {
            super(itemView);
            tx_batch_name = itemView.findViewById(R.id.txt_batch_name);
            txt_startTiming = itemView.findViewById(R.id.text_startTiming);
            txt_endTiming = itemView.findViewById(R.id.text_endTiming);
            circleTextView = itemView.findViewById(R.id.circularTextView);

        }
    }
}
