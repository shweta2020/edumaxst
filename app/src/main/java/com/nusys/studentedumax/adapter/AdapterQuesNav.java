package com.nusys.studentedumax.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nusys.studentedumax.R;
import com.nusys.studentedumax.commonModules.CircularTextView;
import com.nusys.studentedumax.interfaces.GetQuesIds;
import com.nusys.studentedumax.model.ResponeStudentBatch.Datab;
import com.nusys.studentedumax.model.ResponseQuestionList2Method;

import java.util.List;

import static com.nusys.studentedumax.Constants.getCapsSentences;

public class AdapterQuesNav extends RecyclerView.Adapter<AdapterQuesNav.myholder> {
    String batchId;
    Context context;
    List<ResponseQuestionList2Method> data;
    String capText;
    GetQuesIds getQuesIds;

    public AdapterQuesNav(Context context, List<ResponseQuestionList2Method> data, GetQuesIds getQuesIds) {
        this.context = context;
        this.data = data;
        this.getQuesIds = getQuesIds;
    }

    @NonNull
    @Override
    public AdapterQuesNav.myholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.multiple_choice_view_adapter, parent, false);
        return new AdapterQuesNav.myholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterQuesNav.myholder holder, final int i) {
        final int h = holder.getAdapterPosition() + 1;

        holder.tvOptionNum.setText("" + h);
        holder.tvOptionNum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getQuesIds.getQuesId(String.valueOf(h-1));
            }
        });


        if (data.get(i).getReview_status().equals(1)) {

                holder.tvOptionNum.setBackgroundResource(R.drawable.circle_blue);

        }else{
            if (data.get(i).getAttempt_status().equals(1)) {

                holder.tvOptionNum.setBackgroundResource(R.drawable.circle_orange);
            }else{
                holder.tvOptionNum.setBackgroundResource(R.drawable.circle);
            }

        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class myholder extends RecyclerView.ViewHolder {
        TextView tvOptionNum;


        public myholder(@NonNull View itemView) {
            super(itemView);
            tvOptionNum = itemView.findViewById(R.id.tv_option_num);


        }
    }
}