package com.nusys.studentedumax.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.nusys.studentedumax.R;
import com.nusys.studentedumax.interfaces.GetTeacherIds;
import com.nusys.studentedumax.model.TeacherModel;

import java.util.ArrayList;
import java.util.List;


public class AdapterTeacher extends RecyclerView.Adapter<AdapterTeacher.ItemViewHolder> {


    List<TeacherModel> teacher_list;
    private Context a;
    ArrayList<String> get_ID = new ArrayList<String>();
    List<String> Show_select_jobName = new ArrayList<String>();
    GetTeacherIds getTeacherIds;


    public AdapterTeacher(Context context, List<TeacherModel> teacher_list, GetTeacherIds getTeacherIds) {
        this.a = context;
        this.teacher_list = teacher_list;
        this.getTeacherIds = getTeacherIds;

    }

    @Override
    public AdapterTeacher.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.teacher_view, null);

        AdapterTeacher.ItemViewHolder viewHolder = new AdapterTeacher.ItemViewHolder(v);
        return viewHolder;

    }


    @Override
    public void onBindViewHolder(final AdapterTeacher.ItemViewHolder holder, final int position) {

        TeacherModel teacherM = teacher_list.get(position);
        holder.txt_name.setText(teacherM.getName());
        holder.txt_exp.setText(teacherM.getExpr_year());
        Glide.with(a).load(teacherM.getProfile_image()).into(holder.txt_img);


        holder.select_teachers.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (b) {
                    //True condition
                    String id = teacher_list.get(position).getId();
                    get_ID.add(id);

                    getTeacherIds.getTeacher(get_ID);

                }
                if (!holder.select_teachers.isChecked()) {

                    get_ID.remove(teacher_list.get(position).getId());
                    Show_select_jobName.remove(teacher_list.get(position).getName());
                    getTeacherIds.getTeacher(get_ID);
                    // false condition
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return teacher_list.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        TextView txt_name, txt_exp;
        ImageView txt_img;
        public CheckBox select_teachers;

        public ItemViewHolder(View itemView) {
            super(itemView);
            txt_name = itemView.findViewById(R.id.teacher_name);
            txt_img = itemView.findViewById(R.id.tech_img);
            txt_exp = itemView.findViewById(R.id.experience);
            select_teachers = itemView.findViewById(R.id.select_teacher);


        }

    }
}
