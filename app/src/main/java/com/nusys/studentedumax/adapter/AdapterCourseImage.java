package com.nusys.studentedumax.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.nusys.studentedumax.R;
import com.nusys.studentedumax.commonModules.CircularTextView;
import com.nusys.studentedumax.model.ResponeStudentBatch.Datab;
import com.nusys.studentedumax.model.ResponeViewCourse;

import java.util.List;

public class AdapterCourseImage extends RecyclerView.Adapter<AdapterCourseImage.myholder> {
    String batchId;
    Context context;
    List<ResponeViewCourse.Datum.TopicImage> data;

    public AdapterCourseImage(Context context, List<ResponeViewCourse.Datum.TopicImage> data) {
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public AdapterCourseImage.myholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.course_image_adapter, parent, false);
        return new AdapterCourseImage.myholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterCourseImage.myholder holder, final int i) {



        Glide.with(context)
                .load(data.get(i).getImage())
                .placeholder(R.drawable.user_icon)
                .into(holder.iv_topic_image);

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class myholder extends RecyclerView.ViewHolder {
        ImageView iv_topic_image;

        public myholder(@NonNull View itemView) {
            super(itemView);
            iv_topic_image = itemView.findViewById(R.id.ivTopicImage);

        }
    }
}