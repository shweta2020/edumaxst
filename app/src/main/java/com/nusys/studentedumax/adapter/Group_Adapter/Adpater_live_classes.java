package com.nusys.studentedumax.adapter.Group_Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nusys.studentedumax.Group.Live_Class_Student_NameActivity;
import com.nusys.studentedumax.R;
import com.nusys.studentedumax.commonModules.CircularTextView;
import com.nusys.studentedumax.commonModules.SharedPreference_main;
import com.nusys.studentedumax.model.ResponeStudentLiveClasses;

import java.util.List;

import static com.nusys.studentedumax.Constants.getCapsSentences;

public class Adpater_live_classes extends RecyclerView.Adapter<Adpater_live_classes.myholder> {
    @NonNull

    Context context;
    List<ResponeStudentLiveClasses.Datal> data;
    String id;
    String capText;
    SharedPreference_main sharedPreference_main;


    public Adpater_live_classes(@NonNull Context context, List<ResponeStudentLiveClasses.Datal> data) {
        this.context = context;
        this.data = data;
        sharedPreference_main = SharedPreference_main.getInstance(context);

    }

    public Adpater_live_classes.myholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.live_class_list_adapter, parent, false);
        //sharedPreference_main = SharedPreference_main.getInstance(context);
        return new Adpater_live_classes.myholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Adpater_live_classes.myholder holder, final int position) {

        capText= getCapsSentences(data.get(position).getLiveClassName());
        holder.circularTextView.setCustomText(capText.substring(0, 1));
        holder.circularTextView.setSolidColor(position);
        holder.circularTextView.setTextColor(Color.WHITE);
        holder.circularTextView.setCustomTextSize(18F);
        holder.textLiveClassName.setText(capText);
        holder.textLiveStartDateTime.setText("Start On " + data.get(position).getStartDate() + " At " + data.get(position).getStartTime());
        holder.textLiveEndDateTime.setText("End On " + data.get(position).getEndDate() + " At " + data.get(position).getEndTime());
        holder.textPrice.setText("Price: " + data.get(position).getPrice());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                id = data.get(position).getLiveClassId();
                sharedPreference_main.setbatch(data.get(position).getLiveClassId());

                Intent intent = new Intent(context, Live_Class_Student_NameActivity.class);
                intent.putExtra("Batch_id", id);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class myholder extends RecyclerView.ViewHolder {
        TextView textLiveClassName, textPrice, textLiveStartDateTime, textLiveEndDateTime;
        CircularTextView circularTextView;

        public myholder(@NonNull View itemView) {
            super(itemView);
            textLiveClassName = itemView.findViewById(R.id.tvLiveClassName);
            textPrice = itemView.findViewById(R.id.tvPrice);
            textLiveStartDateTime = itemView.findViewById(R.id.tvLiveStartDateTime);
            textLiveEndDateTime = itemView.findViewById(R.id.tvLiveEndDateTime);
            circularTextView = itemView.findViewById(R.id.circleTextView);

        }
    }
}
