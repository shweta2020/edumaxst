package com.nusys.studentedumax.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nusys.studentedumax.CourseSubject;
import com.nusys.studentedumax.R;
import com.nusys.studentedumax.ViewGroupActivity;
import com.nusys.studentedumax.commonModules.CircularTextView;
import com.nusys.studentedumax.model.GetExamResponseModel;


import java.util.List;

public class AdapterCourseExam extends RecyclerView.Adapter<AdapterCourseExam.myholder> {
    String batchId;
    Context context;
    List<GetExamResponseModel.Datum> data;

    public AdapterCourseExam(Context context, List<GetExamResponseModel.Datum> data) {
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public AdapterCourseExam.myholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.course_exam_list, parent, false);
        return new AdapterCourseExam.myholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterCourseExam.myholder holder, final int position) {

        holder.circleTextView.setCustomText(data.get(position).getExamName().substring(0, 1));
        holder.circleTextView.setSolidColor(position);
        holder.circleTextView.setTextColor(Color.WHITE);
        holder.circleTextView.setCustomTextSize(18F);
        holder.tvExamName.setText(data.get(position).getExamName());
        holder.llClickable.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
               // Toast.makeText(context, ""+data.get(position).getExamId(), Toast.LENGTH_SHORT).show();
                Intent i = new Intent(context, CourseSubject.class);
                i.putExtra("exam_id", data.get(position).getExamId());
                context.startActivity(i);
            }
        });



    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class myholder extends RecyclerView.ViewHolder {
        TextView tvExamName;
        CircularTextView circleTextView;
        LinearLayout llClickable;
        public myholder(@NonNull View itemView) {
            super(itemView);
            tvExamName = itemView.findViewById(R.id.tv_exam_name);
            llClickable = itemView.findViewById(R.id.ll_clickable);
            circleTextView = itemView.findViewById(R.id.circularTextView);

        }
    }
}