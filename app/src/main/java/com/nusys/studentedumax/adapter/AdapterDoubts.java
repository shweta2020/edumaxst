package com.nusys.studentedumax.adapter;

import android.app.Dialog;
import android.content.Context;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.nusys.studentedumax.ChangePassword;

import com.nusys.studentedumax.R;
import com.nusys.studentedumax.ViewIndividualPostDetails;
import com.nusys.studentedumax.commonModules.PaginationAdapterCallback;
import com.nusys.studentedumax.commonModules.SharedPreference_main;
import com.nusys.studentedumax.interfaces.GetDoubtIds;
import com.nusys.studentedumax.model.DoubtsModel;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static com.nusys.studentedumax.Constants.BASE_URL;
import static com.nusys.studentedumax.Constants.STUDENT_DOUBT_SATISFY;


public class AdapterDoubts extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    // View Types
    private static final int ITEM = 0;
    private static final int LOADING = 1;

    private List<DoubtsModel> post_list;
    private Context context;

    private boolean isLoadingAdded = false;
    private boolean retryPageLoad = false;

    private PaginationAdapterCallback mCallback;

    private String errorMsg;
    SharedPreference_main sharedPreference_main;
    GetDoubtIds getDoubtIds;

    public AdapterDoubts(Context context, GetDoubtIds getDoubtIds) {
        this.context = context;
        this.mCallback = (PaginationAdapterCallback) context;
        post_list = new ArrayList<>();
        sharedPreference_main = SharedPreference_main.getInstance(context);
        this.getDoubtIds=getDoubtIds;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case ITEM:
                View viewItem = inflater.inflate(R.layout.doubt_adapter, parent, false);
                viewHolder = new ItemVH(viewItem);
                break;
            case LOADING:
                View viewLoading = inflater.inflate(R.layout.item_progress, parent, false);
                viewHolder = new LoadingVH(viewLoading);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final DoubtsModel result = post_list.get(position); // Movie

        switch (getItemViewType(position)) {


            case ITEM:

                ///SARE SET VALUES
                final ItemVH holder1 = (ItemVH) holder;
                // DoubtsModel profilePostM = question_list.get(position);

                holder1.user_name.setText(result.getUser_name());
                holder1.post_time.setText(result.getCreate_at());
                holder1.tv_heading.setText(result.getRemark());
                holder1.tv_content.setText(result.getDescription());
                holder1.tv_path.setText(">>" + result.getSubject_name() + ">>" + result.getUnit_name());
                Glide.with(context)
                        .load(result.getUser_profile())
                        .placeholder(R.drawable.user_icon)
                        .into(holder1.user_profile);
               // Glide.with(context).load(result.getUser_profile()).into(holder1.user_profile);
                Glide.with(context).load(result.getImage()).into(holder1.feed_image);
                if (result.getProgress_status() == 1) {
                    holder1.tv_status1.setText("In Progress");
                    holder1.ll_buttons.setVisibility(View.VISIBLE);

                    holder1.tv_status1.setVisibility(View.VISIBLE);
                    holder1.tv_status2.setVisibility(View.GONE);
                    holder1.tv_status3.setVisibility(View.GONE);

                } else if (result.getProgress_status() == 2) {
                    holder1.ll_buttons.setVisibility(View.GONE);
                    holder1.tv_status2.setText("Solved");

                    holder1.tv_status2.setVisibility(View.VISIBLE);
                    holder1.tv_status3.setVisibility(View.GONE);
                    holder1.tv_status1.setVisibility(View.GONE);
                    holder1.tv_comment.setVisibility(View.GONE);

                } else if (result.getProgress_status() == 3) {
                    holder1.tv_status3.setText("Escalated");
                    holder1.ll_buttons.setVisibility(View.VISIBLE);

                    holder1.tv_status3.setVisibility(View.VISIBLE);
                    holder1.tv_status2.setVisibility(View.GONE);
                    holder1.tv_status1.setVisibility(View.GONE);
                } else if (result.getProgress_status() == 0) {
                    holder1.ll_buttons.setVisibility(View.GONE);
                    holder1.tv_status1.setText("In Progress");

                    holder1.tv_status1.setVisibility(View.VISIBLE);
                    holder1.tv_status2.setVisibility(View.GONE);
                    holder1.tv_status3.setVisibility(View.GONE);

                } else {

                }

                holder1.tv_comment.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                      /*  final Dialog dialog = new Dialog(context);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.activity_comment_doubt);
                        dialog.getWindow().getDecorView().setTop(100);
                        dialog.getWindow().getDecorView().setLeft(100);
                        dialog.show();


                        Window window = dialog.getWindow();
                        dialog.getWindow().setLayout(700, 700);*/
                       // window.setLayout(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                        getDoubtIds.getDoubtIds(result.getDoubt_id());
                       // Toast.makeText(context, ""+result.getDoubt_id(), Toast.LENGTH_SHORT).show();
                        //Toast.makeText(context, ""+sharedPreference_main.getUserId(), Toast.LENGTH_SHORT).show();

/*
                        Intent intent = new Intent(context, CommentDoubt.class);
                        intent.putExtra("doubt_id", result.getDoubt_id());
                        context.startActivity(intent);*/
                    }
                });

                holder1.tv_btn_satisfied_vi.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        //satisfiedApi(result.getDoubt_id(), holder1.ll_buttons);
                        satisfiedApi(result.getDoubt_id(), holder1.ll_buttons, "Solved");
                    }
                });

                holder1.tv_btn_escalated_vi.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        satisfiedApi(result.getDoubt_id(), holder1.ll_buttons, "escalated");
                        //escalatedApi(result.getDoubt_id(), holder1.ll_buttons);
                        // escalatedApi(result.getDoubt_id(), holder1.ll_buttons);
                    }
                });


                break;

            case LOADING:
                LoadingVH loadingVH = (LoadingVH) holder;

                if (retryPageLoad) {
                    loadingVH.mErrorLayout.setVisibility(View.VISIBLE);
                    loadingVH.mProgressBar.setVisibility(View.GONE);

                    loadingVH.mErrorTxt.setText(
                            errorMsg != null ?
                                    errorMsg :
                                    context.getString(R.string.error_msg_unknown));

                } else {
                    loadingVH.mErrorLayout.setVisibility(View.GONE);
                    loadingVH.mProgressBar.setVisibility(View.VISIBLE);
                }
                break;
        }
    }

    private void satisfiedApi(String doubt_id, final LinearLayout ll_buttons, String status) {

        // String satsify_status = "Solved";
        RequestQueue requestQueue = Volley.newRequestQueue(context.getApplicationContext());
        JSONObject jsonBody = new JSONObject();
        try {
            //input your API parameters
            jsonBody.put("doubt_id", doubt_id);
            jsonBody.put("satsify_status", status);


        } catch (JSONException e) {
            e.printStackTrace();
        }
        // Enter the correct url for your api service site
        // String url = getResources().getString(R.string.url);
        //String URL = "http://13.233.162.24/nusys(UAT)/api/student/student_doubt_satsify.php";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL+STUDENT_DOUBT_SATISFY, jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            // Toast.makeText(ChangePassword.this, "true", Toast.LENGTH_SHORT).show();

                            Boolean status = response.getBoolean("status");
                            if (status) {
                                Toast.makeText(context, "" + response.getString("message"), Toast.LENGTH_SHORT).show();
                                ll_buttons.setVisibility(View.GONE);

                            } else {
                                Toast.makeText(context, "" + response.getString("message"), Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        // Toast.makeText(ChangePassword.this, ""+response.toString(), Toast.LENGTH_SHORT).show();
                        //resultTextView.setText("String Response : "+ response.toString());
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //  resultTextView.setText("Error getting response");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json; charset=UTF-8");
                params.put("Authorization", "Bearer " + sharedPreference_main.getToken());
                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);


    }


    @Override
    public int getItemCount() {
        return post_list == null ? 0 : post_list.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return ITEM;
        } else {
            return (position == post_list.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
        }
    }


    public void add(DoubtsModel r) {
        post_list.add(r);
        notifyItemInserted(post_list.size() - 1);
    }


    public void addAll(List<DoubtsModel> moveResults) {
        for (DoubtsModel result : moveResults) {
            add(result);
        }
    }


    public void remove(DoubtsModel r) {
        int position = post_list.indexOf(r);
        if (position > -1) {
            post_list.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        isLoadingAdded = false;
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }


    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new DoubtsModel());
    }


    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = post_list.size() - 1;
        DoubtsModel result = getItem(position);

        if (result != null) {
            post_list.remove(position);
            notifyItemRemoved(position);
        }
    }


    public DoubtsModel getItem(int position) {
        return post_list.get(position);
    }


    public void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(post_list.size() - 1);

        if (errorMsg != null) this.errorMsg = errorMsg;
    }

    /**
     * Main list's content ViewHolder
     */
    protected class ItemVH extends RecyclerView.ViewHolder {
        private TextView user_name, post_time, tv_heading, tv_content, tv_path, tv_comment;
        TextView tv_status1, tv_status2, tv_status3;
        TextView tv_btn_escalated, tv_btn_satisfied, tv_btn_live_class;
        TextView tv_btn_escalated_vi, tv_btn_satisfied_vi, tv_btn_live_class_vi;
        private ImageView user_profile, feed_image;
        LinearLayout ll_buttons;


        public ItemVH(View itemView) {
            super(itemView);
            user_profile = itemView.findViewById(R.id.uploader_img_doubt);
            user_name = itemView.findViewById(R.id.user_name_doubt);
            post_time = itemView.findViewById(R.id.post_time_doubt);
            tv_heading = itemView.findViewById(R.id.tv_heading_doubt);
            feed_image = itemView.findViewById(R.id.feed_image_doubt);
            tv_content = itemView.findViewById(R.id.tv_content_doubt);
            tv_path = itemView.findViewById(R.id.tv_path_doubt);
            tv_status1 = itemView.findViewById(R.id.tv_status1_doubt);
            tv_status2 = itemView.findViewById(R.id.tv_status2_doubt);
            tv_status3 = itemView.findViewById(R.id.tv_status3_doubt);
            tv_comment = itemView.findViewById(R.id.tv_comment_doubt);
            ll_buttons = itemView.findViewById(R.id.ll_buttons_doubt);
            tv_btn_escalated = itemView.findViewById(R.id.tv_btn_escalated_doubt);
            tv_btn_escalated_vi = itemView.findViewById(R.id.tv_btn_escalated_vi_doubt);
            tv_btn_satisfied = itemView.findViewById(R.id.tv_btn_satisfied_doubt);
            tv_btn_satisfied_vi = itemView.findViewById(R.id.tv_btn_satisfied_vi_doubt);
            tv_btn_live_class = itemView.findViewById(R.id.tv_btn_live_class_doubt);
            tv_btn_live_class_vi = itemView.findViewById(R.id.tv_btn_live_class_vi_doubt);

        }
    }


    protected class LoadingVH extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ProgressBar mProgressBar;
        private ImageButton mRetryBtn;
        private TextView mErrorTxt;
        private LinearLayout mErrorLayout;

        public LoadingVH(View itemView) {
            super(itemView);

            mProgressBar = itemView.findViewById(R.id.loadmore_progress);
            mRetryBtn = itemView.findViewById(R.id.loadmore_retry);
            mErrorTxt = itemView.findViewById(R.id.loadmore_errortxt);
            mErrorLayout = itemView.findViewById(R.id.loadmore_errorlayout);


            mRetryBtn.setOnClickListener(this);
            mErrorLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.loadmore_retry:
                case R.id.loadmore_errorlayout:

                    showRetry(false, null);
                    mCallback.retryPageLoad();

                    break;
            }
        }
    }

}
