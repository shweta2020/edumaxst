package com.nusys.studentedumax.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.nusys.studentedumax.R;
import com.nusys.studentedumax.model.PostModel;

import java.util.List;


public class AdapterPost extends RecyclerView.Adapter<AdapterPost.ItemViewHolder> {

    List<PostModel> question_list;
    private Context a;

    public AdapterPost(Context context, List<PostModel> question_list) {
        this.a = context;
        this.question_list = question_list;
    }

    @Override
    public AdapterPost.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.post_adapter, parent, false);
        AdapterPost.ItemViewHolder viewHolder = new AdapterPost.ItemViewHolder(v);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(AdapterPost.ItemViewHolder holder, final int position) {

        holder.main_category.setText(question_list.get(position).getName());
        //holder.id.setText(question_list.get(position).getId());

    }

    @Override
    public int getItemCount() {
        //return question_list.size();

        return (null != question_list ? question_list.size() : 0);
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        TextView main_category, id;

        public ItemViewHolder(View itemView) {
            super(itemView);
            main_category = itemView.findViewById(R.id.user_name);
            //id = itemView.findViewById(R.id.name_id);

        }
    }

}