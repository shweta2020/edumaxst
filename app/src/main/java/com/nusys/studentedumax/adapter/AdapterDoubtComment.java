package com.nusys.studentedumax.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.nusys.studentedumax.R;
import com.nusys.studentedumax.model.DoubtCommentModel;
import com.nusys.studentedumax.model.ShowCommentModel;
import com.squareup.picasso.Picasso;

import java.util.List;


public class AdapterDoubtComment extends RecyclerView.Adapter<AdapterDoubtComment.ItemViewHolder> {

    List<DoubtCommentModel> question_list;
    private Context a;


    public AdapterDoubtComment(Context context, List<DoubtCommentModel> question_list) {
        this.a = context;
        this.question_list = question_list;

    }

    @Override
    public AdapterDoubtComment.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.doubt_comment_adapter, parent, false);
        AdapterDoubtComment.ItemViewHolder viewHolder = new AdapterDoubtComment.ItemViewHolder(v);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(AdapterDoubtComment.ItemViewHolder holder, final int position) {

        if (question_list.get(position).getMode() == 1) {
            //Toast.makeText(a, "left", Toast.LENGTH_SHORT).show();
            holder.tv_comment_name.setText(question_list.get(position).getCommenter_name());
            holder.tv_comments.setText(question_list.get(position).getComment());
            holder.tv_time.setText(question_list.get(position).getTime());
            // holder.iv_media_name.setText(question_list.get(position).getMedia_name());
            Glide.with(a)
                    .load(question_list.get(position).getCommenter_photo())
                    .placeholder(R.drawable.user_icon)
                    .into(holder.civ_commenter_image);
            // Glide.with(a).load(question_list.get(position).getCommenter_photo()).into(holder.civ_commenter_image);
            Glide.with(a).load(question_list.get(position).getMedia_name()).into(holder.iv_media_name);

            holder.relative_left.setVisibility(View.VISIBLE);
            holder.relative_reverse.setVisibility(View.GONE);

        } else {
           // Toast.makeText(a, "right", Toast.LENGTH_SHORT).show();

            holder.relative_left.setVisibility(View.GONE);
            holder.relative_reverse.setVisibility(View.VISIBLE);
            holder.tv_commentuser_reverse.setText(question_list.get(position).getCommenter_name());
            holder.tv_commentm_reverse.setText(question_list.get(position).getComment());
            holder.tv_commentTime_reverse.setText(question_list.get(position).getTime());
            if (question_list.get(position).getCommenter_photo().equals("null")){
                holder.img_user_reverse.setImageResource(R.mipmap.ic_launcher);
               // holder.comment_img_reverse.setImageResource(R.mipmap.ic_launcher);
            } else {
               /* Glide.with(a)
                        .load(question_list.get(position).getCommenter_photo())
                        .placeholder(R.drawable.user_icon)
                        .into(holder.img_user_reverse);

                Picasso.with(a).load(question_list.get(position).getCommenter_photo())
                        .fit().placeholder(R.drawable.user_icon).into(holder.img_user_reverse);*/
                Picasso.get().load(question_list.get(position).getCommenter_photo()).into(holder.img_user_reverse);
                if (question_list.get(position).getMedia_name().isEmpty()) {
                    holder.comment_img_reverse.setImageResource(R.drawable.user_icon);
                } else{
                    Picasso.get().load(question_list.get(position).getMedia_name()).into(holder.comment_img_reverse);
                }
              //  Picasso.get().load(question_list.get(position).getMedia_name()).into(holder.comment_img_reverse);

               //Glide.with(a).load(question_list.get(position).getMedia_name()).into(holder.comment_img_reverse);
            }


        }


    }

    @Override
    public int getItemCount() {
        //return question_list.size();

        return (null != question_list ? question_list.size() : 0);
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        TextView tv_comment_name, tv_comments, tv_time;
        ImageView civ_commenter_image, iv_media_name;
        RelativeLayout relative_reverse, relative_left;

        ImageView img_user_reverse, comment_img_reverse;
        TextView tv_commentuser_reverse, tv_commentTime_reverse, tv_commentm_reverse;

        public ItemViewHolder(View itemView) {
            super(itemView);
            tv_comment_name = itemView.findViewById(R.id.tv_comment_name);
            tv_comments = itemView.findViewById(R.id.tv_comments);
            civ_commenter_image = itemView.findViewById(R.id.civ_commenter_image);
            tv_time = itemView.findViewById(R.id.tv_time);
            iv_media_name = itemView.findViewById(R.id.iv_media_name);
            relative_reverse = itemView.findViewById(R.id.relative_reverse);
            relative_left = itemView.findViewById(R.id.relative_left);

            tv_commentuser_reverse = itemView.findViewById(R.id.tv_commentuser_reverse);
            img_user_reverse = itemView.findViewById(R.id.img_user_reverse);
            tv_commentTime_reverse = itemView.findViewById(R.id.tv_commentTime_reverse);
            tv_commentm_reverse = itemView.findViewById(R.id.tv_commentm_reverse);
            comment_img_reverse = itemView.findViewById(R.id.comment_img_reverse);



        }
    }

}