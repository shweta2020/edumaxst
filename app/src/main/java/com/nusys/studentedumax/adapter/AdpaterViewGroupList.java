package com.nusys.studentedumax.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.nusys.studentedumax.Home;
import com.nusys.studentedumax.R;
import com.nusys.studentedumax.ViewGroupActivity;
import com.nusys.studentedumax.commonModules.SharedPreference_main;
import com.nusys.studentedumax.model.ResponeBatchStudent.Databs;
import com.nusys.studentedumax.model.ResponeDeleteGroup;
import com.nusys.studentedumax.model.ResponeDeleteStudentFromGroup;
import com.nusys.studentedumax.model.ResponeViewGroupList;
import com.nusys.studentedumax.retrofit.ApiClient;
import com.nusys.studentedumax.retrofit.ServiceInterface;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

import static com.nusys.studentedumax.Constants.Content_Type;

public class AdpaterViewGroupList extends RecyclerView.Adapter<AdpaterViewGroupList.myholder> {
    @NonNull
    Context context;
    ArrayList<ResponeViewGroupList.Datum.Student> data;
    String id;
    String teacherId = "";
    String grpId;
    String createdId;

    public String student_id;

    SharedPreference_main sharedPreference_main;

    public AdpaterViewGroupList(@NonNull Context context, List<ResponeViewGroupList.Datum.Student> data, String grpId, String createdId) {
        this.context = context;
        this.data = (ArrayList<ResponeViewGroupList.Datum.Student>) data;
        this.grpId = grpId;
        this.createdId = createdId;
        sharedPreference_main = SharedPreference_main.getInstance(context);

    }


    public AdpaterViewGroupList.myholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.view_group_list_adapter, parent, false);
        //sharedPreference_main = SharedPreference_main.getInstance(context);
        return new AdpaterViewGroupList.myholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final AdpaterViewGroupList.myholder holder, final int position) {
        holder.tvUserName.setText(data.get(position).getStudentName());

        // holder.tv_experience.setText(data.get(position).getStudentName());
        Glide.with(context)
                .load(data.get(position).getStudentImage())
                .placeholder(R.drawable.user_icon)
                .into(holder.userImg);

        student_id = data.get(position).getStudentId();
        id = sharedPreference_main.getUserId();

       // Toast.makeText(context, "mem"+student_id, Toast.LENGTH_SHORT).show();
      //  Toast.makeText(context, ""+createdId, Toast.LENGTH_SHORT).show();
        if (createdId.equals(id)) {
            if (student_id.equals(id)) {
                holder.tvRemove.setVisibility(View.GONE);

            } else {
                holder.tvRemove.setVisibility(View.VISIBLE);

            }

        } else {
            holder.tvRemove.setVisibility(View.GONE);
        }
    /*    if (createdId.equals(id)) {

            holder.tvRemove.setVisibility(View.VISIBLE);
        } else {
            if (student_id.equals(id)) {
                holder.tvRemove.setVisibility(View.VISIBLE);

            } else {
                holder.tvRemove.setVisibility(View.GONE);

            }
        }*/
        holder.tvRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delete_student_from_group(data.get(position).getStudentId(), grpId);

            }
        });

        sharedPreference_main.setStudent_id(teacherId);
    }

    private void delete_student_from_group(String studentId, String groupId) {

        HashMap<String, String> map = new HashMap<>();

        map.put("group_id", groupId);
        map.put("student_id", studentId);

        //   if (NetworkUtils.isConnected(getActivity())) {
        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<ResponeDeleteStudentFromGroup> call = serviceInterface.delete_student_from_group("Bearer " + sharedPreference_main.getToken(), Content_Type, map);
        call.enqueue(new Callback<ResponeDeleteStudentFromGroup>() {


            @Override
            public void onResponse(Call<ResponeDeleteStudentFromGroup> call, retrofit2.Response<ResponeDeleteStudentFromGroup> response) {

                if (response.isSuccessful()) {

                    ResponeDeleteStudentFromGroup bean = response.body();
                    Toast.makeText(context, bean.getMessage(), Toast.LENGTH_LONG).show();
                   // Toast.makeText(context, "sucess", Toast.LENGTH_SHORT).show();
                    if (bean.getStatus()) {
                        ResponeDeleteStudentFromGroup view = response.body();

                        //**refresh activity on adapter**//
                        ((ViewGroupActivity) context).recreate();
                        //Intent intent = new Intent(context, Home.class);
                        //  context.startActivity(intent);


                        Log.e("Group_response", view.toString());

                    } else {
                        Toast.makeText(context, bean.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {

                    Toast.makeText(context, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponeDeleteStudentFromGroup> call, Throwable t) {
                Log.e("error", t.getMessage());
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    public class myholder extends RecyclerView.ViewHolder {
        TextView tvUserName, tv_experience, tvAdd, tvRemove;
        ImageView userImg;

        public myholder(@NonNull View itemView) {
            super(itemView);
            tvUserName = itemView.findViewById(R.id.tv_user_name);
            userImg = itemView.findViewById(R.id.user_img);
            // tv_experience = itemView.findViewById(R.id.tv_experience);
            tvAdd = itemView.findViewById(R.id.tv_add);
            tvRemove = itemView.findViewById(R.id.tv_remove);


        }
    }
}
