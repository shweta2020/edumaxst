package com.nusys.studentedumax.adapter.Group_Adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.nusys.studentedumax.R;
import com.nusys.studentedumax.commonModules.SharedPreference_main;
import com.nusys.studentedumax.model.ResponeBatchStudent.Databs;
import com.squareup.picasso.Picasso;

import java.security.acl.LastOwnerException;
import java.util.ArrayList;
import java.util.List;

public class Adpater_student_name extends RecyclerView.Adapter<Adpater_student_name.myholder> {
    @NonNull
    Context context;
    ArrayList<Databs> data;
    String id;
    private Object List;
    String teacherId = "";
    private ArrayList<String> TeachersIds = new ArrayList<String>();
    SharedPreference_main sharedPreference_main;

    public Adpater_student_name(@NonNull Context context, List<Databs> data) {
        this.context = context;
        this.data = (ArrayList<Databs>) data;
        sharedPreference_main = SharedPreference_main.getInstance(context);

    }


    public Adpater_student_name.myholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.students_list, parent, false);
        //sharedPreference_main = SharedPreference_main.getInstance(context);
        return new Adpater_student_name.myholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final Adpater_student_name.myholder holder, final int position) {
        holder.tv_userName.setText(data.get(position).getStudentName());
        // holder.tv_experience.setText(data.get(position).getStudentName());
        Glide.with(context)
                .load(data.get(position).getStudentImage())
                .placeholder(R.drawable.user_icon)
                .into(holder.userImg);


        holder.tvAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.tvAdd.setVisibility(View.GONE);
                holder.tvRemove.setVisibility(View.VISIBLE);
                TeachersIds.add(data.get(position).getStudentId());
                String id = ",";
                StringBuilder sb = new StringBuilder();
                String teacherId = "";
                for (Object mPI : TeachersIds) {
                    sb.append(mPI).append(id);
                    if (sb.length() > 0)
                        teacherId = sb.substring(0, sb.length() - 1);
                }
                sharedPreference_main.setStudent_id(teacherId);
                Log.e(" ", TeachersIds.toString());
                Log.e("id", teacherId);

            }

        });
        holder.tvRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.tvAdd.setVisibility(View.VISIBLE);
                holder.tvRemove.setVisibility(View.GONE);
                TeachersIds.remove(data.get(position).getStudentId());
                String id = ",";
                StringBuilder sb = new StringBuilder();
                String teacherId = "";
                for (Object mPI : TeachersIds) {
                    sb.append(mPI).append(id);
                    if (sb.length() > 0)
                        teacherId = sb.substring(0, sb.length() - 1);
                }
                sharedPreference_main.setStudent_id(teacherId);
                Log.e(" ", TeachersIds.toString());
                Log.e("id", teacherId);
            }
        });


        sharedPreference_main.setStudent_id(teacherId);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class myholder extends RecyclerView.ViewHolder {
        TextView tv_userName, tv_experience, tvAdd, tvRemove;
        ImageView userImg;

        public myholder(@NonNull View itemView) {
            super(itemView);
            tv_userName = itemView.findViewById(R.id.tv_user_name);
            userImg = itemView.findViewById(R.id.user_img);
            // tv_experience = itemView.findViewById(R.id.tv_experience);
            tvAdd = itemView.findViewById(R.id.tv_add);
            tvRemove = itemView.findViewById(R.id.tv_remove);


        }

    }
}
