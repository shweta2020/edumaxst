package com.nusys.studentedumax.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nusys.studentedumax.R;
import com.nusys.studentedumax.commonModules.CircularTextView;
import com.nusys.studentedumax.interfaces.GetSubjectIds;
import com.nusys.studentedumax.interfaces.GetUnitIds;
import com.nusys.studentedumax.model.GetSubjectResponseModel;
import com.nusys.studentedumax.model.GetUnitResponseModel;

import java.util.List;

public class AdapterCourseUnit extends RecyclerView.Adapter<AdapterCourseUnit.myholder> {
    String batchId;
    Context context;
    List<GetUnitResponseModel.Datum> data;

    GetUnitIds getUnitIds;

    public AdapterCourseUnit(Context context, List<GetUnitResponseModel.Datum> data, GetUnitIds getUnitIds) {
        this.context = context;
        this.data = data;
        this.getUnitIds = getUnitIds;

    }

    @NonNull
    @Override
    public AdapterCourseUnit.myholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.course_exam_list, parent, false);
        return new AdapterCourseUnit.myholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterCourseUnit.myholder holder, final int position) {

        holder.circleTextView.setCustomText(data.get(position).getUnitName().substring(0, 1));
        holder.circleTextView.setSolidColor(position);
        holder.circleTextView.setTextColor(Color.WHITE);
        holder.circleTextView.setCustomTextSize(18F);
        holder.tvExamName.setText(data.get(position).getUnitName());

        holder.llClickable.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                getUnitIds.getUnitId(data.get(position).getUnitId());
            /*    Intent i = new Intent(context, CourseSubject.class);
                i.putExtra("sub_id", data.get(position).getSubId());
                context.startActivity(i);*/
            }
        });


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class myholder extends RecyclerView.ViewHolder {
        TextView tvExamName;
        CircularTextView circleTextView;
        LinearLayout llClickable;

        public myholder(@NonNull View itemView) {
            super(itemView);
            tvExamName = itemView.findViewById(R.id.tv_exam_name);
            llClickable = itemView.findViewById(R.id.ll_clickable);
            circleTextView = itemView.findViewById(R.id.circularTextView);

        }
    }
}