package com.nusys.studentedumax.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.nusys.studentedumax.R;
import com.nusys.studentedumax.commonModules.SharedPreference_main;
import com.nusys.studentedumax.model.ResponeBatchStudent.Databs;
import com.nusys.studentedumax.model.ResponeViewGroupMemberList;
import com.squareup.picasso.Picasso;

import java.security.acl.LastOwnerException;
import java.util.ArrayList;
import java.util.List;

public class AdpaterAddNewMemberGroupList extends RecyclerView.Adapter<AdpaterAddNewMemberGroupList.myholder> {
    @NonNull
    Context context;
    ArrayList<ResponeViewGroupMemberList.Datum> data;
    String id;
    private Object List;
    String teacherId = "";
    private ArrayList<String> TeachersIds = new ArrayList<String>();
    SharedPreference_main sharedPreference_main;
    public AdpaterAddNewMemberGroupList(@NonNull Context context, List<ResponeViewGroupMemberList.Datum> data) {
        this.context = context;
        this.data = (ArrayList<ResponeViewGroupMemberList.Datum>) data;
        sharedPreference_main = SharedPreference_main.getInstance(context);

    }


    public AdpaterAddNewMemberGroupList.myholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.add_new_member_group_list_adapter, parent, false);
        //sharedPreference_main = SharedPreference_main.getInstance(context);
        return new AdpaterAddNewMemberGroupList.myholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final AdpaterAddNewMemberGroupList.myholder holder, final int position) {
        holder.tv_user_name.setText(data.get(position).getStudentName());
        // holder.tv_experience.setText(data.get(position).getStudentName());
        Glide.with(context)
                .load(data.get(position).getStudentImage())
                .placeholder(R.drawable.user_icon)
                .into(holder.userImg);

       if(data.get(position).getJoined()==0) {

           holder.tvAdd.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   holder.tvAdd.setVisibility(View.GONE);
                   holder.tvRemove.setVisibility(View.VISIBLE);
                   TeachersIds.add(data.get(position).getStudentId());
                   String id = ",";
                   StringBuilder sb = new StringBuilder();
                   String teacherId = "";
                   for (Object mPI : TeachersIds) {
                       sb.append(mPI).append(id);
                       if (sb.length() > 0)
                           teacherId = sb.substring(0, sb.length() - 1);
                   }sharedPreference_main.setStudent_id(teacherId);
                   Log.e(" ", TeachersIds.toString());
                   Log.e("id", teacherId);

               }

           });
           holder.tvRemove.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   holder.tvAdd.setVisibility(View.VISIBLE);
                   holder.tvRemove.setVisibility(View.GONE);
                   TeachersIds.remove(data.get(position).getStudentId());
                   String id = ",";
                   StringBuilder sb = new StringBuilder();
                   String teacherId = "";
                   for (Object mPI : TeachersIds) {
                       sb.append(mPI).append(id);
                       if (sb.length() > 0)
                           teacherId = sb.substring(0, sb.length() - 1);
                   }
                   sharedPreference_main.setStudent_id(teacherId);
                   Log.e(" ", TeachersIds.toString());
                   Log.e("id", teacherId);
               }
           });

       }else if(data.get(position).getJoined()==1) {

           holder.tvRemove.setVisibility(View.VISIBLE);
           holder.tvAdd.setVisibility(View.GONE);
           holder.tvRemove.setText("Already Exist");

       }

        sharedPreference_main.setStudent_id(teacherId);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class myholder extends RecyclerView.ViewHolder {
        TextView tv_user_name,tv_experience,tvAdd,tvRemove;
        ImageView userImg;

        public myholder(@NonNull View itemView) {
            super(itemView);
            tv_user_name = itemView.findViewById(R.id.tv_follower_name);
            userImg = itemView.findViewById(R.id.user_img);
            // tv_experience = itemView.findViewById(R.id.tv_experience);
            tvAdd = itemView.findViewById(R.id.tv_add);
            tvRemove = itemView.findViewById(R.id.tv_remove);


        }


    }
}














