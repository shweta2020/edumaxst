package com.nusys.studentedumax.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.nusys.studentedumax.R;
import com.nusys.studentedumax.model.ShowCommentModel;

import java.util.List;


public class AdapterShowComment extends RecyclerView.Adapter<AdapterShowComment.ItemViewHolder> {

    List<ShowCommentModel> question_list;
    private Context a;


    public AdapterShowComment(Context context, List<ShowCommentModel> question_list) {
        this.a = context;
        this.question_list = question_list;
        //this.getPostIds=getPostIds;
    }

    @Override
    public AdapterShowComment.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.comment_adapter, parent, false);
        AdapterShowComment.ItemViewHolder viewHolder = new AdapterShowComment.ItemViewHolder(v);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(AdapterShowComment.ItemViewHolder holder, final int position) {

        holder.tv_comment_name.setText(question_list.get(position).getCommenter_name());
        holder.tv_comments.setText(question_list.get(position).getComment());
        holder.tv_time.setText(question_list.get(position).getTime());
        // holder.iv_media_name.setText(question_list.get(position).getMedia_name());
        Glide.with(a)
                .load(question_list.get(position).getCommenter_photo())
                .placeholder(R.drawable.user_icon)
                .into(holder.civ_commenter_image);
        // Glide.with(a).load(question_list.get(position).getCommenter_photo()).into(holder.civ_commenter_image);
        Glide.with(a).load(question_list.get(position).getMedia_name()).into(holder.iv_media_name);

    }

    @Override
    public int getItemCount() {
        //return question_list.size();

        return (null != question_list ? question_list.size() : 0);
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        TextView tv_comment_name, tv_comments, tv_time;
        ImageView civ_commenter_image, iv_media_name;

        public ItemViewHolder(View itemView) {
            super(itemView);
            tv_comment_name = itemView.findViewById(R.id.tv_comment_name);
            tv_comments = itemView.findViewById(R.id.tv_comments);
            civ_commenter_image = itemView.findViewById(R.id.civ_commenter_image);
            tv_time = itemView.findViewById(R.id.tv_time);
            iv_media_name = itemView.findViewById(R.id.iv_media_name);
            //id = itemView.findViewById(R.id.name_id);

        }
    }

}