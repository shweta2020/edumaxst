package com.nusys.studentedumax.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nusys.studentedumax.Group.Batch_student_name_Activity;
import com.nusys.studentedumax.R;
import com.nusys.studentedumax.commonModules.CircularTextView;
import com.nusys.studentedumax.model.ResponeStudentBatch.Datab;

import java.util.List;

import static com.nusys.studentedumax.Constants.getCapsSentences;

public class AdapterBatch extends RecyclerView.Adapter<AdapterBatch.myholder> {
    String batchId;
    Context context;
    List<Datab> data;
    String capText;

    public AdapterBatch(Context context, List<Datab> data) {
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public AdapterBatch.myholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.batch_list, parent, false);
        return new AdapterBatch.myholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterBatch.myholder holder, final int i) {
        capText= getCapsSentences(data.get(i).getBatchName());
        holder.circleTextView.setCustomText(capText.substring(0, 1));
        holder.circleTextView.setSolidColor(i);
        holder.circleTextView.setTextColor(Color.WHITE);
        holder.circleTextView.setCustomTextSize(18F);

        holder.tv_batch_name.setText(capText);
        holder.startTiming.setText("Start at: " + data.get(i).getDateFrom());
        holder.endTiming.setText("End on: " + data.get(i).getDateTo());
        /*holder.rlD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rv.getLayoutManager().scrollToPosition(i);
            }
        });*/

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class myholder extends RecyclerView.ViewHolder {
        TextView tv_batch_name, startTiming, endTiming;
        RadioButton radioButton;
        CircularTextView circleTextView;
        RelativeLayout rlD;

        public myholder(@NonNull View itemView) {
            super(itemView);
            tv_batch_name = itemView.findViewById(R.id.txt_batch_name);
            startTiming = itemView.findViewById(R.id.text_startTiming);
            endTiming = itemView.findViewById(R.id.text_endTiming);
            circleTextView = itemView.findViewById(R.id.circularTextView);
            rlD = itemView.findViewById(R.id.rl_d);

        }
    }
}