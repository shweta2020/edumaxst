package com.nusys.studentedumax;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.nusys.studentedumax.adapter.AdapterTestListView;
import com.nusys.studentedumax.adapter.AdpaterAddNewMemberGroupList;
import com.nusys.studentedumax.commonModules.DialogProgress;
import com.nusys.studentedumax.commonModules.NetworkUtil;
import com.nusys.studentedumax.commonModules.SharedPreference_main;
import com.nusys.studentedumax.model.ResponeViewGroupMemberList;
import com.nusys.studentedumax.model.ResponseTestList;
import com.nusys.studentedumax.retrofit.ApiClient;
import com.nusys.studentedumax.retrofit.ServiceInterface;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;

import static com.nusys.studentedumax.Constants.Content_Type;
import static com.nusys.studentedumax.commonModules.Extension.showErrorDialog;

public class PracticeTest extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    TextView tv_toolbar_head;
    LinearLayout ll_back_activity;
    SharedPreference_main sharedPreference_main;
    RecyclerView recyclerView_test_view;
    AdapterTestListView Adapter;
    String id;
    TextView tv_create;

    SwipeRefreshLayout refresh_layout;

    ProgressBar main_progress;
    LinearLayout errorLayout;
    TextView error_txt_cause;
    Button error_btn_retry;
    Context context;
    private ProgressDialog pd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_practice_test);
        sharedPreference_main = SharedPreference_main.getInstance(this);
        sharedPreference_main.setTest_type("practice");
        tv_toolbar_head = findViewById(R.id.tv_toolbar_head);
        ll_back_activity = findViewById(R.id.ll_back_activity);

        tv_toolbar_head.setText("Practice Tests");

        ll_back_activity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });



        refresh_layout = findViewById(R.id.refresh_layout);
        main_progress = findViewById(R.id.main_progress);
        errorLayout = findViewById(R.id.error_layout);
        error_txt_cause = findViewById(R.id.error_txt_cause);
        error_btn_retry = findViewById(R.id.error_btn_retry);
        refresh_layout.setOnRefreshListener(this);
        refresh_layout.setColorSchemeColors(Color.RED, Color.YELLOW, Color.BLUE);

        context = PracticeTest.this;
        recyclerView_test_view = findViewById(R.id.recyclerView_test_view);
        pd = new DialogProgress(context, "");
        pd.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        pd.setCancelable(false);

        view_test_list();
    }

    private void view_test_list() {
        if (NetworkUtil.isConnected(context)) {

            pd.show();
            HashMap<String, String> map = new HashMap<>();

            map.put("student_id", sharedPreference_main.getUserId());
            map.put("test_type", getIntent().getStringExtra("test_type"));

            //   if (NetworkUtils.isConnected(getActivity())) {
            ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
            Call<ResponseTestList> call = serviceInterface.view_test_list("Bearer " + sharedPreference_main.getToken(), Content_Type, map);
            call.enqueue(new Callback<ResponseTestList>() {
                @Override
                public void onResponse(Call<ResponseTestList> call, retrofit2.Response<ResponseTestList> response) {

                    if (response.isSuccessful()) {

                        ResponseTestList bean = response.body();

                        // Toast.makeText(GroupActivity.this, "sucess", Toast.LENGTH_SHORT).show();
                        if (bean.getStatus()) {
                            errorLayout.setVisibility(View.GONE);
                            recyclerView_test_view.setVisibility(View.VISIBLE);

                            ResponseTestList view = response.body();
                            //Toast.makeText(AddNewMembersToGroup.this, "gfcfdhgjkd", Toast.LENGTH_SHORT).show();
                            Adapter = new AdapterTestListView(PracticeTest.this, bean.getData());
                            //recyclerView_group_view.setLayoutManager(new GridLayoutManager(getBaseContext(), 2));
                        /*recyclerView_test_view.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
                        recyclerView_test_view.setItemAnimator(new DefaultItemAnimator());*/
                            recyclerView_test_view.setAdapter(Adapter);
                            pd.dismiss();
                            Log.e("test_response", view.toString());

                        } else {
                            pd.dismiss();
                            errorLayout.setVisibility(View.VISIBLE);
                            recyclerView_test_view.setVisibility(View.GONE);
                            Toast.makeText(context, bean.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        pd.dismiss();
                        errorLayout.setVisibility(View.VISIBLE);
                        recyclerView_test_view.setVisibility(View.GONE);
                        Toast.makeText(context, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseTestList> call, Throwable t) {
                    pd.dismiss();
                    errorLayout.setVisibility(View.VISIBLE);
                    recyclerView_test_view.setVisibility(View.GONE);
                    Log.e("error", t.getMessage());
                    Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }else {
            showErrorDialog(context, new Dialog(context));
        }
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                view_test_list();
                refresh_layout.setRefreshing(false);
            }
        }, 2000);

    }
}
